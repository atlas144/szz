# Přispívání

## Software

Pro participaci na tvorbě materiálů je třeba mít nainstalovaný [GIT](https://git-scm.com/downloads) a jakýkoliv textový editor (ideálně [VSCodium](https://vscodium.com/#install) či jiný editor s podporou *VS Code Extensions*).

Proces publikování vytvořených materiálů zajišťuje CLI nástroj [mdBook](https://rust-lang.github.io/mdBook) (ke stažení [zde](https://github.com/rust-lang/mdBook/releases)). Po rozbalení archivu je ideální přidat cestu k binárce do `PATH`, poté není při volání nutné udávat celou její cestu.

## Nastavení prostředí

### Klonování repozitáře

``` bash
git clone https://codeberg.org/atlas144/szz.git
```

### VS Code Extensions

Pokud používáte editor s podporou *VS Code Extensions*, doporučujeme nainstalovat tyto:

* [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
* [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
* [Czech - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-czech)
  * je nutné upravit `Předvolby > Nastavení > cSpell.language` na `en,cs`
* [Better TOML](https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml)

## Postup

Řekněme, že budeme zpracovávat okruh *2.1.5. Soubor instrukcí* (2.1.5. je číslo kapitoly, viz [seznam](https://atlas144.codeberg.page/szz)).

### Příprava

V [repozitáři](https://codeberg.org/atlas144/szz) vytvoříme novou větev v větve `main` s názvem *2-1-5-soubor-instrukci*:

![Nová větev](./contribution/new_branch.png)

Poté stáhneme změny pomocí

``` bash
git pull
```

a přesuneme se do nově vytvořené větve.

``` bash
git checkout 2-1-5-soubor-instrukci
```

### Obsah

Nyní můžeme začít upravovat soubor pro daný okruh. Zdrojové soubory se nachází ve složce `./src` a následujících podsložkách podle předmětu. Zde `Informační a komunikační technologie > Architektura počítačů > Soubor instrukcí`, cesta tedy je `./src/ikt/ap/soubor-instrukci.md`.

Přidáváme-li obrázky či jiný dodatečný obsah, vytvoříme složku s názvem okruhu (zde tedy `./src/ikt/ap/soubor-instrukci`) a obsah přidáváme do ní.

V průběhu editace je užitečné vidět finální podobu dokumentu. K tomu slouží náhledy v editoru:

![Tlačítko náhledu ve VSCodium](./contribution/md_preview.png)

Rovněž si lze výslednou podobu prohlédnou přímo pomocí *mdBook*, a to spuštěním příkazu

``` bash
mdbook serve -o             # pokud je lokace v PATH
[cesta]/mdbook serve -o     # pokud se binarka spousti primo
```

Změny je dobré průběžně commitovat a pushovat na server!

``` bash
git commit -m "feat(content): lorem ipsum"
git push
```

#### Formát zápisu

Obsah je zapisován ve formátu [Markdown](https://commonmark.org/help/) s rozšířeními, která jsou popsána [zde](https://rust-lang.github.io/mdBook/format/markdown.html).

Pro zápis matematických výrazů lze použít [MathJax](https://www.mathjax.org/) (s drobnými [omezeními](https://rust-lang.github.io/mdBook/format/mathjax.html)).

### Zdroje

Zdroje přidáváme nakonec souboru, v samostatné sekci. Ideální formát je citace podle normy [ČSN ISO 690](https://www.citace.com/CSN-ISO-690). Minimální formát je odkaz na web.

### Konec editace

Považujeme-li okruh za dokončený, pushneme změny a následně otevřeme pull request ze zpracovávané větve (`2-1-5-soubor-instrukci`) do `main`:

![Nový pull request](./contribution/pr.png)

Po schválení se nově zpracovaný okruh objeví na [stránce](https://atlas144.codeberg.page/szz).