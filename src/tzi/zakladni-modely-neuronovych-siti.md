# Základní modely neuronových sítí

> perceptron a RBF, typické aktivační funkce neuronů a topologie neuronových sítí

Tento okruh vychází ze znalostí toho, co je to **neuron** a známe základní pojmy, jež se k **neuronu** vztahují. Tyto základní pojmy, společně s popisem **neuronu** lze najít v okruhu [Neuronové sítě, základní pojmy](https://atlas144.codeberg.page/szz/tzi/neuronove-site,-zakladni-pojmy.html).

## Základní topologie neuronové sítě

Neuronová síť je tvořena z **neuronů** a vazeb mezi těmito neurony. Klasická neuronová síť se skládá ze tří typů vrstev:

- **vstupní vrstva**:
  - tato vrstva zpracovává zadané vstupy
- **N skrytých vrstev**:
  - každá z těchto vrstev přijímá jako vstupy výstupní hodnoty **aktivačních  funkcí** předchozí vrstvy
  - ne vždy jsou potřeba skryté vrstvy, záleží na konkrétním typu neuronové sítě a toho, čeho chceme její aplikací dosáhnout
  - obecně platí, že pokud nemůžeme výstupy klasifikovat na základě **lineární regrese**, je potřeba použít skryté vrstvy
  - příklad rozhodnutí, zdali skrytou vrstvu použít, či nikoliv, je uveden [zde](https://www.baeldung.com/cs/hidden-layers-neural-network)
- **výstupní vrstva**:
  - vstupy jsou výstupní hodnoty **aktivačních funkcí** předchozí vrstvy a výstupem je/jsou výsledky (počet výsledků odpovídá počtu **neuronů** ve výstupní vrstvě)

|![Příklad podoby neuronové sítě](zakladni-modely-neuronovych-siti/network_basic.PNG "Příklad podoby neuronové sítě")|
|:--:|
|*obr. 1:* Příklad podoby neuronové sítě, kde se počet skrytých vrstev = 2|
|*dostupné z:* <https://www.baeldung.com/cs/hidden-layers-neural-network>|

## Druhy topologií neuronových sítí

Topologii můžeme dělit na dva druhy:

- **dopředná neuronová síť**
  - signál proudí jedním směrem (dopředu)
  - *perceptron* i *RBF* jsou dopředné neuronové sítě
  - **jednovrstvá dopředná síť:**
    - neobsahuje **skrytou vrstvu**, vstup je přímo napojený na výstup
  - **vícevrstvá dopředná síť:**
    - obsahuje jednu či více **skrytých vrstev**
- **síť se zpětnou vazbou/rekurentní neuronová síť**
  - signál může proudi nejen dopředu, ale i do předchozích vrstev (tento okruh se zaobírá **dopřednými neuronovými sítěmi**, více o **rekurentních sítích** lze nalézt v okruhu [Rekurentní neuronové sítě](https://atlas144.codeberg.page/szz/tzi/rekurentni-neuronove-site.html))
  - **plně rekurentní síť:**
    - každý neuron je propojený s každým a propojení fungují obousměrně jako vstup i výstup
    - **Jordan síť:**
      - výstupní vrstva je napojena na vstupní vrstvu sloužící jako **zpětná vazba**

|![Plně rekurentní topologie](zakladni-modely-neuronovych-siti/fully_recurrent.PNG "Plně rekurentní topologie")|
|:--:|
|*obr. 2:* Plně rekurentní topologie, kde se počet skrytých vrstev = 2|
|*dostupné z:* <https://www.tutorialspoint.com/artificial_neural_network/artificial_neural_network_building_blocks.htm>|

<br>

|![Plně rekurentní topologie](zakladni-modely-neuronovych-siti/Jordan.PNG "Plně rekurentní topologie")|
|:--:|
|*obr. 3:* Plně rekurentní topologie, kde se počet skrytých vrstev = 1|
|*dostupné z:* <https://www.tutorialspoint.com/artificial_neural_network/artificial_neural_network_building_blocks.htm>|

## Základní aktivační funkce

Účelem **aktivační funkce** je rozhodnout, jaká bude výstupní hodnota neuronu na základě hodnoty jeho **vnitřního stavu**. Každá aktivační funkce má své využití, některé aktivační funkce se hodí používat pouze v určitých vrstvách.

Rozebereme si následující druhy používaných aktivačních funkcí:

- **Lineární**
- **Sigmoid**
- **Tangens hyperbolická funkce (Tanh funkce)**
- **RELU funkce**
- **Softmax funkce**

### Lineární aktivační funkce

- **rovnice**: \\( y = x \\)
- pokud máme na všech vrstvách lineární aktivační funkci, tak platí, že nezávisle na počtu vrstev je **aktivační funkce** výstupní vrstvy pouze lineární funkce vstupní vrstvy
- **použití**:
  - pouze na jednom místě, například ve výstupní vrstvě
  - hodí se na řešení lineárně regresních problémů (dělení do kategorií)

### Sigmoid aktivační funkce

- **rovnice**: \\( y = \frac{1}{1 + e^{-x}} \\)
- nelineární funkce, na základě tvaru křivky a toho, že v intervalu \\(<-2, 2>\\) hodnoty \\(y\\) rostou velmi rychle lze vidět, že i malá změna \\(x\\) má velký vliv na výstupní hodnotu \\(y\\)
- rozsah výstupních hodnot \\(je <0, 1>\\)
- **použití**:
  - klasicky ve výstupní vrstvě **binárních klasifikátorů**, kde výsledek může být buď `0`, nebo `1`
  - výstupní hodnota této funkce se pohybuje mezi `0` a `1`, funkce tedy může říct, že výstup je `1`, pokud je spočtená hodnota větší než například `0,5`.

|![Sigmoida](zakladni-modely-neuronovych-siti/sigmoid.PNG "Sigmoida")|
|:--:|
|*obr. 4:* Sigmoida|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

### Tanh aktivační funkce

- **rovnice**:

|![Tanh rovnice](zakladni-modely-neuronovych-siti/tanh_equation.PNG "Tanh rovnice")|
|:--:|
|*obr. 5:* Tanh rovnice|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

- jedná se o posunutou **sigmoid** funkci (často se využívá namísto této funkce)
- rozsah výstupních hodnot je \\(<-1, 1>\\)
- **použití**:
  - obvykle se využívá ve skrytých vrstvách
  - protože rozsah výstupu funkce je mezi `-1` a `1`, používá se ke stáhnutí středních hodnot k `0` a vycentrování dat, **umožňuje tedy jednotlivým vrstvám jednodušší proces učení**.

|![Tanh funkce](zakladni-modely-neuronovych-siti/tanh.PNG "Tanh funkce")|
|:--:|
|*obr. 6:* Tanh funkce|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

### RELU aktivační funkce

- **rovnice**: \\(A(x) = max(0,x)\\) 
  - vrací \\(x\\) pokud je \\(x\\) pozitivní, jinak vrací `0`
- jedná se o **nejvíce používanou funkci**, hlavně ve **skrytých vrstvách**
- rozsah výstupních hodnot je \\(<0, \infty>\\)
- **použití**:
  - **RELU** je méně výpočetně náročná nežli **Sigmoid** nebo **Tanh** funkce
  - povětšinou svým výstupem aktivuje jen pár dalších neuronů, je tedy více vhodná i k následnému učení

|![RELU funkce](zakladni-modely-neuronovych-siti/RELU.PNG "RELU funkce")|
|:--:|
|*obr. 7:* RELU funkce|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

### Softmax aktivační funkce

- jedná se o typ **sigmoid** funkce
- rozsah výstupních hodnot je \\(<0, 1>\\)
- **použití**:
  - pokud se snažíme zařadit data do více tříd, tak se funkce používala ve výstupní vrstvě při klasifikaci obrazu
  - **funkce umožňuje namapovat velké hodnoty \\(x\\) do intervalu \\(<0, 1>\\)**
  - funkce se nyní často používá ve výstupní vrstvě **klasifikátorů**, kdy je třeba určit pravděpodobnost, s jakou data spadají do určité třídy

|![Softmax funkce](zakladni-modely-neuronovych-siti/softmax.PNG "Softmax funkce")|
|:--:|
|*obr. 8:* Softmax funkce|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

### Jak se rozhodnout, kterou funkci použít?

- pokud je náš výstup v podobě **binárního klasifikátoru**, použijeme **Sigmoid aktivační funkci** pro výstupní vrstvu
- pokud potřebujeme určit s jakou pravděpodobností data spadají do určité třídy a je techto tříd více (**multi-class classification**), použijeme **Softmax aktivační funkci**
- pokud nevím, jakou funkci použít pro skrytou vrstvu, použiji **RELU aktivační funkci**, protože se momentálně jedná o nejvíce používanou aktivační funkci ve skrytých vrstvách

## Modely neuronových sítí

Modely neuronových sítí se mohou lišit buď **topologií** nebo postupem výpočtu **vnitřního potenciálu neuronu**. V tomto okruhu budou probírány dva z těchto modelů:

- **Perceptron**
- **RBF (Radial Basic Network)**

## Perceptron

Jedná se o **nejjednodušší model dopředné neuronové sítě**. **Zároveň se jedná i o matematický model biologického neuronu**. Můžeme říct, že libovolný model je složený z **perceptronů**, ale pokud se koukáme na **perceptron** jako na samostatný model, jedná se o **binární klasifikátor**, který na základě **vstupů** spočte svůj **vnitřní stav** a svůj vnitřní stav předává jako argument **aktivační funkci**. Perceptron řeší lineárně separovatelné problémy. Pokud je třeba dělit do více tříd než dvou, používá se tzv. **Multi-layer Perceptron**, který mezi vstupní a výstupní vrstvou obsahuje další, skryté vrstvy.

**Aktivační funkce vypadá následovně:** 

|![Aktivační funkce perceptronu](zakladni-modely-neuronovych-siti/perceptron_activation.PNG "Aktivační funkce perceptronu")|
|:--:|
|*obr. 9:* Aktivační funkce perceptronu|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

- **w** je vector vah vstupů a **x** je vektor hodnot na vstupech
- **b** je bias, nebolí **práh**
  - tento práh si perceptron upravuje sám na základě toho, jak moc se lišila výsledná hodnota od hodnoty očekávané
  - více o tomto tématu v okruhu **[Učení bez učitele](https://atlas144.codeberg.page/szz/tzi/uceni-bez-ucitele.html)**
- všimněme si, že **model perceptronu je stejný, jako obecný model neuronu z okruhu [Neuronové sítě, základní pojmy](https://atlas144.codeberg.page/szz/tzi/neuronove-site,-zakladni-pojmy.html)**
- abychom dosáhli požadovaného výsledku **aktivační funkce**, tak se nejčastěji používá **sigmoid aktivační funkce**

|![Model Perceptronu](zakladni-modely-neuronovych-siti/perceptron.PNG "Model Perceptronu")|
|:--:|
|*obr. 10:* Model Perceptronu|
|*dostupné z:* <https://www.geeksforgeeks.org/activation-functions-neural-networks/>|

<br>

|![Perceptron - neuronová síť](zakladni-modely-neuronovych-siti/perceptron_simple.PNG "Perceptron - neuronová síť")|
|:--:|
|*obr. 11:* Perceptron - neuronová síť (žlutá je **vstupní vrstva** a oranžová je **vrstva výstupní**)|
|*dostupné z:* <https://towardsdatascience.com/the-mostly-complete-chart-of-neural-networks-explained-3fb6f2367464>|

## RBF (Radial Basis Function network)

Na rozdíl od neuronových sítích založených čistě na **Perceptronech** využívají RBF sítě RBF neurony. 

- RBF nepočítá svůj vnitřní stav jako součin vektorů vstupů a vah (**w** a **x**), ale má v sobě uložený jeden vektor z **trénovací množiny**, označený jako **střed**. Tyto středy se dají ideálně získat pomocí **K-means** algoritmu. Tento vektor (**střed**) se používá k měření **Euklidovské vzdálenosti** od **vstupních vektorů**. Výsledek poté pošle do aktivační funkce, která spočte, jak moc je aktuální vektor vstupů podobný **středu**.
- aktivační funkce RBF neuronu je **Gaussova funkce**

|![Aktivační funkce RBF neuronu, kde X je jeho vnitřní stav](zakladni-modely-neuronovych-siti/gauss_equation.PNG "Aktivační funkce RBF neuronu, kde X je jeho vnitřní stav")|
|:--:|
|*obr. 12:* **Aktivační funkce** RBF neuronu, kde **x** je jeho **vnitřní stav**|
|*dostupné z:* <https://en.wikipedia.org/wiki/Radial_basis_function_network>|

- výstupem RBF aktivační funkce je hodnota, která určuje, jak moc je vstupní set podobny **středu**
- tyto hodnoty se sečtou ve výstupní vrstvě
- RBF neurony jsou náchylnější ke skončení v **lokálním minimu**, ale naopak umí dobře pracovat s **odlehlými hodnotami** - velké změny v parametrech neuronu způsobí malý posun u hodnot, jež jsou daleko od **středu**
- RBF sítě mají obvykle jednu skrytou vrstvu, která obsahuje RBF neurony a lineární výstupní vrstvu
- výstupem z celé této sítě je tedy skalární součin **vektoru výstupu z RBF vrstvy a vah synapsí vedoucí ze skryté vrstvy do výstupní vrstvy**
- **RBF** se tedy liší od **Multi-layer Perceptron** sítě jinou **aktivační funkcí a způsobem použití**

|![RBF síť](zakladni-modely-neuronovych-siti/rbf_simple.PNG "RBF síť")|
|:--:|
|*obr. 13:* RBF síť s jednou skrytou vrstvou (zelená), ve které se nacházejí RBF **aktivační funkce** (výstup těchto funkcí vede do **výstupní vrstvy**, která již nemusí být RBF)|
|*dostupné z:* <https://towardsdatascience.com/the-mostly-complete-chart-of-neural-networks-explained-3fb6f2367464>|

## Zdroje

- Baeldung. Hidden Layers in a Neural Network. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: https://www.baeldung.com/cs/hidden-layers-neural-network
- Great Lakes E-Learning Services Pvt. Ltd. Types of Neural Networks and Definition of Neural Network. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: https://www.mygreatlearning.com/blog/types-of-neural-networks/
- Towards Data Science. Types of Neural Networks and Definition of Neural Network. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: 
https://towardsdatascience.com/the-mostly-complete-chart-of-neural-networks-explained-3fb6f2367464
- GeeksForGeeks. Activation functions in Neural Networks. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: 
https://www.geeksforgeeks.org/activation-functions-neural-networks/
- Wikipedia - Open encyclopedia. Perceptron. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: https://en.wikipedia.org/wiki/Perceptron
- TutorialsPoint. Artificial Neural Network - Building Blocks. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: 
https://www.tutorialspoint.com/artificial_neural_network/artificial_neural_network_building_blocks.htm
- Luthfi Ramadhan. Radial Basis Function Neural Network Simplified. [online]. @2021 [cit. 2023-04-24].<br> Dostupné z: 
https://towardsdatascience.com/radial-basis-function-neural-network-simplified-6f26e3d5e04d
- Wikipedia - Open encyclopedia. Radial basis function network. [online]. @2023 [cit. 2023-04-24].<br> Dostupné z: https://en.wikipedia.org/wiki/Radial_basis_function_network
