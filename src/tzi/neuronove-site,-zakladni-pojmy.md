# Neuronové sítě, základní pojmy

> neuron, synapse, axon, dendrit, váha, práh, aktivační funkce

## Úvod 

Koncept neuronový sítí byl v počátcích **inspirován** a vytvářen na základě **biologické analogie** s nervovým systémem. Vezeme si příklad hodu míčem. Co se stane, pokud cíl při hodu mineme? Pomocí receptorů (například vizuálních) máme možnost výsledek vyhodnotit a v případě toho, že cíl například přehodíme, upravit sílu hodu.

Zjistili jsme tedy odchylku od požadovaného ideálního stavu na výstupu a na základě zpětné vazby trénujeme náš nervový systém tak, aby byl schopen danou úlohu plnit co nejlépe. Člověk tedy získává prostřednictvím svého nervového systému i schopnost učit se a zapamatovat si získané informace.

Celý nervový systém člověka je představován obrovským počtem navzájem provázaných biologických funkčních jednotek, **neuronů**. Například mozek obsahuje až 10<sup>11</sup> neuronů, kde každý neuron může mít až 5000 spojů s ostatními neurony. Tyto spoje se nazývají **synapse**, mohou mít jak **excitační**, tak **inhibiční** charakter, mohou tedy vést k **posílení**, či **potlačení** odezvy neuronu na přicházející podněty, vzruchy.

Pokud se budeme držet popisu biologického neuronu, který nás má dovést k matematickému modelu, můžeme konstatovat, že každý neuron se skládá z z těla a dvou typů výběžků: **dendritů a axonu**.

- **Dendrit**: Z pohledu šíření vzruchu do těla se jedná o **vstupy neuronu**.
- **Axon**: Axon představuje **výstup neuronu**, kterým se siří vzruch z těla neuronu a který **je
pomocí synapsí napojen na dendrity neuronů dalších**

|![Biologický neuron](neuronove-site-zakladni-pojmy/biological_neuron.png "Biologický neuron")|
|:--:|
|*obr. 1:* Biologický neuron<br>Převzato z: https://portal.matematickabiologie.cz/res/f/neuronove-site-jednotlivy-neuron.pdf|

## Matematický model a aktivní dynamika neuronu

Neuronová síť je tvořena matematickými **neurony**, primitivními jednotkami kde **každá
zpracovává vstupní signály a generuje výstup**.

|![Matematický neuron - model](neuronove-site-zakladni-pojmy/neuron.png "Matematický neuron - model")|
|:--:|
|*obr. 2:* Matematický neuron - model<br>Převzato z: https://portal.matematickabiologie.cz/res/f/neuronove-site-jednotlivy-neuron.pdf|

Neuron lze rozdělit na několik částí:

- **Synapse**: Přivádějí vstupy X do těla neuronu. **Každá synapse je ohodnocena vahou**, tedy důležitostí této synapse. Můžeme říct, že každá synapse, **každý vstup, je pro neuron jinak důležitý**. Vstupy mohou být buď inicializační (jedná se o vstupní vrstvu) nebo se může jednat o výstupy jiných neuronů.

- **Váha**: Číselná hodnota, jež udává, jaký vliv má hodnota na vstupu.

- **Tělo neuronu**: Do těla neuronu jsou svedeny všechny vstupy skrze synapse + **práh**, který je často vedený také jako vstup a na základě těchto hodnot se zde získává **vnitřní potenciál neuronu**

- **Práh**: Z praktických důvodů se práh zpravidla modeluje jako jedna z vah tak, že vstupní vektor i vektor vah je rozšířen o nultou pozici. Vstup na nulté pozici je vždy uvažován za rovný 1 a nultá váha je nastavena na hodnotu rovnu –h. V takovém případě se práh stává jednou z vah a v průběhu trénování podléhá adaptaci. 

- **Vnitřní potenciál neuronu**: Značí se \\(\xi\\) (Ksí).  Jedná se o hodnotu, kterou dostaneme tak, že každý vstup X vynásobíme vahou synapse. Vstupy v těle neuronu sečteme a odečteme **práh**. Tato hodnota je poté vstupem do **Aktivační přenosové funkce**. 

- **Aktivační funkce**: Značí se \\(\delta\\) (Delta). Je obecně nelineární funkcí transformující hodnotu **vnitřního potenciálu neuronu**.

|![Výstup neuronu](neuronove-site-zakladni-pojmy/neuron_output.PNG "Výstup neuronu")|
|:--:|
|*obr. 3:* Výstup neuronu, kde $\delta$ je aktivační funkcí a $\xi$ vnitřním potenciálem neuronu. (Práh je vedený jako vstup na 0-t= pozici) <br>Převzato z: https://portal.matematickabiologie.cz/res/f/neuronove-site-jednotlivy-neuron.pdf|	


### Fungováním neuronových sítí jako celku se dále zaobírá okruh [Základní modely neuronových sítí](https://atlas144.codeberg.page/szz/tzi/zakladni-modely-neuronovych-siti.html).

## Zdroje

- HOLČÍK, Jiří, KOMENDA, Martin (eds.) a kol. Neuronové sítě - jednotlivý neuron. [online]. Brno: Masarykova univerzita, @2015 [cit. 2023-01-24]. Dostupné z: https://portal.matematickabiologie.cz/res/f/neuronove-site-jednotlivy-neuron.pdf
