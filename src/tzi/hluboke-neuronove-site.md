# Hluboké neuronové sítě

Hluboké učení je disciplína v rámci strojového učení, která se zabývá učením neuronových sítí s velkou hloubkou. Hloubkou modelu se myslí počet vrstev neuronů, které jsou za sebou zapojeny tak, že výstup jedné vrstvy je vstupem vrstvy následující. U modelů hlubokého učení se přitom hloubka nachází často v řádech desítek a více vrstev.

Při tréninku hluboké neuronové sítě se **váhy** mezi neurony upravují tak, aby sítě byly schopné přesněji predikovat výsledky na základě poskytnutých dat. K tomuto účelu se používá metoda zvaná **zpětné šíření chyby (backpropagation)**, která upravuje váhy sítě na základě rozdílu mezi předpovídanými výstupy a správnými výstupy.

Pěkná ukázka funkčnosti hluboké neuronové sítě je znázorněna ve [videu](https://www.youtube.com/embed/6M5VXKLf4D4?start=94) (01:34 - 03:00).

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/6M5VXKLf4D4?start=94" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>

## Trénování

Proces trénování lze rozdělit (viz obr. 1) na:
- **Forward propagation** - vstupní data jsou předány síti, která vypočítá postupně hodnoty aktivační funkce a výstupy jednotlivých neuronů a pošle je do další vrstvy. Spojení mezi vrstvami funguje prostřednictvím **parametrů** (**váhy** a **bias**y). Tento proces se opakuje až do dosažení výstupní vrstvy, kde se vypočítají predikce.
- **Výpočet chyby** (loss function) - následuje výpočet chyby mezi předpovězenými výstupy sítě a správnými výstupy ze vstupních dat. To poskytuje informaci o tom, jak moc se síť mýlí.
- **Backpropagation** - po výpočtu ztrátové funkce se provádí aktualizace váh a biasů pomoci **optimalizačního algoritmu**. Cílem optimalizačního algoritmu je najít globální minima, kde má ztrátová funkce minimální hodnotu. Při backpropagation se výpočty provádějí od výstupní vrstvy ke vstupní vrstvě (zprava doleva).
  
|![Proces trénování hluboké neuronové sítě](hluboke-neuronove-site/training.png "Proces trénování hluboké neuronové sítě")|
|:--:|
|obr. 1: Proces trénování hluboké neuronové sítě|

## Rozdělení hlubokých neuronových sítí

Hluboké neuronové sítě se mohou rozdělovat do několika kategorií na základě jejich architektury a propojení mezi vrstvami:
- **Vícevrstvý perceptron (MLP)** - nejzákladnější forma hluboké neuronové sítě. Skládá se z jedné vstupní vrstvy, jedné nebo více skrytých vrstev a výstupní vrstvy. Váhy mezi neurony jsou přepojeny a každý neuron je propojen se všemi neurony v předchozí a následující vrstvě.
- **Konvoluční neuronové sítě (CNN)** - často používány pro zpracování obrazových dat. Mají speciální konvoluční vrstvy, které efektivně extrahují rysy z obrázků pomocí konvolucí a sdílených vah. Konvoluční vrstvy jsou často střídány s vrstvami **subvzorkování (pooling layers)**, které redukují rozměr dat.
- **Rekurentní neuronové sítě (RNN)** - navrženy pro zpracování sekvencí dat, jako jsou časové řady nebo přirozený jazyk. Mají rekurentní vrstvy, které umožňují předávání stavů mezi časovými kroky, což umožňuje sítím uchovávat informace o historii vstupních dat.

## Vrstvy

Hluboká neuronová síť může obsahovat různé vrstvy, v závislosti na konkrétní architektuře sítě a úkolu, který má řešit:
- **Konvoluční vrstvy** - využívají konvoluce pro extrakci rysů z dat a sdílené váhy pro efektivní zpracování obrazových dat. **Konvoluce** je matematická operace, která se používá ke kombinaci dvou funkcí a v kontextu konvolučních vrstev umožňuje provádět lokální vážené součty mezi vstupními daty a váhami. Konvoluční vrstvy se skládají z několika **filtrů**, které jsou převedeny přes vstupní data. Každý filtr v konvoluční vrstvě obsahuje váhy, které reprezentují rysy, které se mají vyextrahovat z dat. Tyto váhy jsou optimalizovány během trénování sítě. Konvoluční vrstvy také často zahrnují aktivační funkci, jako je například **ReLU (Rectified Linear Unit)**.
- **Pool vrstva (pooling layer)**, známá také jako subvzorkovací vrstva - používaná v konvolučních neuronových sítích pro snižování rozměru prostorových dat. Provádí subvzorkování, což je operace, která snižuje rozměr konvoluční mapy tím, že redukuje počet prvků na základě nějakého pravidla, například maxima nebo průměru. To pomáhá snížit počet parametrů v síti.
  
|![Pool vrstva](hluboke-neuronove-site/pool.png "Pool vrstva")|
|:--:|
|obr. 2: Pool vrstva|

- **Plně připojené vrstvy (fully connected layers)**, také nazývané jako "fc layers" - každý neuron je propojen s každým neuronem ve předchozí a následující vrstvě. V těchto vrstvách neexistuje žádná prostorová struktura jako u konvolučních vrstev. Typicky se používá na výstupu hluboké sítě a slouží k vykonání konečné klasifikace nebo regrese na základě naučených příznaků. Po průchodu plně připojenými vrstvami se obvykle používá softmax aktivační funkce.
- **Rekurentní vrstvy** - mají zpětné propojení, které umožňuje předávání informací z předchozích časových kroků do budoucích.

Rozdělení vrstev u rozpoznávání obrázků je znázorněno na obrázku 3. ReLU vrstvu můžeme chápat jako skupinu neuronů, která používá ReLU aktivační funkce pro výpočet výstupů.

|![Vrstvy CNN](hluboke-neuronove-site/deep_learning.png "Vrstvy CNN")|
|:--:|
|obr. 3: Vrstvy CNN|

## Aktivační funkce

Aktivační funkce se používají v neuronových sítích k přidání nelinearity, normalizaci výstupů a vytvoření hladkých přechodů, což umožňuje síti aproximovat složité nelineární vztahy mezi vstupy a výstupy  a dosáhnout plynulého učení a zpracování dat. Typické aktivační funkce jsou například:
- **ReLU (Rectified Linear Unit)**- jedna z nejčastěji používaných aktivačních funkcí. Je definována jako **max(0, x)**. To znamená, že pro nezáporné vstupy vrátí přímo hodnotu vstupu a pro negativní vstupy vrátí 0. ReLU přispívá k nelinearitě sítě a pomáhá snižovat problémy s postupným úmrtím neuronů.
- **Leaky ReLU** - modifikace ReLU, která přidává malou nenulovou hodnotu pro negativní vstupy, například f(x) = max(ax, x) s a > 0. Tím se zabývá problémem nulového gradientu pro negativní vstupy v ReLU.
- **Parametrická ReLU (PReLU)** - obdoba Leaky ReLU, ale místo pevně stanovené hodnoty má parametr, který je optimalizován během trénování.
- **Softmax** - používá se pro klasifikaci mezi více třídami. Funkce se
aplikuje na vnitřní potenciály všech neuronů vrstvy. Pro výstupy pak platí, že jsou v rozsahu **(0,1)** a **součet všech výstupů je roven 1**. Výstupy tak mohou být interpretovány jako **pravděpodobnosti**. Softmax je často používán jako poslední aktivační funkce v klasifikačních úlohách.
- **Sigmoidní funkce (sigmoid)** - mapuje vstupní hodnoty na rozsah mezi 0 a 1, což je užitečné pro binární klasifikaci nebo jiné úlohy, které vyžadují pravděpodobnosti.
- **Hyperbolický tangent (tanh)** - je podobný sigmoidní funkci, ale mapuje vstupní hodnoty na rozsah mezi -1 a 1.

## Frameworky
Seznam nejznámějších frameworků pro učení hlubokých neuronových sítí:
- **TensorFlowm** - jeden z nejpopulárnějších frameworků pro strojové učení a hluboké učení. Je vyvinutý společností Google a je známý pro svou efektivitu, flexibilitu a podporu pro distribuované výpočty.
- **PyTorch** - open-source framework pro strojové učení, vyvinutý Facebookem. Má silnou podporu pro práci s grafickými procesory (GPU). PyTorch je nástupcem frameworku Theano.
- **Keras** - vysoce abstraktní knihovna pro hluboké učení. Je nadstavbou nad frameworky jako TensorFlow nebo Theano a nabízí rychlý vývoj a experimentování s modely.
- **MXNet** - framework pro hluboké učení s otevřeným zdrojovým kódem. Je navržený pro efektivní výpočetní grafy a podporuje různé programovací jazyky, včetně Pythonu, R, Julia a C++ a také podporuje distribuované učení.

## Použití hlubokých neuronových sítí

Metodologie hlubokého učení se prosadila jako základní možnost pro řešení složitých problémů strojového učení jako je klasifikace obrazů, mluvené či psané řeči nebo překlady z jednoho přirozeného jazyka do jiného.

## Zdroje
- Přispěvatelé Wikipedie, Hluboké učení [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 7. 03. 2023, 15:08 UTC, [citováno 19. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Hlubok%C3%A9_u%C4%8Den%C3%AD&oldid=22524090> 
- https://medium.com/data-science-365/overview-of-a-neural-networks-learning-process-61690a502fa
