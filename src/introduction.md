# Úvod

Cílem této knihy je shromáždit na jednom místě základní informace z celého spektra oboru **Informační technologie** a speciálně pak z oblasti **Embedded systémů**. Tyto informace by měly posloužit především studentům oboru *Aplikovaná informatika* na *Přírodovědecké fakultě Jihočeské univerzity v Českých Budějovicích* (a doufejme že stejně dobře i studentům příbuzných oborů na jiných univerzitách), při studování na Státní závěrečné zkoušky.

Struktura knihy vychází ze Seznamů zkušebních okruhů pro SZZ:

- [Teoretické základy informatiky](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6068tzi.pdf) 
- [Informační a komunikační technologie](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6020ikt.pdf)
- [Embedded systémy](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6029vestavne-systemy.pdf)

## Šíření obsahu - licence

Tato kniha a tedy i šíření jejího obsahu podléhá licenci <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Uveďte původ 4.0 Mezinárodní License</a>.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>