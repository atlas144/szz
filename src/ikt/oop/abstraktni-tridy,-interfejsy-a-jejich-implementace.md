# Abstraktní třídy, interfejsy a jejich implementace


## Abstraktní třída

Abstraktní třída je obyčejná třída, která jenom nemá definované některé metody; předpokládá se, že nějaká jiná třída z ní bude dědit a tyto metody si doplní podle potřeby.

Je to hybrid mezi rozhraním (interface) a klasickou třídou. Od klasické třídy má schopnost implementovat vlastnosti (proměnné) a metody, které se na všech odvozených třídách budou vykonávat stejně. Od rozhraní zase získala možnost obsahovat prázdné abstraktní (**čistě virtuální**) metody, které si každá odvozená podtřída musí naimplementovat sama. S těmito výhodami má abstraktní třída i pár omezení, a to že jedna podtřída nemůže zdědit víc abstraktních tříd a od rozhraní přebírá omezení, že nemůže vytvořit samostatnou instanci (operátorem new).

Klasické využítí abstraktní třídy je, pokud potřebujeme vágního, společného předka. Příkladem může být třída Shape a její potomoci Cirle, Sphere a Square. Potomci třídy mají společné vlastnosti a metody, jež se nachází ve třídě Shape (například proměnná barva, metoda changeColor, nebo metoda Render). Třída Shape bude abstraktní, protože nemá smysl, aby existoval objekt typu Shape. Ale má smysl, aby existoval objekt typu Square, který dědí ze abstraktní třídy Shape. V našem příkladu je v třídě Shape prázdná proměnná Color, čímž říkame, že každý potomek této třídy musí mít barvu, předem vyplněná (implementovaná) metoda changeColor a prázdná metoda Render, jež si musí potomci třídy implementovat zvlášť.

### Abstraktní třída

- může obsahovat instanční proměnné
- může obsahovat konkrétní metody i metody bez implementace
- dědění z (abstraktní) třídy vylučuje možnost dědit z jiné třídy

## Interface

Interface je seznam **čistě virtuálních metod**, které by měla třída podporovat, aby plnila nějaký účel. Jestliže se třída hlásí k tomu, že podporuje daný interface, musí definovat všechny metody, které předepisuje interface a tím je přepsat.

Interface předepisuje třídě, která od ní bude odvozena, jaké metody (případně properties) musí implementovat. Odvozený objekt může implementovat i další metody. Interface nedefinuje žádné proměnné ani neobsahuje naimplementované metody (pouze jejich hlavičky). Přitom třída může implementovat libovolný počet rozhraní (na rozdíl od dědičnosti).

Klasické využití interface (rozhraní) je příklad, kdy máme několik různých tříd, u kterých nemá smysl, aby dědily ze stejného předka (světlo, sekačka, rádio), ale tyto třídy mají některé stejné vlastnosti (spínač). Je tedy nasnadě, aby tyto třídy implementovali stejný interface s metodou turnOn / turnOff. Každá třída poté bude mít vlastní implementaci této metody.

### Implementace interface 
Implementaci interface můžeme dělit na
- implicitní 
- explicitní

#### Implicitní implementace interface
- Třída T, která interface I implementuje má implementované metody implementované napřímo (public void interfaceMethod1)
- Metody jsou volatelné na objektu typu T
- Metody jsou volatelné na objektu typu I
- Metody jsou volatelné na potomcích třídy T

|![Implicitní implementace interface](abstraktni-tridy-interfejsy-a-jejich-implementace/implicit_interface_implementation.png "Implicitní implementace interface IGetsMessage1 ve třídě ImplicitImplementationBase")|
|:--:|
|*obr. 1:* Implicitní implementace<br>*Dostupné z: <https://www.pluralsight.com/guides/distinguish-explicit-and-implicit-interface-implementation-csharp>*|

#### Explicitní implementace interface
- Třída T, která interface I implementuje má implementované metody skrze interface (public void ourInterface.interfaceMethod1)
- Metody nejsou volatelné na objektu typu T
- Metody jsou volatelné na objektu typu I
- Metody nejsou volatelné na potomcích třídy T

|![Explicitní implementace interface](abstraktni-tridy-interfejsy-a-jejich-implementace/explicit_interface_implementation.png "Explicitní implementace interface IGetsMessage1 ve třídě ExplicitImplementationBase")|
|:--:|
|*obr. 2:* Explicitní implementace<br>*Dostupné z: <https://www.pluralsight.com/guides/distinguish-explicit-and-implicit-interface-implementation-csharp>*|

pozn. Zatímco implicitní implementace je možná téměř všude, explicitní implementace není možná například v jazyce Java. (ale v c# ano).

### Interface (rozhraní)
- nesmí obsahovat proměnné
- všechny metody jsou bez implementace
- do jedné třídy lze implementovat více rozhraní (Řešení problému vícenásobné dědičnosti)

### Typy interface

Interface, jako takový, můžeme dělit na dva typy. (Neplést si s implementací interface, viz výše)

- implicitní interface (public metody)
- explicitní interface (musí se napsat struktura Interface a poté implementovat)

#### Explicitní interface

Jedná se o interface, který musíme vytvořit a naimplementovat. (viz výše, implementace interface)

#### Implicitní interface 

Jedná se o public metody třídy. Skrze tyto třídy přístupujeme k objektu. Je to tedy jakési jeho rozhraní (interface).

***Abstraktní třídy a Interface jsou jedněmi ze základních nástroju pro dosažení polymorfismu. Viz. okruh [2.4.5](https://atlas144.codeberg.page/szz/ikt/oop/dedicnost-a-polymorfismus.html)***

## Citace

- Jaký je rozdíl mezi abstraktní třídou a interface? [online] ©2015 [cit. 2.12.2022] Ivo Kostecký. Dostupné z: <https://blog.kostecky.cz/2015/06/jaky-je-rozdil-mezi-abstraktni-tridou.html>
- Třídy, polymorfismus [online] ©2022 [cit. 2.12.2022] Fakulta elektrotechnická
České vysoké učení technické. Dostupné z: <https://cw.fel.cvut.cz/old/_media/courses/a0b36pr2/lectures/02/36pr2-02_tridyiii.pdf>
- Distinguishing the Explicit and Implicit Interface Implementation in C# [online]  ©2015 [cit. 3.12.2022] Christian Findlay. Dostupné z: <https://www.pluralsight.com/guides/distinguish-explicit-and-implicit-interface-implementation-csharp>
- Implicit and Explicit InterfaceExamples [online] ©2022 [cit 3.12.2022] C#Corner. Dostupné z: <https://www.c-sharpcorner.com/UploadFile/8911c4/implicit-and-explicit-interface-examples/>
