# Řídící struktury jazyka

> Složený příkaz resp. blok. Příkazy pro větvení programu (podmíněný příkaz - varianty, vícenásobné větvení). Příkazy cyklu a možnosti jejich ukončení. Strukturování programu pomocí metod, rekurzivita.

## Složený příkaz

Jedná se o zapsanou posloupnost příkazů, které se nacházejí v bloku, jež je ohraničený **složenými závorkami**

```java
{
    int age;
    age = 10;
    System.out.println("My age is: " + age);
}
```

Jednotlivé bloky mohou být vnořené do sebe

```java
{
    int age;
    age = 10;
    System.out.println("My age is: " + age);

    {
        age = 15;
        System.out.println("My age is: " + age);
    }
}
```

Je důležité dát pozor na **platnost proměnných**. Proměnná deklarovaná v bloku je **přístupná pouze uvnitř tohoto bloku** (a ve vnořených blocích, které jsou rovněž jeho součástí). Jakmile se program dostane **mimo tento blok**, proměnná přestává existovat (**její paměť je uvolněna**).

## Příkazy pro větvení programu

Program se v určité částí může větvit na dvě části pomocí `IF-ELSE` tvrzení. Toto tvrzení je podmínka, která se musí splnit pro vykonání IF větve. Tato podmínka může být:

 - **jednoduchá** `(age > 5)`
 - **složená** `(age > 5 && height > 120)`

```java
if (age >= 15) {
    System.out.println("I'm almost an adult");
} else {
    System.out.println("Go home kid");
}
```

Nebo na více částí pomocí `IF-ELSE IF`

```java
if (age >= 18) {
    System.out.println("I'm big boy now");
} else if (age >= 15) {
    System.out.println("I'm almost an adult");
} else {
    System.out.println("Go home kid");
}
```

Platí, že program postupuje odshora dolů a při splnění podmínky se ukončí celý **blok** -> přeskočí se zbytek `if-else` tvrzení.

Dalším způsobem jak větvit program je použít `switch`.

```java
value = 2;

switch (value) {
    case 1:
        System.out.println("one");
        break;
    case 2:
        System.out.println("two");
        break;
    case 3:
        System.out.println("three");
        break;
    default:
        System.out.println("I dont know");
        break;
}
```

mezi case a break se nachází také **blok kódu**. Pokud by jsme neměli break, je podmínka vyhodnocena, blok proveden, ale pokračovali by jsme ve vyhodnocování **`switche`**.

Obojí může být vnořené do sebe, stejně jako **bloky**.

```java
if (age >= 18) {
    System.out.println("I'm big boy now");

    if (height > 168) {
        System.out.println("And I'm not a hobbit");
    } else {
        System.out.println("What about second breakfast?");
    }
}
```

## Cykly

Cyklus je **blok kódu**, který se opakuje dokud:

- není naplněna podmínka
- není ukončen příkazem `break`
    - namísto ukončení celého cyklu je možné použít příkaz `continue` **pro ukončení aktuální iterace cyklu**

### Druhy cyklů

- **For cyklus**
- **While cyklus**
- **Do-While cyklus**
- **For each**

#### For cyklus

Jedná se o cyklus, ve kterém **iterujeme** **dokud** není naplněn **počet iterací**. Ten je uveden v **podmínce cyklu**. Nejčastěji se jedná o **inkrementující se proměnnou** (kterou však můžeme upravovat v rámci cyklu), říkáme ji **iterátor**. Některé datové struktury mají vlastní **iterátory** -> například struktura `HashMap`.

```java
for (int i = 0; i > age; i++) {
    System.out.println("Now I'm" + i + "years old" );
}
```

#### While cyklus

Zde **iterujeme** dokud není splněna **vstupní podmínka**.

```java
while (age > 5) {
    System.out.println("Now I'm" + age + "years old" );
    age++;
}
```

#### Do-while cyklus

Obměna `while` cyklu. I když je podminka nesplněna při vstupu do cyklu, provede se tělo cyklu **alespoň jednou**.

```java
do {
    System.out.println("Now I'm" + age + "years old" );
    age++;
} while (age > 5)
```

#### For each cykly

Tyto cykly **iterují** nad nějakou kolekcí, **prvek po prvku**. Velmi používané pro práci s kolekcemi.

```java
for (int number : numberArray) {
    System.out.println(number);
}
```

#### Vnořené cykly

Cykly mohou být do sebe vnořené, to se často používá při průchodu **vícerozměrných polí**. **Vnořené** cykly značně zvyšují **algoritmickou složitost**.

```java
for (int i = 0; i > array.size; i++) {
    for (int j = 0; i > array[i].size; j++) {
        System.out.println(array[i,j]); 
    }
}
```
## Strukturování programu za pomoci metod

Pokud chceme udělat nějaký **blok** kódu volatelným, uděláme z něj **metodu**.

*Výhody užití metod:*

- **zpřehlednění kódu** (rozdělení na menší bloky)
- **redukce duplicit** (metody můžeme volat vícekrát)
- **testování** (můžeme testovat konkrétní malou část kódu - metodu)

```java
class Program {

    public static void main(String[] args) {
        method(); // Tady voláme metodu
    }

    public void method() {
        System.out.println("method called");
    }

}
```

Metoda může přijímat parametry nebo i vracet hodnotu pomocí klíčového slova `return`. Taková metoda má poté namísto typu `void` **návratový typ**.

```java
public void method(int num) {
    System.out.println("method called with number: " + num);
}
```

```java
public int getSquared(int num) {
    return num * num;
}
```

Metody je možno volat rekurzivně -> **metoda volá zevnitř sebe sama**. Je zde nutná nějaká ukončující podmínka, jinak to bude dělat do nekonečna. 

 ```java
class Program {

    public static void main(String[] args) {
        int number = 4;
        int result = 0;

        result = factorial(number);
        System.out.println(number + " factorial = " + result);
    }

    public int factorial(int n ) {
        if (n != 0) { // ukončovací podmínka
            return n * factorial(n-1);  //rekurzivní volání, jednotlivé výsledky jsou po skončení rekurze vynásobeny mezi s sebou
        } else {
            return 1;
        }
    }

}
```

## Zdroje 

- Recursion is not hard: a step-by-step walkthrough of this useful programming technique. *FreeCodeCamp* [online] #2018 [citováno: 17.05.2023] <br>Dostupné z: <https://www.freecodecamp.org/news/recursion-is-not-hard-858a48830d83/>
