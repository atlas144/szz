# Základní pojmy objektově orientovaného paradigmatu

**Klíčové pojmy: *OOP – objektově orientované programování, třída, objekt, instance třídy, datové složky a metody třídy,
konstruktor, parametrizovaný konstruktor.***

## Třídy a objekty

V podstatě vše, co lze pojmenovat podstatným jménem je **objekt** (židle, skříň, stůl | tričko, šála, kalhoty). Tyto věci okolo nás lze rozřadit do **tříd** (konceptů) (Nábytek | Oblečení). U každého konkrétního **objektu** můžeme poté říct, že je konkrétní realizací některé **třídy**. Čili, **objekt** je **instance třídy**. **Třída** je obecná (**třída** je vzor). **Objekt** je konkrétní. 

Každá **třída** je charakterizována svými vlastnostmi, funkčními možnostmi a parametry.

U každého **objektu** vnímáme dvě věci: jaké má objekt vlastnosti (student má jméno, příjmení,
datum narození, známky ...) a činnosti daného **objektu** (každý student musí chodit do školy, učit se,
psát písemky, ...). Tomu odpovídá zdrojový kód programu: **třída** se skládá z deklarace proměnných
(datových složek, vlastností dané **třídy**) a definice metod (činnosti dané **třídy**). 

## Instance třídy

V programu vytváříme **objekty** podle předpisu **třídy**. Tomuto objektu se poté říká **instance třídy**.  Pokud chceme vytvořit **instanci**, využijeme k tomu operátor **new**. V následujícím příkladu vytváříme **instanci třídy** `Person`.

```java
Person person1 = new Person();
```

Operátor `new` alokuje paměť potřebnou pro uložení **objektu**. Dále následuje volání **konstruktoru** za pomocí `Person()`. V proměnné person1 je poté uložena reference na místo v paměti, kde se **objekt** skutečně nachází.

## Konstruktor

**Konstruktor** slouží primárně pro předání parametrů při vytváření **instance třídy**. (Ale můžeme si do něj ve výsledku psát cokoliv chceme, aby se s **instancí** stalo ihned po jejím vytvoření)

**Konstruktor** je metoda, která se volá vždy při vytváření **instance třídy**. Ve většině objektově orientovaných jazycích má stejné jméno jako **třída**. 

Rozlišujeme dva druhy konstruktorů:

- Implicitní konstruktor
- Parametrizovaný konstruktor

Pokud **třída** konstruktor nemá, používá se namísto toho automaticky tzv. implicitní konstruktor. Jedná se o prázdný konstruktor bez vstupních parametrů. Tento konstruktor nemusíme vytvářet. Má ho každá **třída**.

Druhou možností je použít parametrizovaný konstruktor, který si přidáme do
definice **třídy** (V našem případě třída Person). Vede nás k tomu několik důvodů:

- pomocí parametrizovaného konstruktoru můžeme nastavit hodnoty atributů **třídy** už při
vytváření **instance třídy**
- nastavování hodnot proměnných po jedné je zdlouhavé a nepřehledné
- datové složky ve **třídě** by měli být správně z jiné **třídy** neviditelné, takže tato úprava by neměla být možná, jen přes speciální metody (`get` a `set`)

Parametrizovaný konstruktor je třeba ručně vytvořit, tedy, definovat ho ve **třídě**. 
Řekněme. že naše **třída** `Person` má atribut `Name`. My bychom rádi nastavili tento atribut již při vytvoření **instance**. Definice konstruktoru bude následovná.

```java
public class Person(){

    private String name;

    public Person(String nameFromOutside){
        this.name = nameFromOutside;
    }
}
```

Pokud budeme nyní chtít vytvořit **instanci třídy** Person, musíme při inicializaci vyplnit všechny parametry, jež po nás konstruktor požaduje.

```java
Person person1 = new Person("Pája");
```

### Destruktor

Stejně jako se konstruktor volá při vytvoření **instance třídy**, destruktor se volá, když opouštíme Scope (zjednodušeně chlupaté závorky) ve kterém byla instance vytvořena. 

Jedná se o speciální metodu jejíž účelem je uvolnění paměti a zrušení reference. V některých jazycích, jako je například Java, nemusíme destruktor řešit, neboť je implementován automaticky pomocí tzv. *garbage collectoru*. V jiných, jako je C++, je potřeba ve **třídě** destruktor ručně definovat a v odpovídající chvíli ho na **instanci** zavolat.

## Metody

Již jsme řekli, že třída se skládá z datových složek (proměnných) a definic metod. Metoda je něco, co třída umí.

- Metody určují chování objektu
- Definují operace nad daty objektu
- Metody představují služby objektu, proto jsou často veřejné
- Metody mohou vracet hodnoty
- Mohou být deklarovány jako privátní, např. pro pomocné funkce/výpočtu zlepšující čitelnost kódu.

### Volání metod

- Samotnou deklarací (napsáním kódu) metody se žádný kód neprovede.
- Chceme-li vykonat kód metody, musíme ji zavolat.
- Volání se realizuje (tak jako u proměnných) "tečkovou notací", viz dále.
- Při volání metody je možné předat metodě parametry
- Volání lze provést, jen je-li metoda z místa volání přístupná - "viditelná". Přístupnost regulují podobně jako u proměnných modifikátory přístupu.

## Statické proměnné a metody

Doteď každá proměnná nebo třída patřili vždy určitému **objektu**. (**instanci třídy**)

Lze deklarovat také metody a proměnné patřící celé **třídě**, tj. skupině všech objektů daného typu - statická proměnná existuje pro jednu třídu jen jednou.

Takové metody a proměnné nazýváme statické a označujeme v deklaraci modifikátorem static.

### Statická třída

Statická třída je taková třída, jejíž všechny prvky (proměnné i metody) jsou statické.

## Modifikátory přístupu

Účelem modifikátorů přístupu je implementace enkapsulace, která je jedním ze základním principů OOP.

Modifikátory přístupu říkají, které další třídy mohou mít přístup k proměnné nebo metodě.

- *private* - pouze aktuální třída má přístup k proměnné či metodě
- *protected* - pouze aktuální třída a její potomci mají přístup k proměnné či metodě
- *public* - jakákoliv třída má přístup k proměnné či metodě. (Pokud není metoda statická, je třeba stále mít referenci na instanci třídy, ve které se tato public metoda nachází)

## 4 základní principy OOP

Aby jazyk vyhovoval objektově orientovanému paradigmatu, musí umožňovat dosažení 4 následujících věcí

- Enkapsulace
- Dědičnost (Viz okruh [2.4.5](https://atlas144.codeberg.page/szz/ikt/oop/dedicnost-a-polymorfismus.html))
- Polymorfismus (Viz okruh [2.4.5](https://atlas144.codeberg.page/szz/ikt/oop/dedicnost-a-polymorfismus.html))
- Abstrakce (Viz okruh [2.4.6](https://atlas144.codeberg.page/szz/ikt/oop/abstraktni-tridy,-interfejsy-a-jejich-implementace.html))

### Enkapsulace 

Zapouzdření kombinuje data a metody do jednoho celku. Hlavním cílem zapouzdření je zabezpečení dat.
Dosažení zapouzdření je možné pomocí modifikátorů přístupu **public, protected a private**. Mění viditelnost dat nebo metod. Soukromí členové jsou přístupní pouze v rámci třídy, zatímco veřejní členové jsou přístupní v rámci třídy a jiných tříd.

Zapouzdření v jazyce OOP je možné implementovat tak, že se členové dat nebo vlastnosti zpřístupní prostřednictvím veřejných metod, jako jsou gettry a settry. Podobně **enkapsulace** komprimuje data a metodu do jedné jednotky. Navíc poskytuje ochranu dat.

V jednoduchosti: **Metody** i **data** jsou zapouzdřeny ve **třídě**.  **Data** mají modifikátor přístupu private. K přístupu k **datům** (získání či nastavení hodnoty) se využívají speciální metody. Gettry a settry. Tyto metody mají modifikátor přístupu **public**.

## Citace 
- Objektově orientované programovaní [online] ©2015 [cit. 3.12.2022] ČVUT Jan Faigl. Dostupné z: <https://cw.fel.cvut.cz/old/_media/courses/a0b36pr1/lectures/lecture07-handout.pdf>
- Methods (OOP) Brilliant.org [online] ©2022 [cit. 3.12.2022]  Brilliant.org. Dostupné z : 
<https://brilliant.org/wiki/methods-oop/>
- Objektově orientované programování. [online] ©2004 [cit. 3.12.2022] František Dařena. Dostupné z:  <https://akela.mendelu.cz/~darena/Perl/objekty.html>
