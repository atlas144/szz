# Syntaxe a sémantika programovacího jazyka
 
 > Pojem proměnná, její deklarace a inicializace. Rozsah platnosti proměnných (lokální, statické). Syntaktická kategorie příkaz a výraz v programovacím jazyce

 ## Syntaxe vs Sématika

 - **Syntaxe** je správnost gramatiky -> správnost napsaných slov, v programovacím jazyce **příkazů** a **výrazů**. Syntaxe se kontroluje [syntaktickým analyzátorem](https://atlas144.codeberg.page/szz/tzi/syntakticka-analyza-a-ll-gramatiky.html).

 - **Sématika** je význam jednotlivých slov -> v programovacím jazyce význam **příkazů** nebo **výrazů**.


 ## Proměnná

 Proměnná je **odkaz na hodnotu**. Tato hodnota je uložená v paměti. Skrze proměnnou k této hodnotě přistupujeme.

 V programování můžeme zápis proměnné rozdělit na **deklaraci** a **inicializaci**.

 ### Deklarace

 Jedná se o **vytvoření odkazu do paměti**, kde paměť ještě nenese žádnou hodnotu. **Deklarace vytvoří proměnnou**, ale **hodnotu** této proměnné přiřadíme až pomocí **inicializace**.

 ```java
int number; // Deklarace primitivní proměnné typu int
Car car; // Deklarace objektové proměnné typu Car
```

### Inicializace

**Inicializací přiřadíme** do paměťového místa, na které proměnná odkazuje, **hodnotu**. Tedy, přiřazujeme proměnné hodnotu. Můžeme to udělat ihned při **deklaraci**, nebo až později.

 ```java
int number = 5; // Deklarace s inicializací
Car car; // Deklarace objektové proměnné typu Car
car = new Car(); // inicializace po deklaraci
```

## Rozsah platnosti proměnných 

Rozsah platnosti nám určuje, **v jakých částech kódu můžeme k proměnné přistupovat vzhledem k tomu, kde byla vytvořena**

Obecně platí, že pokud **deklarujeme** proměnnou v určitém bloků kódu, tak lze k této proměnné přistupovat pouze v tomto bloku a ve vnořených blocích.


 ```java
while (a > 6){
    int value = a;
}

System.out.println(value); // Tady k proměnné již přistupit nelze. Není zde vidět. Paměť, která se vztahovala jak k referenci (jméno proměnná), tak k inicializované hodnotě již byla uvolměna.
```

## Lokální proměnná 

Jedná se o **proměnnou, která je deklarována v rámci určitého bloku, funkce**. Je tedy lokální, stejně jako na vesničce (bloku) je lokální jednota. Lidi ve vesničce o ni ví, ale ve vesnici vedle, nedej bože městě už nikdo netuší, že tam něco takového je.

Lokální proměnná je příklad výše. Je nepřístupná vně bloku. Lokální proměnná se značí **private**.

## Statická proměnná

Tato proměnná **náleží třídě**. To znamená, že nezávisle na instanci třídy (objektu té samé třídy) se jedná o přístup do **stejné paměti**.


 ```java
class People(){
    public static int peopleCount = 0;

    public People(){
        peopleCount++;
    }


}
```

 ```java

class main{

    public static void main[String[] args]{
        People somePeople = new People(); 
        System.out.println(somePeople.peopleCount) // výsledek bude 1

       People someOtherPeople = new People(); 
        System.out.println(somePeople.peopleCount) // výsledek bude 2

    }

}
```
## Proměnné třídy

Proměnná třídy může být několika typů :

- **private** je viditelná pouze v rámci třídy (classA.variable **nefunguje**) -> potřebujeme **getter**<br>
 ```java
class ClassA(){
    private int variable = 5; // privátní proměnná

    public int getVariable(){ // public getter, který nám dá hodnotu privátní proměnné
        return variable; 
    }


}
```
- **public** (viditelná i mimo třídu (classA.variable **funguje**))
- **protected** (chová se jako private, ale pro **potomky třídy** se chová jako **public**)

## Syntaktická kategorie příkaz a výraz (příkaz vs výraz)

**Příkaz (Statement)**  vyjadřuje činnost, která má být provedena. **Příkazy** obsahují **klíčová slova** (while,if, new, return....) a určují vzhled programovacího jazyka **Klíčová slova** nelze použít jako **názvy proměnných**. Příkazy nám jsou také schopné vrátit hodnotu.

**Výraz (Expression)** na rozdíl od příkazu slouží k **vyhodnocení**. Jeho vedlejším účinkem, výstupem je **true** nebo **false**. Není nám schopný vrátit hodnotu a používá se k vyhodnocení, ergo **vyhodnotí se na určitou hodnotu**.

Např.: \\(2+5\\), \\(y-6\\), \\(4\neq4\\) se vyhodnotí jako NEPRAVDA

## Zdroje

- Příkaz (programování)  **Wikipedia : The Free Encyclopedia** [online] @2023 [citováno 18.05.2023] Dostupné z: <https://cs.wikipedia.org/wiki/P%C5%99%C3%ADkaz_(programov%C3%A1n%C3%AD)>
- Příkaz (programování)  **Wikipedia : The Free Encyclopedia** [online] @2023 [citováno 18.05.2023] Dostupné z: <https://cs.wikipedia.org/wiki/V%C3%BDraz_(programov%C3%A1n%C3%AD)>