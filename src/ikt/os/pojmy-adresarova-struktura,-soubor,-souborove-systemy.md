# Pojmy adresářová struktura, soubor, souborové systémy

> Pojmy adresářová struktura, soubor, souborové systémy. Metody plánování disku. Ochrana souborů a adresářů v reálných OS.

## Adresářová struktura

Je organizační struktura souborového systému. Adresáře slouží k seskupování souborů a další adresářů.
Z pohledu OS je adresář jen speciální soubor, který obsahuje informace o adresářích a souborech v něm obsažených. Nejvyšší adresář se nazývá **kořen (root)** (v UNIXech `/`, u Windows má každý oddíl vlastní root, např. `C:\`).

<div class="note">

### Složka vs adresář

- **adresář** (directory)
  - označuje skutečný prvek souborového systému
  - např. `/bin/bash`
- **složka** (folder)
  - abstrakce používaná v grafickém rozhraní
  - nemusí označovat jen *adresář*, ale i např. tiskárnu či ovládací panely ve Windows
  - cílem je zjednodušení a unifikace přístupu uživatele k různým prvkům  

Zjednodušeně, *adresář* se tomu říká z pohledu FS, *složka* z pohledu grafického rozhraní.

</div>

### Druhy adresářových struktur

- **jednoúrovňová**
  - existuje pouze adresář root a vše jev něm
  - nejstarší metoda
  - příklad OS: *CP/M*
- **dvouúrovňová**
  - v rootu jsou adresáře, ale ty už mohou obsahovat jen soubory
  - příklad OS: *RSX*
- **stromová**
  - v každém adresáři mohou byt další soubory i adresáře
  - příklad OS: *FAT*
- **acyklická**
  - organizace je rovněž stromová
  - soubory a adresáře mohou být ve více adresářích (pomocí odkazů (linků))
  - nesmí dojít k cyklení (dva adresáře se nemohou odkazovat navzájem)
  - nejpoužívanější
  - příklad OS: *NTFS*, *Ext*
- **cyklická**
  - může obsahovat cykly

## Soubor

Soubor je základní jednotkou pro ukládání dat v rámci souborového systému. Jedná se o označenou (pojmenovanou) množinu dat uloženou na paměťovém médiu, se kterou může OS pracovat. Formát dat v souboru je definován **typem souboru**. Ten lze rozpoznat následujícími způsoby:

- **podle hlavičky**
  - prvních několik bytů souboru
  - je to jednoznačné
  - trochu pomalejší (musí se projít tabulka známých formátů)
  - používá se UNIXových systémech
- **podle přípony**
  - např `.jpg`
  - přípona není jednoznačná, lze ji jednoduše změnit a BFU poté soubor neotevře
  - používá se ve Windows
- **podle metadat**
  - ta jsou uložena mimo soubor
  - spravuje to souborový systém
  - při přenosu mezi systémy se musí konvertovat
  - existují různé varianty, nejběžnější je **MIME type**, který se používá hlavně na internetu (např. `application/json`, `text/html`)

Soubory můžeme kategorizovat podle různých hledisek, např.:

- **kolik souborů ve skutečnosti obsahuje**
  - *jednoduchý* - jen jeden soubor (obyčejný textový dokument, obrázek, atd.)
  - *složený* - obsahuje více souborů (archivy (`.zip`, `.tar.gz`), multimédia (video + titulky), atd.)
- **podle typu obsahu**
  - *textový* - prostý text, HTML, MD, atd.
  - *binární* - aplikace, obrázky, audio, atd.
- **podle účelu z pohledu OS**
  - *standardní* - klasické soubory
  - *adresáře* - viz [výše](#adresářová-struktura)
  - *simulované* - reprezentují I/O zařízení
  - *odkládací* - SWAP
- **podle přístupu k médiu**
  - *přímý* - můžeme přistoupit na kterýkoliv soubor kdykoliv (např. pevný disk, CD)
  - *sekvenční* - musíme projet celé médium od aktuálního místa až na požadované (např. magnetická páska)
  - *indexový* - lze přecházet přímo na vybraná místa - indexy (např. magnetická páska s přeskakováním)

OS zajišťuje nad souborem následující operace:

- vytvoření
- čtení
- zápis
- vyhledání/změna místa v souboru (odkud čteme, kam zapisujeme)
- smazání

### Fragmentace

Podobně jako u primární paměti, dochází k fragmentaci i u sekundární. Máme opět dva druhy:

- **vnitřní** - malé soubory nezabírají celý cluster, ve kterém potom zbývá nevyužité místo
- **vnější** - jednotlivé clustery, ze kterých se soubor skládá jsou rozmístěny po celém disku (jejich čtení trvá dlouho)


## Souborové systémy

Souborový systém je **způsob organizace dat** na paměťovém médiu. Určuje jejich strukturu a další vlastnosti, které ovlivňují to, jak se s nimi pracuje.

Jednou z důležitých vlastností souborových systémů jsou *způsoby zápisu dat na úložiště*. Ty jsou následující:

- **okamžitý zápis**
  - zápis probíhá ihned po zadání požadavku
  - v daném okamžiku probíhá jen jedna operace zápisu
  - nedojde ke ztrátě dat, ale je to *pomalé*
  - např. **FAT**
- **opožděný zápis**
  - data se nejprve zapisují do *cache paměti* a poté postupně na trvalé místo (až když není systém zaneprázdněn)
  - *rychlejší*, ale při výpadku může dojít ke ztrátě dat (těch co byla zatím pouze v *cache*)
- **opatrný zápis**
  - používají se transakce (jako u DB)
- **žurnálovací systémy**
  - uchovávají se informace (metadata) o provedených operacích
  - při výpadku je možné data obnovit
  - doplňuje *opožděný zápis*
  - tvorba žurnálu zpomaluje souborový systém (cca o 30%)
  - např. **EXT4**, **NTFS**

Související pojmy:

- **RAID** (Redundant Arrays of Inexpensive/Independent Disks)
  - použití více disků dohromady
  - verze (jen ty nejpoužívanější):
    - **RAID 0** (stripping) - zápis na více disků najednou - vyšší rychlost
    - **RAID 1** (mirroring) - stejná data na více discích - vyšší bezpečnost
    - **RAID 5** - data se zapisují na více disků a jeden se používá k ukládání *kontrolních součtů* (přijdeme-li o nějaký disk, jeho obsah se po náhradě dopočítá z kontrolních součtů) - vyšší rychlost i bezpečnost
    - **RAID 6** - pro kontrolní součty se používá více disků (každý může mít jinou metodu tvorby kontrolních součtů)
  - můžeme mít HW (dedikované HW zařízení v PC - je rychlejší) a SW (stará se o něj OS - v případě problému s OS můžeme přijít o data) RAID
- **síťové souborové systémy**
  - spojování více úložišť z fyzicky oddělených PC do jednoho FS (něco jako RAID)
  - *Ceph*, *GlusterFS*
- **VFS** (Virtual File System) - definice operací, které musí zvládnout každý souborový systém bez ohledu na skutečné HW umístění (nezáleží na tom, na jakém zařízení se nachází); cílem je **uniformita**
- **LVM** (Logical Volume Manager) - kombinování fyzických disků pomocí logických (které potom můžeme upravovat podle potřeby)

### FAT

- nejstarší dosud používaný systém (od *MS-DOS*)
- na začátku disku se nachází FAT (File Allocation Table), ve které se nacházejí informace o souborech
- FAT se snadno prohledává → systém je velmi **rychlý**
- pokud dojde k poškození FAT, znamená to ztrátu všech dat (ve skutečnosti je lze obnovit pomocí různých nástrojů); aby se tomu předešlo, obsahují nové verze FAT tabulky dvě, na různých místech disku
- **nepoužívá žurnálování**
- používá se tam, kde je potřeba spíše rychlost než spolehlivost → *paměťové karty*, *flash disky*
- verze (chronologicky): **FAT12**, **FAT16**, **VFAT** (Virtual FAT), **FAT32**, **exFAT** (Extended FAT)
- omezení:
  - v *rootu* (kořenovém adresáři) může být *maximálně 255 souborů*
  - maximální velikost souboru je \\( 2^n \\) bytů, kde \\( n \\) odpovídá číslu v názvu verze, tedy např. `32` pro **FAT32**
  - maximální velikost oddílu (ve Windows) 32 GB
  - původně maximální délka názvu souboru 8 znaků + 3 přípona (s VFAT došlo k prodloužení)
  - používá stromovou strukturu, kvůli které nepodporuje odkazy
  - neumožňuje přidávat k souborům a adresářům přístupová práva (pouze lze soubor označit jako *read-only*)

### NTFS

- *New Technology File System*
- poprvé u Windows NT (dnes prakticky na všech strojích s Windows)
- proprietární
- používá **žurnálování**
- názvy používají *Unicode* (lze použít jiné znaky než ASCII)
- umožňuje nastavení práv
- má indexování souborů (= rychlé vyhledávání)
- umožňuje *odkazy* (i pevné (vydrží i po přemístění původního souboru))
- lze jej *šifrovat*
- efektivně ukládá soubory, které obsahují převážně `0` (*řídké soubory*)
- vyhledává, opravuje a případně blokuje vadné sektory za běhu

### EXT (Extended)

- používá se v UNIXových systémech
- disk se dělí na bloky (o velikosti 512, 1024, 2048 nebo 4096 B)
  - první je *boot block* (na systémovém disku obsahuje zavaděč, jinde je nevyužit)
  - další se dělí do skupin, z nichž každá obsahuje *superblock* s informacemi o celém disku (při poškození superblocku se může použít jiný)
- podporuje odkazy
  - **soft link** - odkazuje na místo (jako zástupce na Windows)
  - **hard link**
    - odkazuje přímo na soubor
    - cíl ten nelze smazat, dokud existuje hard link
    - nesmí se zacyklit
    - nemůže odkazovat na jiný disky a sám na sebe
- informace o souboru jsou obsaženy v tzv. *i-node* (uzlech)
- volný prostor je evidován v řetězovém seznamu
- verze:
  - **Ext** - již se nepoužívá
  - **Ext2**
    - nemá žurnálování
    - používá se u zaváděcích oddílů UNIXových systémů (lze i tam, kde FAT)
    - maximální velikost souboru je 12 TB
    - maximální velikost oddílu je 16 TB
  - **Ext3** - již se nepoužívá (je jako Ext4, ale má více omezení velikosti)
  - **Ext4**
    - používá se jako hlavní FS pro Linux
    - maximální velikost souboru je 16 TB
    - maximální velikost oddílu je 1 EB

### XFS

- od IBM
- je 64 bitový
- používá se v rozsáhlých UNIXových úložištích
- žurnálují se jen metadata (rychlejší a méně bezpečné než klasické žurnálování souborů)

### ZFS

- *Zetabyte File System*
- původně od SUN (pro OS Solaris)
- licence blokuje použití v Linuxu
- je 128 bitový
- umí automaticky opravovat chyby

### btrfs

- *B-tree File System*
- podobný ZFS
- vznikl kvůli licenčním problémům ZFS
- obsah adresářů má strukturu *B-stromů*
- začíná být v Linuxu populární (např. Fedora jej využívá jako hlavní FS)

### APFS

- *Apple File System*
- pro MacOS
- nahradil starší HFS

### ISO 9660

- pro CD, DVD a BR
- délka souboru maximálně 32 znaků
- maximální hloubka adresářové struktury je 7 úrovní
- existují rozšíření:
  - **Joliet** - dlouhá jména pro Windows
  - **Rock Ridge** - totéž pro UNIXy
  - **El Torito** - umožňuje boot

## Metody plánování disku

Je činnost, která nám říká, jakým způsobem dochází k vyřizování požadavků na čtení a zápis, aby byly minimalizovány zbytečné pohyby čtecí hlavy, otáčení ploten a prodleva mezi požadavky (týká se tedy jen HDD).

Metody:

- **FCFS** (First-Come First-Served)
  - požadavky jsou vyřizovány v pořadí, v jakém došly (tedy FIFO)
  - může docházet k větším prodlevám, pokud jsou požadované oblasti daleko od sebe
- **SSTF** (Shortest-Seek-Time-First)
  - nejprve se vyřídí ten požadavek, který vyvolá nejmenší posun hlavy
  - hrozí zastarání vzdálenějších požadavků
- **SCAN plánování**
  - hlava projíždí disk tam a zpět z jednoho konce na druhy a cestou vyřizuje požadavky (tzv. *Elevator algoritmus*)
- **C-SCAN**
  - ještě lepší než *SCAN*
  - hlava se po dojeti na konec disku vrátí na začátek, jako by začátek navazoval na konec
  - *možné vylepšení:* pokud už dále není žádný požadavek, vrací se rovnou (bez dojetí na konec), a to na místo prvního požadavku (ne na začátek)

## Ochrana souborů a adresářů v reálných OS

### Linux

- systém oprávnění z UNIXu
- neřeší se dědění (kromě práva vstupu do adresáře)
- velmi jednoduché
- každý soubor (adresář) má tři kategorie oprávnění:
  - **vlastník**
  - **skupina**
  - **ostatní**
- každá z těchto kategorií se skládá z následujících práv (každému z práv náleží jedno oktální číslo):
  - **čtení - r (4)**
  - **zápis - w (2)**
  - **spuštění souboru/vstup do adresáře - x (1)**
- celková oprávnění pro každý soubor (adresář) se skládají ze *tří oktálních čísel*, která odpovídají součtu *práv* pro každou *kategorii*
- *příkladem může být:* `764`, což znamená, že **vlastník může vše (4 + 2 + 1)**, **příslušník skupiny** souboru může **číst a zapisovat (4 + 2)** a **ostatní** mohou jen **číst (4)**
- často se používá rozložený zápis (např. při použití `ls -l`), který pro předešlý příklad vypadá takto: `-rwxrw-r--` (první pomlčka neznamená oprávnění, ale **typ souboru**: `-` obyčejný soubor, `d` adresář, `l` linka, `b` blokové zařízení, `c` znakové zařízení, `p` roura, `s` soket)
- pokud uživatel spadá do více kategorií, bere se v potaz vždy ta nejkonkrétnější kategorie, tedy ta nejvíce vlevo (v oktálních číslech)
- vždy je nutné, aby uživatel měl právo na vstup ke všem nadřazeným adresářům
- k editaci oprávnění se používá příkaz `chmod`, např.: `sudo chmod a+x soubor.sh` přidá (`+`) právo spouštět soubor (`x`) všem třem kategoriím (`a`)

### Windows

- oprávnění lze přidělit nejen souborům a adresářům, ale i objektům registrů, systémovým objektům nebo Active Directory objektům
- *oprávnění lze přidělit:*
  - skupinám, uživatelům a dalším objektům s identifikátory zabezpečení v doméně
  - skupinám a uživatelům v této doméně a ve všech důvěryhodných doménách
  - místním skupinám a uživatelům (v počítači, kde se objekt nachází)
- *lze přidělit tato oprávnění:*
  - **full control** - umožňuje dělat prakticky vše (včetně úpravy oprávnění a přebírání vlastnictví)
  - **modify** - umožňuje všechny úpravy a samozřejmě čtení (vytvářet, editovat a mazat soubory, vytvářet a mazat adresáře)
  - **read & execute** - čtení (jak dat souborů, tak obsahu adresářů) a spouštění aplikací
  - **list folder contents** - čtení obsahu adresářů a podadresářů (ne souborů)
  - **read** - jako *list folder contents* + obsah souborů
  - **write**	- jako *read* + tvorba adresářů a souborů a mazání souborů
- **vlastník** souboru (adresáře) může bez ohledu na oprávnění vše
- je možno oprávnění **dědit**, tedy soubory a podadresáře dostávají oprávnění od svého předka

### macOS

- používá UNIXová oprávnění, stejně jako Linux

### ACL

- *Access Control List*
- tabulka se záznamy o oprávněních ke každému souboru (adresáři)
- umožňuje jemnější nastavení oprávnění (každý záznam v ACL se váže ke konkrétnímu uživateli/skupině)
- lze je použít jak ve Windows, tak UNIXových systémech (nejsem si jistý, ale Windows je možná užívají ve výchozím stavu - nebo je jejich model dost podobný)

## Zdroje

- PECH, Jíří. *Operační systémy: Elektronická skripta*. České Budějovice, 2012. ISBN 978-80-7394-384-4.
- PECH, Jiří. *Operační systémy 1 - Souborové systémy*. České Budějovice.
- Přispěvatelé Wikipedie, *File Allocation Table* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 2. 02. 2023, 15:38 UTC, [citováno 8. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=File_Allocation_Table&oldid=22404578>
- Přispěvatelé Wikipedie, *Btrfs* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 2. 08. 2022, 14:42 UTC, [citováno 8. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Btrfs&oldid=21550356>
- Přispěvatelé Wikipedie, *Soubor* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 9. 11. 2022, 10:08 UTC, [citováno 9. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Soubor&oldid=21860541>
- Přispěvatelé Wikipedie, *Formát souboru* [online], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 14. 05. 2021, 01:58 UTC, [citováno 9. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Form%C3%A1t_souboru&oldid=19844688>
- Přispěvatelé Wikipedie, *Adresář (informatika)* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 9. 11. 2022, 09:44 UTC, [citováno 9. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Adres%C3%A1%C5%99_(informatika)&oldid=21860455>
- <https://www.pslib.cz/milan.kerslager/P%C5%99%C3%ADstupov%C3%A1_pr%C3%A1va_v_Linuxu>
- <https://learn.microsoft.com/en-us/windows/security/identity-protection/access-control/access-control>
- <https://www.uwec.edu/kb/article/drives-establishing-windows-file-and-folder-level-permissions/>
- <https://www.imperva.com/learn/data-security/access-control-list-acl/>
- Přispěvatelé Wikipedie, *Access Control List* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 7. 02. 2022, 07:57 UTC, [citováno 9. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Access_Control_List&oldid=20912840>