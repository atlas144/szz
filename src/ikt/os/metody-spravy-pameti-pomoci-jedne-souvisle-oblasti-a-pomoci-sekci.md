# Metody správy paměti pomocí jedné souvislé oblasti a pomocí sekcí

> Metody správy paměti pomocí jedné souvislé oblasti a pomocí sekcí. Pojmy vnitřní a vnější fragmentace paměti.

Mluvíme-li o správě paměti, máme na mysli **operační paměť**, tedy paměť využívanou procesorem při zpracování instrukcí a dat.

Modul správy paměti má na starost:

- sledování stavu každého místa operační paměti
- určování strategie přidělování paměti
- přidělování paměti úlohám
- uvolňování paměti od ukončených úloh
- správa virtuální paměti

## Přidělování jedné souvislé sekce paměti

Jedná se o nejstarší, ale stále používanou metodu přidělování paměti. Stejně jako u všech dalších metod zde obdrží svou část paměti OS. Zbytek paměti se buď celý, nebo jeho část přidělí právě běžící úloze. Operační systém má tedy na starost jen *přidělení paměti úloze*, *kontrolu, zda úloha nezasahuje do paměti OS* a *uvolnění paměti po dokončení úlohy*.

Je zřejmé, že tato metoda nemůže být použita v systémech vyžadujících multitasking (paměť může být přidělena vždy jen jedné úloze a nebylo by možné mezi úlohami přepínat). Uplatnění však nachází u jednoduchých mikrokontrolérů (používaných např. k jednoduchému řízení spotřebičů), u kterých je vždy vykonávána najednou jen jedna úloha, jako např. ATmega328 (Arduino UNO).

|![Přidělování jedné souvislé sekce paměti](metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci/jedna_souvisla_sekce.png "Přidělování jedné souvislé sekce paměti")|
|:--:|
|*obr. 1:* Přidělování jedné souvislé sekce paměti|
| *zdroj:* PECH, Jiří. *Operační systémy 1 - Přidělování paměti*. České Budějovice. |

### Výhody

- jednoduchost řešení
- rychlost
- moznost pracovat i s malou pamětí

### Nevýhody

- není využita celá paměť
- když úloha čeká, není využita ani paměť ani procesor
- úlohu nelze vykonat, požaduje-li více paměti, než je k dispozici (neexistuje zde [virtuální paměť](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html))

## Přidělování paměti po sekcích

Problém absence multitaskingu u předešlé metody se snaží řešit *přidělování paměti po sekcích*. Zde je paměť rozdělena do sekcí a každá z nich náleží jedné úloze. Tyto sekce mohou být:

- všechny stejně velké - **statické přidělování**
- různě velké (podle potřeb úlohy) - **dynamické přidělování**

Operační systém tedy musí krom výše zmíněných úkolů ještě *kontrolovat, odkud kam sahá přidělený prostor úloh* (při dalším přidělování) a hlídat, aby si úlohy *neovlivňovaly přidělený prostor navzájem*.

Tato metoda pochází z 80. let a dnes se v praxi **nevyužívá**.

|![Přidělování paměti po sekcích](metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci/po_sekcich.png "Přidělování paměti po sekcích")|
|:--:|
|*obr. 2:* Přidělování paměti po sekcích|
| *zdroj:* PECH, Jiří. *Operační systémy 1 - Přidělování paměti*. České Budějovice. |

### Výhody

- umožňuje multitasking
- jednoduchá implementace

### Nevýhody

- z pohledu výkonu poměrně náročná
- stále zde neexistuje [virtuální paměť](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html))
- dochází navíc k *vnitřní* a *vnější fragmentaci*

## Vnitřní fragmentace

K vnitřní fragmentaci dochází uvnitř alokované oblasti paměti. U *statické alokace* k ní dochází proto, že každá úloha obdrží stejně velký blok, bez ohledu na to, jestli tolik paměti skutečně potřebuje. U *dynamické alokace* je důvodem to, že úlohy mají tendenci žádat pro jistotu o větší množství paměti, než reálně potřebují (u *přidělování paměti po sekcích* totiž není možnost dodatečně přidělit více paměti).

|![Vnitřní fragmentace](metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci/vnitrni_fragmentace.png "Vnitřní fragmentace")|
|:--:|
|*obr. 3:* Vnitřní fragmentace|
| *zdroj:* PECH, Jiří. *Operační systémy 1 - Přidělování paměti*. České Budějovice. |

## Vnější fragmentace

K vnější fragmentaci dochází mimo alokovanou oblast paměti. Spočívá v tom, že ač se zdá, že je v systému dostatek paměti pro přidělení nové úloze, správce paměti ji není schopen přidělit, protože tato paměť je rozdělena na více místech a alokace pro novou úlohu se nevejde do žádného z nich.

Nastává, pokud na více místech dojde k uvolnění oblasti paměti (při ukončení úlohy), která se nachází mezi dvěma jinými alokovanými bloky. Řešením je využití metody **[stránkování](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html)** (případně dynamické přemisťování sekcí, to je ale technicky a hlavně časově náročné a a také nebezpečné - jsou-li úlohy špatně naprogramovány, mohou se po přemístění snažit přistupovat k paměti na původním místě).

|![Vnější fragmentace](metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci/vnejsi_fragmentace.png "Vnější fragmentace")|
|:--:|
|*obr. 4:* Vnější fragmentace|
| *zdroj:* PECH, Jiří. *Operační systémy 1 - Přidělování paměti*. České Budějovice. |

## Zdroje

- PECH, Jiří. *Operační systémy 1 - Přidělování paměti*. České Budějovice.