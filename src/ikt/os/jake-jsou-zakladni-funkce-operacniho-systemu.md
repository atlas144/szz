# Jaké jsou základní funkce operačního systému?

> Popište stručně zavedení operačního sytému u PC. Jak se dělí operační systémy? Uveďte příklady. Pojmy multitasking, monotasking, multiuser, monouser, real time.

## Definice OS a jeho zavedení

OS je **soubor programových modulů**, ve výpočetním zařízení ovládajících řízení prostředků, jimiž je tento výpočetní systém vybaven (procesory, paměti, I/O zařízení a soubory dat). Tyto programové moduly rozhodují spory, dále se snaží optimalizovat výkon, zjednodušit a zefektivnit využívání výpočetního systému.

### POST

Před zavedením OS (tedy přímo po spuštění) provádí PC tzv. **Power-On Self Test**, což je kontrola funkčnosti HW (a dalších esenciální věcí, jako např. integrita BIOS firmwaru). Pokud chybí některá z komponent nebo pokud nefunguje správně, POST to detekuje a pomocí **beep codes** (série pípnutí) to oznámí uživateli a počítač vypne. 

### Zavedení OS

1. z **BIOS** (Basic Input Output System) - programovatelné paměť nebo **UEFI** (Unified Extensible Firmware Interface) se zjistí medium, na kterém se OS nachází
2. na tomto mediu se na **MBR** (Master Boot Record) načte OS nebo zavaděč OS
    - u Linuxů je zavaděčem GRUB a nebo LILO
    - jádro **/boot/vmlinuz** a RAM disk **/boot/initrd** se zavedou do paměti a převezmou řízení PC

<div class = "note">

### Master boot record

**MBR** je hlavním spouštěcím záznamem umístěným na pevném disku (na jeho počátku - v prvním sektoru). Velikost **MBR** je 512 bajtů a obsahuje:

- **zavaděč OS**, kterému BIOS předá řízení PC
  - krátký úsek kódu, který je BIOSem zavaden do paměti a spuštěn; úkolem tohoto kódu je načíst do paměti bootovací sektor (v tabulce označen jako aktivní) a spustit ho
- **tabulku rozdělení disku** na logické oddíly
  - tabulka obsahuje 4 záznamy včetně informací o zaváděcích sektorech (boot sektory)
- **číselný identifikátor disku**

**MBR** dokáže adresovat maximálně 2 TB disky. 

</div>

#### Bootovaní Red Hat Linux

1. provede se **POST**
2. z **BIOS** ze zjistí bootovací disk/medium
3. z **MBR** se zavede GRUB a načte se jeho konfigurační soubor `(/etc/grub/grub.conf`)
4. zavede se **jádro** (`/boot/kernel`)
5. zavede se počáteční RAMka (`/boot/initramfs`) obsahující potřebné ovladače a následně se připojí kořenový disk systému
6. spustí se startovací scripty a (`/etc/init.d`) a zavedou potřebné moduly (`/lib/modules`)
7. nastaví se síť, připojí se souborové systémy a připraví se login uživatele

## Základní funkce OS

1. správa a přidělování paměti ([zde](https://atlas144.codeberg.page/szz/ikt/os/metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci.html) a [zde](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html))
2. správa procesů - [synchronizace](https://atlas144.codeberg.page/szz/ikt/os/vysvetlete-pojmy-synchronizace-procesu-a-kriticka-sekce.html)
3. přidělování procesoru - [multitasking](https://atlas144.codeberg.page/szz/ikt/os/stavovy-diagram-procesu.html)
4. řešení [Deadlocku](https://atlas144.codeberg.page/szz/ikt/os/deadlock.html)
5. [správa systému souborů](https://atlas144.codeberg.page/szz/ikt/os/pojmy-adresarova-struktura,-soubor,-souborove-systemy.html) (virtuální souborový systém)
6. správa uživatelů

## Dělení OS

OS můžeme dělit dle různých kritérií:

1. **Podle správy uživatelů**
   - monouser (DOS)
   - multiuser (UNIX, Windows NT 4.O Terminal Server, MacOS, nebo např. [distribuovaný operační systém](https://en.wikipedia.org/wiki/Distributed_operating_system))
     - více uživatelů pracuje naráz v reálném čase
     - OS musí zajistit rozdělení a oddělení prostředků mezi uživatele
     - OS také dokáže uživatele udržet v bezpečí před vlivy jiných uživatelů (umí je vzájemně oddělit a kontrolovat)
     - existuje zde entita superuživatele (root, administrátor)
2. **Podle počtu spuštěných programů**
   - monotasking (běží vždy jen jeden program a po jeho ukončení může běžet další; DOS)
   - multitasking
3. **Způsoby užití (výkon a odezva)**
   - Real-Time (v přesných, deterministických systémech)
   - Handheld systémy (tablety, mobilní telefony)
     - požadavek na energetickou úsporu
     - zde není kladen vysoký nárok na výpočetní výkon
     - menší display, omezená paměť

<div class ="note">

### Distribuovaný OS

U **distribuovaného OS** běží software nad sítí propojených zařízení (uzlů). Každý uzel poté běží pod subsystémem globálního OS a HW daného uzlu je řízen tzv. [microkernelem](https://en.wikipedia.org/wiki/Microkernel). Uzly mezi sebou komunikují a kooperují pod správou speciálních řídících komponent, přičemž celá síť uzlů se navenek chová jako jedno výpočetní zařízení

</div>

### Real time OS

Více v otázce [3.3 - Architektura OS ve VS](https://atlas144.codeberg.page/szz/vs/architektura-operacnich-systemu-uzivanych-ve-vestavnych-systemech.html) a otázce [3.9 Řízení periferií](https://atlas144.codeberg.page/szz/vs/architektura-softwaru-rizeni-siti-periferii.html). 

V kostce se jedná o OS, které musí pracovat v reálném čase, tedy reagovat na vstupy do přesně stanoveného času (letový provoz, elektrárny, zdravotnictví). Tyto OS se vyznačují tím, že mají většinou jen malé jádro, které ovládá jen základní funkce a na vše další se volají moduly. 

- FreeRTOS
- QNX
- RTLinux

## Zdroje

- PECH, J. Operační systémy 1: Úvod do teorie operačních systémů. České Budějovice: Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta, 2019.
- <https://cs.wikipedia.org/wiki/Master_boot_record>
- <https://cs.wikipedia.org/wiki/V%C3%ADceu%C5%BEivatelsk%C3%BD_opera%C4%8Dn%C3%AD_syst%C3%A9m>
- <https://cs.wikipedia.org/wiki/Opera%C4%8Dn%C3%AD_syst%C3%A9m#Typy_opera%C4%8Dn%C3%ADch_syst%C3%A9m%C5%AF>
- <https://cw.fel.cvut.cz/b172/_media/courses/a3b33osd/lekce1.pdf>
- Wikipedia contributors, *Power-on self-test* [online], Wikipedia, The Free Encyclopedia; 2023 May 13, 07:33 UTC [cited 2023 May 22]. Available from: <https://en.wikipedia.org/w/index.php?title=Power-on_self-test&oldid=1154554990.>
