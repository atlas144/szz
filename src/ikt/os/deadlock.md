# Deadlock

> ## **Pojmy : Deadlock. Vysvětlete příčiny deadlocku. Jak lze deadlocku zamezit. Jak je řešen deadlock v reálných OS.**
<br>

**Deadlock** je v OS čekání na událost, která nemůže nastat, neboli **situace, kdy úspěšné dokončení první akce je podmíněno předchozím dokončením druhé akce, přičemž druhá akce může být dokončena až po dokončení první akce.**

Příkladem deadlocku mohou být dva procesy, kde každý drží otevřený soubor a
potřebuje zapsat do souboru druhého procesu. Dalším příkladem může být dopravní situace (viz obr. 1).

|<img src="deadlock/deadlock_example.png" width="300"/>|
|:--:|
|*obr. 1:* Příklad Deadlocku (v dopravní situaci)|

## Podmínky vzniku deadlocku

K uváznutí dojde jen při splnění všech následujících podmínek:
- *Vzájemné vyloučení* - každý nesdílitelný zdroj může v jednom okamžiku používat nejvýše jeden proces (aby nedošlo k porušení konzistence dat)
jen jeden proces
- *Drž a čekej* - proces může žádat o další zdroje, i když už má nějaké přiděleny
- *Nepreemptivnost* - jakmile proces zmíněný zdroj vlastní, nelze mu ho odejmout, musí ho sám vrátit
- *Kruhové čekání* - procesy čekající vzájemně na zdroje

**Nesdílitelné zdroje** jsou zdroje, které nemohou sdílet dva procesy najednou (např. tiskárna nebo soubor pro zápis). Sekvence užití zdrojů:
1. Dotaz na dostupnost zdroje - nelze-li dotaz vyplnit
okamžitě, musí proces čekat
2. Užití - proces pracuje se zdrojem
3. Uvolnění zdroje
   
Dotaz a uvolnění jsou kritická sekce. Pokud nemůže
proces dostat zdroj - čeká. Pokud čekají nejméně dva
procesy na zdroj, který drží jiný čekající proces
máme *deadlock*.

## Metody řešení deadlocku

- **Prevence deadlocku**
    - Zrušení některé podmínky deadlocku:
      - Nepoužívat zámky u prostředků umožňujících sdílený
přístup
      - Proces může žádat o další prostředky až tehdy když
všechny své uvolnil
      - Pokud proces požádá o nedostupný prostředek je
pozastaven, jsou mu všechny prostředky odebrány a čeká
se až může všechny prostředky obdržet
      - Prostředky mají stanovené pořadí a Ize je získat pouze v
tomto pořadí
- **Vyhýbání se deadlocku**
    -  Každý proces před začátkem oznámí kolik bude potřebovat
prostředků. Nelze jej spustit pokud existuje možnost, že mu
prostředky nebudou moci být zajištěny <br> (příkladem je tzv. **Bankéřův algoritmus** - bankéř nemůže půjčit, pokud nemůže zajistit požadavky na výběr všech vkladů)
- **Detekce deadlocku a zotavení po uváznutí**
    - Vychází z předpokladu, že je deadlock nepravděpodobný. Systém se snaží cyklicky detekovat deadlock a řešit ho. Možné způsoby jsou:
      - Ukončení procesu (restart procesu)
      - Odebraní zdroje nějakému procesu
- **Ponecháaní řešení deadlocku na lidské obsluze**
    - Postupné ukončování procesu a sledování, zda se systém
rozběhne
    - Restart systému (může dojít ke ztrátě dat a opětovnému deadlocku)  

*Body 1 až 3 jsou složité a náročné na zdroje, proto je běžné operační systémy používají pouze okrajově a většinou nechávají řešení deadlocku na obsluze.*

## Citace

*Wikipedie: Otevřená encyklopedie: Deadlock* [online]. c2022 [citováno 22. 01. 2023]. Dostupný z WWW: <https://cs.wikipedia.org/w/index.php?title=Deadlock&oldid=22253321> 
