# Stavový diagram procesů

>  Jednotlivé stavy. Pojem multitaskingu a jeho druhy.
Správa procesů v reálných OS.

## Proces

**Proces** je **instancí** aplikace/programu uvnitř OS. Uživatel spustí appku a ta si po svém spuštění vytvoří **proces**. 

**Jedná se o základní jednotku práce OS.**

V rámci OS běží současně jednak procesy appek a jednak procesy OS. Každý z procesů si může pustit další procesy, těm následně říkáme **synovské**. Jsou tedy 2 druhy procesů:

- rodičovské
- synovské

Synovský zaniká se zánikem rodičovského procesu.

Každý proces je reprezentován záznamem v paměti - **Process Control Block (PCB)** obsahujícím informace o procesu:

- status procesu
- program counter
- CPU registry
- CPU plánovaní - priorita procesu
- info správy paměti
- info o času přidělení CPU, časový limit, číslo procesu
- I/O stavové informace

|![Stavový diagram procesu](stavovy-diagram-procesu/diagram.PNG "Stavový diagram procesu")|
|:--:|
|obr. 1: Stavový diagram procesu|
| zdroj: PECH, J. *Operační systémy 1: Procesy*. České Budějovice: Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta, 2019.|

### Popis diagramu

1. **Nový** - proces je zaveden a vzniká jeho PCB
2. **Připraven** - proces je v operační paměti
      - OS vybere proces ke zpracování
3. **Probíhající** - pokud jej OS vybral pro zpracování
     - zde se proces nachází na CPU a běží
4. **Ukončen** - proces končí
5. **Čekající** - proces čeká na I/O událost, pro ukončení I/O události se opět proces dostane do stádia čekající (I/O zde může být **čtení z klávesnice**, **čtení z disku**, **tisk na tiskárně**)

Pokud je procesu odejmut čas na CPU, tak se proces **přeruší** a rovněž se vrací do stádia **Připraven**.

### Přidělení CPU

Slouží k tomu dva moduly:

1. **CPU Scheduler** - vybere vhodný proces pro spuštění
2.  **Dispatcher** - zavede proces na CPU
       - **přepnutí kontextu** - uvolnění procesu z CPU a uložení důležitých informací (stav registru, následující instrukce ke zpracování, stack...) a zavedení nového procesu na CPU (zde info o stacku, registrech a následující zpracovávané instrukci nahráváme)

Čas, který má proces na CPU přidělen se nazývá **časové kvantum**.

Metody výběru procesu ke zpracování:

- **FIFO**
- **First Come First Served** - úlohy, které je třeba spravovat opakovaně nemusí dostat potřebný čas na CPU
- **Short Job First** - úlohy s dlouhým během se moc nedostanou k CPU
- **Prioritní fronta** - tasky s nízkou prioritou se nemusí na CPU vůbec dostat; tomuto se zamezuje tím, že po určitém počtu cyklů se zvýší priorita daného procesu, takže se časem na CPU zkrátka dostane

### Priority v Reálných OS

**Linux** - priority od -19 (nejvyšší) do 20 (nejnižší)
  - **Defaultní priorita je 0.** Uživatel může prioritu snížit, ale nemá oprávnění ji navyšovat. 

**Windows** - 0 až 31 (nejvyšší)

<div class = "note">
Systémové procesy mají vždy vyšší prioritu, než procesy uživatelské.
</div>

Priority je možné nastavit **staticky** - tj. při spuštění, nebo **dynamicky**, kdy se mění za běhu.

## Multitasking

Střídání procesů na procesoru a současné spouštění vícera procesů.

Rozlišujeme:
- **Preemptivní** multitasking - zaručuje pravidelné střídání
  - pokud proces neodejde do určité doby do stavu **Přerušen** nebo **Ukončen**, tak je mu CPU odebrán a dojde k přepnutí kontextu 
- **Nepreemptivní** multitasking - nemáme zaručeno pravidelné střídání procesů na CPU
  - **pseudomultitasking** - MS-DOS
    - MD-DOS je sice monotaskový OS, ale například při tisku na tiskárně; uživatel jinak procesu přepínat nemůže
  - **kooperativní multitasking**
    - procesor sám předává proces
    - externí program přepíná appky na CPU (program Finder na MacOS 8 a 9)
    - procesy přepíná sám uživatel

### Procesy a vlákna

V moderních OS se procesy dělí na vlákna a při přidělování vláken mluvíme o přidělování nikoli na CPU, ale na jádro CPU. Rozdíl mezi procesem a vláknem je dobře představitelný na Browseru a jeho záložkách. **Browser je proces** a jeho **záložky jsou jednotlivá vlákna**.

Vlákna se mohou nacházet ve stavu: **připraveno**, **blokováno**, **spuštěno** či **ukončeno**. Dále mohou vytvářet synovská vlákna. 

Vlákna jsou na sobě (na rozdíl od procesu) závislá. Všechna vlákna mají přístup na libovolnou adresu úlohy. Jedno vlákno může číst nebo zapisovat do libovolného zásobníku jiného vlákna téže úlohy. Ochrana paměti mezi vlákny není zapotřebí. Vlákna jsou naprogramována tak, aby si navzájem pomáhala.

### Životní cyklus procesu v reálném OS - Linux

|![Životní cyklus procesu na Linuxu](stavovy-diagram-procesu/linux.PNG "Životní cyklus procesu na Linuxu")|
|:--:|
|obr. 2: Životní cyklus procesu na Linuxu|
| zdroj: PECH, J. *Operační systémy 1: Procesy*. České Budějovice: Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta, 2019.|

<div class="note">
<b>Rozdíly oproti původnímu obrázku:</b><br>

<b>Běh jádra</b> leží zcela mimo cyklus, a to proto, že procesu jádra mají absolutní přednost před všemi ostatními.

Stavy <b>Čeká</b>, <b>Čeká na disku</b> a <b>Připraven na disku</b> jsou stavy, kde je proces odložen na disk (mimo RAM).

<b>Zombie</b> - je-li proces ukončen na CPU, tak se nejprve dostane do stavu ZOMBIE, zde může ještě zůstat, pokud má spuštěny synovské procesy. Z tohoto stavu může být proces již jen ukončen, nemůže se vrátit do žádného jiného stavu.
</div>

## ZDROJE

- PECH, J. *Operační systémy 1: Procesy*. České Budějovice: Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta, 2019.

