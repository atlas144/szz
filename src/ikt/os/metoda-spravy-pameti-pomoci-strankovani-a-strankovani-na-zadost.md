# Metoda správy paměti pomocí stránkování a stránkování na žádost

Důvodem pro využití **stránkování** nebo **stránkování na žádost (virtuální paměť)** je odstranění problému **vnitřní** a **vnější fragmentace** (viz kapitola [2.3.2](https://atlas144.codeberg.page/szz/ikt/os/metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci.html)).

## Stránkování

Máme paměť, do které chceme zavést aplikaci. Tuto paměť rozdělíme na stránky a naši aplikaci také rozdělíme na stránky o stejné velikosti a poté jednotlivé stránky zavedeme do paměti - stránku na stránku. Tato metoda je **hojně užívána**.

### Výhody

- pokud aplikace potřebuje více místa, přidělíme ji další stránku
- odstranění vnitřní fragmentace

### Nevýhody

- musíme udržovat přehled, jaká stránka patří jaké aplikaci
- musíme hlídat hranice jednotlivých stránek
- pomalá metoda
- stále jsme omezeni celkovou velikostí paměti

|![Stránkování](metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost/strankovani.png "Stránkování")|
|:--:|
|*obr. 1:* Stránkování<br>*Dostupné z: <https://elearning.jcu.cz/pluginfile.php/343263/mod_resource/content/0/05-Pridelovani_pameti.pdf>*|

## Virtuální paměť (Stránkování na žádost)

Vezmeme si úložiště, z něho si sebereme kus a použijeme ho jako nástavec pro paměť RAM. Tuto paměť rozdělíme na stránky. Když chceme přistoupit k paměti na disku, přehodíme požadovanou stránku z disku do operační paměti. **Velmi používané v moderních OS**.

### Výhody

- odstranění vnitřní a vnější fragmentace
- odpadá omezení velikostí paměti

### Nevýhody

- přístup k úložišti je pomalejší než přístup k operační paměti (Klidně až 100x pomalejší)
- OS si musí udržovat přehled o volných nebo využitých stránkách
- nelze zavést virtuální stránku, pokud máme plnou operační paměť (musíme vybrat stránku, která půjde pryč z operační paměti)
	- stránka, která byla nejdéle v paměti, jde ven (FIFO) 
	- Nejčastěji se používá hodinový algoritmus. Pokud je stránka používaná, nastaví se stavový bit stránky na `1`. Algoritmus prochází stránky a nastavuje `0`, pokud není používaná. Pokud přijde požadavek na přidělení stránky, algoritmus přidělí první stránku, kde nalezne `0`.
 		- Dnes se používá místo 1 bitu 8 bitů, které se dekrementují o 3. Zabere 3 průchody než se na stránce objeví příznak nuly.

*Virtuální paměť se děli na soubor nebo oddíl.*
 - na disku je vyhrazen prostor, který je vyhrazen pro virtuální paměť

**Oddíl**: Používá se pro Linux nebo MacOS

- lze měnit za běhu
- pokud máme stránku, kterou budeme jen číst, tak se nezavádí do operační paměti
- na MacOS se mění velikost virtuální paměť v závislosti na požadavcích aplikace

**Soubor**: Používá se na Windows
- jedná se o soubor `pagefile.sys` nacházející se v kořenovém adresáři Windows

|![Stránkování na žádost](metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost/virtualni_pamet.png "Stránkování na žádost")|
|:--:|
|*obr. 2* Stránkování na žádost<br>*Dostupné z: <https://elearning.jcu.cz/pluginfile.php/343263/mod_resource/content/0/05-Pridelovani_pameti.pdf>*|

## Segmentace

Segmentace spočívá v tom, že část paměti je rozdělena na jednotlivé části, v jedné části jsou knihovny v druhé data, ve třetí programy apod. Aplikace poté sdílí přístup k jednotlivým částem a datům. **Nepoužívá se**.

### Nevýhody

- aplikace si navzájem mění data v částech, ke kterým přistupovali

## Segmentace se stránkováním

Se segmentací se používá zároveň stránkování. Používalo se ve starších Windows (pro částí, kde je to bezpečné - dynamicky linkované knihovny).

## Citace

- Fragmentace [online] ©2015 [cit 10.12.2022] Jiří Kysela. Dostupné z: <http://fim.uhk.cz/wikiinf/web/index.php/home/6-operani-systemy/409-fragmentace>
- Operační systémy 1 : Přidělování paměti [online] ©2019 [cit 10.12.2022] Jiří Pech. Dostupné z: <https://elearning.jcu.cz/pluginfile.php/343263/mod_resource/content/0/05-Pridelovani_pameti.pdf>
