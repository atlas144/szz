# Linková vrstva

> Linková vrstva MAC, ETHERNET (MAC adresa, rámec); Přepínač (popis a fungování, přepínání, přepínací tabulka, VLAN)

## Účel linkové vrstvy

- poskytuje spojení mezi dvěma sousedními systémy
- uspořádává data z fyzické vrstvy do logických celků – **rámců**
- hlídá integritu (bezchybnost) dat pomocí **kontrolních součtů**

## MAC adresa

Jednoznačně určuje zařízení v **lokálním měřítku** (v konkrétní Ethernetové síti). **Adresa je přiřazena zařízení při jeho výrobě (spousta dnešních systémů ji však umí měnit)**.

### Struktura MAC adresy

**MAC adresa** se skládá z 12 čísel v **hexadecimální soustavě**. Je rozdělena do 6 **oktetů**. Každý **oktet** obsahuje 8 bitů.

První 3 **oktety** se používají jako **unikátní identifikátor výrobce**.  Jsou přiřazeny každé **organizaci nebo prodejci**. Toto přiřazení dělá **Výbor registrační autority IEEE**. Příklady:

- CC:46:D6 - Cisco
- 3C:5A:B4 - Google, Inc.
- 3C:D9:2B - Hewlett Packard
- 00:9A:CD - HUAWEI TECHNOLOGIES CO.,LTD

Zbylé 3 **oktety** jsou využívány výrobci NIC (Network Interface Controller - síťová karta).  Výrobci zde mohou použít jakoukoliv kombinaci znaků, ale prefix (první 3 **oktety**) by měly vždy zůstat takové, jaké bylo přiřazení od IEEE. 

|![Struktura MAC adresy](linkova-vrstva/MAC_address.PNG "Struktura MAC adresy")|
|:--:|
|*obr. 1:* Struktura MAC adresy|
|*Dostupné z:* <https://www.javatpoint.com/what-is-mac-address>|

## Rámec

Jedná se o základní stavební blok síťové komunikace v rámci určité LAN. Pokud zařízení A komunikuje se zařízením B, tak je tato komunikace vedena v podobě toho, že si mezi s sebou posílají **pakety**, jež jsou součástí **rámců** (*rámec* je obsahuje ve své datové části).

Rámec obsahuje jak MAC adresu zdrojového zařízení, tak adresu cílového zařízení. Dále obsahuje **IP adresy** zdrojového i cílového zařízení. Součástí rámce jsou i samotná data, jež chceme přenést. Části **rámce**, jež obsahuje IP adresu cílového/zdrojového zařízení a data, říkáme **packet**.

Na základě toho, jakým standardem IEEE 802.xx (xx je číslo označující verzi standartu) je **rámec** daný, může být jeho struktura mnohem složitější. Základ však zůstává stejný. 

Pro kontrolu toho, zdali dorazil rámec v pořádku slouží **kontrolní součet** jež je obsažený v samotném rámci. Opětovné přepočítání **kontrolního součtu** musí souhlasit s tím, který je v **rámci** uvedený.

|![Struktura MAC rámce](linkova-vrstva/frame.PNG "Struktura MAC rámce")|
|:--:|
|*obr. 2:* Struktura MAC rámce|
|*Dostupné z:* <https://www.javatpoint.com/what-is-mac-address>|

## Přepínač (Switch)

Účelem **přepínače** je nasměrovat data do zařízení, kterému jsou určena na základě **MAC adresy**. Zařízení, jež jsou připojená k **přepínači** skrze jeho **porty**, tvoří **síť**.

Když **přepínač** příjme **rámec**,  uloží **zdrojovou MAC adresu (MAC adresa toho, kdo rámec poslal)** do tabulky, zvané **CAM** (Content Addressable Memory; bývá také nazývána MAC tabulka) tabulka. Do této tabulky si také **přepínač** poznamená, k jakému portu se tato adresa váže.

|![CAM tabulka](linkova-vrstva/CAM.PNG "CAM tabulka")|
|:--:|
|*obr. 3:* CAM tabulka|

Poté se **přepínač** podívá do této tabulky a najde si **cílovou MAC adresu**. **Přepínač** tedy ví, na jaký port má **rámec** přeposlat.

Ale co když cílová adresa v tabulce není? **Přepínač** pak pošle **rámec** na všechny své porty.

|![Proces přeposlání rámce](linkova-vrstva/decision.png "Proces přeposlání rámce")|
|:--:|
|*obr. 4:* Proces přeposlání rámce|

Tím, že **přepínač** zašle **rámec** na všechny porty docílí toho, že **rámec** přijme zařízení, kterému je určeno. Toto zařízení bude pravděpodobně chtít odpovědět a jakmile do **přepínače** dorazí **rámec** od tohoto zařízení, tak si **přepínač** MAC adresu toho zařízení společně s portem poznamená. 

## VLAN

**VLAN**, nebo také **virtuální LAN** slouží k logickému rozdělení sítě nezávisle na fyzickém uspořádání. Můžeme tedy naši síť segmentovat na menší sítě uvnitř fyzické struktury původní sítě.

Pomocí VLAN můžeme dosáhnout stejného efektu, jako když máme skupinu zařízení připojených do jednoho **přepínače** a druhou skupinu do jiného **přepínače**. Jsou to dvě nezávislé sítě, které spolu nemohou komunikovat (jsou fyzicky odděleny). Pomocí VLAN můžeme takovéto dvě sítě vytvořit na jednom **přepínači**.

### Důvody zavedení VLAN

- seskupování uživatelů v síti podle skupin či oddělení nebo podle služeb místo podle fyzického umístění a oddělení komunikace mezi těmito skupinami
- snížení broadcastů v síti
- **zjednodušená správa** - k přesunu zařízení do jiné sítě stačí překonfigurovat zařazení do VLANy, tedy správce konfiguruje SW (zařazení do VLAN) a ne HW (fyzické přepojení)
- **zvýšení zabezpečení** - oddělení komunikace do speciální VLANy, kam není jiný přístup. Toho se dá samozřejmě dosáhnout použitím samostatných **přepínačů**, ale s využitím **virtuálních LAN** nám k tomu může postačit jen jeden

### Komunikace mezi VLAN

Konkrétní **VLAN** se aplikuje na jednotlivé porty. **Pro každou VLAN, která je nakonfigurována na **přepínači** je separátní CAM tabulka**.

|![MAC tabulka pro určitou VLAN](linkova-vrstva/VLAN_MAC_table.PNG "MAC tabulka pro určitou VLAN")|
|:--:|
|*obr. 5:* MAC tabulka pro určitou VLAN|
|*Dostupné z:* <https://lemp.io/a-switch-with-multiple-vlans-will-have-multiple-cam-tables/>|

Zařízení, která jsou v té samé VLAN tedy mohou komunikovat spolu. Avšak, zařízení, kde je každé v jiné VLAN, se nevidí. Pokud chtějí tyto zřízení komunikovat spolu, musí být tato možnost nakonfigurována na routeru, nebo **přepínači**, jež se nacházejí na **síťové vrstvě**. Říká se tomu **routing mezi VLAN** [(Síťová vrstva)](https://atlas144.codeberg.page/szz/ikt/ps/sitova-(internetova)-vrstva-ipv4.html).

Problém nastává, pokud máme propojené 2 **přepínače** a tyto přepínače mají k sobě připojená zařízení, jež jsou v těch samých VLAN. Dvě zařízení se mohou nacházet ve stejné VLAN, ale každé zařízení může být **připojené** k jinému **přepínači**. Toto řeší tzv. **trunk port**. Jedná se o port, ke kterému může být přiřazeno více VLAN. 

|![Příklad sítě s využitím VLAN](linkova-vrstva/VLAN.PNG "Příklad sítě s využitím VLAN")|
|:--:|
|*obr. 6:* Příklad sítě s využitím VLAN|
|*Dostupné z:* <https://www.samuraj-cz.com/clanek/vlan-virtual-local-area-network/>|

Pro komunikaci mezi **přepínači** s využitím **VLAN** se využívá tzv. **tagování rámců**. Pokud posíláme **rámec**, chceme vědět, do jaké **VLAN** tento rámec spadá (chceme to vědět i na druhém **přepínači**, kterému tento rámec skrze **trunk** přijde).

Tomuto procesu se říká **IEEE 802.1q tagging**, který je umožněný protokolem **IEEE 802.1q**. Pokud je komunikace vedena skrze **trunk port**, tak je **rámec** upraven tak, že je mu přidána informace o tom, do jaké **VLAN** patří. Nakonec se přepočítá **kontrolní součet**. 

|![Struktura rámce při přenosu skrze trunk port](linkova-vrstva/tagging.PNG "Struktura rámce při přenosu skrze trunk port")|
|:--:|
|*obr. 7:* Struktura rámce při přenosu skrze trunk port|
|*Dostupné z:* <https://www.samuraj-cz.com/clanek/vlan-virtual-local-area-network/>|

Cisco ještě před existencí standardu IEEE 802.1q vytvořilo vlastní standard, kdy se celý rámec zabalí do nového.

## Zdroje

- Javapoint. *What is MAC Address?* [online] @2021. [citováno: 04.05.2023]<br>Dostupné z: <https://www.javatpoint.com/what-is-mac-address>
- Cloudflare. *WWhat is a network switch?* [online] @2023. [citováno: 04.05.2023]<br>Dostupné z: <https://www.cloudflare.com/learning/network-layer/what-is-a-network-switch/>
- ComputerNetworkNotes. *How Switches Forward Frames Explained* [online] @2019. [citováno: 04.05.2023]<br>Dostupné z: <https://www.computernetworkingnotes.com/ccna-study-guide/how-switches-forward-frames-explained.html>
- Samuraj-cz.com. *VLAN - Virtual Local Area Network* [online] @2007. [citováno: 04.05.2023]<br>Dostupné z: <https://www.samuraj-cz.com/clanek/vlan-virtual-local-area-network/>
- Lemp. *A SWITCH WITH MULTIPLE VLANS WILL HAVE MULTIPLE CAM TABLES*. Morales. [online] @2022. [citováno: 04.05.2023]<br>Dostupné z: <https://lemp.io/a-switch-with-multiple-vlans-will-have-multiple-cam-tables/>
