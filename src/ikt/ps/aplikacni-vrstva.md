# Aplikační vrstva

> Protokoly (DHCP, HTTP, FTP, POP3, IMAP, SMTP, SSH,
SSH, TELNET); DNS (popis a pojmy, systém a hierarchie DNS, iterace, TLD, doména, DNSSEC, útoky)

Jedná se o poslední vrstvu (abstraktní), jejímž účelem je umožnit aplikacím na různých počítačích komunikovat mezi sebou.

Dle ISO/OSI je aplikační vrstva 7. vrstvou modelu, zatímco u modelu TCP/IP se jedná o 4 vrstvu.

Pro přenos dat pod aplikační vrstvou slouží TCP/UDP protokol, TCP garantuje přenos dat mezi aplikacemi (ztracený paket je zopakován), zatímco pomocí UDP se data buď doručí či nedoručí.

## HTTP

Protokol určený pro komunikaci s WWW servery. Slouží pro přenos hypertextových dokumentů v HTML a XML. Jedná se také o bezstavový protokol, což znamená, že každý dotaz od uživatele/klienta je zpracován samostatně a bez návaznosti na předchozí.

Běžně běží na portu TCP/80.

S pomocí standardu MIME může http přenést jakýkoli soubor. Pro zjištění umístění zdroje v internetu používá tento protokol tzv. URL - Uniform Resource Locator.

Samotný protokol HTTP neumožňuje security, a proto se používá ve spojení s [TLS](https://cs.wikipedia.org/wiki/Transport_Layer_Security) (kryptografický protokol). Toto použití se označuje jako HTTPS.

1. Uživatel napíše do browseru http://cs.wikipedia.org
2. místní DNS přeloží jméno na IP a vrátí klientovi
3. prohlížeč naváže se serverem TCP spojení
4. prohlížeč pošle požadavek GET + specifikaci souboru, který chce obdržet + protokol, přes nějž chce soubor obdržet + hlavičku z jakého serveru to chceme
5. např.\
   GET /wiki/Wikipedie/ HTTP/1.1\
   Host: cs.wikipedia.org\
   User-Agent: Opera/9.80 (Windows NT 5.1; U; cs) Presto/2.5.29 Version/10.60\
   Accept-Charset: UTF-8*
6. odpovědí serveru je <verze><výsledkový kód><poznámka>
7. např. HTTP/1.1 200 OK

        HTTP/1.0 200 OK
        Date: Fri, 15 Oct 2004 08:20:25 GMT
        Server: Apache/1.3.29 (Unix) PHP/4.3.8
        X-Powered-By: PHP/4.3.8
        Vary: Accept-Encoding,Cookie
        Cache-Control: private, s-maxage=0, max-age=0, must-revalidate
        Content-Language: cs
        Content-Type: text/html; charset=utf-8

Host se používá proto, že na portu TCP/80 může být hostováno vícero HTTP serverů.

**Výsledkové kódy**\
1XX - informativní odpověď\
2XX - úspěšná akce\
3XX - přesměrování\
4XX - chyba klienta (chyba v dotazu třeba)\
5XX - chyba serveru\

### Proxy

**Proxy** je mezibodem mezi uživatelem a serverem (na cestě k serveru jich může být mnoho). Proxy se skládá z:

1. Klientské části
2. Serverové části
3. Vlastní logiky proxy

Uživatelský klient kontaktuje serverovou část proxy, ta předá info klientské části a ta kontaktuje jménem klienta/uživatele daný server.

Vnitřní logika proxy může sloužit jako filtr, který odfiltruje dle nastavených parametrů, jaké servery může uživatel kontaktovat či nikoli. Např. může být nastaveno, že nemůžeme na server Jakup.Schwarz.ug, tím pádem nás proxy nepustí dál.

Proxy má také vlastní CACHE, kam uloží data ze serveru (třeba obrázky). Při dalším dotazu může proxy vzít data rovnou z CACHE.

Uživatel v místní síti nekomunikuje s DNS serverem sám, ale prostřednictvím proxy, tedy do PROXY jde celá URL a proxy teprve rozkouskuje URL a dotazuje se jednotlivých DNS na překlad až poté naváže s cílovým serverem spojení.

### URI - uniform resource identifier

Obecný identifikátor:

1. URL - konkrétní server i soubor/službu
2. URN - neřeší cestu ke zdroji

URI: <schéma>:<na schématu závislá část>\
URL: //<'user>:<'password>@host<'port>'/<'url-path>

user a pw se většinou nevyužívá.

http://server.firma.cz případně (ale nepoužívá se) http://novak:heslo@firma.cz

nebo např: mailto:schwarzjakupbrundosemerek@ahoj.cz

V URI se mohou vyskytovat pouze znaky US-ASCII, pokud se zadává jiný znak, tak se escapuje %.

### Metody HTTP

Slouží ke komunikaci mezi aplikacemi přes http.

1. **GET** - dotazování klienta na konkrétní info na serveru
2. **POST**- odesílá uživatelská data na server
3. **HEAD** - jako GET, ale požaduje hlavičku odpovědi bez dat (pouze metadata)
4. **OPTIONS** - jaké server podporuje metody
5. **PUT** - nahraje data na server
6. **TRACE** - zjistíme kolik je mezi námi a serverem bran a proxy - zjišťujeme co servery na požadavku mění
7. **CONNECT** - spojí se s uvedeným objektem přes daný port
8. **DELETE** - smaže uvedený objekt na serveru

### Cookie - relace

Potřebuji držet relaci (e-shopy typicky). Protože HTTP je defaultně bezstavový.

Server při návštěvě webu přiřadí klientovi ID pomocí hlavičky Set-Cookie.

Server vrací cookie klientovi a on ji pak v dalších dotazech posílá serveru, aby server věděl, o koho se jedná a co si třeba v tom daném obchodě vybral.

Krok, kdy je klient již u pokladny a uživatel chce potvrdit objednávku obleku:

Client: POST / obchod/doprava HTTP/1.1\
Client: Cookie: Version="1";\
Zakaznik="007"; Path="/obchod";\
Zbozi="sako_05"; Path="/obchod";\
Doprava="dobirka"; Path="/obchod";\

#### Cookie třetí strany

Může být přidán další parametr Domain="";\
Na web se dostane například pomocí javascriptu s reklamou od reklamní agentury. 

Používají se také analytická cookie.

### HTTP/2.0

- zpětná kompatibilita s HTTP/1.1
- pipelining požadavků - možnost paralelně stahovat objekty ze stránky současně - streamy skrze jedno TCP spojení
- TLS 1.2
- Server Push - komunikaci aktivuje klient

## DNS - Domain Name System

Jedná se o databázový systém distribuovaný po celém světě provozující Jmenné Servery. V podstatě překládá jména na IP adresy a obráceně.

Tedy, bubu-li chtít na seznam.cz, tak potřebuji nejprve kontaktovat DNS server, který mi přeloží název adresy na IP.

Domény jsou zachyceny ve stromové struktuře, jejímž kořenem je TEČKA ".".

Pod kořenovou doménou jsou tzv. **TLD - Top Level Domain**: arpa, cz, ua, uk, com.

Dále jsou domény 2. úrovně (cuni, jcu, siemens, firma apod.) a 3. úrovně (prf, brno, cbu).

Syntaxe: www.firma.cz. Pomlčku lze použít pouze uprostřed domény. Zároveň každý znak může být ASCII. 

Dříve se v zemích, kde se nepoužívá latinka, ale třeba arabština, nebo hebrejština obsahovali často doménové názvy číslice. www.68-5412.com. Dnes je toto vyřešeno pomocí IDN - Internationalized Domain Names. To využívá Ponycode, který umí převést jakékoli znaky UniCode do ASCII znaků.

V ČR spravuje domény NIC.CZ. V EU je TLD .eu nebo totéž napsané v azbuce. Podporovány jsou tyto znakové sady (azbuka i novořečtina a latinka).

Překlady IPv4 zajišťuje .arpa -> .in-adrr. Adresy jsou napsány opačně tedy: 195.50.45.1 bude: **1.45.50.195.in-addr.arpa**. Tomuto se reverzní doména.

IPV6 se píše také pozpátku, ale namísto čtveřice hexadecimálních čísel, píšeme jednotlivé znaky po jednom a oddělujeme TEČKOU. Na konci je pak X.X.X.X.X.IP6.arpa. .

### Implementace DNS

- jmenné servery (server)
- resolver (klient) - jedná se o knihovnu, která se volá pro překlad jména na IP
- pro komunikace UDP nebo TCP

1. **Pahýlový resolver** - ptá se rovnou nejbližšího jmenného serveru
2. **Resolver s Cache pamětí**, který si pamatuje předchozí odpověď uživateli

Překlady v DNS z resolveru na jmenný server běží pod UDP

### Doména a zóny

Domény mohou být sloučeny do zón, odpovídající nějakému NameServeru. Budu-li mít doménu tvpcom a 10 sub-domén, tak v zóně může být jen 5 těchto sub-domén a dalších 5 využívají zcela jiný NameServer. Tedy tvpcom je **autoritativní** pro 5 sub-domén v její zóně.

Zóny nám říkají, na jakých NS jsou uložena data o jednotlivých doménách/sub-doménách.

|![Zonace domén](aplikacni-vrstva/zona.PNG "Zonace domén")|
|:--:|
|obr. 1: Zonace domén|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 11 DNS*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=kL7dbKo1UBA&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=4>|

### PŘEKLAD domén

Resolvery komunikují s vícero NS, tedy resolver pošle požadavek na překlad nejprve jednomu, pak druhému apod. Jakmile obdrží informaci, tak ostatní odpovědi NS již ignoruje.

NS má jednak CACHE a dále překládá odzadu, tedy zeptá se, kdo je zodpovědný za TLD, doménu 2. stupně atd. až po www.

www.firma.cz

1. NS se zeptá kořenového NS ("."), kdo je zodpovědný za cz
2. kořenový NS odpoví IP adresou pro cz 
3. dále se NS zeptá TLD NS, kdo je odpovědný za firma.cz
4. TLD NS mu vrátí IP adresu takového serveru
5. firma.cz je již autoritativní, takže vrátí NS ip adresu pro www

Podstatné je, že resolver požádá nejbližší NS a očekává celou odpověď. Zatímco NS se ptají postupně na jednotlivé domény a sub-domény. Resolver žádá rekurzivně - chce celou odpověď. NS žádá jen dílčí odpovědi. 

|![Dotazování DNS](aplikacni-vrstva/DNS.PNG "Dotazování DNS")|
|:--:|
|obr. 2: Dotazování DNS|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 11 DNS*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=kL7dbKo1UBA&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=4>|

### Typy DNS serverů

1. **autoritativní** - spravují databázi zóny
   1. primární - master, administrátor vyplňuje informace o IP
   2. sekundární - slave (přebírá informace od master)
   3. stealth server - tajný server

2. ne-autoritativní
    1. caching only server (nemá databázi, le pracuje jen s cache)
    2. autoritativní server pro jinou zónu

Každý DNS server je primárním jmenným serverem pro loopback. 127.0.0.X -> X.0.0.127.in-adrr.arpa

Ve firmě se klient zeptá místního serveru (používá cache) a ten se iterativně dotazuje jednotlivých NS a poté si odpověď uloží.

### RR věty - Resource Records věty

Zde mají NS uloženy informace viz [video](https://youtu.be/kL7dbKo1UBA?t=1488) (odkaz přesměruje rovnou na daný čas, kde se RR řeší). Vždy jsou uloženy v CACHE.

- A - a host address
- NS - authoritative name server
- PTR - DN pointer  - umožní překlad IP adresy na doménové jméno
- MX - mail exchange - umožní doručit e-mail

[NAME] [TTL] IN typ Data_závislá_na_typu_věty

SOA - věta, která je vždy na začátku konfiguračního souboru zóny: cbu.pvt.cz IN SOA cbu.pvt.cz. bindmaster.cbu.pvt.cz

Dále se zachycuje 5 čísel:

- Serial - verze dat
- Refresh - interval, jak často sekundární servery ověřují svá data
- Retry - interval, kdy sekundární server kontaktuje opakovaně primární server
- Expire - limit pro opakované kontaktování primárního serveru
- TTL - jak dlouho mohou neautoritativní servery udržovat data z primárního serveru ve své cache

Věta typu A a AAAA (IPV6) nám vrátí IP adresy k jménům.\
Věta typu NS definuje autoritativní servery pro danou zónu.\
Věta MX specifikuje poštovní server domény, tedy nemusíme psát: xxxx@pocitac.firma.cz, ale jen xxxx@firma.cz.

#### Postup komunikace a DNS protokol

Primární server po zapnutí načte do CACHE databázi RR vět, kterou má na disku. Sekundární NS si tato data tahají do své CACHE. 

HEADER QUESTION ANSWER AUTHORITY ADDITIONAL

|![Paket protokolu DNS](aplikacni-vrstva/paket_DNS.PNG "Paket protokolu DNS")|
|:--:|
|obr. 3: Paket protokolu DNS|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 11 DNS*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=kL7dbKo1UBA&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=4>|

### Správa zón

- . kdo ví (čína + USA)
- TLD (u nás NIC.CZ) - Network Information Centre
- dále uživatelé (banka, univerzita, firma...)

**Reverzní zóny (poskytování IP adres)**
- TLD: in-adrr.arpa, IPV6.arpa
- Regional Internet address Registry
- Poskytovatelé
- Uživatelé

### DNSSEC

Security extensions, která mají za úkol zabezpečit informace poskytované DNS servery proti spoofingu (podvržení) a proti úmyslné manipulaci. 

Resolver může pomocí elektronického podpisu ověřit pravost dat poskytnutých DNS serverem. 

#### Funkce DNSSEC

- asymetrické šifrování
- držitel domény používající DNSSEC vygeneruje dva klíče (privátní a veřejný)
- privátním klíčem podepíše technické údaje, které o své doméně do DNS vkládá
- veřejný klíč je držen nadřazenou autoritou - zde je m ožné ověřit pravost údajů držitele domény
- u TLD "cz" je veřejný klíč předán nadřazené autoritě - kořenové DNS servery

#### Zone Signing Key a Key Signing Key

- ZSK slouží k podpisu vlastních dat
- KSK slouží k podpisu ZSK - tedy vlastně první klíč se opět zašifruje a hash veřejné části klíče je uložen u rodičovské domény
- takto se šifrují Resources Records - RRSIG - de facto vytvoříme otisk záznamů a ten zašifrujeme privátním klíčem
- autenticita potomka se také ověřuje, a to pomocí Delegation Sign - rodič podepíše veřejný hash potomka

#### Řetězec důvěry

-  kořenová doména má veřejný hash klíče na svém webu
-  RRSIG je validován přes její privátní hash
-  RRSIG validuje záznam o potomku zahashovaný KSK potomka
-  takto pokračujeme dále k dalším potomkům až se dostaneme na konec

#### Útoky

- běžný překlad adres nemá žádnou ochranu, tedy může dojít k zneužití názvu (PHISHING) *banka.cz* a podvržení jiné IP adresy

**PHISHING**
Hacknutí sdíleného webového serveru. Namísto infiltrace jednotlivých webů změní útočník konfiguraci celého serveru a nahraje tam sůj obsah, tedy každá doména - webová stránka - bude mít na pozadí nový falešný obsah.

Zneužití poddomén: www.banka.novefunkce.cz
Na první pohled to vypadá, že se přihlašujeme na web banky.cz, ale skutečně se přihlašujeme na web novefunkce.cz

Překlepy v odkazech: www.cs.wikipedia.org/wiki/Domecek

Působí to, jako bychom se měli dostat na web o domečkách, ale jedná se o podvrh s velkým "D", tedy za názvem může být cokoli jiného.

### DHCP

Protokol slouží k automatické konfiguraci PC připojených do sítě. DHCP přiděluje pomocí DHCP protokolu jednotlivým zařízením:
- ip adresu
- masku
- implicitní bránu
- adresu DNS serveru

Platnost těchto informací je omezená, tudíž na každém počítači běží DHCP klient, který prodlužuje jejich životnost.

### Princip DHCP

1. klient požádá server o IP
2. server zaznamenává "půjčenou" IP adresu a čas, do kdy ji smí klient používat
3. po vypršení ji server přiřadí jiným zařízením
4. klient komunikuje na UDP portu 68, server poslouchá na portu 67
5. po připojení do sítě broadcastem vyšle klient DHCP paket DHCPDISCOVER
6. DHCP server odpoví paketem DHCPOFFER s nabídkou IP adres
7. klient si vybere z nabídky IP adresu a požádá server paketem DHCPREQUEST
8. server potvrdí DHCPACK paketem - má určitou životnost (klient ji obnoví nebo nesmí IP používat)

### Relay agent

Pokud mám více sítí oddělených Routery a jen jedna síť obsahuje DHCP server, tak admin nastaví/zapne na Routeru tzv. relay-agenta. Ten má za úkol broadcastované požadavky ze sítě bez DHCP severu přeposílat do sítě s DHCP serverem. K dotazu relay-agent přidá i číslo sítě a masku, z které klient pochází. DHCP server potom ví, z jakého rozsahu má klientovi adresu přidělit.

### Přidělení IP

1. ručně
2. statická alokace - DHCP server obsahuje seznam MAC adres a k nim přidělené IP
3. DHCP vymezí rozsah adres - **lease-time** IP adresy

## SMTP a POP3 a FTP

### Simple Mail Transfer Protocol

1. Klient napíše ve svém poštovním editoru zprávu
2. Zpráva se uloží na disk do e-mailové fronty
3. SMTP Client prohlíží onu frontu
4. SMTP Client se pokouší dopravit zprávu na SMTP server adresáta
5. Mezi odesílatelem a adresátem může být mnoho "mezilehlých" serverů
6. Mezilehlé servery uloží zprávu opět do fronty
7. SMTP klient mezilehlého serveru prohlédne frontu a pošle ji na adresátům SMTP server, tam se vloží do INBOXU (Doručená pošta)
8. Adresátův editor umí onu zprávu již z INBOXU přečíst

SMTP agent = SMTP client + SMTP server (oboje na jednom serveru)

Každý DNS server má uloženo v Resource Records (např. serverů firmy.cz), kde každý server firmy.cz má svoji prioritu. Čím nižší číslo, tím vyšší priorita a tomu zařízení se to snaží doručit jako k prvnímu.

|firmy.cz.| IN MX 10 smtp.firmy.cz.|
|---|---|
| | IN MX 20 smtp.firmy.cz.|
| | IN MX 30 smtp.firmy.cz.|

|![SMTP](aplikacni-vrstva/smtp.PNG "SMTP")|
|:--:|
|obr. 4: SMTP|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 12 Mail (SMTP, POP3, MIME)*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=8tlrsoCs-60&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=3>|

|![SMTP a POP](aplikacni-vrstva/POP.PNG "SMTP a POP")|
|:--:|
|obr. 5: SMTP a POP|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 12 Mail (SMTP, POP3, MIME)*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=8tlrsoCs-60&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=3>|

Protokol POP (Post Office Protocol) slouží k čtení zpráv ze schránky, (může je také rušit či stahovat). SMTP stále slouží k odesílání zpráv na poštovní server a také ke komunikaci mezi servery.

#### IMAP - Internet Message Access Protocol

Vedle toho existuje také protokol IMAP (dnes IMAP4), který je určen pro vzdálený přístup k e-mailové schránce - prostřednictvím klienta.

Oproti POP3 má pokročilejší funkce - práce se složka mi, přesouvání zpráv mezi složkami, mazání, prohledávání na straně serveru. 

S poštovní schránkou pracujeme přes IMAP remotely - tedy veškeré zprávy zůstávají na serveru a do PC se stahují pouze nezbytné informace. Tedy, při zobrazení složky stáhne pouze záhlaví a obsah až když chceme číst.

Rozdíl oproti POP3 je, že POP3 stáhne zprávu zcela, takže ze serveru zmizí a máme ji uloženou lokálně, zatímco IMAP4 ji na serveru ponechá.

U zpráv se uchovává jejich stav - nepřečtená, důležitá, odpověď... .

Protokol umožňuje připojení více klientů zároveň.\

**VÝHODY IMAP OPROTI POP3**

- synchronizace - tedy v klientovi i na serveru jsou stejné údaje pro správu, zatímco u POP3 dojde ke stažení zprávy do klienta
- připojení je tak dlouhé, jak je dlouhé aktivní spojení, zatímco u POP3 dojde jen ke stažení nových zpráv a spoje ní se přeruší
- více klientů připojených současně
- zprávy jsou ve formátu MIME (standard pro přenos zpráv v různých kódováních)
- práce se složkami/schránkami na serveru - mazání, přesun zpráv mezi nimi apod.
- vyhledávání zpráv přímo na serveru

**NEVÝHODY**

- komplikovaný protokol - složitá implementace
- prohledávání ve zprávách může značně zatěžovat server

#### Princip SMTP

1. Server čeká na portu 25 na požadavky klienta
2. Klient naváže TCP spojení
3. Server vrátí hlášku 220 (220 server.firma.cz SMTP a to že je ready)
4. klient pošle příkaz HELO pc.firma.cz
5. server pošle odpověď 250 a představí se sám (250 server.firma.cz)
6. již běží dialog
7. klient chce na server poslat samotný e-mail
8. MAIL from (mám mail od tohoto klienta)
9. server odpoví, že je klient ok / případně SPAM
10. klient RCPT To (chci to doručit tomuto adresátovi)
11. server zkontroluje adresáta a řekne adresát ok
12. když jsou adresáti vyřízeni, tak klient pošle příkaz DATA
13. server vrátí 354 - dej na poslední řádek tečku
14. klient pošle zprávu CRLF . CRLF (samostatná tečka na řádku)
15. server pošle ID zprávy ve frontě a řekne, že zprávu přijal 
16. klient pošle nakonec QUIT a tím se TCP ukončí

|![ESMPT](aplikacni-vrstva/esmtp.PNG "ESMPT")|
|:--:|
|obr. 6: ESMTP|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 12 Mail (SMTP, POP3, MIME)*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=8tlrsoCs-60&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=3>|

Počátek je stejný, ale v HELO jsou přehozena písmenka na EHLO. Server pak vrátí více řádků. Max velikost zprávy, způsob autentizace či rozšíření DSN (Delivery Status Notification).

|![Příkazy POP3](aplikacni-vrstva/pop3.PNG "Příkazy POP3")|
|:--:|
|obr. 7: Příkazy POP3|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 12 Mail (SMTP, POP3, MIME)*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=8tlrsoCs-60&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=3>|

#### FTP

File transfer protocol.

Slouží k přenosu souborů mezi PC a servery po síti. Data lze poslat pomocí browseru, nebo pomocí FTP clienta. Data jsou uložena (většinou na dedikovaných FTP serverech).

FTP protokol je stavový protokol. Uživatel se může autentizovat pro upload dat na server, práva mohou být omezena na určité soubory/adresáře. Autentizační údaje jsou však přenášeny na server v nezašifrované podobě (plane text), což je velké bezpečnostní riziko. 

FTP využívá porty TCP/21 a TCP/20. Na 20 se přenáší vlastní data a 21 příkazy. 

Přenos může být binární či ascii. 

V protokolu je použit model klient-server. Byť režimy jsou aktivní a pasivní. V aktivním režimu navazuje připojení pro přenos dat server a klient poslouchá. V Pasivním režimu navazuje data connection klient.

#### FTP a SSH

FTP využívá SSH pro tzv. tunelování - SFTP. Problémem může být zajištění bezpečnosti pro obě větve připojení - datové i řídící. 

Pro vlastní přenos dat využívá FTP SSH-2, nicméně může běžet i nad jiným protokolem. Právě SSH-2 zajišťuje autentizaci. 

SFTP umožňuje:

- pokračovat v přerušených přenosech
- vypisovat adresáře
- odstraňovat soubory na remote PC

## Telnet a SSH

Secure Shell je zabezpečený komunikační protokol pro sítě používající TCP/IP. Jedná se o náhradu ze Telnet, rlogin, rsh.

Prostřednictvím SSH posíláme zabezpečená/šifrovaná data skrze síť. Telnet a další posílali data v plane textu.

SSH umožňuje tunelovou výměnu dat (kopírování, přenos) a zabezpečuje autentizaci obou účastníků komunikace, integritu přenášených dat, transparentní šifrování a bezztrátovou kompresi.

Standardně naslouchá SSH na portu TCP/22.

SSH má 3 vrstvy:

1. transportní - přenášení dat a klíčů, ověření integrity
2. autentizační - password, public key
3. spojení

SSH lze narušit metodou man in the middle (Burp Suite). Aby k tomu nedošlo, používá se asymetrická kryptografie.

Dva uživatelé vygenerují private a public keys. Private keys si nechají u sebe (je chráněn heslovou frází). Public key je odeslán na cílový server (uživatelé si jej mezi sebou vymění). Následně server veřejným klíčem zašifruje blok náhodných dat (výzva challenge-response) a pošle ji klientovi, který se snaží na server přihlásit. Pokud klient pomocí svého privátního klíče zprávu dešifruje. Má tímto server ověřeno, že se jedná o klienta se správným klíčem. 

Private key neopustí uživatelův počítač a stejně server povolí vstup.

## Zdroje

- https://www.youtube.com/watch?v=jVuAaBS1t6o&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=3
- https://www.youtube.com/watch?v=kL7dbKo1UBA&list=TLPQMTYwNTIwMjPbta9ot0k1YQ&index=5
- https://cs.wikipedia.org/wiki/Man_in_the_middle
- https://cs.wikipedia.org/wiki/Secure_Shell
- https://cs.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://cs.wikipedia.org/wiki/Domain_Name_System