# Síťová (Internetová) vrstva IPv6

> adresa, pravidla zápisu, rozdíly oproti IPv4, anycast, stavová/bezstavová konfigurace, metody tvorby IPv6 adresy, specifické IPv6 adresy, objevování sousedů, IPv6-IPv4/VLAN mapping, privacy extensions, přechodové mechanismy, bezpečnost

IPv6 je **protokol síťové vrstvy**, který umožňuje komunikaci po síti. **Jedná se o nástupce [IPv4 protokolu](https://atlas144.codeberg.page/szz/ikt/ps/sitova-(internetova)-vrstva-ipv4.html)**. V dnešní době je **IPv4** pomalu vytlačován **IPv6**.

## Rozdíly oproti IPv4 aneb proč byla nutná změna

Starší **protokol IPv4** poskytuje pouze omezené množství adres. **Adresní prostor IPv4 je 2<sup>32</sup> adres**, což je něco přes 4 miliardy. Prakticky je toto číslo menší kvůli sdružování adres do podsítí skrze **masku sítě** a část adres je rezervována pro speciální využití.

Protokol **IPv4** též **přestává vyhovovat z hlediska rychlosti přenosu dat**, hlavně u multimediálních datových přenosů (videokonference, internetová TV, audiohovory apod.)

**Hlavní změnou IPv6** oproti IPv6 je mnohem větší **adresní prostor**. Jedná se o **3.4 * 10<sup>32</sup>** různých adres. Pro představu na každého člověka na zemi připadá potenciálních **5x10<sup>28</sup>** adres, nebo také  **2<sup>52</sup> pro každou známou hvězdu ve vesmíru**. Zatímco u **IPv4 je to pouze 0,5 adresy na člověka** -> ano, počet IPv4 již byl dávno vyčerpán (poslední adresní prostory byly rozděleny v roce 2011).

Přečíslování sítě při změně poskytovatele internetu je s IPv4 obtížný proces. U IPv6 je mnohem jednodušší -> stačí změnit **prefix sítě**. V adresním prostoru IPv6 existuje možnost přidělit tzv. **provider independent** rozsahu, který je zcela nezávislý na poskytovateli a je přidělen přímo koncovému uživateli.

Díky takto velkému adresnímu prostoru již není potřeba **NAT**, který byl zaveden kvůli vyčerpání prostoru IPv4 adres a kvůli **[bezpečnosti](#bezpečnost)**.

IPv6 dále na rozdíl od IPv4 umožňuje tzv. **[bezestavovou konfiguraci (SLAAC)](#bezestavová-konfigurace-slaac)**. Host si může vzít svojí vlastní adresu bez nutnosti použití **DHCP** protokolu.

Většina protokolů ostatních vrstev podporuje IPv6 (TCP, UDP, ...). Problémy nastávají, pokud IPv6 není podporované.

## IPv6 adresa a pravidla zápisu

**IPv6 adresa je složena z 128 bitů a můžeme ji rozdělit na dvě části**:

- 64 bitový **síťový prefix**
- 64 bitová **část hosta**

Část hosta je vytvářena na základě **MAC adresy koncového uživatele** nebo přiřazena následně. To vytváří **bezpečností riziko** neboť je možné vystopovat koncové zařízení, jež odpovídá **MAC adrese** rozhraní.

- řešení je v podobě mechanismu, který mění kus IPv6 adresy

### Notace

IPv6 je zapsaná jako 8 čtveřic čísel v **hexadecimálním tvaru**. 

Například tedy:  `2001:0db8:85a3:08d3:1319:8a2e:0370:7334`

- pokud je jedna nebo více po sobě jdoucích skupin `0000` tak mohou být nuly vynechány
  - adresa `2001:0db8:85a3:0000:0000:0000:0370:7334` Může být tedy zkrácena na `2001:0db8:85a3::0370:7334`
- stejně tak můžou být vynechána počáteční nuly každé čtveřice

Následující adresy jsou si rovnocenné:

```
2001:0db8:0000:0000:0000:0000:1428:57ab
2001:0db8:0000:0000:0000::1428:57ab
2001:0db8:0:0:0:0:1428:57ab
2001:0db8:0:0::1428:57ab
2001:0db8::1428:57ab
2001:db8::1428:57ab
```

- Poslední 4 bajty IPv6 adresy mohou být uvedeny v desítkové soustavě za použití tečky jako oddělovače. Toto je často uváděno v **kompatibilních adresách**.
- IPv6 adresu lze namapovat na IPv4 adresu, například `::ffff:10.120.78.40`

### Notace sítě

Síť v IPv6 je popsána pomocí notace **[CIDR](https://cs.wikipedia.org/wiki/Classless_Inter-Domain_Routing)**. 

- IPv6 **síť (nebo podsíť)** je skupina blízkých adres, jejíž velikost je mocnina čísla `2`.
- počáteční bity adresy jsou stejné pro všechny hosty sítě -> toto nazýváme **prefixem sítě**
- síť je označena první adresou v sítí a lomítkem oddělenou délkou prefixu v bitech (desítkově)
  - například `2001:0db8:1234::/48` označuje **síť** s **adresami** od `2001:0db8:1234:0000:0000:0000:0000:0000` do `2001:0db8:1234:ffff:ffff:ffff:ffff:ffff`
  - **host** se někdy označuje s lomítkem **`/128`**, protože ho můžeme chápat jako **síť s prefixem dlouhým `128` bitů** (**sít** pro jedno zařízení)
- **routování** je totožného principu, jako je tomu v případě **IPv4**, jediný **rozdíl** je v **délce adresy** v bitech

### Druhy adres

Adresy IPv6 můžeme dělit na:

- **unicast**
  - identifikují jedno jediné rozhraní
- **multicast**
  - identifikují skupinu rozhraní
  - mají prefix `FF00::/8` a její **druhý oktet** určuje **dosah adresy** -> rozsah, v jakém je tato **multicast adresa zviditelněna**
- **anycast**
  - je podobná multicastu s tím rozdílem, že vyslaný packet dorazí pouze jednomu zařízení, obvykle tomu nejbližšímu s pohledu směrovače
  - **anycast** adresy jsou nerozlišitelné od **unicast** adres
  - pokud přiřadíme **unicast** adresu více rozhraním, jedná se o **anycast**

Dále máme několik vyhrazených IPv6 adres. **Tyto adresy mají speciální význam:**

- **Místní linka**
  - `::1/128` adresa místní smyčky, je **náhradou za localhost** -> packet vyslaný na tuto adresu je vrácen zpět
- **ULA (Unique Local Adress)**
  - `fc00::/7` jedná se o **unikátní lokální adresy**, jsou směrovatelné pouze v podmnožině spolupracujících sítí
  - adresy zahrnují 40-bitové pseudonáhodné číslo minimalizující konflikty při **sloučení sítí** nebo **úniku packetů**
- **IPv4 (speciální adresy)**
  - `::ffff:0:0/96` používá se [přechodových mechanismů](#přechodové-mechanismy)
  - `2002::/16` používá se u IPv6 -> IPv4 [tunelů](#přechodové-mechanismy)
- **Multicast**
  - `ff00::/8` použití pro **multicast** adresy

### Adresování

Pži **adresování** dělíme síť na tři části:

- **site prefix** (48 bitů)
  - **prefix sítě, popisuje veřejnou topologii, je přidělený poskytovatelem internetu (ISP)**
- **subnet prefix** (16 bitů)
  - **prefix podsítě**, specifikuje rozsah interní topologie (možnost vytváření podsítí, můžeme se na to koukat jako na **prefix určující lokální síť**)
  - je náhradou za subnetting v **IPv4**
- **interface ID**
  - také se mu říká **token**, jedná se o číslo, určující konečně zařízení

|![Rozdělení IPv6 adresy při adresaci](sitova-vrstva-ipv6/adressing.PNG)|
|:--:|
|*obr. 1:* Rozdělení IPv6 adresy při adresaci|
|*zdroj:* <https://docs.oracle.com/cd/E18752_01/html/816-4554/ipv6-overview-10.html>|

## Přidělování IPv6 adres

- **site prefix** je přidělený ISP
- **subnet prefix** je přidělený administrátorem sítě
- **adresa rozhraní hosta (zbytek)** je přidělena tedy jak?

Jde to pomocí tří způsobů:

- **statická konfigurace** (manuální přidělení adresy)
- **dynamická konfigurace**
    - **stavová**
    - **bezestavová**

## Stavová a bezestavová konfigurace

### Stavová konfigurace

Jedná se o konfiguraci pomocí DHCPv6, což je upravený DHCP protokol pro IPv6 adresování.

- od svého předchůdce se moc neliší
- poskytuje adresu s předem vybraného rozsahu
- je podmíněný podporou UDP komunikace na obou zařízeních
- princip výměny 4 zpráv zůstává
  - zažádání klientem o adresu
  - zasláni volných adres ze serveru
  - vybrání adresy klientem
  - potvrzení od DHCP serveru

### Bezestavová konfigurace (SLAAC)

- specifické pro IPv6

**Jedná se o metodu která přiděluje hostovi adresu automaticky**, nebo pokud si o ni požádá. **Využívá toho, že v každé podsíti se nachází směrovač nebo server, který ji propojuje s okolním světem**. Tyto **parametry** poté směrovač nebo server **rozesílá do všech k němu připojených sítí**.

Tyto parametry jsou rozesílány v podobě **ICMPv6** zprávy. 

Ve stručnosti:

- Před zahájením komunikace si každý **uzel** (**host**) vygeneruje svoji **local-link adresu** pro každý port, která začíná **prefixem** FE80::/10 a připojí k tomu **identifikátor rozhraní**.
- **Host** nyni musí ověřit unikátnost tohoto rozhraní v rámci lokální sítě. Použije k tomu mechanismus hledání duplicitních adres za pomocí **vyhledávání sousedů (viz dále)**.
- Pokud dorazí **ohlášení** souseda tak to znamená, že nějaký jiný **host** v síti má stejný **identifikátor rozhraní** a automatická konfigurace je zastavena. V případě **negativního** ohlášení si **host** vygenerovanou **local-link adresu** přidělí a čeká na ohlášení směrovače/serveru.
- **Ohlášení směrovače/serveru** uzlu říká, jestli nyní má použít stavovou, nebo bezestavovou konfiguraci. 
- Stavová konfigurace jest popsána [zde](#stavová-konfigurace).
- V **ohlášení** jsou uvedené prefixy (to jsou ty **porty hosta**), ke kterým host přidělí svůj dříve vygenerovaný **identifikátor (local link adresu?)**. Dále je v **ohlášení** uvedeno, jak dlouho bude **identifikátor** platný, než bude potřeba **vytvořit nový**.

**Celý SLAAC je velmi složitý. Je popsaný společně se strukturou ohlášení od směrovače/serveru [zde](http://access.feld.cvut.cz/view.php?cisloclanku=2012060001).**

## Objevování sousedů

Jedná se o protokol s označením **NDP (Neighbor Discovery Protocol)**. Protokol definuje 5 různých ICMPv6 paketů, jež vykonávají funkce pro IPv6 podobným způsobem, jako pracuje v IPv4 protokol **ARP**, **ICMP router discovery** a **ICMP redirect**. 

Používá se pro **autokonfiguraci síťových rozhraní**

Definuje tedy 5 různých ICMPv6 typů paketů:

- **Router Solicitation**
  - host se snaží pomocí této zprávy najít router
- **Router advertisement**
  - směrovač touto zprávou ohlašuje svoji přítomnost a posílá různé linkové a internetové parametry buď periodicky nebo v reakci na **router solicitation**
- **Neighbor solicitation**
  - tyto zprávy jsou používány hosty k určení adresy ke komunikaci se sousedem nebo k ověření, že je soused dosažitelný
- **Neighbor advertisement** 
  - používaná jako reakce na **neighbor solicitation**
- **Redirect**
  - směrovač pomocí této zprávy informuje hostitele, že existuje lepší **první hop** na cílový směrovač

Tyto zprávy mohou být použity pro **následující** funkce:

- **Router discovery:** hostitelé mohou najít **směrovače** umístěné na připojených linkách.
- **Prefix discovery:** hostitelé mohou zjistit prefixy adres, které jsou připojené na lince.
- **Parametr discovery:** hostitelé mohou zjistit parametry linky (např. MTU).
- **Automatická konfigurace adres:** volitelná bezstavová konfigurace adres síťových rozhraní.
- **Rezoluce Adres:** mapování mezi IP adresami a adresami linkové vrstvy (MAC adresami).
- **Stanovení next–hop:** hostitelé mohou najít další směrovače na místo určení.
- **Detekce nedostupnosti sousedních uzlů (NUD):** určení, že soused již není na této lince dostupný.
- **Detekce duplicitních adres (DAD ):** uzly mohou zkontrolovat, jestli je adresa už používaná.
- **Packet redirection:** pro poskytnutí lepší next-hop cesty pro některá cílová místa.

## IPv6-IPv4/VLAN mapping

- pro IPv6 je povětšinou potřeba manuálně povolit (podporují pouze Cisco switche)
  - celkově, řádné pracování s IPv6 je podporováno hlavně v rámci Cisca, ostatní organizace jsou dosti pozadu
  - **Cisco začalo plánovat adaptaci na IPv6 již v roce 2002**

Jedná se o **techniku**, která umožňuje *namapovat* **ID VLAN na jednom switchi na ID VLAN na jiném switchi**. Tímto je možné referovat tu a samou **VLAN** pomocí různých **VLAN ID** na různých switch zařízeních. 

## Privacy extensions

Jedná se o **formát**, který **definuje dočasné adresy,** jež se **mění v pravidelných časových intervalech**

- nová adresa vypadá, jako že nemá nic společného s tou starou pro kohokoliv zvenčí 
- jedná se o ochranu proti **korelaci adres**
- regulární změna adres je nezávislá na **prefixu** sítě -> ochrana proti **trackování** pohybu po síti (útočník musí najít **vygenerovanou pseudo-adresu namísto statické**)

## Přechodové mechanismy

Jedná se o mechanismy, které **umožňují IPv6 hostům využívat IPv4 služby a izolovaným IPv6 hostům a sítím dát možnost se pohybovat skrze IPv4 infrastrukturu** (například **NAT**).

Tyto mechanismy jsou například:

- **duální implementace**
  - protože IPv6 rozšiřuje IPv4, je možné vytvořit **síťovou implementaci** které podporují oba dva protokoly
  - host využívající tuto implementaci se nazývá **dual-stack host**
- **tunelování** (něco jako VPN)
  - nejznámější je tunelování **6to4**
    - koncové body jsou určeny známou anycast IPv4 adresou na **místní straně** a zapouzdřením IPv4 adresy do IPv6 adresy na **vzdálené straně**
  - jiné používané **tunelování** je pomocí protokolu **ISATAP**
    - tento protokol považuje IPv4 místní síť za IPv6 virtuální linku a mapuje všechny IPv4 adresy na IPv6 adresy místní linky
- **spravované tunelování**
  - koncové body tunelu jsou výslovně uvedeny, povětšinou lidským operátorem nebo službou **zprostředkovatele tunelu**
  - hodí se pro velké sítě, protože je více deterministické než předchozí případy
  - při tomto způsobu se do hlavičky IPv4 paketu přidává informace o protokolu číslo `41`

## Bezpečnost

- jednotlivé **datagramy** je možno **šifrovat** pomocí protokolu **IPSec**
- **v IPv6 má každá stanice globálně dostupnou adresu (neexistuje NAT)**
  - je nutné nastavit hraniční firewall
  - lze obejít tím, že uživatel si vytvoří vlastní tunel (třeba za pomocí VPN)    
    - řešením je použít NAT64, což je nadstavba nad klasickým NAT, který ale umí pracovat s IPv6 adresami a schovává všechny zařízení v sítí za jednou adresou
    - stejně tak musí jít veškerá komunikace do internetu přes tento směrovač
- využití **DNS64**
  - jedná se o obdobu DNS, které je schopné pracovat s IPv6 adresami skrze **NAT64**
  - směrovač odešle požadavek na DNS, DNS mu v případě IPv6 adresy vrátí chybu
  - směrovač se poté zeptá na IPv4 adresu cílové služby
  - tuto adresu namapuje na IPv6 adresu, jejíž prefix je daný směrovačem (jedná se o speciální prefix)
  - takto zabalenou odpověď přepošle směrovač zpět klientovi
- validaci příchozích zpráv do sítě provádí **NAT64**
  - zabrání se tím označení příchozích namapovaných IPv6 zpráv jako podvržených

## Zdroje

- IPv6. *Wikipedie: Otevřená Encyklopedie* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://cs.wikipedia.org/wiki/IPv6>
- What Is IPv6 (Internet Protocol Version 6)? Definition, Features, and Uses. *Chiradeep BasuMallick* [online] @2022 [citováno 20.05.2023]<br>Dostupné z: <https://www.spiceworks.com/tech/networking/articles/what-is-ipv6/>
- IPv6 Addressing Overview. *Oracle* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://docs.oracle.com/cd/E18752_01/html/816-4554/ipv6-overview-10.html>
- IPv6 Anycast Address. *Cisco* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ipv6_basic/configuration/xe-16-5/ip6b-xe-16-5-book/ip6-anycast-add-xe.pdf>
- Unique local address. *Wikipedia: The Open Encyclopedia* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://en.wikipedia.org/wiki/Unique_local_address>
- Neighbor Discovery Protocol. *Wikipedie: Otevřená Encyklopedie* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://cs.wikipedia.org/wiki/Neighbor_Discovery_Protocol>
- VLAN mapping configuration. *h3c* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://www.h3c.com/en/Support/Resource_Center/EN/Home/Public/00-Public/Technical_Documents/Configure___Deploy/Configuration_Guides/H3C_CG-9254/03/202303/1790810_294551_0.htm>
- IPv6 Addresses, Security and Privacy. *Ripe Labs* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://labs.ripe.net/author/johanna_ullrich/ipv6-addresses-security-and-privacy/>
- Bezpečnost protokolu IPv6. *Nic.cz* [online] @2023 [citováno 20.05.2023]<br>Dostupné z: <https://www.nic.cz/files/nic/doc/Computerworld_IPv6_062011.pdf>
- Konfigurace adres při implementaci IP verze 6 v koncových sítích. *L. Čepa, J. Kačer. České vysoké učení technické v Praze, FEL* [online] @2012 [citováno 20.05.2023]<br>Dostupné z: <http://access.feld.cvut.cz/view.php?cisloclanku=2012060001>
