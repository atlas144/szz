# Transportní vrstva

> TCP (popis, TCP segment, hlavička, navazování spojení, služby a porty, velikost okna); UDP (popis, UDP datagram, pseudo IPv4, kontrolní součet, služby a porty, výhody, nevýhody)

Transportní vrstva, se nachází mezi **aplikační** a **síťovou** vrstvou. Na **aplikační** vrstvě se nacházejí jednotlivé aplikace, které pracují s **protokoly aplikační vrstvy**.

- SMTP (používaný pro posílání emailů)
- HTTP (nejpoužívanější protokol pro webové stránky)
- FTP (protokol pro přenos souborů)
- a další....

Tyto protokoly komunikují s **protokoly transportní vrstvy** skrze **porty**. Můžeme říct, že **porty** tvoří spojení mezí **aplikační** a **transportní** vrstvou. Každý port je poté přiřazen k určitému protokolu na **aplikační vrstvě**. Protokol HTTP například využívá port s číslem 80. **Porty využívají služby na aplikační vrstvě**. Služba může být webová stránka, nebo i aplikace. **Port může využívat buď TCP, nebo UDP spojení, popřípadě obojí**.

**Port** je 16-bitová hodnota, jež může nabývat rozsahu 0 - 65535.

**Porty** můžeme dělit do 3 kategorií.

- **dobře známé**
  - porty v rozsahu 0 až 1023; vyhrazené pro nejběžnější služby
- **rezervované**
  - v rozsahu 1024 až 49151, použití portu by se mělo registrovat u **ICANN (Internet Corporation for Assigned Names and Numbers)**
- **dynamické a soukromé porty**
  - v rozsahu 49152 až 65535, vyhrazené pro dynamické přidělování a soukromé využití, nejsou pevně přiděleny žádné aplikaci

## TCP (Transmission Control Protocol)

### Základní popis

TCP přenáší **packety**, což jsou série bitů, které mají určité uspořádání. **TCP protokol**  přidává k **packetům** další část, **header**. **Packet**, společně s **headerem/hlavičkou** se poté nazývá **segment**.

Pro **navázání a udržování spojení** skrze **TCP** se využívá **3 way handshake**. Přijímající strana musí vždy potvrdit, že odeslanou zprávu přijala v pořádku. Pokud toto nepotvrdí, je **segment** odeslán znovu. (více dále)

TCP je označovaný jako **spolehlivý protokol**. TCP používá službu **síťového protokolu IP** opakovaným odesíláním ztracených nebo poškozených **packetů/segmentů**.

### TCP segment

Segment je složený z **dat aplikace** a **hlavičky** kterou mu TCP přiřadilo.

### Hlavička segmentu (header)

**Header** obsahuje informace o segmentu.

|![Struktura hlavičky TCP segmentu](transportni-vrstva/header.PNG "Struktura hlavičky TCP segmentu")|
|:--:|
|*obr. 1:* Struktura hlavičky TCP segmentu|
|*Převzato z:* <http://www.myreadingroom.co.in/notes-and-studymaterial/68-dcn/850-tcp-segment.html>|

- **Zdrojový port**
- **Cílový port**
- **Sequence number (Sekvenční číslo)**
  - Hodnota v poli se chová jako **čítač**, který nám říká, kolik bytů bylo odesláno. Pokud máme packet, který obsahuje data o velikosti 1400 bytů, tak **sequence counter** bude inkrementovaný o tuto hodnotu po **odeslání segmentu**.
- **Acknowledgement number**
  - Pole obsahuje číslo, které říká, **kolikátý packet příjemce očekává od odesílatele**. Pokud **příjemce** úspěšně přijal číslo X od odesílatele, tak se ack number definuje jako x+1 (očekáváme následující packet). Pokud hodnota nesouhlasí, je packet **pozdržen** tak, aby byl předán ve správném pořadí.
- **HLEN**
  - Pole obsahuje číslo udávající **velikost hlavičky**. Ta může být 20 - 60 bytů.
- **Control flag**
  - Pole definuje celkem 6 různých příznaků. Každý příznak má jeden bit (ano/ne) a pole má celkem 6 bitů:
    - **URG**: označí data jako **urgentní**, nastavuje tomuto packetu vyšší prioritu
    - **ACK**: hodnota **acknowledgment field (acknowledgment number)** je validní
    - **PSH**: pošli data ihned jak je to možné (systém nebude čekat a **bufferovat data**), často se požívá v kombinaci s URG příznakem
    - **SYN**: synchronizuj sekvenční číslo během připojení
    - **FIN**: ukonči spojení
- **Velikost okna**
  - Definuje velikost v bytech, kterou musí druhá strana dodržovat. Délka tohoto pole je 16 bitů, takže **maximální velikost okna** je 65535 bytů. **Tato hodnota je určována příjemcem**. Odesílatel se musí přizpůsobit a odesílat packety jen takové velikosti, jakou udá **příjemce** pomocí této položky v hlavičce.
- **Kontrolní součet**
  - Používá se pro ověření poškození packetu při přijmutí. Příjemce znovu vypočítá z obsahu **kontrolní součet**. Pokud se neshoduje, je zažádáno o zaslání packetu znovu.
- **Options**
  - 40 bytů, nejčastěji obsahuje informace o maximální velikosti segmentu nebo časové razítko.

### Navazování spojení

Navázání spojení, se provádí metodou, které se říká **3-Way handshake**. Tento proces se skládá ze 3 částí.

- **SYN**
- **SYN/ACK**
- **ACK**

|![3-Way handshake](transportni-vrstva/handshake.PNG "3-Way handshake")|
|:--:|
|*obr. 2:* 3-Way handshake|
|*Převzato z:* <https://www.geeksforgeeks.org/tcp-3-way-handshake-process/>|

1. **SYN**
   - Klient chce navázat spojení. Klient tedy odesílá segment, který obsahuje **Synchronize Sequence Number**, jež druhé straně říká, že někdo chce spojení navázat a **jakým sekvenčním číslem budou segmenty začínat**.
2. **SYN/ACK**
   - Příjemce **SYN** segmentu odpovídá odesílateli **s pozitivním ACK příznakem** a **sekvenčním číslem**, kterým chce segmenty začínat.
3. **ACK**
   - Příjemce **SYN/ACK** odesílá potvrzující zprávu. Po jejím přijetí druhou stranou může začít přenos dat.

Při samotném přenosu je zajištěna **spolehlivá komunikace** pomocí **Positive Acknowledgement with Re-transmission (PAR)**. Princip spočívá v tom, že odesílatel posílá ten a samý segment do té doby, dokud neobdrží od příjemce **acknowledgement**.  Pokud dorazili data v pořádku, tak ho dostane. Ale pokud jsou data na straně příjemce poškozena (kontrola pomocí **kontrolního součtu**), tak příjemce segment zahodí a **neposílá acknowledgement**. Odesílatel tedy znovu posílá segment, na který **acknowledgement** nedostal, dokud ho nedostane.

<div class="note">

### Ukončování spojení

Pro ukončení spojení se využívá **4-way handshake**. Skládá se ze 4 částí.

1. Jeden z účastníků spojení posílá segment, ve kterém je **FIN** příznak nastavený na 1. Přeje si ukončit spojení.
2. Příjemce segmentu s **FIN** příznakem odesílá segment s **ACK** příznakem. Odesílatel **FIN** příznaku je nyní ve fází, kdy přijal **ACK** na jeho žádost o ukončení spojení a čeká.
3. Příjemce původního **FIN** příznaku nyní odesílá segment s **FIN příznakem** jako že si tedy také přeje ukončit spojení.
4.  Odesílatel původního **FIN** příznaku odesílá **ACK** příznak a spojení je ukončeno.

Body 2 a 3 se mohou zdát, že by mohli být spojené do jednoho -> poslat **FIN** a **ACK** příznak najednou. To ale nelze z důvodů toho, že příjemce iniciální zprávy s **FIN** příznakem můž mít ještě nějaká data k odeslání. Před ukončením celého spojení by bylo vhodné tyto data odeslat, aby nedošlo ke ztrátě packetů.

</div>

### Výhody a nevýhody TCP

**Výhody:**

- bezpečný způsob přenosu dat -> vyžaduje potvrzení doručení každé zprávy, umožňuje přijmutí zpráv ve správném pořadí
- může být použít ve velkém množství odvětvích přenosu dat po síti
- může komunikovat s velkou škálou protokolů

**Nevýhody:**

- spojení je časově náročné -> každá zpráva vyžaduje potvrzení a ukončení spojení není okamžitý proces

## UDP (User Datagram Protocol)

### Základní popis

UDP přenáší série bitů, které mají určité uspořádání. **UDP** přidává k přenášeným datům vlastní **hlavičku**, která je o proti TCP mnohem jednodušší. Takto přenášenému packetu pomocí UDP se říká **datagram**.

Na rozdíl od TCP je **UDP** takzvaně **nespolehlivý protokol**. K navázání spojení nepotřebuje žádnou odezvu od druhé strany. Vlastně nemůžeme úplně mluvit o **navázání spojení**, protože UDP protokolu je jedno, zdali **datagram** příjemci dorazí.

**UDP**, stejně jako TCP, používá k určení cíle zprávy **porty**. TCP a UDP mají svá číslování portů. Při komunikaci skrze **UDP** mluvíme o **UDP portech**. 

### UDP datagram

Stejně jako u TCP, je **UDP datagram** složený z **dat a hlavičky**.

|![Struktura UDP datagramu](transportni-vrstva/datagram.PNG "Struktura UDP datagramu")|
|:--:|
|*obr. 3:* Struktura UDP datagramu|
|*Převzato z:* <https://cs.wikipedia.org/wiki/User_Datagram_Protocol>|

Kromě přenášených **dat** je součástí **hlavička**, která obsahuje 4 pole, z čehož dvě jsou volitelná.

- **Zdrojový port**
  - volitelný, pokud odesílatel neočekává odpověď na svoji zprávu, je hodnota nastavena na 0
- **Cílový port**
- **Délka UDP packetu (datagramu)**
  - obsahuje hodnotu určující velikost **UDP datagramu**, včetně hlavičky, minimální hodnota je 8 bajtů
- **Kontrolní součet**
  - hodnota určující správnost přijatého packetu na straně příjemce
  - tato hodnota je volitelná, ale téměř vždy se používá
  - pokud příjemce obdrží **datagram** a po přepočtení **kontrolního součtu** se hodnoty neshodují, je **datagram** příjemcem zahozen
  - protože používáme **UDP**, odesílatel to nikdy nezjistí

### Výhody a nevýhoy

**Výhody:**

- UDP nepotřebuje navazovat či udržovat spojení.
- UDP používá malou velikost packetů (datagramů). Zpracování celého packetu je tedy mnohem rychlejší, než v případě UDP.
- Může být použít pokud je zde pouze jeden packet, který je třeba přenést mezi klienty. Boradcast a multicast jsou dostupné pomocí UDP.
- Kotrola příjmání/odesílání zpráv může být ponechána na uživatelských programech a může být implementována pouze v takové míře, v jaké je potřeba.

**Nevýhody:**

- Nemáme jistotu, že data jsou doručena.
- Nemáme jistotu, že data jsou přijímána ve stejném pořadí, jako jsou odeslána (TCP má toto zajištěné pomocí **sekvenčního čísla**).
- Implementace **flow controll** (vzájemné komunikace) je čistě na uživatelské aplikaci (což může být i plus) -> UDP neposkytuje žádnou garanci. Packet mohl být doručen, ale i nemusel. Mohl být doručen dvakrát, packety mohli dorazit v chybném pořadí. Odesílatel nic z toho neví, pokud se mu to naslouchající program na druhé straně neuvolí říct (posláním UDP zprávy, například). 

## Pseudo IPv4

Počet IPv4 adres je značně omezený oproti IPv6 adresám. Některé služby využívají čistě IPv4 a klienti mohou mít problémy s připojením skrze IPv6. 

Protokol byl vytvořený jako **lightweight** alternativa k IPv4 a IPv6 protokolům, **podobně jako je UDP alternativa k TCP** -> UDP neposkytuje téměř žádné transportní služby, na rozdíl od TCP. Pseudo IPv4 si tedy klade za cíl poskytovat minimum síťových služeb.

**Pseudo IPv4 je síťový protokol, který mapuje IPv6 adresy na IPv4 adresy třídy E - (240.0.0.0 - 255.255.255.255)**. Toto nám dává 268,435,456 (250 milionů) možných IPv4 adres. Je to sice málo oproti 340 undecillionům možných IPv6 adres, ale pořád dost velké množství, které nevystačí jen těm největším webovým stránkám.

<div class="note">

### Třída E

Třída E se povětšinou nepoužívá, pouze ke specifickým účelům, je tedy ideální pro pokrytí potřeb převodu IPv6 na IPv4.

</div>

### Princip mapování

Pseudo IPv4 používá **hashovací algoritmus**, který vezme prvním 64 bitů IPv6 adresy a převede je na IPv4 adresu třídy E.  Tento algoritmus má pro stejný vstup vždy ten samý výstup, konkrétní IPv6 adresa je tedy vždy přemapována na stejnou pseudo IPv4 adresu.

## Zdroje

- Transmission Control Protocol. *Wikipedie: Otevřená Encyklopedie* [online] @2022 [citováno: 14.05.2023] <br> Dostupné z: <https://cs.wikipedia.org/wiki/Transmission_Control_Protocol>
- Transmission Control Protocol (TCP). *IBM* [online] @2010 [citováno: 14.05.2023] <br> Dostupné z: <https://www.ibm.com/docs/en/zos-basic-skills?topic=4-transmission-control-protocol-tcp>
- TCP 3-Way Handshake Process. *GeeksForGeeks* [online] @2022 [citováno: 14.05.2023] <br> Dostupné z: <https://www.geeksforgeeks.org/tcp-3-way-handshake-process/>
- User Datagram Protocol. *Wikipedie: Otevřená Encyklopedie* [online] @2021 [citováno: 14.05.2023] <br> Dostupné z: <https://cs.wikipedia.org/wiki/User_Datagram_Protocol>
- Pseudo-IP: Providing a Thin Network Layer Protocol for Semi-Intelligent Wireless Devices. *Kevin C. Almeroth* [online] @2014 [citováno: 14.05.2023] <br> Dostupné z: <http://www.nmsl.cs.ucsb.edu/papers/DARPA-98.pdf>
- Eliminating the last reasons to not enable IPv6. *Matthew Prince* [online] @2014 [citováno: 14.05.2023] <br> Dostupné z: <https://blog.cloudflare.com/eliminating-the-last-reasons-to-not-enable-ipv6/>
