# Síťová (Internetová) vrstva IPv4

> (adresa, hlavička, maska, unicast, multicast, broadcast, zápisy, distribuce IP); Základy směrování (popis, princip, TTL, gateway, Djikstra & Belmann-Ford)

Jedná se o protokol síťové vrstvy využívající další služební protokoly **ARP**, **RARP**, **ICMP**, **IGMP** a **IPsec**. 

Tento protokol přepravuje data bez záruky. Záruku a kontrolu integrity přenechává na vyšší transportní vrstvě TCP.

**RARP** je v dnešní době nahrazen protokolem [DHCP](https://atlas144.codeberg.page/szz/ikt/ps/aplikacni-vrstva.html#DHCP).

## IP Datagram

IP datagram se skládá ze **ZÁHLAVÍ** a **Datové části**. Obě tyto části se vloží do datové části **Linkového rámce**.

Formát IP datagramu vypadá následovně:

|![IP datagram)](sitova-(internetova)-vrstva-ipv4/IPV4_datagram.png "IP datagram")|
|:--:|
|obr. 1: IP datagram|
| zdroj: Přispěvatelé Wikipedie, IPv4 [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 29. 04. 2023, 17:57 UTC, [citováno 17. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=IPv4&oldid=22749106>|

1. verze protokolu = (0x4)
2. **IHL** - délka záhlaví
3. **typ služby** - klasifikace paketu (způsob a rychlost dopravy do místa určení) a ECN (co s paketem v případě zahlcení sítě - zda pakety zahodit)
4. **celková délka** - délka datagramu v bajtech (zde 2B)
5. **identifikace** - každý datagram má jednoznačný identifikátor, je-li datagram fragmentován, pozná se, které fragmenty patří k sobě
6. **příznaky** - řízení fragmentace, první fragment je vždy 0, druhý se značí DF (do not fragment) a třetí MF (more fragments); poslední fragment nastavuje zda není fragmentem posledním
7. **offset fragmentu** - udává na jaké pozici v původním datagramu fragment začíná
8. **TTL** - každý směrovač snižuje o 1, poslední směrovač, který sníží hodnotu na 0 DTG zahodí a pošle protokolem ICMP zprávu o zahození (poslední směrovač vrací echo request)
9. **číslo protokolu** - určuje jakému protokolu vyšší vrstvy se mají data předat
10. **kontrolní součet hlavičky** - ověří zda datagram není poškozený, pokud ano, je DTG zahozen
11. **zdrojová adresa** - adresa odesílatele (32 bitů)
12. **cílová adresa** - IP adresa rozhraní, pro které je DTG určen
13. nepovinné položky záhlaví

### Fragmentace DTG

|![Fragmentace paketu](sitova-(internetova)-vrstva-ipv4/fragmentace.png "Fragmentace paketu")|
|:--:|
|obr. 2: Fragmentace paketu|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 06 IP IPv4*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=8PRkgDOHfkI>|

Ethernet umí transportovat pouze pakety o velikosti 1500 (MTU). Každý fragment má svoje záhlaví, ale mění se offset, posléze délka a příznak u jednotlivých fragmentů.

#### IGMP

Protokol pro **skupinové oběžníky** (určeny jen pro některé stanice) na LAN. Protokol slouží jen k tomu, jak se má oběžník šířit po mé lokální síti.

3 typy paketu: 
- **typ 16**, ten posílá počítač na svůj směrovač s požadavkem na členství ve skupině - odběr multicastu (třeba radio); 
- **typ 17** - opuštění členství ve skupině; 
- **typ 11** - dotaz směrovače do LAN zda ještě někdo nechce poslouchat radio (multicast);

## IP adresa, Maska, síť, sub-síť a supersíť

**IP adresa** 
- jednoznačně identifikuje každý interface v síti, která používá IP protokol 
- skládá se vždy ze dvou částí:
  - adresa sítě (případně podsítě)
  - adresa rozhraní
- 4B velikost (32 bitů)
- historicky rozdělené do tříd A, B, C, D, E
  - označovali kolik bitů je určeno pro síť
- pro intranety máme speciální adresy

|TŘÍDA|ADRESA|ROZSAH|
|---|---|---|
|Třída A|10.0.0.0/8|10.0.0.0 až 10.255.255.255|
|Třída B|172.16.0.0/12|172.16.0.0 až 172.31.255.255|
|Třída C|192.168.0.0/16|192.168.0.0 až 192.168.255.255|

|![Třídy IP](sitova-(internetova)-vrstva-ipv4/tridyIP.PNG "Třídy IP")|
|:--:|
|obr. 3: Třídy IP|
| zdroj: Přispěvatelé Wikipedie, Třídy IP adres [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 8. 03. 2023, 10:32 UTC, [citováno 18. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=T%C5%99%C3%ADdy_IP_adres&oldid=22526161>|

**Distribuce IP**
- na vrchu pyramidy je organizace IANA, která dohlíží na přidělování IP
  - IANA alokuje bloky adres do **Regional Internet Registry**
  - svět je rozdělen na oblasti a každá **RIR** přiděluje IP ve své oblasti směrem k **Internet Service Providerům** (ISP)

|![Regional Internet Registry](sitova-(internetova)-vrstva-ipv4/distribuce.png "Regional Internet Registry")|
|:--:|
|obr. 4: Regional Internet Registry|
| zdroj: In: Juniper Networks: BGP Origin Validation Using Resource Public Key Infrastructure [online]. US, 2020 [cit. 2023-05-18]. Dostupné z: <https://www.juniper.net/documentation/en_US/release-independent/nce/topics/concept/nce-187-bgp-rpki-technical-overview.html>|

**Typy adres**
- **unicast** - odesílatel adresuje pro konkrétní interface
- **multicast** - odesílatel adresuje skupině adresátů
- **broadcast** - adresují se všichni adresáti v LAN
  - funguje pouze u LAN
  - funguje pouze u IPV4
- **anycast** - adrsuje se více interfacům, ale DTG stačí doručit jen jednomu
  - funguje pouze u IPV6

**Unicast**
- jednoznačná adresa interface (16 bajtů)
  - adresa sítě (64 bitů)
  - adresa interface (64 bitů)
- 2 typy:
  - globálně jednoznačné
  - lokálně jednoznačné

|![Unicast](sitova-(internetova)-vrstva-ipv4/unicast.PNG "Unicast")|
|:--:|
|obr. 5: Unicast|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 08 IP adresa*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=hh7DImZB60A>|

**Multicast**
- metoda přeposílání IP DTG z jednoho zdroje více adresátům
- místo odeslání jednotlivých DTG, je odeslán pouze jediný DTG, jediným datovým tokem, přes směrovače 
- pro účely multimédií a nebo oprav chyb OS apod.
- cílem je zmenšení zátěže vysílajícího uzlu a přenosové sítě

**Přenos dat v multicast**
- zařízení v síti se přihlásí do některé ze skupin - Multicast Group - pro zachytávání dat (třeba sledování videa)
- router sítě musí multicast podporovat
  - pokud podporuje, tak v sobě udržuje tabulku skupin
  - router se periodicky dotazuje do LAN, zda existuje stanice, která chce informace ze skupiny odebírat
  - router také naslouchá stanicím, které mu přes **IGMP protokol** pošlou zprávu "Membership report" s **IP adresou třídy D** (přidají se do skupiny) nebo "Leave Group"

**Broadcast**
- šíření informace pro všechny interface
- sdílení v síti MS, nebo identifikace zařízení
- typy broadcast
  - **MAC broadcast**
    - MAC adresa ve tvaru FF:FF:FF:FF:FF:FF
  - **IP broadcast**
    - IP adresa broadcastu je 192.168.0.255
  - **síťový broadcast**
    - IP DTG odeslán na adresu, která je v cílové síti broadcastem

**Maska sítě**
 - říká nám do které sítě IP adresa spadá
 - rozdělí IP na dvě části, adresu sítě a adresu interfacu na této síti
 - **255.255.255**.0 - část určena pro síť
 - 255.255.255.**0** - část určena pro zařízení v síti
 - lze zapsat i takto **IP/24**
 - část za lomítkem zachycuje počet bitů s log. jedničkou MASKY zleva, nebo také, kolik log. jedniček máme (do maximální hodnoty 32) pro jednotlivá zařízení 

**Adresa sítě**
- zjistíme ji log. operací **AND** IP adresy a Masky sítě

**Supersíť a sub-síť**
- supersíť může zahrnovat naši síť
- maska má méně bitů s log. jedničkami (tj. více prostoru pro interfejs -> naši síť)
- sub-síť pod naší sítí má naopak masku s větším počtem log. jedniček (je tam méně prostoru pro interfejsi)

## Směrování

Problém je založen na tom, že chceme-li z místní sítě odeslat paket na druhou stranu světa, nebo přes ulici, tak směrovače (routery) se musí rozhodnout, jakou linkou data vedou k adresátovi.

    Cílem je doručit datagram co nejefektivněji k adresátovi.

Jelikož jsou cesty k adresátům vzhledem k velikosti sítě složité, řeší se pouze vždy jeden krok, a tedy, komu datagram předat jako dalšímu. Je to obdobné jako jízda autem, na křižovatce se rozhodnu dle značení, kam odbočím a na další křižovatce se opět rozhoduji dle značení. Nemám na první křižovatce všechna značení až do cílové cesty - křižovatka by byla přehlcená. Totéž by platilo o směrovači, kdyby měl uchovávat všechna data pro celou cestu až k adresátovi.

**Směrovací protokoly**
1. **Interior Gateway Protocol (L1)**
   - [IS-IS](https://cs.wikipedia.org/wiki/Intermediate_System_to_Intermediate_System) (linková vrstva)
   - [OSPF](https://cs.wikipedia.org/wiki/Open_Shortest_Path_First) (zapouzdřený v IP)
2. **Interior Gateway Protocol v2 (L2)**
   - [RIP](https://cs.wikipedia.org/wiki/Routing_Information_Protocol)
   - [RIPv2](https://cs.wikipedia.org/wiki/Routing_Information_Protocol)
   - IGRP 
3. **Exterior Gateway Protocol**
   - [BGP](https://cs.wikipedia.org/wiki/Border_Gateway_Protocol)
   - Path Vector Routing Protocol

**Směrovací tabulka**
- uložena ve směrovači
- obsahuje seznam cílových sítí a rozhraní, za nimiž se sítě nachází

1. adresa cílové stanice se vynásobí maskou
2. porovná se s hodnotou v prvním sloupci, pokud odpovídá adrese sítě, nacpe se DTG do daného rozhraním (uvedený v témže řádku)
3. pokud adresa neodpovídá, postoupíme o řádek níže

|![Routovací tabluka](sitova-(internetova)-vrstva-ipv4/tabulka.png "Routovací tabulka")|
|:--:|
|obr. 6: Routovací tabulka|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 09 IP Routing*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=i5AJ4EdInWg>|

- poslední řádek tabulky označuje **default gateway**
- **Metrika** nám říká, která z cest se zvolí, pokud budeme mít dvě stejné cesty
- v OS (WIN) vypíšeme RT pomocí příkazu: *netstat -r*

Do RT se adresy dostanou buď:
- ručně
- dynamicky (směrovacím protokolem)
- tím, že daná stanice má nakonfigurovanou adresu a masku administrátorem (nejběžnější)

### RVP - routing vector protocol (Bellman-Ford algoritmus) 

Směrovací protokol funguje následovně:

1. řekněme, že se zapnou všech a zařízení v síti
   - routery mají nakonfigurované interface
   - počáteční konfigurace vytvoří položky v RT
2. směrovače si se svými sousedy vymění RT
3. RT na jednotlivých směrovačích se doplní o hodnoty ze sousedních směrovačů
    - zvětší se o 1 metrika u nově obdržených položek
4. opět si vymění informace - dle RT již nesousedí s prvním prvkem vedle, ale se vzdálenějším (v korku 3 se totiž doplnili o blízkých sousedech info)
   - metrika se opět zvýší u nových hodnot o 1
5. opět si směrovače vymění RT a všichni mají info o všech

**Každé položce v RT se říká vektor, proto RVP.**

**Problém** RVP spočívá v tom, že vypadne-li spojení mezi sítí a směrovačem, tak první směrovač u dané sítě ví, že spojení je přerušeno, tak si položku o dané síti vymaže. Nicméně, druhý směrovač měl přístup do té samé sítě, ale s metrikou dvě, takže v další iteraci pošle tomu prvnímu směrovači záznam o předmětné síti a ten první směrovač si položku uloží, ale protože hje pro něj (jakoby) nová, tak ji nastaví metriku 3. 

Tím pádem si první směrovač myslí, že za směrovačem 2 je síť (pro kterou má tento první směrovač přiřazenou metriku 3), ale ve skutečnosti se jedná o onu zrušenou síť. Takto se záznam neustále cyklí a zvyšuje se metrika.

Například protokol **RIP** nastavuje limit pro metriku, je-li metrika sítě **15** považuje se za nedostupnou.

### LSP - link state protocols a Dijkstra

Řeší problémy RVP protokolů. LSP jsou založeny na [Dijkstrově algoritmu](https://www.youtube.com/watch?v=pVfj6mxhdMw) (hledání nejkratší cesty).

- uzavřená oblast
- routery vytvoří graf
- každý směrovač hleda shortest path pro DTG
- linky mezi Routery jsou hrany grafu
- každá linka je hodnocena metrikou

1. router zjistí svoje sousedy (určí hrany a dle **PINGU** ohodnotí metrikou)
2. router rozešle info z kroku jedna všem sousedům v dané oblasti
3. každý uzel v oblasti zná kompletní topologii grafu
4. každý uzel si doplní do RT nejkratší cestu k dalšímu uzlu (k jeho lokální síti)

### OSPF směrování

Open short path first je **nejpoužívanějším protokolem** pro směrování uvnitř autonomních systémů. Nevýhodou je, že má jasně danou topologii.

Základem topologie je **páteřní síť** (značí se 0.0.0.0 - 4B, ale jen připomínají IP) k níž se připojují další oblasti.

**Pahýlová oblast** má pouze jeden hraniční směrovač s páteřní oblastí. 

|![OSPF topologie](sitova-(internetova)-vrstva-ipv4/OSPF.png "OSPF topologie")|
|:--:|
|obr. 7: OSPF topologie|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 09 IP Routing*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=i5AJ4EdInWg>|

**Princip**\
OSPF je založen na tom, že každý Router zná topologii celé sítě (oblasti). Směrování má 3 kroky:

1. **Správa sousedských relací**
   - relace mezi dvěma směrovači z jiných oblastí (sdílí L2 topologii)
2. **Šíření směrových informací**
   - změnou se rozumí vznik nebo zánik sousedské relace
   - routery uvnitř oblasti ví vše
   - routery mimo oblast pouze případnou změnu metriky nebo směru
3. Určení nejkratší cesty (Dijsktra)

**Autonomní oblast**
- v její rámci probíhá směrování pomocí interior gateway protokolů (viz výše). Pro externí komunikaci slouží exterior gateway protocol. Na hranici AS je tzv. **hraniční router**.

Hraniční router ve své RT nese dva typy informací:
- z interior protokolu
- z exterior protokolu

Poslání infa do další AS se nazývá právě **Šíření směrových informací**. 

|![Komunikace AS to AS](sitova-(internetova)-vrstva-ipv4/ASTOAS.png "Komunikace AS to AS")|
|:--:|
|obr. 8: Komunikace AS to AS|
| zdroj: DOSTÁLEK, L. Youtube. *Počítačové sítě 09 IP Routing*.[online] [cit. 16-05-2023] Dostupné z: <https://www.youtube.com/watch?v=i5AJ4EdInWg>|

#### Pojem Gateway a Default Gateway

**Gateway**
- název síťového uzlu
- propojuje dvě sítě pracující s odlišnými komunikačními protokoly
- vykonává i funkci routeru
- v hierarchii aktivních zařízení má nejvyšší postavení - stojí i nad směrovačem

**Default Gateway**
- tzv. implicitní brána - označuje router, přes který se stanice připojují do vnější sítě

## ZDROJE

- https://www.youtube.com/watch?v=i5AJ4EdInWg
- https://www.youtube.com/watch?v=hh7DImZB60A
- https://www.youtube.com/watch?v=U5W8-gGblXs
- https://www.youtube.com/watch?v=xr1u3LUwcek
- https://www.youtube.com/watch?v=8PRkgDOHfkI
- https://cs.wikipedia.org/wiki/Sm%C4%9Brov%C3%A1n%C3%AD
- https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
- https://cs.wikipedia.org/wiki/Gateway
- https://cs.wikipedia.org/wiki/Autonomn%C3%AD_syst%C3%A9m






















