# Paměťový podsystém počítače

> Typy pamětí a jejich hierarchie. Skrytá paměť (cache), princip činnosti, konstrukce a vliv na rychlost výpočtu. Virtuální adresní prostor, překlad virtuální adresy na fyzickou metodou stránkování. TLB (Translation lookaside buffer).

## Typy pamětí

### Registry

Registry jsou tou **nejdůležitější pamětí počítače**. Nacházejí se **uvnitř procesoru**, který je využívá při prakticky veškeré své činnosti. Jsou **nejrychlejší**, ale zároveň **nejmenší** pamětí v počítači. Máme dva základní druhy registrů:

- **obecné**
  - slouží k ukládání **operandů a výsledků operací** procesoru (tedy dat) nebo **paměťových adres** (někdy jsou datová a adresní registry odděleny, jindy se pro obojí používá jedna sada)
  - můžeme s nimi programově pracovat
  - jsou jich obvykle desítky a mají **velikost jednoho slova** (odpovídá počtu bitů architektury), ale většinou lze přistupovat i k jejich částem (např. pro 64 b jen k 32 nebo 16 b)
  - první z nich (**RAX**) měl historicky speciální postavení, nazýval se **akumulátor** a používal se k ukládání výsledků aritmetických operací (bal tedy vytížen mnohem více než ostatní)
- **speciální**
  - mají konkrétní účel a jejich hodnotu obvykle nemůžeme sami v programu měnit, pouze ji číst
  - **Instrukční registr (IR)**
    - drží instrukci, která bude provedena v rámci instrukčního cyklu
    - v programu nepřístupný
  - **Program Counter (PC)**
    - drží adresu instrukce právě načtené v *IR*
    - mění se obvykle o délku právě vykonané instrukce
    - můžeme jej měnit pomocí skokových instrukcí (`JUMP`)
  - **Program Status Word (PSW)**
    - udržuje stavové informace a příznaky (flags) procesoru
    - někdy též nazýván **FLAGS**
    - nejběžnější příznaky:
      - **Z (zero)** - výsledek operace je nulový
      - **C (carry)** - výsledek aritmetické operace způsobil přetečení z nejvyššího řádu, často se užívá také v posuvech
      - **V (overflow)** - výsledek aritmetické operace v *doplňkovém kódu* je mimo rozsah (např. v osmibitovém registru `127 + 1` nebo `-127 - 2`).
      - **N (negative)** - výsledek je záporný
      - **I (interrupt)** - příznak povolení přerušení (1 povoleno, 0 - zakázáno); nastavuje se programově
    - **Stack pointer (SP)**
      - udržuje adresu vrcholu zásobníku (tedy jeho elementu, se kterým se bude pracovat příště)

|![Hlavní část registrů pro architekturu AMD64](pametovy-podsystem-pocitace/reg.png "Hlavní část registrů pro architekturu AMD64")|
|:--:|
|*obr. 1:* Hlavní část registrů pro architekturu AMD64 (SSE registry slouží pro SIMD operace nad daty v plovoucí řádové čárce)|
| *zdroj:* SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019. |

### Cache

Paměť typu cache je vzhledem ke struktuře otázky popsána [níže](#skrytá-paměť-cache).

### Hlavní paměť

*Hlavní*, nebo též **operační paměť** slouží k ukládání jak instrukcí, tak dat spuštěných úloh, stejně jako samotného operačního systému. **Nejedná se o trvalé ukládání**, drží se pouze po dobu činnosti počítače (když je zapnutý). Jde o paměť typu **RAM** (Random Access Memory), což znamená, že v kteroukoliv chvíli lze přistoupit k jakékoliv její části. 

*Hlavní paměť* je adresována nejčastěji po **bytech** (může být i povětších sekcích). K vývěru paměťové buňky, na kterou chceme přistoupit slouží **adresová sběrnice**. Ta má velikost odpovídající počtu bitů dané architektury (např. 64 b). Tím je také omezena maximální velikost paměti, protože můžeme adresovat maximálně \\( 2^n \\) paměťových buněk, kde \\( n \\) je počet bitů sběrnice.

Operační systém řídí to, jakým způsobem se s pamětí nakládá, jak se přiděluje a uvolňuje, atd. Ve většině systémů mu k tomu slouží **MMU** (Memory Management Unit). Principy nakládání s pamětí jsou blíže popsány v otázkách [2.3.2](https://atlas144.codeberg.page/szz/ikt/os/metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci.html) a [2.3.3](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html).

|![Základní činnost hlavní paměti](pametovy-podsystem-pocitace/ram.png "Základní činnost hlavní paměti")|
|:--:|
|*obr. 2:* Základní činnost hlavní paměti|
| *zdroj:* SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019. |

Význam jednotlivých vstupů na obrázku je následující:

- **CS** (Chip Select) - říká RAM, že bude docházet ke čtení nebo zápisu (startuje činnost); řadič začne číst adresu
- **WR** - indikuje zápis (*data* ze sběrnice se začnou zapisovat na pozici *adresy*)
- **OE** - indikuje čtení (*data* z *adresy* se zapíší na sběrnici)

Máme dva základní druhy *RAM*:

- **statická (SRAM)**
  - paměťové buňky jsou realizovány jako **BKO** (bistabilní klopný obvod) typu *CMOS*
  - **cache** je často tvořena *SRAM*
  - má skvělé vlastnosti:
    - nízkou spotřebu
    - minimální přístupovou dobu
    - vysokou odolnost proti šumu
  - je však **velmi drahá**, a proto se vyplatí pouze pro malé paměti (max desítky MB)
- **dynamická (DRAM)**
  - buňky jsou realizovány **parazitními kapacitami** (funkčně ekvivalent **kondenzátoru**)
  - jsou mnohem *levnější* a *jednodušší na výrobu*
  - mají však výrazně horší všechny vlastnosti
  - velkou nevýhodou je **nutnost periodicky obnovovat náboj buněk** (říká se tomu **refresh**)
  - v klasických počítačích se používají jako *hlavní paměť* takřka výhradně
  - typů *DRAM* existovala historicky celá řada:
    - SIMM
    - DIMM
      - SDR (SDRAM)
      - DDR
        - **DDR1...5**
    - SO-DIM
    - RIMM (RDRAM)

#### Čtení z hlavní paměti

1. na adresní sběrnici se **vystaví adresa**
2. **započne** čtecí **cyklus** (vyšle se čtecí signál)
3. na datovou sběrnici se **vystaví data**
4. **ukončí** se čtecí **cyklus** (čtecí signál se vrátí do neaktivní hodnoty)

Pro urychlení čtení se hlavní paměť (pouze u DDR) dělí na tzv. **banky**, které mohou pracovat nezávisle, tedy je možno z nich **číst paralelně**. Jejich rozhraní (adresová a datová sběrnice, atd.) jsou však společné, požadavky je tedy nutno **zadávat postupně**. Zadávání je ale mnohem rychlejší, než samotné čtení, díky čemuž tato metoda i tak přináší výrazné zrychlení.

#### Zápis do hlavní paměti

1. na adresní sběrnici se **vystaví adresa**
2. na datovou sběrnici se **vystaví data**
3. **započne** zápisový **cyklus** (vyšle se zápisový signál)
4. **ukončí** se zápisový **cyklus** (zápisový signál se vrátí do neaktivní hodnoty)

|![Přenos dat mezi CPU a hlavní pamětí](pametovy-podsystem-pocitace/cpu-ram.png "Přenos dat mezi CPU a hlavní pamětí")|
|:--:|
|*obr. 3:* Přenos dat mezi CPU a hlavní pamětí|
| *zdroj:* SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019. |

### Sekundární paměť

Slouží k **trvalému ukládání dat** (je nevolatilní), tedy i po vypnutí počítače. Může být:

- **interní** - přímo součást PC; je zde uložen OS a data uživatelů
- **externí** - připojuje se podle potřeby; často se užívá pro přenos mezi zařízeními

Jako interní se obvykle používají:

- **HDD** (Hard Disk Drive)
  - mechanická komponenta
  - pomalejší (neexistuje zde přímý přístup, *plotny* se musí správně natočit a *hlava* nastavit)
  - zatím stále nižší cena na jednotku paměti (oproti *SSD*), ale postupně se vyrovnávají
  - HDD má následující součásti:
    - **hlavy** - čtecí zařízení
    - **plotny (desky)** - kovové disky, na které se zmagnetizováním konkrétních míst ukládají data
    - **stopy** - soustředné kružnice na *plotnách*
    - **sektory**
      - výseče kruhu (jednotlivé paměťové bloky)
      - čím dále od středu, tím více sektorů ve *stopě*
    - **cylindry** - množina stop nad sebou
- **SSD** (Solid State Drive)
  - polovodičová paměť (NAND flash)
  - přístup k paměti je přímý
  - mnohem rychlejší než *HDD*
  - data se po dlouhé době bez napájení začínají ztrácet
  - počet zápisů je omezen (avšak často odejde řídicí elektronika dříve než podstatná část paměťových buněk)

|![Vnitřní struktura HDD](pametovy-podsystem-pocitace/hdd.jpeg "Vnitřní struktura HDD")|
|:--:|
|*obr. 4:* Vnitřní struktura HDD|
| *zdroj:* <https://www.cs.uic.edu/~jbell/CourseNotes/OperatingSystems/10_MassStorage.html> |

Externí paměti jsou obvykle variacemi **ROM** (Read Only Memory). Dnešní typy *ROM* už obvykle umožňují i zápis, ale stále je zde omezení počtu přepisů. Běžné druhy jsou:

- **ROM** - obsah je "vypálen" od výrobce (např. jednoúčelové čipy, předzapsané CD-ROM, ...)
- **PROM** (Programable ROM) - obsah může zapsat uživatel, ale pouze jednou (CD-ROM, DVD-ROM, ...)
- **EPROM** (Erasable PROM)
  - obsah lze i smazat (nejčastěji pomocí UV světla do speciálního okénka)
  - velmi nízký počet přepisů
  - už se nepoužívají
- **EEPROM** (Electrically EPROM)
  - lze vymazat elektricky
  - většinou stovky až tisíce přepisů
  - např. pro uchování hodnot po vypnutí mikrokontroléru
- **Flash**
  - vysoká kapacita a rychlost RW
  - stovky tisíc přepisů

## Hierarchie pamětí

*Hierarchie paměti* počítače je z pohledu rychlosti (od nejrychlejší) a celkově postupu činnosti následující:

1. registry
2. skrytá paměť
3. hlavní paměť
4. sekundární paměť

|![Hierarchie paměti](pametovy-podsystem-pocitace/hierarchie.png "Hierarchie paměti")|
|:--:|
|*obr. 5:* Hierarchie paměti (zleva doprava)|
| *zdroj:* SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019. |

## Skrytá paměť (cache)

**Druhým nejrychlejším** typem paměti je tzv. *skrytá paměť*, neboli *cache*. Jejím primárním účelem je **vyrovnávat rozdíly mezi rychlostí přístupu k registrům** v procesoru (a obecně rychlosti jeho činnosti) **a rychlostí přístupu k hlavní paměti** počítače (která je mnohonásobně pomalejší). Z pohledu **velikosti** leží cache, stejně jako u rychlosti, **mezi registry a hlavní pamětí**. Cache je navržena tak, aby procesor nepoznal, jestli komunikuje s ní, nebo s hlavní pamětí, je tedy **transparentní**.

Z funkčního hlediska lze činnost cache shrnout tak, že při prvním přístupu k datům musím procesor čekat na hlavní paměť, avšak při dalších přístupech je bržděn mnohem méně (protože už přistupuje pouze do cache).

Cache bývá obvykle **třístupňová** (**L1**, **L2**, **L3**), díky tomu je vyrovnávání pozvolnější. Platí, že **čím blíž** je stupeň **k procesoru**, tím je **rychlejší**, ale má **menší kapacitu**.

| Vrstva | Velikost | Rychlost |
| --- | --- | --- |
| L1 | ~ 16 kB | podobná rychlosti činnosti procesoru |
| L2 | ~ 1 MB | pomalejší |
| L3 | ~ jednotky až desítky MB | ještě pomalejší |

#### Princip činnosti

Procesor potřebuje získat data z *hlavní paměti*, zadá tedy požadavek. Ten však nejde přímo do *hlavní paměti*, ale nejprve do *cache* (**L1**). Tam dojde ke zjištění, zdali tato data obsahuje. Pokud ano, nazývá se to **cache HIT**, data jsou přečtena z cache a požadavek je dokončen. Pokud data neobsahuje, nazýváme to **cache MISS**, pokračuje požadavek k dalším vrstvám cache a pokud ani tam není úspěšný, dojde až do *hlavní paměti* a data jsou vyčtena odtamtud. V tomto případě také dochází k **zápisu vyčtených dat do cache**, pro urychlení příštího čtení.

|![Princip činnosti cache](pametovy-podsystem-pocitace/cache.png "Princip činnosti cache")|
|:--:|
|*obr. 6:* Princip činnosti cache|
| *zdroj:* SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019. |

Z popisu výše je patrné, že je důležité, aby cache **obsahovala především ty části paměti**, které jsou **častou využívány**. To je potřeba řešit při odebírání položek (ke kterému dochází při přidávání nových) a máme k tomu různé postupy:

- **náhodný výběr** - náhodně se zvolí položka z řádku, do kterého zapisujeme
- **FIFO** - odebírá se nejstarší položka
- **LFU** (Least Frequently Used) - každá položka má svůj čítač; vždy když se položka použije, ostatní čítače se inkrementují (odstraňuje se nejvyšší)

#### Konstrukce a vliv na rychlost výpočtu

*Cache* paměti mohou být konstruovány třemi základními způsoby (podle jejich asociativnosti):

- **plně asociativní**
  - v tomto případě mohou být paměťové bloky (z hlavní paměti) uloženy kdekoliv (v kterémkoliv řádku) v *cache*
  - toto je nejjednodušší na provedení, ale následné hledání požadovaného bloku je extrémně náročné (musí se projít celá paměť, řádek po řádku)
- **přímo adresovaná**
  - má stupeň asociativity **= 1**
  - toto je opačný způsob, zde má každý blok přesně dané jedno místo (jeden řádek), kam může být uložen
  - zde je naopak prohledávání velmi jednoduché, ale nelze zde optimálně odstraňovat nepoužívané bloky (vždy musíme odstraňovat z toho jednoho řádku, ka zrovna zapisujeme)
- **s omezenou asociativitou**
  - stupeň asociativity **> 1**
  - kompromis mezi předešlými metodami
  - zde máme cache rozdělenou na segmenty, z nichž každý obsahuje \\( n \\) řádků (\\( n \\) se nazývá **stupeň asociativity**)
  - často se používají stupně asociativity **2** a **4**

Adresa, pomocí které přistupujeme k paměťové buňce se skládá za tří částí:

- **tag** - určuje *segment*, kde se buňka nachází
- **index** - určuje *řádek* v segmentu
- **offset** - určuje *pozici v řádku*

## Virtuální adresní prostor

Je **logický adresní prostor** (není fyzický), který se do fyzického (skutečně instalovaná paměť v počítači) pouze mapuje. Děje se tak skrze soustavu tabulek.

- virtuální adresní prostory mohou být např. *přiřazené konkrétnímu procesu* (pro ukládání dat jeho úlohy)
- celková velikost všech VAP převyšuje velikost fyzické paměti
- celková velikost aktuálně namapované VAP však musí být menší než je velikost fyzické paměti
- **mapa adresního prostoru**
  - popisuje obsazení adresního prostoru fyzickou pamětí
  - bývá nesouvislá
  - obsahuje různé paměti, které jsou k počítači připojeny (RAM i ROM)
  - ja nutné ji znát při programování na úrovni OS
- způsoby, jakými k mapování dochází, popisují detailně otázky [2.3.2](https://atlas144.codeberg.page/szz/ikt/os/metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci.html) a [2.3.3](https://atlas144.codeberg.page/szz/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html).

### Translation lookaside buffer

*Translation lookaside buffer* (**TLB**) je asociativní paměť (s částečnou nebo plnou asociativitou), která urychluje překlad adres z VAP na fyzický adresní prostor. Zrychlení spočívá v tom, že *TLB* v sobě uchovává páry *virtuální adresa – fyzická adresa* a v těchto párech vyhledává asociativně s klíčem *virtuální adresa*. 

## Zdroje

- SKRBEK, Miroslav. *Architektura počítačů I: UAI/698 Přednášky*. Ver 1.0.9. 2019.
- Přispěvatelé Wikipedie, *Registr procesoru* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 23. 11. 2022, 09:50 UTC, [citováno 17. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Registr_procesoru&oldid=21929380>
- <https://cvw.cac.cornell.edu/codeopt/cacheassociativity>
- <https://en.algorithmica.org/hpc/cpu-cache/associativity/>
- Přispěvatelé Wikipedie, *RAM* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 1. 05. 2023, 02:57 UTC, [citováno 19. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=RAM&oldid=22752301>
- PECH, Jiří. *Operační systémy 1 - Struktura odkládacího zařízení*. České Budějovice.
