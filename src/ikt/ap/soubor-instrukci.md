# Soubor instrukcí

> Soubor instrukcí: typy a kódování instrukcí, adresní módy. Zásobník procesoru. Rozdíly v souborech instrukcí v architekturách CISC a RISC. 

Než začneme rozebírat soubory instrukcí, je třeba si říci, co je to **instrukce**.

## Instrukce

Instrukce je kódový příkaz pro provedení elementární operace procesoru. Posloupnost instrukcí se nazývá strojový kód.

Některé procesory podporují více sad instrukcí (např. současné procesory pro počítače IBM PC kompatibilní podporují sadu x86-16, IA-32 a x86-64, které se celkově označují jako x86).

### Zápis instrukce

Instrukce se skládá ze dvou částí:

- operace
- operandů

Operace označuje, co konkrétní instrukce dělá. Například operace `SUB` může být operace pro odečtení dvou čísel, `JUMP` může být operace pro přesun do jiné částí programu apod.

Operand určuje, s čím tato instrukce operaci dělá. Typicky se jedná o adresový prostor registrů a operační paměti. 

### Velikost instrukce

Velikost instrukce odpovídá počtu bitů dané architektury. Moderní procesory mají obvykle **64** bitů. Tzn. signál, který pokud by všude byly nuly tak vypadá takto:

`0000000000000000000000000000000000000000000000000000000000000000`

### Druhy instrukcí

Instrukce dělíme podle svého určení 
- **Přesunové instrukce**
    - Instrukce, jež se používají pro přesun dat mezi registry, pamětí a I/O. Instrukce i data jsou uloženy v paměti a procesor potřebuje číst instrukce a data z paměti a po zpracování je opět do paměti uložit.
        - **Load** (fetch) instrukce 
        - **Store** (Write) instrukce
- **Aritmeticko-logické instrukce**
    - Nazývají se také instrukce pro manipulaci dat. Mohou to být aritmetické operace, logické operace nebo posuny.
        - **Aritmetické operace** : Sčítání, odčítání, sčítání s přenosem, násobení.....
        - **Logické operace**: AND, OR, XOR...
        - **Posuny**: Umožňují bitové posuny a rotace. Při přetečení nastavují *carry bit*.
- **Sekvenční / Řídící instrukce**
    - Tyto instrukce řídí tok programu tím, že mění *program counter*. Díky tomu může procesor vykonat instrukci na jiné adrese, než je ta sekvenčně následující. (Skočí na jiný řádek strojového kódu, než je ten následující). 
- **Vstupně / výstupní instrukce**
    - Používají se pro přesun dat mezi pamětí / registry a vstupně / výstupními zařízeními.

### Adresa operandu (Adresní módy)

- **Přímé adresování**: Adresa může být určena buď přímo (tzv. přímé adresování) – jako konstanta určující index buňky v operační paměti počítače. 
- **Nepřímé adresování**: Druhou základní možností je nepřímé adresování, kdy je operandem registr jehož obsah je interpretován jako index buňky v operační paměti počítače. Je také možné schovat tento registr za *ukazatel*. 
- **Okamžité adresování**: Namísto, abychom se odkazovali na registr či adresu, používáme konstantní hodnotu.
- **Indexové adresování**: Adresa operandu je vygenerována pomocí přidání konstantní hodnoty k obsahu registru. Tento registr může být speciální registr pro tento účel, nebo jeden z registrů procesoru. Tato generace se děje poté, co přistoupíme k operandu skrze předchozí adresu.
- **Relativní adresování**: Podobný indexovému registrování, akorát namísto používání hodnoty registru ke generování adresy se využije *program counter*
- **Adresování s inkrementací / dekrementací**: Po tom, co za pomocí instrukce přistoupíme k operandu skrze adresu v registru, je adresa tohoto registru inkrementovaná / dekrementována o 1.

### Příklady instrukcí

- `add r0, r2;` – sčítání – přičte do registru `r0` hodnotu uloženou v registru `r2`
- `addc r0, r1;` – sčítání s přenosem – přičte do registru `r0` hodnotu registru `r1` a příznaku přenosu (*carry bit*)
- `mov 1234h, r0;` – přesun – uloží do paměti na adresu `1234h` (přímé adresování) hodnotu z registru `r0` (nepřímé adresování)
- `mov r0, 10;` – přesun – uloží do registru `r0` hodnotu `10` (okamžité adresování) 
- `add 8(r0),r2;` - sčítání - uloží do registru `r0` hodnotu v registru `r2` a poté přičte k adrese v registru `r0` 8 bitů. (indexové adresování)

## Zásobník procesoru (Stack)

**Jedná se o strukturu pro uložení dat**. Jednotlivé hodnoty jsou v něm ukládaná způsobem LIFO (Last in, First out). 

K zásobníku se váže speciální registr -> **stack pointer**. Tento registr obsahuje adresu prvku,který se přidal na zásobník **jako poslední**. **Říká nám tedy, jak je zásobník velký.**

**Nejčastěji je zásobník část paměti počítače s pevně danou počáteční adresou a proměnnou velikostí.**

Využití zásobníku je může být pro několik věcí :

- Ukládání **návratových adres** a příznaky stavu **procesoru** při **přerušeních**. Po návratu je ze zásobníku získána návratová adresa a zpracování pokračuje. 
- Odkládání dat programátorem (například kopie registrů)
- Některé instrukce používají zásobník pro uložení a obnovení adres (`CALL` a `RET`)


Tak nebo tak se jedná o **odkládání a znovuzískávání dat**.

### Základní instrukce, jež se váží k zásobníku

Přesné znění těchto instrukcí se může lišit podle architekturu procesoru, ale jejich význam zůstává stejný.

- `PUSH r0` - vloží hodnotu v registru `r0` na konec zásobníku
- `POP r1` - načte hodnotu na vrcholu zásobníku a uloží ji do registru `r1`. Na tuto adresu ukazuje **stack pointer**. Hodnota **stack pointeru** je poté změněna o velikost paměti kterou na zásobníku zabírala "popnutá" data.

### Přetečení zásobníku

Pokud se stane, že **stack pointer** bude ukazovat na adresu vyšší, než je maximální povolená velikost zásobníku, dojde k **přetečení**. Pokud naopak bude ukazovat na adresu nižší, než je **počáteční adresa zásobníku** dojde k **podtečení**.  Obojího lze docílit pomocí opakovaného používání instrukce  `PUSH`  a `POP`.

### Hardwarová implementace

Většina CPU má zásobník implementovaný v **hlavní paměti**, některé počítače mohou mít zásobník implementovaný jako **sérii registrů** nebo jako hardwarově **oddělenou paměť** (speciální paměť, určena pouze pro zásobník).

## Soubor instrukcí / Instrukční sada / ISA

Soubor instrukcí aneb **Instrukční sada** je abstraktní soubor pravidel, který jednoznačně definuje rozsah funkcionality procesoru = co může procesor dělat a jak to bude dělat.

Definuje tedy:
- jednotlivé strojové instrukce
- datové typy
- registry
- podporu hardware pro správu paměti
- zvládání vyjimek a přerušení
- klíčové vlastnosti (konzistenci paměti, adresní módy, virtuální paměť)
- vstupně/výstupní model rodiny implementací ISA.

Tyto vlastnosti mají přímý vliv na výslednou implementaci procesoru. Pro programátora, resp. kompilátor, se pak jedná o dostupné prostředky procesoru implementujícího danou ISA (Instruction Set Architecture).

## Implementace ISA

Zařízení (CPU), které provádí instrukce definované v určité ISA se nazývá **implementace**. ISA definuje pouze chování strojového kódu, který běží na **implementaci** této ISA, nezávisle na **charakteristice** této implementace. Toto poskytuje tzv. **binární kompatibilitu** mezi implementacemi. **Binární kompatibilita** umožňuje různé implementace totožné ISA, i když se tyto implementace liší v charakteristice (charakteristikami je myšlena například rychlost procesoru nebo fyzická velikost). 

### Mikroarchitektura

Mikroarchitektura zahrnuje **implementaci** základních operací, které jsou podporované počítačem a definované v ISA. Můžeme říct, že *AMD Athlon* a *Intel Core 2 Duo* jsou procesory, které jsou založení na **stejné ISA** ale mají každý **jinou mikroarchitekturu** s různým výkonem a efektivitou.

## Typy ISA

Obecně rozlišujeme dva typy **instrukčních sad**. Když se vytvoří nová instrukční sada, povětšinou (pokud není hodně specifická) ji lze zařadit do dvou tříd.

- **RISC** (Reduced Instruction set Computer, Redukovaná sada instrukcí)
- **CISC** (Complex Instruction set Computer, Komplexní sada instrukcí)

### Redukovaná sada instrukcí (RISC)

*RISC* je architektura, využívající jednoduché instrukce. Tyto instrukce mohou být procesorem vykonávány rychleji bez potřeby používat paměť tak často. *RISC* si klade za cíl redukovat exekuční čas pomocí zjednodušení instrukční sady. Díky tomu, že všechny instrukce mají stejnou délku, je možné je řetězit, což zefektivňuje jejich vykonávání (**pipelining**).

Charakteristiky *RISC*:
- Relativně málo instrukcí
- Relativně málo adresních módů
- Přístup do paměti limitován na načítací a ukládací instrukce
- Všechny operace se vykonávají pouze pomocí registrů CPU
- Každá instrukce se vykoná v jednom instrukčním cyklu
- Instrukce mají pevnou délku
- Jednotlivé instrukce jso napevno dány sadou tranzistorů, nežli za pomocí mikro-programu.

### Komplexní sada instrukcí (CISC)

*CISC* je architektura, kde jedna instrukce může provádět více operací. Například jediná instrukce může načíst z paměti a uložit do paměti. *CISC* tím minimalizuje počet instrukcí na program za cenu zvýšení cyklů na instrukci (instrukce se vykonává déle).

#### Charakteristiky CISC

- Velký počet instrukcí (klasicky 100 - 250 instrukcí)
- Některé instrukce, hlavně ty, které provádějí velmi specifické úkony, nejsou využívány často
- Velké množství adresních módů (5 - 20)
- Jedna "složitá" instrukce se může skládat z více "základních" instrukcí, tato instrukce zabere více cyklů na vykonání
- Mikroprogramy určují vykonávání "složitých" instrukcí 
- Formáty instrukcí mají proměnnou délku
- Instrukce, které manipulují s operandy v paměti

### Příklad

Při provádění `ADD` operace, *CISC* provede jeden `ADD` příkaz, který vykoná jednotlivé potřebné kroky pro načtení a uložení zpět do paměti.

*RISC* provede každou operaci pro načítání dat z paměti, sečtení hodnot a zpětné uložení hodnoty do paměti pomocí jednotlivých low-level instrukcí.




- Zásobník (datová struktura). *Wikipedie : Otevřená Encyklopedie*. [online] @2023 [citováno: 14.05.2023] <br> Dostupné z: <https://cs.wikipedia.org/wiki/Z%C3%A1sobn%C3%ADk_(datov%C3%A1_struktura)>

- Lekce 1 - Assembler - Zásobník. *itnetwork.cz*. [online] @2023 [citováno: 14.05.2023] <br> Dostupné z: <https://www.itnetwork.cz/assembler/os/assembler-zasobnik>
