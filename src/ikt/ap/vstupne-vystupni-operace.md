# Vstupně/výstupní operace

> Vstupně/výstupní operace, vyvolání a obsluha přerušení, přímý přístup do paměti. Registry periferií, mapování periferií do paměti a do vstupně výstupního prostoru

## Úvod

Jedná se o přenos dat mezi **periferním zařízením či vnější pamětí** a **počítačem**. Směr přenosu je definován z hlediska počítače, takže **vstupní operace je čtení z periferie**, zatímco **výstupní operací je zaslání dat do periferie**.

Periferní zařízení dělíme na 3 typy:

- **Vstupní**: zařízení pouze zasílá data do počítače, například senzor
- **Výstupní**: zařízení pouze přijímá data od počítače, například monitor
- **Vstupně/Výstupní**: zařízení jak přijímá, tak zasílá data. Například síťová karta nebo externí disk

Přenos dat probíhá skrze **vstupně-výstupní rozhraní** (I/O interface), klasicky řečeno **port**.

Každé **vstupně-výstupní rozhraní** má svoji **adresu**, které se říká **Vstupně/výstupní port**. K této adrese je přiřazen **hardwarový registr vstupně-výstupního rozhraní** nebo samotné zařízení, které je na toto rozhraní připojené -> **adresa buď odkazuje na hardwarový registr, nebo na samotné zařízení**.

Počítač může mít tuto **adresu** uloženou na dvou různých místech

- **v adresním prostoru paměti**
    - procesor s nimi může pracovat jako s klasickou pamětí
    - mluvíme pak o **adresově mapovaných portech**
    - adresy, které se zde nacházejí se sem dostali pomocí **mapování periferií do paměti**
- **v odděleném adresním prostoru**
    - jsou procesoru přístupné pomocí speciálních instrukcí IN a OUT
    - mluvíme pak o **izolovaných portech**
    - adresy, které se zde nacházejí se sem dostali pomocí **mapování periferií do vstupně výstupního prostoru**

Periferní zařízení se **vždy** připojuje k **hardwarovému rozhraní**. Součástí tohoto rozhraní je několik **hardwarových registrů**, jejichž adresy má počítač uložené v jednom ze dvou prostorů zmíněných výše. **Hardwarové registry** se také nazývají **registry periferií**.

> Procesor komunikuje se **vstupně/výstupními zařízeními** pomocí **hardwarových registrů** jež jsou dostupné pomocí **sběrnice** daného rozhraní.

<div class="note">

### Výstupní hardwarové registry

Vstupní zařízení mají navíc **výstupní hardwarové registry**, pro synchronizaci, časování apod. Registry mohou mít velikost 1B a mohou sloužit jako vyrovnávací paměť daného HW (hardware cache), neboť v sobě udrží data do doby. než jsou předána CPU, RAM či naopak zpracována samotnou periferií.

</div>

<div class="note">

### Ovladače

Jednotlivá **hardwarová rozhraní** jsou ovládány pomocí **ovladačů** (například Realtek Audio pro zvukový výstup).
- ovladače pro **hardwarové rozhraní** lze nalézt na stránce výrobce **základové desky**

Stejně tak mají periferní zařízení vlastní ovladače dodávané výrobci.
- Nabouchaná myš od Logitechu potřebuje speciální software, aby mohla svítit jak vánoční stromeček.

**Ovladač** je software, který umožňuje počítači komunikovat s periferií, nebo naopak periferii komunikovat s počítačem.

</div>

## Připojení zařízení ke sběrnici - mapování periferií do paměti a do I/O prostoru

Pokud připojíme zařízení ke sběrnici skrze **vstupně/výstupní rozhraní** tak je adresa **hardwarových registrů** uložena do jednoho ze dvou paměťových prostorů. Tomuto procesu říkáme **mapování periferie**.

**Výběr prostoru je dán typem periferie**, která při připojení vyšle na jeden z **hardwarových registrů** rozhraní, na které je připojena, **signál**, určující, kam bude namapována. O **mapování periferie**, stejně jako o správu komunikace, se starají **ovladače periferií**.  Již jsme si tyto dva prostory zmínili výše. **Periferii** (její **hardwarové registry**) můžeme namapovat do: 

- **paměti RAM**
- **vstupně / výstupního prostoru**

### Mapování periferie do paměti

Adresa **periferie** je uložena do **operační paměti**. 

- Procesor může k této adrese přistupovat stejně, jako k ostatním adresám, tj. pomocí klasické sady instrukcí. Může tedy používat klasické adresná módy a číst / zapisovat do registru klasickým způsobem
- Je vhodnější (jednodušší) pro programování
- Procesor se chová k periferiím namapovaným zde jako k adresám
- Adresní prostor je velký
- Přístup zde je pomalejší (je potřeba dekódovat adresu, což trvá dlouho kvůli velkému adresnímu prostoru)

### Mapování periferie do vstupně/výstupního prostoru

Adresa **periferie** je uložena do **speciálního adresního prostoru**.

 - Prostor oddělený od operační paměti. Tento adresní prostor **má vlastní sběrnici**, která je připojena k CPU
 - Adresní prostor je malý
 - Přístup je zde rychlejší, nežli do paměti (malý adresní prostor = jednodušší dekódování)

  <div class="note">

  ### Dekodér adres

  Jedná se o integrální součást řadiče. Používá se pro **dekódování adres**. Pokud chceme přistoupit k nějakému místu v paměti, tak procesor vystaví na sběrnici adresu. Prací **řadiče** je vybrat správnou paměťovou buňku v paměti, ke které přistupujeme, převést adresu ze sběrnice na místo v paměti (toto právě řeší **adresový dekodér**), které je symbolizováno danou adresou. De facto řadič sáhne do správné paměťové buňky (dle adresy vystavené procesorem a dekodérem převedené na adresu paměťové buňky) a vytáhne z ní data, o něž si CPU řekl. 

  </div>

## ŘÍzení komunikace - přerušení a přímý přístup do paměti

Periferie a CPU spolu potřebují komunikovat.  Mohou nám nastat dvě různé situace:

- **procesor chce komunikovat s periferií** (chce ji zaslat data)
- **periferie chce komunikovat s procesorem** (chce mu zaslat data)

V prvním případě jednoduše procesor zašle data na danou adresu, která odpovídá adrese **hardwarového registru periferie**.

V druhém případě je to složitější, **periferie si může přát zaslat data**, ale procesor může být zaneprázdněný a nemá žádný způsob, kterým by mohl vědět, že periferie chce komunikovat.

**Periferní zařízení** může komunikaci uskutečnit dvěma způsoby :

- **přerušení**
- **přímý přístup do paměti (DMA)**

### Přerušení

Princip přerušení spočívá v tom, že periferie vystaví signální bit, kterým si **zažádá o čas procesoru**. Pokud je procesor volný, tak periferii obslouží na základě toho, jaké signály jsou periferií vystaveny (těmto signálům se říká **signal word**). 

Pokud procesor volný není, tak periferie musí čekat, až bude obsloužena. Povětšinou není jediná, která vystavila signál k **přerušení**. O tom, jaké přerušení bude obslouženo nejdříve, rozhoduje **řadič přerušení**.

### Přímý přístup do paměti

Ne vždy je nutné vyžadovat po CPU čas pro vykonání operace. Příkladem může být přesun velkého bloku dat z a nebo do paměti. V případě **přímého přístupu do paměti** je toto uskutečněno bez asistence procesoru.

## Zdroje 


- Vstup/výstup. Wikipedie: Otevřená Encyklopedie. [online] @2022 [citováno 19.05.2023] <br>Dostupné z: <https://cs.wikipedia.org/wiki/Vstup/v%C3%BDstup>

- Komunikace s perifériemi. Ing. Petr Olivka. [online] @2010 [citováno 19.05.2023] <br>Dostupné z: <https://poli.cs.vsb.cz/edu/arp/down/komunikace.pdf>

- Memory-mapped I/O and port-mapped. Wikipedia : The Free Encyclopedia. [online] @2023 [citováno 19.05.2023] <br>Dostupné z: <https://en.wikipedia.org/wiki/Memory-mapped_I/O_and_port-mapped_I/O>


