# Instrukční cyklus počítače

> Jednotlivé fáze zpracování instrukce. Pojem proudové zpracování (pipelinig) instrukcí.

V každém zařízení, které nazýváme počítačem, se nachází CPU. Toto CPU se skládá z několika základních částí, nutných pro vykonávání **instrukčního cyklu**.

## Části CPU účastnící se **instrukčního cyklu**

### Hodiny 

Křemíkový krystal, který vibruje s určitou frekvencí. Jedna vibrace se nazývá **tik**. Počet **tiků** za vteřinu určuje **frekvenci procesoru**. S každým **tikem** CPU vykoná jednu část **instrukčního cyklu**.

### Program counter (PC; čítač)

Jedná se o **registr**, ve kterém se nachází **adresa právě vykonávané instrukce**.

### Instruction register (IT; instrukční registr)

**Registr**, ve kterém je uložena **právě vykonávaná instrukce**

### Accumulator (akumulátor)

Jde o **registr**, ve kterém je uložen **výsledek právě vykonávané instrukce** (ne vždy ho CPU má; může místo něj použít jiný *registr pro obecné použití*).

**Instrukce a data, se kterými procesor pracuje, se nacházejí v RAM. Každá instrukce má dvě části: instrukce samotná a adresa dat, se kterými pracuje**

|![Výchozí stav CPU před započetím instrukčního cyklu](instrukcni-cyklus-pocitace/init_state.png "Výchozí stav CPU před započetím instrukčního cyklu")|
|:--:|
|*obr. 1:* Výchozí stav CPU před započetím instrukčního cyklu|
|*Dostupné z:* <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>|

## Instrukční cyklus

Instrukční cyklus se skládá ze 3 částí. **Fetch, Decode a Execute**.

### Fetch

První částí je získání instrukce na adrese **čítače (PC)** a natažení této instrukce do **instrukčního registru**.

### Decode

Ve druhé části se instrukce dekóduje. Tedy CPU zjistí, co má instrukce dělat a kde se nachází hodnoty, se kterými má provádět operaci, jež instrukce definuje.

### Execute 

Nakonec CPU provede instrukci a výsledek instrukce uloží do **akumulátoru**. Adresa v **PC** se zvýší o 1 (**pokud není dáno jinak právě vykonávanou instrukcí**).

## Příklad průběhu instrukčního cyklu

**Čítač** se nastaví na 0 (někde se začít musí).

### První průchod

- Hodiny tiknuly. CPU **získá (fetchne)** instrukci na adrese 0 a načte ji do **instrukčního registru**.
- Hodiny tiknuly. CPU **dekóduje** instrukci v **instrukčním registru**. Zjistí tedy, že operace je `LOAD` a adresa je *6*.
- Hodiny tiknuly. CPU **vykoná** instrukci v **instrukčním registru**. Instrukce `LOAD` znamená načti. CPU tedy načte hodnotu z adresy *6*. Tuto hodnotu uloží do **akumulátoru**. **PC** se inkrementuje o 1.

|![Stav CPU a paměti po prvním průchodu instrukčním cyklem](instrukcni-cyklus-pocitace/first_cycle.png "Stav CPU a paměti po prvním průchodu instrukčním cyklem")|
|:--:|
|*obr. 2:* Stav CPU a paměti po prvním průchodu instrukčním cyklem|
|*Dostupné z:* <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>|

### Druhý průchod

- Hodiny tiknuly. CPU **získá (fetchne)** instrukci na adrese 1 a načte ji do **instrukčního registru**.
- Hodiny tiknuly. CPU **dekóduje** instrukci v **instrukčním registru**. Zjistí tedy, že operace je `ADD` a adresa je *7*.
- Hodiny tiknuly. CPU **vykoná** instrukci v **instrukčním registru**. Instrukce `ADD` znamená přidej. CPU tedy načte hodnotu z adresy *7*. Tuto hodnotu přidá k hodnotě v **akumulátoru**. **PC** se inkrementuje o 1.

|![Stav CPU a paměti po druhým průchodu instrukčním cyklem](instrukcni-cyklus-pocitace/second_cycle.png "Stav CPU a paměti po druhým průchodu instrukčním cyklem")|
|:--:|
|*obr. 3:* Stav CPU a paměti po druhým průchodu instrukčním cyklem|
|*Dostupné z:* <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>|

### Třetí průchod

- Hodiny tiknuly. CPU **získá (fetchne)** instrukci na adrese 2 a načte ji do **instrukčního registru**.
- Hodiny tiknuly. CPU **dekóduje** instrukci v **instrukčním registru**. Zjistí tedy, že operace je *Store* a adresa je *6*.
- Hodiny tiknuly. CPU **vykoná** instrukci v **instrukčním registru**. Instrukce *Store* znamená ulož. CPU tedy uloží hodnotu, co má v **akumulátoru**, do registru, jež se ukrývá pod adresou *6*.  **Programme counter** se inkrementuje o 1.

|![Stav CPU a paměti po třetím průchodu instrukčním cyklem](instrukcni-cyklus-pocitace/third_cycle.png "Stav CPU a paměti po třetím průchodu instrukčním cyklem")|
|:--:|
|*obr. 4:* Stav CPU a paměti po třetím průchodu instrukčním cyklem<br>*Dostupné z: <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>*|

### Čtvrtý průchod

- Hodiny tiknuly. CPU **získá (fetchne)** instrukci na adrese 3 a načte ji do **instrukčního registru**.
- Hodiny tiknuly. CPU **dekóduje** instrukci v **instrukčním registru**. Zjistí tedy, že operace je `JMP` a adresa je *1*.
- Hodiny tiknuly. CPU **vykoná** instrukci v **instrukčním registru**. Instrukce `JMP` znamená **změň PC na**. CPU tedy nastaví **PC** na adresu 1.

|![Stav CPU a paměti po čtvrtém průchodu instrukčním cyklem](instrukcni-cyklus-pocitace/fourth_cycle.png "Stav CPU a paměti po čtvrtém průchodu instrukčním cyklem")|
|:--:|
|*obr. 5:* Stav CPU a paměti po čtvrtém průchodu instrukčním cyklem|
|*Dostupné z:* <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>|

Tento program bude do nekonečna inkrementovat hodnotu v akumulátoru a ukládat ji na adresu 6. 

### **Pozor! Ve skutečnosti (při práci s JSA (jazykem symbolických adres)) nejsou adresy číslovány v desítkové soustavě, ale v hexadecimální.** 

## Proudové zpracování instrukcí (Pipelining)

 **Pipelining** je způsob zvýšení výkonu procesoru současným prováděním různých částí několika strojových instrukcí. Základní myšlenkou je rozdělení zpracování jedné instrukce mezi různé části procesoru a tím i **umožnění zpracovávat více instrukcí najednou** (umožnění **paralelizace na úrovni instrukcí**). Počátek této technologie byl úspěšný zejména u procesorů s RISC architekturou, kde je díky jednoduchému setu instrukcí a **především stejné délce všech instrukcí** jednodušší implementace. Nicméně dnes se **proudové zpracování** nachází již téměř ve všech procesorech.

 Je toho docíleno tím, že se snažíme udržet každou část procesoru zaneprázdněnou nějakou **instrukcí**. Každá instrukce je při **proudovém zpracování** rozdělena do sekvenčních kroků. 

 Při jednom **tiknutí** se tedy tedy instrukce může **dekódovat**, ale zároveň se již může **natahovat / fetchovat** další. 

|![Postup proudového zpracování instrukcí](instrukcni-cyklus-pocitace/pipeline.PNG "Postup proudového zpracování instrukcí")|
|:--:|
|*obr. 6:* Postup proudového zpracování instrukcí|
|*Dostupné z:* <https://en.wikipedia.org/wiki/Instruction_pipelining>|

O řízení **proudového zpracování instrukcí** se stará **plánovač**, který říká jednotlivým **jádrům** procesoru, na čem mají dělat. Často se také využívají **buffery**, do kterých se ukládají mezivýsledky jednotlivých fází **proudového zpracování**.

Ač se tato technologie dnes používá téměř ve všech procesorech, **nese si i jisté nevýhody**, zde jsou tři nejpodstatnější.
- **nutnost větších zdrojů**: každá fáze zpracování musí být vykonávána samostatně - to vede k nutnosti více obvodů,  procesorových jednotek, paměti pro buffery...
- **zvýšení latence**: v některých případech nemůže procesor zpracovat část instrukce, dokud není plně zpracovaná předchozí část  nebo se vyskytnou jiné **potíže s režií proudového zpracování**
- **zahození dat při přepnutí programu**: data a instrukce, které se v pipelině právě nacházejí, musejí být zahozeny, aby se mohly začít vykonávat nové, jež nemají s předchozími nic společného (toto "uklizení" vede k **větší režii**)

## Zdroje

- The Fetch-Execute Cycle: What's Your Computer Actually Doing? [online] @2019 [cit. 15. 12. 2022] Tom Scott.<br> Dostupné z: <https://www.youtube.com/watch?v=Z5JC9Ve1sfI>
- Instruction pipelining [online] @2023 [cit. 11.05.2023] Wikipedia: The Free encyclopedia. <br> Dostupné z: <https://en.wikipedia.org/wiki/Instruction_pipelining>
