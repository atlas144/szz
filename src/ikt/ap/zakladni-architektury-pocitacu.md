# Základní architektury počítačů

**Architektura počítačů** je technický obor zaměřený na návrh a konstrukci zařízení na zpracování dat *(počítač)*. Zajišťuje:
- stanovení vnitřní reprezentace dat a operací, které se s nimi budou provádět
- specifikaci **funkčních bloků (jednotek)** počítače a jejich propojení
- formát strojových instrukcí
- způsob jak pracuje CPU a jakým způsobem přistupuje k paměti

Architektura počítače je tedy **konkrétní způsob realizace počítače**. Existují dvě základní koncepce konstrukce počítače:
- **Von Neumannova architektura**:
  -  používá **jednu sběrnici**, na kterou jsou připojeny všechny aktivní prvky (procesor, paměť, vstupy a výstupy)
  -  činnost počítače je **sekvenční** (oproti harvardské) a je řízena **řadičem**. Řadič načítá jednu instrukci po druhé, dekóduje je a postupně je provádí
  - každou instrukci lze rozdělit na jednodušší operace
  - skládá se z:
    - **řadiče** - elektronická řídicí jednotka, realizovaná **sekvenčním obvodem**, která řídí činnost všech částí počítače. Toto řízení je prováděno pomocí **řídicích signálů**, které jsou zasílány jednotlivým modulům (dílčím částem počítače). Dílčí částí počítače je např. operační paměť, která rovněž obsahuje řadič, který je podřízen hlavnímu řadiči počítače, jenž je součástí CPU. 
    - **aritmeticko-logické jednotky (ALU)** - provádí všechny aritmetické (např. sčítání, násobení, bitový posuv, …) a logické (logický součin, negace, …) výpočty. Operace jsou prováděny nad operandy s pevně daným rozsahem závislým na architektuře. Výpočty s libovolnou přesností je tak zapotřebí provádět pomocí softwarových knihoven.
    - **vstupní/výstupní jednotka** - zařizuje přenos dat mezi periferním zařízením nebo vnější pamětí a počítačem.

|![Von Neumannova architektura](zakladni-architektury-pc/von_neumannova_architektura.png "Von Neumannova architektura")|
|:--:|
|*obr. 1:* Von Neumannova architektura|

- **Harvardská architektura**: 
  - používá **oddělenou paměť pro data a pro program**
  - může používat (pro čtení i zápis) paměť programu i paměť dat najednou - tzn. **paralelně** (oproti Von Neumannové architektuře)
  - často se používá u **jednočipových počítačů**, **malých vestavěných systémů** (PDA, mobilní telefony a podobně) a především u **signálových procesorů** (DSP) u nichž dovoluje dosáhnout velké rychlosti zpracování dat

|![Harvardská architektura](zakladni-architektury-pc/harvardska_architektura.png "Harvardská architektura")|
|:--:|
|*obr. 2:* Harvardská architektura|

## Architektura současného PC

Současné počítače nejsou konstruovány důsledně ani podle jednoho z těchto dvou základních schémat.
Univerzální osobní počítače obsahují jen jednu paměť, do které se umisťují programy i zpracovávaná data.

|![Architektura současného PC](zakladni-architektury-pc/architektura_soucasneho_PC.png "Architektura současného PC")|
|:--:|
|*obr. 3:* Architektura současného PC|

**Procesor (CPU)** - je základní elektronická jednotka, která umí vykonávat **strojové instrukce** (**označení kódovaného příkazu pro provedení elementární operace procesoru**), ze kterých je tvořen počítačový program a obsluhovat jeho vstupy a výstupy. Mezi hlavní součásti procesoru patří aritmeticko-logická jednotka, **registry** a řadič.

**Registr procesoru** má za úkol:
- udržovat stav procesoru
- sloužit o odkládání mezivýsledků
- tvořit operandy aritmetických a logických operací

# Citace

- *Wikipedie: Otevřená encyklopedie: Architektura počítače*[online]. c2020 [citováno 16. 02. 2023]. Dostupný z WWW: <https://cs.wikipedia.org/w/index.php?title=Architektura_po%C4%8D%C3%ADta%C4%8De&oldid=18604330> 
- KUBA, Pavel. *ARCHITEKTURA POČÍTAČŮ.* Katedra informatiky, PřF, UJEP v Ústí nad Labem. Ústí nad Labem, 2016.

