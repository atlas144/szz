# Vícejádrové a vícevláknové procesory

> ### **Vícejádrové a vícevláknové procesory, architektura VLIW, procesory pro grafické karty (GPU)**

## Vícejádrové procesory

Jedná se o **jeden procesor**, který obsahuje více **jader**. Jednotlivá **jádra** mohou individuálně číst a vykonávat programová instrukce. -> Fyzicky vykonávají více instrukcí zároveň. Toto zrychluje vykonávání programu a podporuje **paralelní zpracování**.

**Vícejádrové** procesory jsou používány v aplikacích jako je síťování, embedded systémy, zpracování digitálního signálu a grafice (GPU).


## Vícevláknové procesory

Jedná se o procesory, kde je jedno **jádro** schopné **konkurentně** zpracovávat více **vláken**.

**Vlákno**

- Jedná se o sekvenci **programových instrukcí**. Vlákna, nebo spíše to, kdy se jaké vlákno vykonává (na určitém jádru), je řízeno plánovačem skrze **přepínání kontextů**.

|![Přeměna programu na vlákna](vicejadrove-a-vicevlaknove-procesory/thread.jpg "Přeměna programu na vlákna")|
|:--:|
|*obr. 1:* Přeměna programu na vlákna<br>Dostupné z: <https://en.wikipedia.org/wiki/Thread_(computing)>|

## VLIW  (Very Long Instruction Word) architektura

Jedná se o architekturu, která si klade za cíl **paralelismus na úrovni samotných instrukcí**. Instrukce je zde rozdělena na samostatné bloky, tak aby se každá část instrukce vykonávala v jednom tiku procesoru. Tomuto říkáme [proudové zpracování](https://atlas144.codeberg.page/szz/ikt/ap/flynnova-klasifikace-paralelnich-systemu.html).

**VLIW architektura**, oproti klasickému **proudovému zpracování** přesouvá vnitřní složitosti ohledně **proudového zpracování** na kompilátor nebo vývojové prostředí. Vývojáří musejí instrukce předem naplánovat a zorganizovat je do jednoho tzv. **slova**, aby se mohli provádět paralelně. Toto **slovo** se skládá z jednotlivých nízkých instrukcí.

Výhodou tohoto přístupu oproti obyčejnému *proudovému** zpracování je možnost použít jednodušší hardware, protože nám odpadá nutnost režie **proudového zpracování**.

Nicméně, nevýhody jsou ty, že architektury typu **VLIW** jsou jsou závislé na plánování instrukcí kompilátorem nebo vývojovým prostředím. Pokud tomu tak není, vznikají prodlevy, které vedou ke snížení výkonu.

### Využití :

Všude tam. kde máme vysoký stupeň paralelismu a ve kterých lze instrukce dobře předem plánovat. Převážně zpracování digitálního signálu nebo multimediální aplikace.

## Procesory pro grafické karty (GPU)

**GPU** byly původně vytvořeny pro vykreslování počítačové grafiky, ale časem se jejich využití rozšířilo i na zrychlení výpočtů, jež zahrnují velké množství dat.

**GPU** umožňuje provádění velkého množství **repetitivních** výpočtů paralelně s během hlavního programu na **CPU**. Na **CPU** se můžeme dívat jako na **task mastera** celého systému, který koordinuje velké množství obecných výpočetních úkonů. Pokud jsou nějaké úkony **matematicky** výpočetně náročné, jsou předány **GPU**.

|![Struktura CPU vs GPU](vicejadrove-a-vicevlaknove-procesory/cpu_gpu.PNG "Struktura CPU vs GPU")|
|:--:|
|*obr. 2:* Struktura CPU vs GPU<br>Dostupné z: <https://www.heavy.ai/technical-glossary/cpu-vs-gpu>|


Ač dokáže **GPU** zpracovávat data několikrát rychleji díky **masivnímu využití paralelismu** nežli **CPU**, tak **GPU** není tak **všestranné**. Je uzpůsobeno pouze na matematicky jednoduché výpočty, nejčastěji práci s maticemi.

Kromě **renderování** se **GPU** využívá take v:

- strojové učení: (snížení doby trénování)
- simulace

## Zdroje

- Multithreading (computer architecture). *Wikipedia : The Free encyclopedia* [online] @2023 [citováno: 18.05.2023] <br>Dostupné z: <https://en.wikipedia.org/wiki/Multithreading_(computer_architecture)>

- Thread (computing). *Wikipedia : The Free encyclopedia* [online] @2023 [citováno: 18.05.2023] <br>Dostupné z: <https://en.wikipedia.org/wiki/Thread_(computing)>

- CPU vs GPU. *HeavxAI [online] @2022 [citováno: 18.05.2023] <br>Dostupné z: <https://www.heavy.ai/technical-glossary/cpu-vs-gpu>
