# Databázový systém

> Databázový systém, data, databáze, systém řízení báze dat (SŘBD/DBMS), typy DBMS, relační, objektové, postrelační DBMS.

Databázový systém je program, který se zaměřuje na **ukládání, modifikaci a výběr velkého množství dat**. **Databázový systém** se skládá z:

- **databáze**
- **SŘBD (systém řízení báze dat)**
  - raději používejte pojem **Database Management System** (DBMS)!

Protože **databázový systém** je zodpovědný za práci s velkým množstvím **perzistentních** (trvalých/uložených) dat, tak jsou na něj kladeny jisté nároky:

- **podpora pro definice datového modelu** (logický, relační)
- **využití některého z jazyků (SQL,QBE atd.) vyšší úrovně** pro manipulaci s daty
- **správa transakcí**
- **autentizace uživatelů a autorizace operací nad daty**
- **robustnost a zotavitelnost po chybách beze ztráty dat spravovaných systémem**

**Databáze + program na manipulaci databáze (DBMS) = Databázový systém**

## Data

**Data** jsou uložena v **databázi**. Data jsou surovou matérií: 1, II, Bruno, obrázek domečku. **Data se stávají informací jen a pouze tehdy, pokud datům dáme kontext, strukturu**: 1. patro v budově, II značí 2 antické sloupy, Bruno je pilot letadla, obrázek domečku je domovská stránka v browseru.

## Databáze

> Definice: **Systém pro ukládání dat s pevnou strukturou záznamů.** 

S databází je možno manipulovat pomocí **DBMS**. Databáze nemusí být v elektronické podobě, jako předchůdce **databází** se uvádí **kartotéka** -> umožňovala třídění dat podle různých kritérií a zařazování nových dat (záznamů).

<div class="note">

### Databáze a DBMS není to samé

Pokud budeme hledat, zjistíme že se termín **databáze** často zaměňuje s termínem **systém řízení báze dat**. **Databáze** v **databázovém systému** je pouhopouhá **struktura** pro ukládání dat -> **báze dat**. Tato **struktura** je spravována a definována pomocí **DBMS**.

</div>

## Systém řízení báze dat

Jedná se o **software, jež nám umožní provádět operace nad databází**. Tvoří rozhraní mezi aplikací a uloženými daty v **databázi** (**bází dat**).

Aby byl nějaký systém považován za **DBMS**, musí být nejen schopný pracovat s velkým množstvím dat, ale musí být schopný i **řídit** (mazat, vkládat, modifikovat) a **definovat** **strukturu** těchto dat. **Liší se tím** od klasického **souborového systému**.

**DBMS** jsou schopné i **databáze** vytvářet -> **definovat strukturu dat**. 

Systémy pro řízení báze dat můžeme dělit podle toho, jakým způsobem definují **strukturu databáze**.

- **Hierarchické** (**data jsou uspořádána ve stromové struktuře**)
- **Síťové** (**založen na Hierarchicke struktuře, ale navíc poskytuje vztahy n:m**)
- **Relační** (založen na **relačním modelu** -> je založena na tabulkách, řádky jsou záznamy a sloupce atributy. Některé sloupce mohou obsahovat **klíče** které uchovávají **vztahy mezi tabulkami**)
- **Objektové** (na rozdíl od **relačního modelu** jsou zde tabulky nahrazeny **objekty**)
- **Objektově relační** (kombinace relačního a objektového -> objekty místo tabulek, klíče jež symbolizují vztahy mezi objekty)


### Relační

> Jedná se o **systém**, který nám umožní řídit a definovat strukturu **relační databáze**

**Relační databáze** je **struktura** dat, která nám umožní identifikovat a přistupovat k záznamům podle toho, v jakém jsou vztahu s jinými daty. Data jsou organizovány do **relací** (tabulek).

Každá tabulka obsahuje řádky (**záznamy**) a sloupce (**atributy**). Atribut je vždy nějakého typu.

Většina **relačních systému řízení báze dat** využívá pro manipulaci **relační databáze** jazyk SQL.

### Objektové

> Jedná se o **systém**, který nám umožní řídit a definovat strukturu **objektové databáze**

**Objektová databáze** je **struktura**, ve které namísto **tabulek** používáme objekty. Tyto objekty jsou stejné, jako známe z **objektově orientovaného programování**. Databáze řízené tímto systémem mají jednotlivé vztahy mezi daty uložené přímo v sobě, na rozdíl od **relačních**, které je mapují pomocí tabulek. Povětšinou jsou tyto  vztahy reprezentovány pomocí **ukazatelů**.

Pro manipulaci **objektové databáze** je možné použít **objektově orientované jazyky**.

Výhodou těchto systému je možnost lepší práce s bází dat. Mapování dat na objekty bývá složitý proces a v **relačních systémech** se to musí dělat ručně, avšak můžeme si usnadnit práci pomocí **Object-relational mapping (ORM)** (C# někdo ?).

### Post-relační 

> Jde o **rodinu databázových systémů**, které rozšiřují klasický **relační** systém

Tyto **databázové systémy** se **odchylují** od klasického **relačního** modelu, jež má striktní strukturu (data jsou v tabulkách, tabulky mezi s sebou mají vztahy pomocí primárních a cizích klíčů, normalizace, ACID....)

> Vlastně můžeme říct, že v dnešní době se databázové systémy dělí na dva typy. Ty, které jsou relační, a ty které ne.

**Objektový databázový systém** je také **post-relačním** systémem.

Některé další příklady post-relačních systémů jsou:

- NoSQL
  - využívá jako **systém řízení báze dat** **MongoDB**; používá se, pokud máme velké množství nestrukturovaných, nebo polo-strukturovaných dat
- Grafové databáze
  - používají se pro modelování komplexních vztahů mezi **entitami**; jako systém řízení báze dat lze využít například **Neo4j**
- Dokumentové databáze<br>
  - využívají k ukládání dat **dokumentové struktury**, povětšinou **typu JSON**; pro řízení báze dat můžeme využít **MongoDB**


Pokud databáze není relační, tak je post-relační.

## Zdroje

- Co je databázový systém. *Sprava-site.eu* [online] @2022 [citováno 18.05.2023]<br> Dostupné z: <https://www.sprava-site.eu/databazovy-system/>

- Systém řízení báze dat. *Wikipedie : Otevřená encyklopedie* [online] @2022 [citováno 18.05.2023]<br> Dostupné z: <https://cs.wikipedia.org/wiki/Syst%C3%A9m_%C5%99%C3%ADzen%C3%AD_b%C3%A1ze_dat>

- Relational database. *Wikipedia : The Free Encyclopedia* [online] @2023 [citováno 18.05.2023]<br> Dostupné z: <https://en.wikipedia.org/wiki/Relational_database>

- What is a Relational Database Management System?. *Codecademy Team* [online] @2022 [citováno 18.05.2023]<br> Dostupné z: <https://www.codecademy.com/article/what-is-rdbms-sql>

- What Is an Object-Oriented Database?. *MongoDB* [online] @2022 [citováno 18.05.2023]<br> Dostupné z: <
https://www.mongodb.com/databases/what-is-an-object-oriented-database>
