# Postup návrhu databáze

> Postup návrhu databáze, analytik, designér, kodér, analytický model, konceptuální model, fyzický model.

## Analytický model

Jedná se o model, který nám **říká, co bude databáze obsahovat**. Vytváří ho **analytik** na základě **požadavků klienta**.

Na analytický model se můžeme také divat z jiného úhlu pohledu. Když je již databáze vytvořena, můžeme v budoucnu chtít v ní existující data analyzovat. (například pomocí regresní analýzy, kdy chceme vědět, jaké vztahy spolu jednotlivé proměnné mají).

## Konceptuální model

Zaobírá se **popisem významu dat v databázi nezávisle na fyzickém uložení dat**. 

Samotná data mohou mít podobu například 03.12.2021, 30, Škoda, Martin Jelínek. Z toho můžeme odhadnout, že se jedná o nějaký datum, spojený s nějakým jménem a autem a jako bonus tam je nějaké číslo.  Nevíme, co jednotlivé údaje znamenají. Jedná se o datum zapůjčení automobilu, nebo jeho vrácení ? Je 30 doba zapůjčení ? 

**Konceptuální modely** se snaží vytvořit **konceptuální** pohled člověka na danou část reálného světa. Používáme k tomu **E-R model** (Entitně relační model). Existují i další **konceptuální modely** (například hierarchický model, síťový model), přičemž tím **nejjednodušším typem modelu, zároveň sloužícím jako omezení pro vyjádření všech dalších je *přirozený jazyk***.

### E-R model

Jedná se o **množinu pojmů** které nám umožní na **konceptuální** úrovni abstrakce popsat uživatelskou aplikaci za účelem následné specifikace struktury databáze.

Tento model identifikuje: 

- typy entity jako třídy objektů stejného typu (Zákazník, Zaměstnanec, ...)
- typy vztahů, do kterých mohou entity vstupovat (např. Zákazník MÁ PŮJČENÉ (*to je ten vztah*) Auto)
- na základě jisté úrovně abstrakce přiřadí jednotlivým entitám a vztahům **atributy**:<br>
  jméno, příjmení, ...
- formuluje různá **integritní omezení** např:
  - doména **atributu** `VÁHA` je reálně číslo
  - `RDONE_CISLO` je identifikačním klíčem entity typu `ZAMESTNANEC`
  - Hodnota atributu A musí být větší než hodnota atributu 

Více o E-R modelu lze nalézt v okruhu [Entitně relační model](https://atlas144.codeberg.page/szz/ikt/db/entitne-relacni-model.html).


## Fyzický model

Jde o **způsob toho, jak jsou data uložena**. Nazývá se **relační model**. 

Určuje **strukturu tabulek**, **názvy sloupců (atributů)**, **primary key** dané tabulky, **foreign key** a index sloupce tabulky, ke kterému se váže, **relační vztahy mezi tabulkami**, **uložené procedury** a **views**.

**View**: Virtuální tabulka. Obsah této tabulky je definován na základě **query** (tedy je výstupem vykonání query).

Konečný uživatel je od této vrstvy odstíněn **SŘBD** (Systém řízení báze dat), častěji nazývaným anglicky - **DBMS** (Database management system), například *MySQL*.


## Postup návrhu databáze

Na počátku existuje zákazník, který má přání na to, že chce vytvořit databázi. Tento zákazník o databázích neví povětšinou zhola nic. 

Jeho požadavek si vyslechne **analytik**, který vytvoří **analytický model**. Už tedy víme, co bude databáze obsahovat.

Na základě toho, co bude databáze obsahovat, **designér** udělá **konceptuální model** (někde se uvádí, že analytický a konceptuální je totéž). Ten již obsahuje databázi z pohledu reálného světa. **Schéma uložení dat**. Není závislý na konkrétním **fyzickém modelu**, je zde tedy míra abstrakce.

Na základě tohoto modelu lze vytvořit **fyzický model**. K tomuto fyzickému modelu přistupují **kodéři** skrze SŘBD a jsou schopní ho buď manipulovat (rozšiřovat, měnit), nebo vytvářet **views**.

**Views** jsou určeny po koncového uživatele. Jedná se o výstup z databáze za pomocí **query**. Struktura tohoto **view** nemusí a často nemá s **fyzickým model** (tím jak jsou data skutečně uložena) nic společného.

Příkladem může být že po vybrání časové rozsahu a kliknutí na tlačítko se uživateli zobrazí, jaká auta jsou v tomto časovém intervalu kým vypůjčena. Jedná se o **virtuální tabulku**, která byla vytvořena z několika dalších na základě jejich spojování. **To, jak koncový uživatel vidí data, neodpovídá tomu, jak jsou fyzicky uložena**.

## Zdroje

- Sandra Suszterova. *Physical Data Model vs. Logical Data Model* [online] @2023 [citováno: 01.05.2023]<br>Dostupné z: <https://www.gooddata.com/blog/physical-vs-logical-data-model/>

- Střední průmyslová škola Brno. *Databáze – úvod* [online] @2023 [citováno: 01.05.2023] <br>Dostupné z: <https://moodle.sspbrno.cz/pluginfile.php/1950/mod_resource/content/1/databaze-uvod.pdf>

- Michal Valenta. Katedra softwarového inženýrství Fit. České vysoké učení technické v Praze. *DBS – Databázové modely*. [online] @2023 [citováno: 01.05.2023] <br>Dostupné z: <https://users.fit.cvut.cz/valenta/doku/lib/exe/fetch.php/bivs/dbs_02_databazove_modely.pdf>

- Wikipedia : Free encyclopedia. *Database abstraction layer*. [online] @2022 [citováno 01.05.2023] <br>Dostupné z: <https://en.wikipedia.org/wiki/Database_abstraction_layer>

