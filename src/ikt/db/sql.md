# SQL

> historie jazyka, DDL, DML, DCL, TCL

**SQL** - **S**tructured **Q**uery **L**anguage je deklarativní a doménově orientovaný jazyk. Zároveň je lidsky čitelný.

**Deklarativní** znamená, že se neptáme **jak něco udělat**, ale **co se má udělat**.

**Doménově orientovaný** znamená, že SQL můžeme použít jen pro určitou věc -> **manipulaci s daty v SŘBD**.

## Historie SQL

**1970** - E.F. Todd - vymyslel relační model pro správu databází, článek **A relational Model of Data for Large Shared Data Banks**

**1974** - v IBM vymysleli Structured English Query Language (SEQUEL)

**1980s** - vývoj SŘBD Oracle, Sybase, DB/2

**1987** - první standard ANSI SQL-86 (SQL1)

**1989** - SQL-89 (přidána integritní omezení)

**1992** - SQL-92 (SQL2) - přidány transakce a nové datové typy

**1999** - SQL:1999 (SQL3) - přidány regulární výrazy a prvky procedurálního programování (proměnné, rekurzivní volání, triggery) a strukturované datové typy

Následovali další standardy: SQL:2003, SQL:2006, SQL:2008, SQL:2011, SQL:2016. Dále podpora XML a JSON, automaticky generované sloupce.

## Syntaxe SQL

- SQL je ANSI standard
- mohou však existovat nuance v zápisu pro různé SŘBD, jednotlivé SŘBD pak mají rozšíření platná pouze pro ně
- SQL výrazy nejsou case-sensitive, pro odlišení se však píší velkými písmeny
- většina SŘBD vyžaduje po SQL příkazu středník, tak aby bylo možné odlišit více příkazů zadaných společně na vstupu 

## Datové typy v SQL

- mohou se lišit dle SŘBD
- mezi základní skupiny patří:
  - Textové (CHAR, VARCHAR, NVARCHAR, TEXT, ...)
  - Číselné (INT, DECIMAL, NUMERIC, FLOAT, DOUBLE, ...)
  - Datum/čas (DATE, TIME, DATETIME, YEAR, ...)
  - Ostatní (BIT, BITARRAY, GPS, XML, IMAGE, ...)

Další datové typy můžete nalézt např. [zde](https://www.w3schools.com/sql/sql_datatypes.asp)

## Příkazy jazyka SQL

1. **Data Manipulation Language (DML)** - pro manipulaci s daty
   - SELECT: výběr dat z databáze, výběr podmnožiny
   - INSERT: vložení nových dat do databáze
   - UPDATE: mění data v DTB (editace)
   - MERGE: kombinace INSERT a UPDATE, pokud není odpovídající klíč, tak data vloží, jinak je upraví
   - DELETE: odstraňuje záznamy z DTB
   - EXPLAIN: speciální příkaz, který zobrazuje postup zpracování SQL příkazu
   - SHOW: zobrazuje DTB, tabulky či jejich definice

2. **Data Definition Language (DDL)** - vytvářející či upravující strukturu DTB
   - CREATE: vytvoření nových objektů
   - ALTER: změny existujících objektů
   - DROP: odstraňování objektů

3. **Data Control Language (DCL)** - řízení uživatelských práv
   - GRANT: přidělí userovi oprávnění k určitým objektům
   - REVOKE: odejme práva userovi

4. **Transaction Control Language (TCL)** - řízení transakcí
   - START TRANSACTION (BEGIN): zahájení transakce
   - COMMIT: potvrzení transakce
   - ROLLBACK: zrušení transakce, návrat do původního stavu

5. **OSTATNÍ** - příkazy pro nastavení systémových parametrů, přidávání uživatelů
   - SET: (kódování, časová zóna, )

**SELECT** - standardně se uvádí v DML, ale samotný příkaz SELECT se někdy uvádí jako **Data Query Language - DQL**, až když se k příkazu SELECT doplní klauzule:

- WHERE: zadání podmínek filtrů
- GROUP BY: seskupování záznamů
- HAVING: zadání podmínek nad agregační funkcí
- ORDER BY: seřazení dat

Syntaxe příkazu:

1. SELECT
2. FROM
3. WHERE
4. GROUP BY
5. HAVING
6. ORDER BY

**Konkrétní případy použití na WIKI nebo na YT :D**

## ZDROJE

- https://cs.wikipedia.org/wiki/P%C5%99%C3%ADkazy_jazyka_SQL
- https://elearning.jcu.cz/pluginfile.php/194326/mod_resource/content/1/SQL_uvod.pdf








