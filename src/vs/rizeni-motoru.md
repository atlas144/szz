# Řízení motorů

> Servomotory, krokové motory, stejnosměrné motory, synchronní a asynchronní pohony. Řízení otáček a točivého momentu. Bez senzorové řízení motorů.

V kostce jde o odeslání signálu / impulsu motoru, přičemž délka onoho impulsu ovlivňuje úhel o nějž se motor pootočí. Pro kontrolovanou a přesně řízenou změnu polohy hřídele (otáčku motoru) využíváme tzv. PWM - Pulse-Width Modulation (Pulzně šířkovou modulaci). PWM řídíme DC motory, serva i krokové motory. Pro krokové motory i DC musíme však využít drivery s externím napájením, které upraví napájecí signál dle řídícího z MCU.

## Servo motory, krokové motory a DC motory

Na úvod je třeba říci, že termín servomotor nevypovídá o struktuře motoru jako takového. Servomotory mohou být AC, DC, kartáčové (brushed), bezkartáčové (brushless). Ve většině industriálních robotických systému se využívají právě DC motory.

### DC motor

DC motor využívá vinuté cívky (rotoru) jež rotuje v magnetickém poli vytvořeném permanentními magnety nebo vynutím cívky statoru. Pro hladší a účinnější chod rotoru je využit komutátor. Komutátor zajišťuje přívod a přepínání proudu vedených do rotorových cívek tak, aby byla napájena vždy cívka pod aktivním pólem. Komutátor je rozdělen na izolované lamely po nichž se třou kartáče rotoru (grafitové nebo uhlíkové).

|![DC kartáčové motory - dvoupólový a čtyřpólový](rizeni-motoru/DC_brushed_motor.png "DC kartáčové motory - dvoupólový a čtyřpólový")|
|:--:|
|obr. 1: DC kartáčové motory - dvoupólový a čtyřpólový|
| zdroj: Electronics Tutorials: DC Motors [online]. 2014 [cit. 2023-05-13]. Dostupné z: <https://www.electronics-tutorials.ws/io/io_7.html>|

### DC brushed & brushless motor

Kartáčové DC motory jsou levné a snadno ovladatelné motory, které využívají kartáčů (z grafitu / uhlíku) jež se třou o komutátor (oddělené kovové lamely připojené na hřídeli rotoru) a nimiž vedou proud, čímž mění polaritu a stator se otáčí. Kartáče jsou připojeny k externímu zdroji napájení (viz např. na videu [zde](https://www.youtube.com/watch?v=JU08mR_isaw)). Jde o to, že opačné póly na statoru a rotoru se přitahují a zároveň (na opačné straně) odpuzují a aby se motor neotočil jen o půl otáčky do mrtvého bodu, kde budou proti sobě opačné póly jež se přitáhly, tak kartáče nabité externě skrze komutátor změní polaritu cívek rotoru a tím pádem se motor otočí úplně. 

Brushless motory (bez kartáčů) nemají vinutí na statoru, ale permanentní magnety, které vytváří magnetické pole. Na statoru je vinutí cívkami vytvářející elektromagnetické pole. Jak se otáčí rotor, mění se polarita na jednotlivých cívkách statoru, což vede ke kontinuální rotaci.

**Výhody a nevýhody**
- **DC motor brushed:** menší počáteční náklady (provoz může být dražší, neboť je třeba měnit kartáče); vyšší spolehlivost; lepší kontrola rychlosti motoru (velikostí napětí)
- **DC motor brushless:** delší životnost, bezúdržbový, vyšší účinnost

#### Zapojení DC motoru - sériové a paralelní zapojení

|![Zapojení motorů sériově a paralelně](rizeni-motoru/zapojeni.png "Zapojení motorů sériově a paralelně")|
|:--:|
|obr. 2: Zapojení motorů sériově a paralelně|
|zdroj: Electronics Tutorials: DC Motors [online]. 2014 [cit. 2023-05-13]. Dostupné z: <https://www.electronics-tutorials.ws/io/io_7.html>|

#### Seriové

|![Sériové zapojení cívek](rizeni-motoru/serial.jpg "Sériové zapojení cívek")|
|:--:|
|obr. 3: Sériové zapojení cívek |
|zdroj: YAM, Ian Jonas. Wiring Connection of Direct Current (DC) Motor. Technovation - Technological Innovation and Advanced Industrial Control Technologies [online]. 2013 [cit. 2023-05-15]. Dostupné z: <http://ijyam.blogspot.com/2013/11/wiring-connection-of-direct-current-dc.html>|

#### Paralelní

|![Sériové zapojení cívek](rizeni-motoru/paralel.jpg "Sériové zapojení cívek")|
|:--:|
|obr. 4: Sériové zapojení cívek |
|zdroj: YAM, Ian Jonas. Wiring Connection of Direct Current (DC) Motor. Technovation - Technological Innovation and Advanced Industrial Control Technologies [online]. 2013 [cit. 2023-05-15]. Dostupné z: <http://ijyam.blogspot.com/2013/11/wiring-connection-of-direct-current-dc.html>|

### Krokové motory

Jedná se synchronní brushless motor. Ergo, rotor krokáče tvoří mnoho "zubů" z permanentních magnetů (mohou jich být i stovky) a stator je tvořen mnoha cívkami. Krokový motor se hýbe dle jednotlivých kroků (diskrétně) (na základě počtu magnetů a vynutí). Krokový motor se může otáčet o setiny či desetiny stupně. Např. otočí-li se motor o 360° v průběhu 100 kroků, tak každý krok odpovídá 3.6°. Nejpoužívanější krokové motory se otáčí 200 kroky za otáčku (1 krok = 1.8°).

Použití krokáčů je nasnadě v 3D tiskárnách, ploterech případně robotických systémech s velmi jemnou kontrolou pohybu.

#### Schéma krokového motoru a princip pohybu

|![Schéma krokového motoru](rizeni-motoru/stepper.gif "Schéma krokového motoru")|
|:--:|
|obr. 5: Schéma krokového motoru|
| zdroj: Electronics Tutorials: DC Motors [online]. 2014 [cit. 2023-05-13]. Dostupné z: <https://www.electronics-tutorials.ws/io/io_7.html>|

Stator krokáče výše má cívky generující elektromagnetické pole, označené ADCB. Pokud povedeme proud do cívek ADCB, krokáč se bude točit na jednu stranu, pro otáčení v protisměru ABCD. Rotor se otočí vždy o krok (step angle).

Krokáč má 6 magnetických zubů a 4 cívky (nabité +-, aby bylo možné realizovat jeden krok). Tedy motor může udělat 24 kroků (6x4), ergo jeden krok je 15° (360 / 24). Teto motor je 4-fázový, právě dle počtu cívek.

Pozici motoru je možné odvodit od počtu impulsů, neboť každý impuls (budeme-li je posílat odděleně -> time-delayed) odpovídá 1 step angle. 

Takže, pokud náš motor bude mít jeden step angle 3.6° a budeme-li chtít otočit jej na 216° vůči původní pozici, tak motoru pošleme 60 impulsů (216/3,6).

Dosažení  kroku lze několika způsoby:

- přidáním zubů na rotor a stator
- spínáním více cívek naráz, čímž se rotor dostane do polohy "mezi cívkami" a tím realizuje fragment kroku

**Typy krokových motorů**
1. **Pasivní krokové motory - Variable Reluctance Motors**\
    Fungují tak, jak je popsáno výše, Rotor je tvořen ocelovými pláty seřazenými za sebou (jakási laminace) a otáčí se na základě působení elektromagnetického pole generovaného cívkami statoru.
2. **Aktivní krokové motory**\
   Rotor je tvořen permanentním magnetem. Točí se opět na bázi působení elektromagnetického pole.
3. **Hybridní krokové motory**\
    Rotor je tvořen magnetem a zuby, které se řadí proti zubům statoru, buď se zarovnají plně proti sobě, nebo jsou zuby rotoru srovnané přesně proti mezerám mezi zuby statoru, nebo jsou vyrovnány do poloviny zubů statoru (viz obrázek).

 |![Hybridní krokový motor](rizeni-motoru/stepper_h.PNG "Hybridní krokový motor")|
|:--:|
|obr. 6: Hybridní krokový motor|
| zdroj: Lesics. YouTube. *How does a Stepper Motor work?* [online]. [cit. 2023-05-15]. Dostupné z: <https://www.youtube.com/watch?v=eyqwLiowZiU>|

### Servo motor

Servo motory jsou jakýmsi komplexnějším systémem složeným z těchto komponent: DC motor, micro-controller, poziční sensor, převodovka. Převodovka je napojena přímo na hřídel DC motoru, čímž zvětšuje kroutící moment. Zabudované MCU zpracovává signál z externí řídící jednotky a prostřednictvím impulsu jej předá motoru. Zpětnou vazbu o natočení motoru nám dává potenciometr.

Schéma servo-motoru můžete vidět níže.

|![Schéma servomotoru](rizeni-motoru/servo.jpg "Schéma servomotoru")|
|:--:|
|obr. 6: Schéma servomotoru|
| zdroj: Servos explained. Sparkfun: Start something [online]. [cit. 2023-05-15]. Dostupné z: <https://www.sparkfun.com/servos>|

### Řízení motoru pomocí PWM

Pulsně-šířková modulace je obšírně rozebírána v otázce č. [3.10](https://atlas144.codeberg.page/szz/vs/pulzni-modulace.html).

Servomotory mají na rozdíl od DC většinou omezený rozsah otočení hřídele, byť některé servomotory se mohou otáčet kontinuálně. Obecně se servomotory otáčí o 180°nebo 210°.

V závislosti na délce signálu se hřídel serva otočí pro/proti směru hodinových ručiček. Otočení dle délky signálu ilustruje obrázek níže. Vyšleme-li signál o délce 1,5ms, servo se otočí v protisměru hodinových ručiček o 90° (do neutrální polohy), pošleme-li impuls kratší než 1,5ms, otočí se servo po směru hodinových ručiček opět o 90°. Bude-li délka signálu větší než 1,5ms otočí se hřídel proti směru do polohy 180° (tedy o 90° vůči neutrální poloze).

Konvenční interval mezi impulsy bývá u hobby serva 20ms, tedy MCU pošle 50 signálů za vteřinu.

|![Schéma servomotoru](rizeni-motoru/servo_rot.PNG "Schéma servomotoru")|
|:--:|
|obr. 7: Rotace servomotoru|
| zdroj: SCHERZ, Paul a Simon MONK. Practical electronics for inventors. Fourth edition. New York: McGraw-Hill Education, [2016]. ISBN 978-1-25-958754-2.|

### Synchronní a asynchronní motory

Synchronní motory jsou takové motory, u nichž se rotor otáčí stejnou rychlostí s jakou jsou zapojovány cívky statoru. Frekvence otáčení rotoru je synchronní k frekvenci zapínání.

Asynchronní motory se vyznačují tím, že rotor se točí s pomaleji, než je zapínání/frekvence pouštění proudu do cívek.

Jen pro zajímavost - kroutící moment může dodávat ASM jen tehdy, točí-li se rotor pomaleji, než se točí magnetické pole statoru. Tomuto se říká **SKLUZ**. 

\\[S = \frac {n_1 - n_2}{n_1} * 100\\]

- S - skluz
- \\(n_1\\) - otáčky magnetického pole statoru
- \\(n_2\\) - otáčky rotoru

Skluz bývá mezi 2-10 %.

Třífázové ASM jsou nejrozšířenější motory. Lehké na údržbu a levné. Používají se tam, kde je třeba robustní pohon (průmyslové dopravníky, pohony výhybek na dráze, jezdící schody...).

## Zdroje

- https://www.youtube.com/watch?v=-J1Km7h38U8&t=227s
- https://www.electronics-tutorials.ws/io/io_7.html
- https://www.analogictips.com/pulse-width-modulation-pwm/
- https://www.sparkfun.com/servos
- SCHERZ, Paul a Simon MONK. Practical electronics for inventors. Fourth edition. New York: McGraw-Hill Education, [2016]. ISBN 978-1-25-958754-2.
- https://cs.wikipedia.org/wiki/Asynchronn%C3%AD_motor
- https://en.wikipedia.org/wiki/Stepper_motor