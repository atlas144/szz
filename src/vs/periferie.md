# Periferie

> Principy řízení periferii, adresové mapování periferii, řadič přerušení, časovače, čítače, watchdog, řadič displeje, řadič klávesnice, programovatelné hradlové pole.

## Úvod

**Periferií** můžeme rozumět jakékoliv zařízení, jež můžeme připojit k počítači, přičemž toto zařízení není klíčové pro chod počítače jako takového. Příklady: *externí disk*, *tiskárna*, *klávesnice*, *monitor*. Periferie rozdělujeme na

- **vstupní (I)**: získávají data z okolí
- **výstupní (O)**: poskytují data do okolí
- **vstupně/výstupní (I/O)**: dělají obojí z výše uvedeného

Jakým způsobem jsou ale periferie řízeny? Neboli, jakým způsobem daná periferie ví, kdy může pracovat?

K přenášení informací mezi procesorem a periferiemi slouží tzv. **sběrnice**. Ty slouží ale také ke komunikaci mezi částmi uvnitř mikroprocesoru nebo také mezi počítačem a okolím.

Rozlišujeme tři druhy **sběrnic**

- **adresová sběrnice**
  - pokud chce mikroprocesor data číst nebo je zapisovat, **musí nějakým způsobem sdělit místo čtení i zápisu**
  - toto místo je identifikováno **adresou**, která je přenášena po adresové sběrnici
  - **zdrojem této informace je mikroprocesor**
  - počet bitů adresy odpovídá počtu vodičů adresové sběrnice
- **řídící sběrnice**
  - řídící sběrnice je souhrn jednotlivých signálů aktivních v různých časových okamžicích s různým významem, které mívají různé zdroje
  - některé signály jsou generovány mikroprocesorem, některé mohou být ovlivňovány jinými bloky
  - k jednotlivým blokům jsou pak **přivedeny pouze signály jim patřící**.
  - mezi nejčastější **signály řídící sběrnice** patří například:
    - **RESET**
      - je jím vybaven každý mikroprocesor
      - aktivován je uživatelem nebo jiným přídavným obvodem
      - tento signál uvede mikroprocesor do jeho výchozího stavu
    - **Memory Read (MR)**
      - zabezpečuje časování čtení z pamětí či jiných bloků
    - **Memory Write (MW)**
      - zabezpečuje časování zápisu do pamětí či jiných bloků
    - **Input/Output Read/Write (IOR/IOW)**
      - slouží pro čtení z nebo zápis do zařízení
    - **READY**
      - určuje připravenost obvodu
- **datová sběrnice**
  - **slouží pro přenos všech dat v počítači**
  - data jsou vždy přenášena mezi dvěma bloky počítače
  - typickým příkladem tohoto přenosu je **přenos z paměti do mikroprocesoru**
  - **mikroprocesor se účastní, až na výjimku jako přijímač a vysílač** všech přenosů v počítači
  - v praxi je nutné, aby v jakémkoliv okamžiku byl aktivní pouze jeden vysílač, jinak řečeno budič sběrnice (pokud by tomu tak nebylo, hrozí zničení obvodu)
  - je tedy nutné vybavit bloky připojované na datovou sběrnici obvody, které umožňují odpojení tohoto bloku od datové sběrnice (**třístavové budiče sběrnice**)
  - nejdůležitější parametry datové sběrnice: 
    - šířka neboli počet bitů (kolik bitů je schopno se přenést najednou; výrazně ovlivňuje čas přenosu; maximálně stejný počet bitů jako procesor)
    - časování

Sběrnice mohou být **sdílené** a **nesdílené**.

- **sdílená**: sdílí vodiče pro adresy a data
    - *ISA* sběrnice (už se léta nepoužívá)
- **nesdílená**: zvlášť dráty pro data, adresy, řízení
    - *PCI* sběrnice

## Řadič

Jedná se o elektrický obvod, sloužící pro připojení periferií. Zároveň slouží jako mezistupeň pro komunikaci mezi periferiemi a zbytkem počítače. Tato komunikace probíhá skrze **sběrnici**.

### Řadič displeje

Jedná se o mikrokontrolér, které **se stará o výstup informací v podobě obrazu na monitor**. Je **zodpovědné za konverzi digitálního obrazu nebo video dat** (kupříkladu z grafické karty) na formát, jež je zobrazitelný na obrazovce.

**Řadič displeje** take obstarává další funkce, jako je obnovovací frekvence monitoru, nastavení jasu, nebo elektrickou spotřebu displeje.

U dnešních stolních počítačů umožňuje připojení displeje skrze různá rozhraní (HDMI, Display Port, VGA...)

### Řadič klávesnice

**Řadič klávesnice** je typicky mikrokontrolér zabudovaný v klávesnici samotné nebo v základové desce počítače. Skenuje **klávesnicovou matici** což je mřížka drátů a obvodů, které připojují každou klávesu k **řadiči**.

Pokud je některá z kláves stlačena, tak **řadič** tuto akci převede na digitální signál, který je zaslán skrze **sběrnici** procesoru. Na základě toho může operační systém, potažmo jiný software tuto akci vyhodnotit. **Řadič** také zajišťuje komunikační protokol mezi klávesnicí a počítačem, aby každý úhoz do klávesnice byl správně vyhodnocen.

## Principy řízení periferií

Vstupní a výstupní zařízení bez ohledu na způsob připojení používají tři základní techniky řízení přenosu o které se stará **řadič periferních zařízení**:

- **programové řízení vstupu a výstupu**
- **řízení na základě přerušení**
- **přímý přístup k operační paměti (DMA – Direct Memory Access)**

### Programové řízení

Procesor řekne skrze **sběrnici** **řadiči**, že chce provést periferní operaci. Nastaví parametry a nakonec „start bit“. Řadič kontroluje start bit a pokud je tam log. `1`, začne provádět operaci na zařízení. To, jakým způsobem bude operace provedena, záleží na řadiči. Po dokončení výsledek vystaví na výstup a procesor pomocí tzv. **pollingu** programově (v nějakém cyklu) kontroluje stav (zaměstnává tím procesor), dokud řadič nebude mít data ke zpracování.

### Řízení na základě přerušení

Procesor řekne skrze **sběrnici** řadiči, že chce provést periferní operaci. Nastaví parametry a nakonec „start bit“. Řadič kontroluje start bit a pokud je tam log. `1`, začne provádět operaci na zařízení. Po dokončení řadič informuje procesor, že má k dispozici data. Procesor má k dispozici **řadič přerušení** který umožňuje vyřizovat přicházející přerušení podle jejich nastavené priority.

Z hlediska procesoru jde o **obsluhu přerušení**, kdy si uloží svůj stav do paměti, obslouží událost jež přerušení vyvolala a poté pokračuje načtením uloženého stavu.

### DMA

Řadič si požádá o přímý přístup do paměti, pokud mu bude vyhověno, tak výsledky operací prováděných řadičem jsou zapisovány do paměti bez účasti procesoru.

Jakým způsobem ale **procesor** ví, kde se daná **periferie** nachází? Tedy, jakou **adresu** má vystavit na **adresní sběrnici**?

## Adresové mapování periferií

Operační systém si vytváří tzv. **virtuální adresové prostory**, které jsou abstrakcí nad fyzickou adresací paměti a dalších adresovatelných komponent počítače (hlouběji popsáno v otázce [2.1.7](https://atlas144.codeberg.page/szz/ikt/ap/pametovy-podsystem-pocitace.html)). **Umístění periferních zařízení do některého z těchto prostorů nazýváme mapováním periferií**.

Podle architektury počítače se **vstupy/výstupy (periferie)** dělí na:

- **paměťově mapované**: registry jsou adresovány jako paměť, přístupné pomocí běžných operací čtení a zápisu do paměti
- **izolované**: registry jsou přístupné pomocí speciálních instrukcí (zpravidla nazývaných `IN` a `OUT`); díky tomu jsou adresní prostory paměti a vstupů/výstupů oddělené

Těmto registrům, obsahující adresy I/O zařízeních, se také říká **vstupní a výstupní řadiče**

**Paměťový prostor** bývá obsazen více jak jednou fyzickou pamětí nebo periferním zařízením, a proto je nutné při přenosech dat s mikroprocesorem rozhodnout, které zařízení je ke komunikaci určeno. Tímto úkolem je právě pověřen **adresový dekodér**. Jeho **výstupy jsou v podstatě signály CS (Chip Select) pro jednotlivé obvody**. 

Signál CS připojuje daný obvod k datové sběrnici tak, že **jeho** sběrnici přepne ze stavu vysoké impedance do aktivního stavu.

|![Připojení V/V zařízení v PC pomocí sběrnice](periferie/connect_VV.PNG "Připojení V/V zařízení v PC pomocí sběrnice")|
|:--:|
|*obr. 1:* Připojení V/V zařízení v PC pomocí sběrnice|

S rozvojem počítačů vznikla **standardizace** v oblasti připojování periferních zařízení došlo k rozdělení sběrnic na 2 úrovně:

- **vnitřní (obvodová)** - sběrnice propojující funkční jednotky uvnitř integrovaného obvodu nebo tištěného spoje
- **vnější (systémová, společná)** - sběrnice propojující zásuvné jednotky nebo funkční celky

## Časovače

Jedná se o **periferní I/O zařízení**.

**Časovač** umožňuje plánovat události při kterých proces může vyvolat službu operačního systému. Zpravidla je k dispozici jen jeden časovač a programová časová fronta. Hardwarový časovač se pak používá pro odměření času do nejbližší události ve frontě. 

Do fronty jsou události řazeny tím způsobem, že se zjistí, po které události má k nově přidávané události dojít a spočítá se a zaznamená čas (který má uplynout mezi těmito dvěma událostmi). **Při vynulování časovače se vybere první událost z fronty, provede se příslušná akce a hardwarový časovač se přeprogramuje na čas následující události**.


## Čítače

Další z **periferních I/O zařízení**

Úkole čítače je **inkrementovat obsah registru**. Pokud se tato hodnota dostane na danou mez, **čítač** se vynuluje.

## WatchDog Timer

Jedná se o speciální případ **čítače**, jež se často používá v **embedded zařízeních**. Jeho účelem je detekce selhání části zařízení. 

Funguje na principu klasického čítače, který se **po určité hodnotě na příkaz operačního systému vynuluje**.  Pokud k tomuto vynulování nedojde, tak **WatchDog timer** vygeneruje **timeout signál**. Pro procesor je tento signál znamením, že má zahájit opravující akce. Může jít například o vypnutí motorů, zahájení restartu apod.

Jeho využití je i v klasických operačních systémech, například pokud spouštíme nedůvěryhodný kód v režimu sandbox. **WatchDog timer** dává tomuto kódu pouze omezený čas procesoru, aby se zabránilo škodlivé činnosti tohoto kódu.

**WatchDog Timer** se také používá v **operačních systémech reálného času** aby bylo zajištěno, že proces dokončí svůj úkon v předem stanoveném čase. Pokud toto neudělá, bude terminován.

Tento čítač se často využívá tam, kde není možné k systému fyzicky přistoupit, nebo alespoň ne v rozumném čase. Systém se tedy musí být schopen zotavit sám.

## Programovatelná hradlová pole (FPGA - Field Programable Gate Array)

Jedná se o **digitální logické zařízení tvořené z logických hradel**, které může být naprogramováno tak, aby jeho logické obvody plnily funkci definovanou uživatel i poté, co je zařízení vyrobeno. Takové hradlové pole může být uzpůsobeno přesně podle potřeb, aby splňovaly požadavky aplikace, jako **zpracování signálu, zpracování dat, nebo řídící logika**. 

Jsou používány hlavně v řídících systémech, kde je díky tomu možnost splnit přesné požadavky aplikace.  Mohou být také integrovány v dalším komponentách, jako jsou například senzory.

Příkladem použití FPGA může být například nutnost zpracovat výstup z mikrofonu. Můžeme toto předat procesoru a nechat ho, ať zjistí frekvence.

Nebo můžeme využít několik bloků programovatelných hradel, aby nám dokázali přečíst jednotlivé vzorky z mikrofonu. Tato logika je naprosto nezávislá na tom, co zrovna dělá procesor, čímž jsme ušetřili cenný čas procesoru. V praxi se **FPGA** používají tedy spíše jako doplněk již existujících logických celků.

Většina moderních FPGA však nejsou plně programovatelná a již obsahují bloky tvořené z napevno (hardwarově) naprogramovaných hradel.

|![Podoba moderního FPGA pro zpracování digitálního signálu](periferie/FPGA.PNG "Podoba moderního FPGA pro zpracování digitálního signálu")|
|:--:|
|*obr. 2:* Podoba moderního FPGA pro zpracování digitálního signálu|

FPGA umožňují "doprogramování" **řadičů**. Například lze díky tomu nastavit **řadiči klávesnice** jiné typy klávesnic, jiné jazyky, nebo možnost vykonání akce při stisknutí více kláves zároveň.

## Zdroje

- Ing. Petr Olivka. Katedra informatiky FEI VŠB-TU Ostrava. *Komunikace s perifériemi*.[online]. @2010 [citováno 27. 04. 2023].<br> Dostupné z: <https://poli.cs.vsb.cz/edu/arp/down/komunikace.pdf>
- Wikipedie: Otevřená encyklopedie. *Řadič přerušení*.[online]. @2023 [citováno 27. 04. 2023].<br> Dostupné z: <https://cs.wikipedia.org/wiki/%C5%98adi%C4%8D_p%C5%99eru%C5%A1en%C3%AD>
- Wikipedia: Free encyclopedia. *Video display controller*.[online]. @2023 [citováno 27. 04. 2023].<br> Dostupné z: <https://en.wikipedia.org/wiki/Video_display_controller>
- Kalábovi: Kalábovic wikina. *Principy řízení a připojování periferních zařízení* [online] @2022 [citováno 27.04.2023]<br> Dostupné z: <https://kalabovi.org/pitel:isz:principy_rizeni_a_pripojovani_perifernich_zarizeni>
- Wikipedia: Free encyclopedia. *Field-programmable gate array*.[online]. @2023 [citováno 27. 04. 2023].<br> Dostupné z: <https://en.wikipedia.org/wiki/Field-programmable_gate_array>
- Yossi Kreinin. *How FPGAs work, and why you'll buy one* [online] @2013 [citováno 27. 04. 2023].<br> Dostupné z: <https://www.embeddedrelated.com/showarticle/195.php>
