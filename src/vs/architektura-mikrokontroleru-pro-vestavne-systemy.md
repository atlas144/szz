# Architektura mikrokontrolérů pro vestavné systémy

> rozdělení mikrokontrolerů (dvě hlavní architektury - von Neumannova, Harvardská architektura, podle souboru instrukcí - CISC, RISC, DSP), paměti ve vestavných systémech (s libovolným přístupem, se sekvenčním přístupem, semipermanentní paměť)

V dnešní době existuje několik typů/rodin **mikrokontrolérů** které se  používají v **embedded** systémech, uvedeme si ty nejvíce používané společně se základními parametry a příklady **vývojových desek**, na kterých se používají.

<div class="note">

### Mikrokontrolér vs Mikropočítač

- **mikrokontrolér** je CPU společně s registry a řadiči + základní periferie
- **mikropočítač** je vývojová deska na které se nachází **mikrokontrolér** společně s dalšími periferiemi

</div>

**Základní používané mikrokontroléry**:

- **PIC**  
  - používá RISC instrukce (RISC architektura) -> lepší modely mají přidané **DSP** instrukce
  - jedná se o **Harvardskou architekturu**
  - klasicky je 16 - bitový, dělají se i 32 - bitové verze
  - obsahuje **JTAG** sběrnici
  - používá se v samostatných embedded systémech jako jsou například alarmy, telefony nebo senzorika (jsou velmi malé)
  - základní adresní módy (přímý, nepřímý, relativní)
- **ARM**
  - používá RISC instrukční sadu
  - umožňuje velké množství adresních módů
  - mají menší spotřebu el. energie
  - v základu 32 bitová architektura (může být i 64 bitová)
  - v embedded systémech se tyto procesory využívají převážně v telefonech, televizích, nebo GPS navigacích
- **8051**
  - používá lehce upravenou CISC instrukční sadu
  - je 8 bitový s 16 bitovou adresní sběrnicí
  - jedná se o **Harvardskou architekturu**
  - jeho využití je pro základní aplikace, které nejsou paměťově náročné -> hodí se pro výukové účely
- **AVR**
  - používá upravenou RISC sadu instrukcí -> **Advanced Virtual RISC**
  - jedná se o **Harvardskou architekturu**
  - dělí se na 3 kategorie:
    - **TinyAVR (ATtiny)**: malá paměť, zabírá málo místa (fyzicky), hodí se pro jednoduché úkony
    - **MegaAVR (ATmega)**: nejpoužívanější, dostatečná paměť, velké množství zabudovaných periferií
    - **XMegaAVR**: používaný komerčně pro komplexní projekty, kde je potřeba velká paměť a rychlost
  - podporuje **proudové zpracování instrukcí**
- **MSP** 
  - 16 bitový
  - RISC architektura
  - malá spotřeba, malá velikost -> stejně jako všechny ostatní

 ## Instrukční soubor

Instrukční soubory RISC a CISC jso u popsaný v otázce [Instrukční soubor](https://atlas144.codeberg.page/szz/ikt/ap/soubor-instrukci.html). 

 ### DSP (Digital SignaL Processing)

Jedná se procesory, které jsou optimalizované pro pro zpracování signálu. Tyto procesory používají speciální sadu instrukcí -> **DSP instrukcí**. Tyto procesory nepodporují přerušení. Používají nestandardní paměť a jsou tím pádem obtížnější na programování.

Platí, že:

 - pokud chceme zpracovávat zvuk, video, rychle řídit motory, nebo cokoliv co vyžaduje zpracovávání proudu dat velkou rychlostí, tak použijeme **DSP procesor**
 - pokud chceme kontrolovat tlačítka, měřit teplotu nebo ovládat LCD display, použijeme klasický mikrokontrolér
 **DSP** slouží ke zpracování signálu tím, že **signál se převede na digitální, zpracuje se a převede se opět na analogový** 

|![Princip fungování DSP](arch-mikorkontroleru-provestavene-systemy/DSP.png "Princip fungování DSP")|
|:--:|
|*obr. 1:* Princip fungování DSP|
| *zdroj:* <https://en.wikipedia.org/wiki/Digital_signal_processor#/media/File:DSP_block_diagram.svg> |

## Paměti

Máme 3 druhy pamětí:

- **paměť s libovolným přístupem**: RAM, rychlost získání dat nezávisí na tom, kde jsou data v paměti uložena. Používá **řadič paměti** k nalezení buňky s daty na základě adresy poskytnuté procesorem.
- **paměť se sekvenčním přístupem**: Paměť lze přečíst jen postupně, například posuvný registr, nebo paměti typu FIFO/LIFO. **Magnetická páska** je příklad **paměti se sekvenčním přístupem**.
- **semipermanentní paměť**: Jedná se o **nevolatilní** druh paměti. Tato paměť si udržuje v ní uložené hodnoty i bez nutnosti stále připojení k elektrické síti. Dřívější druhy těchto pamětí nemohli byt vymazány bez toho, aniž by byl nutná fyzický zásah (vyjmutí paměti, posvícení UV světlem v případě **EPROM**).
  - Dnes se využívá **EEPROM** -> **elektricky vymazatelná paměť pouze pro čtení**. Tato paměť dokáže i zapisovat a klasicky vydrží přes 200 tisíc zápisů. V embedded zařízeních je tento druh paměti využit k **uchování** dat po odpojení zařízené od elektrické sítě.
  - Princip spočívá v tom, že každá paměťová buňka se skládá z tranzistorů NMOS. **Je složena ze dvou vrstev**, jedna je z **nitridu křemíku** a druhá z **oxidu křemičitého**. Mezi těmito vrstvami je náboj uložen.
  - Při **zápisu** se přivede na příslušný adresový vodič (buňku) záporné napětí a datový vodič buňky se uzemní. Tranzistor se otevře a vznikne v něm prahové napětí.
  - Při **čtení** se přivede na adresový vodič **záporný impuls**. **Tranzistor** s malým prahovým napětím se otevře a vyvede proud na **datový vodič**. V případě velkého prahového napětí zůstává otevřen.
  - Dalším druhem paměti je **Flash paměť**. Je také nevolatilní, ale na rozdíl od **EEPROM** je rozdělena do jednotlivých sekcí, které lze zvlášť mazat. Tento druh paměti (resp. jeho NAND varianta) je využíván v **SSD**.

Více o těchto druzích pamětí se lze dočíst v otázce [Paměťový podsystém počítače](https://atlas144.codeberg.page/szz/ikt/ap/pametovy-podsystem-pocitace.html).

## Zdroje
- <https://en.wikipedia.org/wiki/PIC_microcontrollers>
- <https://www.watelectronics.com/pic-microcontroller-architecture-and-applications/>
- <https://en.wikipedia.org/wiki/ARM_architecture_family>
- <https://en.wikipedia.org/wiki/Digital_signal_processor>
- <https://cs.wikipedia.org/wiki/EEPROM>
- <https://geraldine.fjfi.cvut.cz/~oberhuber/data/hpc/paa/prezentace/03-sekvencni-architektury.pdf>
