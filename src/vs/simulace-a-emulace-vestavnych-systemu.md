# Simulace a emulace vestavných systémů

> Postupy pro simulování a emulování reálných procesů v použitých zařízeních. Druhy emulátorů a jejich rozhraní (SPI, JTAG, inside emulátory). Příprava procesorů pro emulaci.

## Simulace vs Emulace

Je rozdíl mezi **simulací a emulací procesů**, ač se oba dva tyto termíny používají zaměnitelně. Emulace umožňuje vytvořit virtuální zařízení, které má stejné schopnosti, jako originál. Emulátory jsou psané v low-level programovacích jazycích, jako je například Assembler. Překládají strojový kód, aby co nejvíce odpovídal chodu daného zařízení,jak **hardwarově, tak softwarově**, což často vede k pomalému chodu tohoto zařízení. Příkladem může být emulátor Android telefonu při programování mobilních zařízení. Jejich hlavní využití je při debuggingu hardware.

Na druhou stranu **simulátor** běží nad operačních systémem jako aplikace. Jeho účelem je pouze navodit pocit toho, že pracujeme s nativním zařízením. **Nereprezentuje hardware, pouze uživatelské prostředí**. Používá se pro **simulování** chování a konfigurace nativního zařízení. Využití nalezne také při testování aplikací vyvíjených pro **simulované** zařízení.

<div class="note">

### Co je to proces?

**Procesem** v **embedded systémech** myslíme **program**, v případě emulace poté **program společně s HW**, který vykonává jednotlivé kroky popsané **programem**.

**Definice**: série kroků nebo operací za účelem dosažení určitého cíle. 

</div>

## Postup simulace procesu

1. **Definujeme cíl simulace**. Cílem může být například **optimalizace výkonu, analýza latence, ověřování nových funkcionalit atd.** Prostě chceme **proces (program) otestovat**.
2. **Identifikujeme proměnné systému**: Musíme zjistit, které části celého systému proces ovlivňují. Může se jednat o vstupní/výstupní signály, výpočetní zatížení, spotřebu energie apod. **Toto je důležité pro vytvoření adekvátního modelu**.
3. **Vytvoření modelu**: Na základě **identifikovaných proměnných** vytvoříme matematický nebo abstraktní model systému.
4. **Specifikujeme vstupní data**: Musíme určit vstupní data, která představují reálné signály - scénáře jež mohou nastat. Při simulaci můžeme systému tyto data předat vytvořením simulační sady dat, nebo vytvořením skriptu pro generování dat.
5.  **Implementujeme simulaci**: Vytvoříme z **abstraktního/matematického modelu** model v **simulačním prostředí (například Simulink)** nebo nahrajeme výkonný kód do vývojové desky.
6.  **Spustíme simulaci**: Můžeme použít různé sady vstupů pro otestování různých scénářů.
7.  **Sesbíráme a analyzujeme data**: Sesbíráme výstupní data, která **odpovídají výsledkům vůči cílům simulace**.

## Postup emulace procesu

1. **Definujeme cíl emulace**: Cíle může být například **ověření správnosti návrhu systému, testování nových funkcionalit, simulaci reálných podmínek atd.**
2. **Identifikujeme klíčové komponenty**: Musíme rozpoznat klíčové komponenty systému, který chceme **emulovat**, jako jsou například mikrokontroléry, senzory, aktuátory atd.
3.  **Vytvoříme emulační prostředí**: Vytvoříme prostředí, které je schopné **emulovat** reálné podmínky, ve kterých bude **embedded** systém provozován. Může jít o testovací laboratoř, nebo software v podobě emulační platformy.
4. **Implementujeme emulační software**: Jedná se o vytvoření softwaru, který simuluje chování hardware. To zahrnuje vytvoření **emulačního kódu**, který simuluje jednotlivé funkce a reakce našeho **embedded systému**.
5.  **Nakonfigurujeme vstupy**: **Musíme určit vstupní data, která představují scénáře jež mohou nastat a podmínky, ve kterých je systém provozován.** To může zahrnovat **emulaci senzorů, vstupů od uživatele, komunikačních rozhraní a dalších vstupních kanálů**.
6. **Spustíme emulaci**: **Spustíme emulaci a budeme sledovat její průběh**. Můžeme měnit vstupy na základě scénářů, jež chceme sledovat. 
7.  **Sesbíráme a analyzujeme vstupní data**: Na základě **cílů emulace** sesbíráme data, která popisují chování **embedded systému**.

## Druhy emulátorů

Emulátory se v základu dělí na dvě částí 

- **hardwarové emulátory**:
  - Jedná se o emulátory, které **jsou schopné vytvořit přesnou kopii hardware, včetně jeho procesoru a paměti.** Nejznámějšími nástroji pro **hardwarovou emulaci** jsou virtualizační nástroje **VMware, nebo VirtualBox**. **Emulují celý systém, popřípadě prostředí, ve kterém systém operuje**.
- **softwarové emulátory**:
  - Jedná se o emulátory, které umožňují spustit program v prostředí, které je pro něj nativní. Můžeme takto spouštět programy, které nejsou na novějších zařízeních spustitelné. **Emulují pouze software, jež na zařízení běží**.
  - Zároveň tyto emulátory **umožňuji komunikaci dvou zařízení, které spolu nejsou kompatibilní, například pomocí JTAG sběrnice**. **Procesory** využívají **JTAG** k **umožnění přístupu k emulačním/debugovacím funkcím**. Pokud bychom chtěli upravovat flash paměť nějakého zařízení, můžeme k tomu použít **emulátor flash paměti**, který připojíme **pomocí SPI sběrnice**. 

<div class="note">

### Další využití JTAG sběrnice

Využití **JTAG sběrnice** je dále převážně v **debuggingu**, protože **je možné díky této sběrnici přistupovat k operační paměti připojeného zařízení**, nebo dokonce k samotným instrukcím mikrokontroléru/procesoru. Vlastně umožňuje low-level programování zařízení, ke kterému ho připojíme, můžeme s ním programovat **flash paměť, FPGA, nebo EEPROM**.

*Zajímavost*: Microsoft u svých konzolí XBox pravidelně banuje účty uživatelů, kteří používají JTAG sběrnici konzole.

</div>

Dále můžeme tyto emulátory dělit podle toho, **jak se liší způsobem využití a prostředím, ve kterém jsou využívány**.

- **Terminálové emulátory**
  - Simuluje klasický terminál (ten se sestává z displaye a klávesnice a představoval rozhraní pro ovládání počítače člověkem). **Terminálové emulátory** slouží ke stejnému účelu. Např. klasické terminálové aplikace v Linuxu.
- **Tiskárnové emulátory**
  - Simulují firmware tiskáren. Spousta těchto emulátorů je zaměřena na HP tiskárny kvůli velkému množství software pro HP tiskárny. **Emulátor je schopný pracovat se softwarem/firmwarem vyvíjeným pro tiskárny**.
- **Herní (konzolové) emulátory**
  - Umožňují emulovat hardware dané herní konzole a spouštět na ní software v podobě her pro tuto konzoli.
- **Celosystémové emulátory**
  - Tyto emulátory emulují vše, včetně CPU, chipsetu, BIOS, periferie a přerušení. **Tyto emulátory jsou těžké vytvořit. Důvodem je nutnost naprostého odproštění od systému, na kterém emulátor běží.**
- **CPU emulátory**
  - **Jedná se o software, který emuluje fyzické CPU.** Pro každou instrukci vykonanou emulátorem, je vykonaná identická instrukce na hostujícím CPU. **Interpert**, který sleduje kód vykonávaný emulovaným programem je nejjednodušším typem CPU emulátoru. **Umožňuje spouštět programy, které jsou psané pro jinou architekturu CPU, než je ta přítomna na nativním zařízení.**
- **Serverové emulátory**
  - **Emulují zařízení, na kterém běží serverová aplikace.** Chod serveru je simulován za pomocí umělé zátěže.
- **Síťové emulátory**
  - Testování sítě v laboratoří je **síťová emulace**. Často **obsahuje ztráty packetů či latenci** pro navození pocitu reálného prostředí.
- **Mobilní emulátory**
  - **Mobilní emulátory** simulují hardware a software zařízení na hostujícím zařízení. Například emulátor Android.

<div class="note">

### Emulace sběrnic

Pokud emulujeme nějaký systém, je možné emulovat i jednotlivé sběrnice, které tento systém využívá. Emulace pak zahrnuje princip fungování jednotlivých sběrnic.

</div>

## Příprava procesorů pro emulaci

K emulačním schopnostem procesoru můžeme přistoupit pomocí JTAG sběrnice. Čtením a zápisem do instrukčního registru můžeme poté skrze procesor emulovat chování jiného systému.

## Zdroje

- Simulation. *Wikipedia: The Free Encyclopedia*. [online] @2023 [citováno 13.05.2023].<br>Dostupné z: <https://en.wikipedia.org/wiki/Simulation>
- What Are Emulators? Definition, Working, Types, and Examples. *Chiradeep BasuMallick*. [online] @2023 [citováno 13.05.2023].<br>Dostupné z: <https://www.spiceworks.com/tech/devops/articles/what-are-emulators/>
- What’s the Difference Between Hardware and Software Emulation?. *Richard Reynolds*. [online] @2022 [citováno 13.05.2023].<br>Dostupné z: <https://history-computer.com/whats-the-difference-between-hardware-and-software-emulation/>
- JTAG. *Wikipedia: The Free Encyclopedia*. [online] @2022 [citováno 13.05.2023].<br>Dostupné z: <https://en.wikipedia.org/wiki/JTAG>
- PROMJet SPI. *EmuTec*. [online] @2023 [citováno 13.05.2023].<br>Dostupné z: <https://www.emutec.com/flash_spi_emulator_hardware_promjet.php>
- JTAG Boundary-Scan Benefits FAQ. *Corelis*. [online] @2023 [citováno 13.05.2023].<br>Dostupné z: <https://www.corelis.com/education/corelis-faq/>
- What is JTAG and how can I make use of it?. *XJTAG*. [online] @2023 [citováno 13.05.2023].<br>Dostupné z: <https://www.xjtag.com/about-jtag/what-is-jtag/>
