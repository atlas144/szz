# Komunikační rozhraní

> Sériové a paralelní, synchronní a asynchronní. Rozdělení podle rychlosti přenosu. Adresování a komunikace po rozhraní. Protokoly pro rozhraní. Rozhraní pro vývojové systémy JTAG, SPI, apod.

Komunikační rozhraní je část systému, která mu **umožňuje vyměňovat si informace s jinými** systémy. Je nutné, aby bylo rozhraní **na obou stranách komunikace shodné** (používalo stejný protokol).

<div class="note">

### Port vs sběrnice vs rozhraní

Tyto pojmy bývají často zaměňovány a pravdou je, že se jejich významy často překrývají. Nejsmysluplnější způsob, jak je rozlišit je patrně:

- **port**
  - fyzická část zařízení, která slouží k propojení s jiným zařízením (obvykle pomocí vodiče)
  - jeho parametry jsou definovány *standardem*
- **sběrnice**
  - komunikační systém, který slouží k přenosu dat mezi zařízeními
  - jeho zakončením je *port*
  - vlastnosti sběrnic definují *protokoly*
- **rozhraní**
  - *zastřešující pojem*, který se používá pro označení obou předešlých samostatně i dohromady

</div>

## Sériové a paralelní rozhraní

***Význam:***

- **sériový** přenos znamená, že dochází k posílání bitů postupně, tedy **jeden po druhém** (obvykle po jednom vodiči)
- **paralelní** přenos znamená, že dochází k **současnému posílání více bitů** (po více vodičích)

Z principu činnosti by se mohlo zdát, že paralelní rozhraní je jednoznačně lepší, jelikož v jeden moment dokáže poslat více dat, a tedy je rychlejší. Pravda je ovšem taková, že dnes se **sériová rozhraní využívají mnohem častěji** než ta paralelní. Důvody jsou následující:

- při paralelním přenosu je nutné zajistit synchronizaci mezi jednotlivými vodiči, což je poměrně náročné (např. máme-li 8 bitovou paralelní sběrnici, potřebujeme, aby všech 8 bitů jednoho bytu dorazilo přibližně ve stejnou chvíli, aby se nám nepromíchal s následujícím)
- kompenzační metody předešlého problému nejsou jednoduché a praktické (např. při paralelní komunikaci na *DPS* (desce plošných spojů) musíme v případě zatáček uměle prodlužovat kratší linky)
- na delší vzdálenosti je také problém cena vícežilových kabelů
- na DPS zabírají sériové linky méně místa - místo navíc přidává extra izolaci proti rušení
- *IC* (integrované obvody) mají omezený počet vývodů, a tedy je pro ně sériové rozhraní lepší
- u sériových linek je také mnohem jednodušší SW implementace rozhraní (pokud nemá mikrokontrolér odpovídající řadič)

Výhod paralelních rozhraní oproti dnešním sériovým rozhraním není mnoho. Tím nejdůležitějším je patrně **obvodová složitost**. Paralelní rozhraní mohou přenášet data "přirozeně" (z PC vyjde byte, který můžeme přenést tak jak je), kdežto u sériových rozhraní je nutné nejprve provést **serializaci**
a následně **deserializaci**. K tomu se používají zařízení zvaná *serializátor* a *deserializátor*.

### Využití

- **sériové rozhraní** - prakticky v každém druhu komunikace
- **paralelní rozhraní** - v rádiovém přenosu (s využitím např. [PAM](https://atlas144.codeberg.page/szz/vs/pulzni-modulace.html#pulzně-amplitudová-modulace-pam))

### Příklady paralelních rozhraní, která byla nahrazena sériovými

- *paralelní port* → *USB* (např. připojení tiskáren k PC)
- *PATA* → *SATA* (připojení sekundární paměti PC)
- *PCI* → *PCI Express*

## Synchronní a asynchronní rozhraní

***První význam (kterému se budeme věnovat níže):***

- **synchronní** rozhraní při komunikaci využívá tzv. **hodinový signál**, který synchronizuje činnost obou komunikujících zařízení
- **asynchronní** rozhraní komunikuje bez pevně dané časové struktury (**zprávy chodí nahodile**), k identifikaci zpráv se často používají **start-stop bity**

***Druhý význam (používá se spíše ve "vyšších vrstvách informatiky", např. programování):***

- **synchronní** rozhraní komunikuje tak, že odesílatel po odeslání zprávy čeká na její odpověď (tomuto přístupu se také říká **blokující**)
- **asynchronní** rozhraní komunikuje bez nutnosti čekat na odpověď (po odeslání vykonává dále svou činnost = **neblokující**)

### Synchronní rozhraní

Základem synchronních rozhraní je **hodinový signál**. Ten zajišťuje, že přijímající strana bude číst data jen ve chvíli, kdy je druhá strana odesílá a naopak (např. máme systém reagující na náběžnou hranu hodinového signálu. Když k ní dojde, odesílatel zapíše data (změní napěťovou hladinu linky) a současně je přijímač začne číst.). Hodinový signál může být:

- **sdílený**
  - obě strany pro něj mají společnou linku (přídavný vodič), např *TWI (I2C)*
  - nedochází k rozsynchronizování
- **oddělený**
  - obě zařízení mají vlastní generátor hodinového signálu
  - může dojít k rozsynchronizování (je nutné řešit - posílat *synchronizační zprávy*)

### Asynchronní rozhraní

Asynchronní přenos (někdy též *arytmický přenos*) se od synchronního liší tém, že k synchronizaci dochází pouze během přenosu datového rámce. To znamená, že k začátku přenosu může dojít kdykoliv (k jeho identifikaci slouží tzv. **start bit**). Po přijetí *start bitu* se další bity čtou podle předem stanovené rychlosti (= **baud rate**; čímž v tuto dobu dochází k synchronizaci) a s přijetím tzv. **stop bitu** (ukončujícího datový rámec) komunikace končí, do přijetí dalšího *start bitu*.

## Běžně používaná komunikační rozhraní

Zde budeme pojem rozhraním často zaměňovat s pojmem **sběrnice**.

### JTAG

Rozhraní *JTAG* (Joint Test Action Group) se užívá pro verifikaci a testování HW vestavěných systémů (lze říci že PCB obecně). Jedná se o **sériové** rozhraní s **nízkou režijí (overhead) přístupu**. Připojuje se k **Test Access Portu (TAP)** na procesoru, prostřednictvím kterého lze přistupovat k registrům, které obsahují informace o stavu a činnosti procesoru.

Používá se k *debugu SW vestavěných zařízení*, *testování různých úrovní HW zařízení*, *Boundary scanu* (testování propojení bloků na PCB či uvnitř IC) a dalším testovacím účelům.
 

JTAG se používá ve většině vestavěných systémů, např.:

- *ARM* procesory (používané např. v Raspberry Pi)
- téměř všechna *FPGA*
- některé 8 a 16 bitové mikrokontroléry s vyšším počtem vývodů

### SPI

*SPI* (Serial Peripheral Interface) je, jak už název napovídá, **sériové**, **asynchronní** a **full-duplexní** rozhraní využívající společnou sběrnici. Jedná se o rozhraní typu **single master, multi slave** (jedno řídicí zařízení, více obyčejných). Používá se primárně pro připojení externí paměti, A/D převodníků, displejů, vzájemnou komunikaci mezi kontroléry, atd.

Fyzicky obsahuje (minimálně) čtyři vodiče:

- **SCLK**
  - slouží k distribuci hodinového signálu (od *mastera* k ostatním)
  - k ní dochází jen během komunikace (= *asynchronní* rozhraní)
  - je společný pro všechna zařízení
- **MOSI**
  - *master out, slave in*
  - přenáší data od *mastra* k vybranému *slavu*
  - je společný pro všechna zařízení
- **MISO**
  - *master in, slave out*
  - přenáší data od vybraného *slava* k *masteru*
  - je společný pro všechna zařízení
- **SS1...n**
  - *slave select*
  - slouží k výběru konkrétního *slave* zařízení (toho, se kterým se bude právě komunikovat)
  - každý *slave* má vlastní (`SS1`, `SS2`, ...)

|![Blokové schéma SPI zapojení](komunikacni-rozhrani/spi.png "Blokové schéma SPI zapojení")|
|:--:|
|*obr. 1:* Blokové schéma SPI zapojení |
| *zdroj:* [en:User:Cburnett](https://commons.wikimedia.org/wiki/File:SPI_three_slaves.svg), [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/), via Wikimedia Commons |

#### Adresování

Adresování probíhá přivedením log. `0` (piny jsou invertované) na vybraný `SSn` pin. Z toho vyplývá nevýhoda tohoto rozhraní - **počet adresovacích pinů odpovídá počtu připojených zařízení**.

#### Komunikace

1. *master* nastaví příslušný `SSn` pin na log. `0`
2. začne generovat hodinový signál
3. začne posílat svá data na *MOSI* a zvolený *slave* zase na *MISO* (délka dat může být 8 nebo 16 bitů)
4. bod 3 se opakuje, dokud *master* nepřestane generovat hodinový signál a nenastaví příslušný `SSn` na `1`

### UART

*UART* (Universal Asynchronous Receiver-Transmitter) je standard popisující **sériové asynchronní** rozhraní pro komunikaci **mezi dvěma zařízeními** (existují i úpravy pro *single master, multi slave* komunikaci). Standard nedefinuje napěťové úrovně rozhraní, ty jsou určeny jinými komplementárními standardy, nejčastěji *RS-232* a *RS-485*.

Existuje i varianta **USART** (Universal Synchronous Asynchronous Receiver-Transmitter), která umožňuje i synchronní činnost.

Fyzicky vyžaduje *UART* pouze dva vodiče (ale může jich být více, pro další pomocné úkoly, ty všask definuje konkrétní použitý standard):

- **Rx** - přijímač; slouží k přijímání dat z druhého zařízení
- **Tx** - vysílač; slouží k odesílání dat do druhého zařízení

Propojení je křížové, tedy **RX** - **Tx**.

#### Adresování

Adresace se zde neprovádí, jelikož je to komunikace **1 - 1**.

#### Komunikace

*UART* umožňuje jak binární, tak znakovou komunikaci. Data jsou odesílána v paketech, které mají následující formát:

| Start bit |	Data | Paritní bit | Stop bity |
| --- | --- | --- | --- |
| 1 | 5 - 9 | 0 - 1 | 1 - 2 |

K synchronizaci při komunikaci dochází jen při samotném posílání paketů a **generování hodinových provádí obě strany samostatně**. Levnější *UART* zařízení vzájemně synchronizují hodiny pouze při **start bitu**, ty pokročilejší při přijetí každého bitu zprávy.

Komunikace probíhá tak, že *odesílatel* postupně (v sérii) odešle všechny bity paketu. *Příjemce* je postupně ukládá do **posuvného registru**, po přijetí celého paketu dojde ke kontrole **parity** (k tomu je *paritní bit*) a pokud projde, překlopí se data do perzistentnějšího bufferu (aby šlo do posuvného registru přijímat další) a systému se oznámí, že jsou k dispozici data (*interrupt* nebo *flag*).

Obě komunikující zařízení musejí mít shodně nastavené následující parametry:

- **baud rate** - určuje rychlost přenosu v *bitech za sekundu* (= frekvence hodinového signálu), nejnižší používaný je *75 Bd*, poté se jedná o dvojnásobky, případně 1,5 násobky; nejběžnější jsou **9600 a 115200 Bd**
- **přítomnost paritního bitu**
- **počet datových bitů**
- **počet datových bitů**

|![Blokové schéma zapojení využívajícího UART](komunikacni-rozhrani/uart.png "Blokové schéma zapojení využívajícího UART")|
|:--:|
|*obr. 2:* Blokové schéma zapojení využívajícího UART |
| *zdroj:* <https://vanhunteradams.com/Protocols/UART/UART.html> |

### I<sup>2</sup>C

*I<sup>2</sup>C* (Inter-Integrated Circuit) je **sériové**, **synchronní** rozhraní typu **multi master, multi slave** (ale často se soužívá jako **single master, multi slave**). Používá se pro připojení periferií (paměti EEPROM, A/D D/A převodníky, displeje, senzory, atd.) k vestavěným systémům, ale i k PC. Původně bylo navrženo pro tzv. **on-board komunikaci** (mezi obvody na jedné DPS), ale úspěšně se používá i pro propojení oddělených zařízení (do 1 m je to s jistotou bezpečné).

*I<sup>2</sup>C* je ochrannou známkou společnosti Philips. Proto mnoho výrobců používá téměř identickou sběrnici **TWI** (Two Wire Interface).

Fyzicky vyžaduje *I<sup>2</sup>C* pouze dva vodiče - **SDA** (přenos dat), **SCL** (hodinový signál). Všechna zařízení se připojují na oba dva. Obě linky jsou zapojeny jako **otevřený kolektor** a musejí být pomocí tzv. **pull-up rezistorů** připojeny k log. `1` (aby byla zajištěna log. `1` v klidovém stavu).

|![Blokové schéma I<sup>2</sup>C zapojení](komunikacni-rozhrani/i2c.png "Blokové schéma I<sup>2</sup>C zapojení")|
|:--:|
|*obr. 3:* Blokové schéma I<sup>2</sup>C zapojení |
| *zdroj:* [en:user:Cburnett](https://commons.wikimedia.org/wiki/File:I2C.svg), [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/), via Wikimedia Commons |

#### Adresace

Každé *slave* zařízení musí mít unikátní **7** nebo **10 bitovou** adresu (ta je obvykle pevně daná). Ta je vysílána vždy na začátku každého požadavku (po příkazu *START*) a zařízení, kterému patří následně reaguje potvrzením.

Je tedy možné připojit najednou **128**, resp. **1024** zařízení. Některá zařízení, u kterých je běžné, že je použto více kusů (např paměti), mívají **část adresy nastavitelnou**. 

#### Komunikace

***Postup:***

1. *master* vyšle příkaz **START**
2. *master* vyšle **adresu cílového zařízení** (*slave*)
3. *master* vyšle **R/W bit** (určuje, jestli se má jednat o čtení, nebo zápis z/do *slave*)
4. *slave* vyšle **ACK**, jako potvrzení naslouchání
5. odesílají se data, podle směru R/W bitu (za každým bytem je další *ACK*)
6. *master* vyšle příkaz **STOP**

Komunikace je řízena metodou **detekce kolizí**. Jelikož může dojít k více požadavkům od více *masterů* najednou, musejí všechna zařízení při odesílání dat sledovat stav *SDA*. Objeví-li se tam rozdíl od odesílaných dat, znamená to **kolizi**. Kolize se vyřeší tak, že zařízení, které vysílá log. `0` a objeví na SDA log. `1` **okamžitě ukončí přenos**.

### CAN

*CAN* (Controller Area Network) je **sériové asynchronní** rozhraní typu **multi master, multi slave**, využívaná nejčastěji pro vnitřní komunikační síť senzorů a funkčních jednotek **v automobilu**. Díky svým metodám pro vyhnutí se a řešení kolizí je *CAN* velmi vhodné pro **systémy reálného času**.

#### Adresace

*CAN* neadresuje jednotlivá zařízení, nýbrž **přímo zprávy**. Každá zpráva nese krom až 8 B dat také **identifikátor**, podle kterého příjemci rozhodují, jestli je zpráva zajímá, nebo nikoliv. Ten zároveň slouží k určení priority zprávy (nižší identifikátor, vyšší hodnota).

#### Komunikace

Jednotlivé stanice připojené na sběrnici vysílají svá data bez ohledu na to, je-li v sytému nějaký "zájemce" nebo nikoliv. Datové rámce proto **neobsahují adresu příjemce**, nýbrž jen identifikaci (11/29 b) která určuje, **jaká data rámec obsahuje**. Rámec je přijat všemi přijímači na sběrnici. Každý z přijímačů nezávisle použije identifikaci rámce k rozhodnutí, má-li být právě přijatý rámec akceptován, tj. předán k dalšímu zpracování nebo má-li být vymazán. Každý z přijímačů tak akceptuje pouze datové rámce, které jsou pro danou stanici významné.

|![Blokové schéma CAN](komunikacni-rozhrani/can.png "Blokové schéma CAN")|
|:--:|
|*obr. 4:* Blokové schéma CAN |
| *zdroj:* ? |

## Rozdělení podle rychlosti přenosu

Rychlosti zde uvedené jsou jen velmi orientační a v reálných podmínkách se mohou lišit klidně i řádově.

| Rozhraní | Teoretická maximální rychlost přenosu |
| --- | --- |
| SPI | 25 Mbit/s |
| UART | 250 kBd (při použití s běžnými RS-232 převodníky); = 250 kbit/s |
| I<sup>2</sup>C | 3,4 Mbit/s (jednosměrně až 5 Mbit/s) |
| CAN | 1 Mbit/s |

## Zdroje

- DUDÁČEK, Karel. *Sériová rozhraní SPI, Microwire, I2C a CAN*. 2002. Dostupné také z: http://home.zcu.cz/~dudacek/NMS/Seriova_rozhrani.pdf
- Přispěvatelé Wikipedie, *Sériová komunikace* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 1. 01. 2023, 11:22 UTC, [citováno 12. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=S%C3%A9riov%C3%A1_komunikace&oldid=22285431>
- Přispěvatelé Wikipedie, *Paralelní komunikace* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 13. 11. 2022, 18:21 UTC, [citováno 12. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Paraleln%C3%AD_komunikace&oldid=21879486>
- Wikipedia contributors, *Comparison of synchronous and asynchronous signalling* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 5, 01:25 UTC [cited 2023 May 13]. Available from: <https://en.wikipedia.org/w/index.php?title=Comparison_of_synchronous_and_asynchronous_signalling&oldid=1148256684>
- Přispěvatelé Wikipedie, *Synchronní sériová komunikace* [online], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 8. 08. 2021, 17:25 UTC, [citováno 12. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Synchronn%C3%AD_s%C3%A9riov%C3%A1_komunikace&oldid=20346129>
- Přispěvatelé Wikipedie, *Arytmický sériový přenos* [online], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 5. 08. 2021, 05:58 UTC, [citováno 12. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Arytmick%C3%BD_s%C3%A9riov%C3%BD_p%C5%99enos&oldid=20287670>
- Wikipedia contributors, *JTAG* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 29, 17:45 UTC [cited 2023 May 12]. Available from: <https://en.wikipedia.org/w/index.php?title=JTAG&oldid=1152346821>
- Přispěvatelé Wikipedie, *Serial Peripheral Interface* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 19. 03. 2022, 22:57 UTC, [citováno 12. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Serial_Peripheral_Interface&oldid=21053330>
- <https://community.silabs.com/s/article/what-is-the-maximum-spi-clock-speed-x?language=en_US>
- Wikipedia contributors, *Universal asynchronous receiver-transmitter* [online], Wikipedia, The Free Encyclopedia; 2023 May 13, 13:42 UTC [cited 2023 May 13]. Available from: <https://en.wikipedia.org/w/index.php?title=Universal_asynchronous_receiver-transmitter&oldid=1154595277>
- Přispěvatelé Wikipedie, *I²C* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 19. 03. 2022, 22:55 UTC, [citováno 13. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=I%C2%B2C&oldid=21053328>
- Přispěvatelé Wikipedie, *CAN bus* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 20. 09. 2022, 13:42 UTC, [citováno 15. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=CAN_bus&oldid=21695185>
