# FPGA systémy

> FPGA systémy - postup syntézy, jazyky VHDL, Verilog. Vlastnosti popis požití pro řízení. Principy číslicových filtrů.

Všechny programovatelné součástky se souhrnně označují **PLD** (Programmable Logic Device). Jedná se o integrované obvody, které umožňují konfigurovat jejich logickou funkci a propojení po výrobě. Na rozdíl od hradel, registrů a jiných číslicových obvodů (které mají z výroby pevně danou funkci) musí být PLD před použitím nejprve naprogramováno. PLD je možné podle vnitřní struktury rozdělit na: 
- klasické PLD - typ, který zahrnuje jednoduchou logiku, jako jsou AND, OR a NOT brány, a registrů. Nejznámější příklady jsou PAL (Programmable Array Logic) a PLA (Programmable Logic Array). 
- komplexní PLD - pokročilejší typ, který obsahuje více logických bloků, registrů a propojovacích matic. Umožňuje větší flexibilitu a zpracování složitějších logických funkcí. Typické příklady jsou XC9500 od společnosti Xilinx a MAX od společnosti Altera (nyní Intel).
- **FPGA** (Field Programmable Gate Array) - nejvýkonnější a nejrozšířenější typ PLD. Obsahuje velké množství logických bloků a programovatelných propojek, které umožňují vytvoření komplexních digitálních obvodů a systémů. Umožňuje vývojářům implementovat a rekonfigurovat vlastní logické funkce a propojení a je široce využíván v oblasti digitálního návrhu a vývoje hardwaru.

## FPGA obvod

Typickou strukturu obvodu FPGA znázorňuje následující obrázek.

|![FPGA obvod](fpga-systemy/fpga.gif "FPGA obvod")|
|:--:|
|obr. 1: FPGA obvod|

- Bloky označené IOB (**Input/Output Block**) představují vstupně-výstupní obvody pro každý pin. Tyto bloky obvykle obsahují registr, budič, multiplexer a ochranné obvody.
- Bloky LB (**Logic Block**) představují vlastní programovatelné logické bloky. Všechny bloky mohou být různě propojeny globální propojovací maticí.

Rozeznáváme dva základní typy FPGA podle uložení konfigurace:
- **FPGA s volatilní konfigurací**, které ukládají konfigurační informace do paměťových buněk typu SRAM.
- **FPGA s nevolatilní konfigurací**, které ukládají konfigurační bity typicky ve flash paměti, EEPROM, nebo tzv. antifuses (antipojistky). 
  
## Syntéza FPGA

Syntéza FPGA je proces transformace vyššího úrovně popisu hardwaru na nízkoúrovňovou implementaci, vhodnou pro konkrétní FPGA čip. 

**Proces**: popis hardwaru ve vyšším jazyce (např. VHDL nebo Verilog) je převeden na logické funkce a obvody, které realizují požadovanou funkčnost. V tomto kroku jsou aplikovány optimalizace a transformace k dosažení požadovaného výkonu a efektivity. V dalším kroku je třeba zajistit konverzi těchto funkcí a obvodů na netlist, využívající prostředky konkrétního FPGA a zajistit jejich "optimální" rozmístění a propojení.

## Jazyky VHDL, Verilog

Hlavními jazyky pro programování obsahu hradlových polí jsou dnes VHDL a Verilog.

**VHDL** je nezávislý na platformě a umožňuje popsat hardware na různých úrovních abstrakce, od nejvyššího (strukturální) po nejnižší (úrověň hradel). Je silně typovaný. Má prostředky pro popis paralelismu, konektivity a explicitní vyjádření času.

```
# Konečný automat ve VHDL
signal stav: integer := 0;
process (clk, reset)
begin
    if reset = '1' then
        state <= 0;
        y <= '0';
    elsif (clk'event and clk = '1')
        case state is
            when 0 =>
                state <= 0;
                y <= '1';
            when 1 =>
                state <= 2
                y <= '0';
...
            when others =>
                state <= 0;
                y <= '0';
        end case;
    end if;
end process;
``` 

- `clk'event and clk='1'` reprezentuje vzestupnou hranu hodinového signálu
- `y` je výstup konečného automatu.

Jazyk **Verilog** má odlišnou syntaxi. Když VHDL je založen na strukturovaném programování a používá klíčová slova jako "entity", "architecture" a "process" pro popis hardware, Verilog je založen na přístupu popisu chování a struktury a používá klíčová slova jako "module", "always" a "assign" pro popis hardwaru. Souhrn rozdílů je znázorněn na následujícím obrázku.

|![Verilog vs VHDL](fpga-systemy/Verilog-vs-VHDL.png "Verilog vs VHDL")|
|:--:|
|obr. 1: Verilog vs VHDL|

## Číslicové filtry

Filtr pro zpracování signálu představuje model, kterým modelujeme nějaký systém (skutečný, virtuální) popisující relace mezi vstupem a výstupem filtru (tzv. přenosová charakteristika). Filtr pro zpracování signálu (zkráceně jen filtr) je využíván v oborech regulace (automatizace), číslicové zpracování signálu, elektronika (radiotechnika), atd. Existuji analogové filtry pro zpracování analogového signálu a číslicové filtry.

**Číslicové filtry** jsou elektronické obvody nebo systémy, které se používají k úpravě a zpracování **digitálního signálu**. Tyto filtry pracují s diskrétním časem, ve kterém jsou signály reprezentovány vzorky.

Číslicové filtry mají za úkol změnit frekvenční charakteristiky signálu, například filtrováním nežádoucích frekvenčních složek nebo zvýrazněním určitých frekvenčních pásem. Mohou se také použít pro úpravu amplitudy, fáze nebo času signálu.

Existují dva základní typy číslicových filtrů:
- **IIR (Infinite Impulse Response)** - filtry s konečnou impulsní odezvou, které mají zpětnou vazbu mezi výstupem a vstupem. To znamená, že výstup filtru závisí nejen na aktuálním vstupním vzorku, ale také na předchozích výstupních vzorcích.
- **FIR (Finite Impulse Response)** - filtry s nekonečnou impulsní odezvou, které neobsahují zpětnou vazbu. To znamená, že výstup filtru závisí pouze na aktuálním vstupním vzorku a předchozích vstupních vzorcích.

Jak FIR, tak IIR filtr se dá zrealizovat pomocí tří
základních bloků, tyty bloky jsou zobrazeny na následujícím obrázku.

|![Realizační bloky](fpga-systemy/bloky.png "Realizační bloky")|
|:--:|
|obr. 1: Základní realizační bloky pro sestavení číslicových filtrů|

Jedná se o:
- **Sčítací člen** - sečte dva signály
- **Násobička** - vynásobí daný signál konstantou
- **Zpožďovací člen** - zpoždění o jeden krok

Číslicové filtry se používají v různých aplikacích, včetně komunikace, zpracování zvuku, zpracování obrazu a mnoha dalších oblastí, kde je potřeba digitálního zpracování signálu. Jsou také důležitou součástí digitálního zpracování signálů (DSP) a jsou implementovány pomocí algoritmů a hardwaru jako číslicové signálové procesory (DSP procesory), FPGA nebo ASIC (Application-Specific Integrated Circuit).

## Zdroje 

- Přispěvatelé Wikipedie, Programovatelné hradlové pole [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 5. 01. 2023, 20:02 UTC, [citováno 17. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Programovateln%C3%A9_hradlov%C3%A9_pole&oldid=22306362>
- https://pediaa.com/what-is-the-difference-between-verilog-and-vhdl/
- Přispěvatelé Wikipedie, Filtr (zpracování signálu) [online], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 17. 06. 2021, 08:59 UTC, [citováno 17. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Filtr_(zpracov%C3%A1n%C3%AD_sign%C3%A1lu)&oldid=20075258> 
- HAMPL, L. Číslicová filtrace - výukové simulace. Brno: Vysoké učení technické v Brně, Fakulta elektrotechniky a komunikačních technologií, 2016. 65 s. Vedoucí bakalářské práce Ing. Soňa Šedivá, Ph.D. https://core.ac.uk/download/pdf/44402309.pdf



