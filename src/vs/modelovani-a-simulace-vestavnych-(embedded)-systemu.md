# Modelování a simulace vestavných (embedded) systémů

> **Přímé vkládání programů z matematických systémů (MATLAB, Mahematica, apod.) do vývojových desek**.

## Motivace

**Modelování je proces návrhu embedded systému jak po stránce hardwarové, tak i softwarové**. Pokud máme návrh, můžeme realizovat sestrojení prototypu.  Avšak ne vždy si můžeme být jistí tím, že software bude na prototypu fungovat podle našich představ. **Je proto vhodné otestovat náš softwarový návrh v takovém prostředí, které nás nebude stát fyzické zdroje**. Můžeme náš návrh vzít a **simulovat** jak program běží ve virtuálním prostředí. Více o modelování a návrhu embedded systému se lze dočíst v otázce [Metody testování vestavných(embedded) systémů](https://atlas144.codeberg.page/szz/vs/metody-testovani-vestavnych-(embedded)-systemu.html), převážně v kapitole **Delta-W model**.

|![Příklad modelování embedded systému za pomocí Simulinku](modelovani-a-simulace-vestavnych-systemu/simulink.jpg "Příklad modelování embedded systému za pomocí Simulinku")|
|:--:|
|*obr. 1:* Příklad modelování embedded systému za pomocí Simulinku|
| Dostupné z: <https://en.wikipedia.org/wiki/Simulink> |

**Simulace** nám dává **možnost simulovat to, jak bude daná aplikace běžet na daném systému**, bez nutnosti fyzické existence cílového zařízení, přičemž je poté možné výkonný kód do daného typu zařízení nahrát / vložit.

Více o principu **emulace** a **simulace** lze nalézt v otázce [Simulace a emulace vestavných systémů](https://atlas144.codeberg.page/szz/vs/simulace-a-emulace-vestavnych-systemu.html).

<div class="note">

### Simulace vs emulace

Mezi **simulací a emulací** je rozdíl, ač se oba dva tyto termíny používají zaměnitelně. Emulace umožňuje vytvořit virtuální zařízení, které má stejné schopnosti, jako originál. Emulátory jsou psané v low-level programovacích jazycích, jako je například Assembler. Překládají strojový kód, aby co nejvíce odpovídal chodu daného zařízení, jak **hardwarově, tak softwarově**, což často vede k pomalému chodu tohoto zařízení. Příkladem může být emulátor Android telefonu (SDK) při programování mobilních zařízení. Jejich hlavní využití je při debuggingu hardware.

Na druhou stranu **simulátor** běží nad operačních systémem jako aplikace. Jeho účelem je pouze navodit pocit toho, že pracujeme s nativním zařízením. **Nereprezentuje hardware, pouze uživatelské prostředí**. Používá se pro simulování chování a konfigurace nativního zařízení. Využití nalezne také při testování aplikací vyvíjených pro **simulované** zařízení.

</div>

## Vkládání programů do vývojových desek

V dnešní době se používají software, které využívají tzv. **Model-based design**. Jde o proces modelování za pomocí některého z **modelovacích jazyků**. Tímto způsobem můžeme vytvořit model jak celého embedded systému, tak i obslužného programu. Tyto programy kromě **vytváření modelů** často umožňují **simulaci, automatické generování kódu a verifikaci**. Jsou tedy nedílnou součástí postupu vývoje (a samozřejmě testování) embedded systémů, o kterém si lze více přečíst v otázce [Metody testování vestavných (embedded) systémů](https://atlas144.codeberg.page/szz/vs/metody-testovani-vestavnych-(embedded)-systemu.html), převážně v kapitole **Delta-W model**.


 Existuje velké množství takových programů, my si tu uvedeme ty nejznámější:

- **Matlab**: Používá *Simulink*, což je placené rozšíření k placenému programu *Matlab*
- **Wolfram SystemModeler**: Placený software, umožňuje trial po dobu 30 dní
- **Modelica**: open software, zdarma

Ne vždy je nutné kód obslužného programu psát. Pokročilé modelovací prostředí, jako je například Simulink, nám **umožní namodelovat systém i jeho chování v grafické podobě, ze které je toto prostředí schopné vygenerovat kód, který lze poté zkompilovat a spustit na cílovém zařízení**.

Než však nahrajeme kód do vývojové desky, je nejdříve nutné ji připojit. Povětšinou se to dělá za pomocí COM portů, což jsou sériové porty, kterými připojíme zařízení, do něhož chceme kód nahrát.

Existuje i možnost vkládání programů přes síť. Tuto možnost nabízí například **Simulink**, společně s Raspberry, kde je možnost nahrát na dálku do zařízení program v podobě spustitelného souboru.

**Nahráním programu do vývojové desky se přepíše obsah paměti**. Drtivá většina vývojových desek (snad jen s výjimkou Raspberry) umožňuje mít v sobě pouze jeden spustitelná program. 

Procesorová jednotka na vývojové desce poté zpracovává nahraný kód v podobě instrukcí. Často je tomu tak v nekonečné smyčce, která umožňuje programovat automatizované činnosti.

## Zdroje

- Programming the World with Arduino and Mathematica. *Ian Johnson*. [online] @2014 [citováno: 12.05.2023] <br> Dostupné z: <https://community.wolfram.com/groups/-/m/t/315748>

- Virtual Device (Emulator and Simulator) vs. Real Device: What is the Difference?. *SauceLabs*. [online] @2022 [citováno: 12.05.2023] <br> Dostupné z: <https://saucelabs.com/resources/blog/mobile-device-emulator-and-simulator-vs-real-device>

- Can I upload Arduino Code written in MATLAB using MATLAB Arduino Support Package?. *Mathworks. Walter Roberson*. [online] @2015 [citováno: 12.05.2023] <br> Dostupné z: <https://www.mathworks.com/matlabcentral/answers/257423-can-i-upload-arduino-code-written-in-matlab-using-matlab-arduino-support-package>

