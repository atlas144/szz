# Vestavné systémy

> Definice. Charakteristika vestavěných systémů. Použití v bankomatu, avionice, mobilní telefony, domácí automatizace, kalkulačky, zabezpečovací systémy, zdravotní přístroje, herní konzole apod.

## Definice

### Krátká

> Kombinace hardwaru a softwaru, jejímž smyslem je řídit externí proces, zařízení nebo systém.

*Zdroj: <https://www.automa.cz/cz/casopis-clanky/vestavne-systemy-charakteristika-vyvoj-pouziti-2007_10_34242_3985/> (upraveno)*

### Dlouhá

> Vestavěný systém (*VS*) je (většinou) jednoúčelový počítač, ve kterém je řídicí systém zcela zabudován do zařízení, které ovládá. Na rozdíl od univerzálních počítačů, jako jsou osobní počítače, jsou zabudované systémy většinou specializované, určené pro předem definované činnosti. Díky tomu jej tvůrci mohou při návrhu zjednodušit a optimalizovat, a tak snížit nároky na prostředky a cenu. Vestavěné systémy jsou často vyráběny sériově ve velkém množství, takže úspora bývá znásobena velkým počtem vyrobených kusů.

*Zdroj: Přispěvatelé Wikipedie, Vestavěný systém [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 10. 03. 2023, 18:19 UTC, [citováno 4. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Vestav%C4%9Bn%C3%BD_syst%C3%A9m&oldid=22532985> (výrazně upraveno)*

## Charakteristika

Uvedené vlastnosti nemusejí být platné vždy pro každý VS. Ale obecně lze říci, že čím víc jich systém má, tím spíše se jedná o vestavěný systém.

Pokud se jedná o porovnání (např. menší výkon), vztahuje se k *univerzálním počítačům* (jako např. osobní počítače).

- specializace pro konkrétní účel
- přímo součástí většího zařízení, které řídí
- bez interakce s člověkem (a tedy bez běžného rozhraní, jako displej, myš a klávesnice)
- interakce se senzory, aktuátory a jinými VS
- OS (obvykle platí jedna z možností):
  - pracuje v reálném čase ([RTOS](https://atlas144.codeberg.page/szz/vs/architektura-operacnich-systemu-uzivanych-ve-vestavnych-systemech.html#rtos))
  - úplně chybí (běží pouze program)
- vysoké nároky na spolehlivost (VS se často používají v kritických aplikacích (zdravotnictví, energetika, atd.); restart či oprava jsou obvykle velmi náročné nebo dokonce nemožné)
- menší výkon (vyšší není nutný)
- výrazně nižší spotřeba
- menší rozměry
- nižší cena

## Použití

### Kalkulačky

Kalkulačky jsou dobře známým a snadno pochopitelným příkladem vestavěných systémů. Mají jeden konkrétní účel, řídicí jednotka je integrována do zařízení, většinou neobsahují operační systém, výkon řídicí jednotky je minimální, ale vzhledem optimalizaci ke konkrétnímu danému účelu naprosto dostačující. Rovněž spotřeba a výrobní cena je velmi nízká.

Odchylkou od běžných charakteristik je rozhraní, které je uzpůsobené interakci s člověkem (displej, klávesnice).

### Domácí automatizace

Domácí automatizace bývá často oblastí, kde dochází k prvnímu vědomému kontaktu s vestavěnými systémy. Ač existuje spousta hotových řešení (Home Assistant, SmartThings, Google Home, atd.), je zde mnoho prostoru a nízká vstupní bariéra pro **DIY**. Na internetu lze nalézt mnoho návodů na stavbu vlastních zařízení (vestavěných systémů), od malých a jednoduchých meteostanic po komplikované řídicí jednotky, komunikující bezdrátově s desítkami senzorů. Tato možnost získat pohled dovnitř světa vestavěných systémů (na rozdíl od toho vnějšího, konzumního), často vede k hlubšímu zájmu o tuto oblast, který může vyústit ve studium a následnou kariéru se zaměřením na vestavěné systémy.

Lze tedy říci, že oblast domácí automatizace má ve světě vestavěných systémů zvláštní místo, jako jakási **vstupní brána**.

Je nutné zmínit zde **IoT** (Internet of Things), což je koncept "pochytřování" obyčejných zařízení (přidávání senzorů a konektivity), který je v domácí automatizaci všudypřítomný. Lze říci, že domácí automatizace je podmnožinou IoT.

Z pohledu souladu s charakteristikami vestavěných systémů ja domácí automatizace poměrně složitá, jelikož zahrnuje celé spektrum rozličných zařízení. Vyjmenujeme si alespoň ta nejběžnější z nich:

- "chytré" termostaty a ovladače proudění vzduchu
- řídicí jednotky osvětlení
- měřiče kvality ovzduší
- meteostanice
- domácí asistenty
- energetické řídicí jednotky (např. k solárním panelům)
- zabezpečovací systémy (viz [níže](#zabezpečovací-systémy))
- pomocné roboty (např. vysavače)
- "chytré" spotřebiče
- řídicí jednotky zavlažování

### Zabezpečovací systémy

Řídicí jednotky zabezpečovacích systémů jsou typickým příkladem vestavěných systémů, jelikož mají mnoho jejich charakteristických rysů. Jsou víceméně jednoúčelové, vestavěné do uzavřeného zařízení, komunikují se senzory (s člověkem jen přes řídicí rozhraní - typicky aplikace), HW není příliš výkonný, využívají vestavěné OS. Příkladem mohou být systémy od společnosti *Jablotron*.

Kromě řídicích jednotek lze za vestavěná zařízení často považovat i jejich periferie, jako např *kamery*, *automatické zámky*, či *pohybová čidly*. Ty totiž obvykle mívají vlastní řídicí vestavěný systém.

### Mobilní telefony

Mobilní telefony jsou specifickým typem vestavěných systémů. Historicky do této kategorie právem spadaly (ač měly rozhraní pro komunikaci s člověkem, jednalo se o malá jednoúčelová zařízení - bezdrátová komunikace odkudkoliv). Dnes se však čím dál více přibližují univerzálním počítačům (univerzální použití, pokročilé OS i aplikace, vysoký výkon, cena i spotřeba, atd.).

### Bankomaty

Řídící jednotky bankomatů jsou jedním z příkladů vestavěných systémů s vysokými požadavky na spolehlivost a bezpečnost (i když bezpečnost je nutno brát s velkou rezervou, viz níže). Jsou totiž velmi lákavým cílem útočníků. Rovněž není z pohledu uživatelů (bankovních klientů) přípustné, aby např. vlivem chyby v SW došlo k restartu aplikace nebo celého zařízení uprostřed úkonu (jako např. vklad hotovosti).

Z technického hlediska bankomaty kombinují rozhraní pro interakci s člověkem (displej, numerická klávesnice, čtečka karet, zařízení pro výdej bankovek, tiskárna stvrzenek) s klasickým (autonomním) rozhraním vestavěných systémů (senzor přítomnosti bankovek při výdeji, podávací mechanizmus pro bankovky, bezpečnostní systém). Co se týče SW, bankomaty, stejně jako většina bankovního sektoru, naprosto nepochopitelně používají zastaralé technologie plné zranitelností (v době ukončení rozšířené podpory Windows XP jej používalo 95% bankomatů na světě). Tyto problémy jsou kompenzovány vysokou fyzickou bezpečností zařízení.

### Avionika

Avionika (elektronika užívaná v letectví) zahrnuje celou řadu vestavěných systémů:

- **komunikační zařízení** - komunikace s řídicí věží, s pasažéry
- **navigátory** - satelitní navigace (GPS, Galileo, WAAS), pozemní rádiová (VOR, LORAN), inerciální navigační systém (kombinace akcelerometrů a gyroskopů)
- **systémy kontroly letu** - autopilot
- **palivová kontrola** - Fuel Quantity Indication System (FQIS)
- **systémy vyhnutí kolizím** - Traffic Collision Avoidance System (TCAS), vysílá signál, který zachytí tentýž systém v jiném letadle, ten pošle odpověď, oba informují pilota a navrhnou cestu pro vyhnutí
- **letové záznamníky** - černé skříňky
- **monitory počasí** - Weather Surveillance Radar, detektor blesků
- **monitory letadla** - sledování vnitřních stavů letadla, životnosti komponent, atd.

Z pohledu SW je zajímavá především specifikace **ARINC 653**, která definuje parametry pro RTOSy používané v avionice.

#### Drony

Drony jsou samostatnou skupinou kombinující *avioniku* a *robotiku*, tedy dvě oblasti, ve kterých mají vestavěné systémy důležité místo.

Příklady vestavěných systémů (a jejich SW) pro drony:

- **Pixhawk** - soubor otevřených standardů definujících HW vlastnosti vestavěných zařízení, aby byla zajištěna vzájemná interoperabilita (řídicí jednotky odpovídající standardu: Pixhawk 4, Auterion Skynode, atd.)
- **Paparazzi UAV** - open-source HW a SW, který umožňuje autonomní činnost UAV (pochytřuje "hloupá" UAV)
- **Neousys Technology POC 200 (a další modely)** - průmyslové minipočítače, používané v avionice a robotice (obvykle na nich běží OS pro univerzální počítače, ale používají se jako vestavěná zařízení)
- **ArduPilot** - sada SW nástrojů pro tvorbu řídicích aplikací

### Zdravotní přístroje

Vzhledem k tomu, že medicínská zařízení často stojí doslova *mezi životem a smrtí*, je zřejmé, že se jedná o kritické aplikace. Ty vyžadují tu nejvyšší spolehlivost, kterou poskytují právě (některé) vestavěné systémy. Konkrétně vestavěné systémy s RTOS, splňující přísné standardy (jako např. *IEC 62304* pro SW). Jedná se např. o *monitory tělesných funkcí*, *mimotělní oběhy*, *plicní ventilátory*, atd. Některá z těchto zařízení bývají navíc bez možnosti opravy (např. *kardiostimulátory*).

Kromě těch nejkritičtějších zařízení obsahuje oblast zdravotnictví i další, která nevyžadují tak přísnou spolehlivost. Jedná se např. o *měřiče tělesných vlastností pro běžné prohlídky*. I tato zařízení obsahují vestavěné systémy, ty však obvykle nedosahují kvalit výše popsaných, což je ovšem vykoupeno nižší cenou.

### Herní konzole

Herní konzole naplňují charakteristiky vestavěných systémů především z pohledu účelnosti (mají jen jeden), ceny a rozměrů. V ostatních charakteristikách se podobají spíše univerzálním počítačům:

- **PlayStation**
  - standardní PC HW (i když low-power varianty) - CPU a GPU od AMD
  - OS modifikované FreeBSD (desktopový systém)
  - rozhraní univerzální (USB, RJ-45, ...), ale obecně určené pro připojení periferií k interakci s člověkem
- **XBOX**
  - HW podobný jako PS
  - OS modifikované Windows
  - rozhraní podobné jako PS
- **Nintendo Switch**
  - nejvíce odpovídá vestavěným zařízením
  - integruje rozhraní (biť pro komunikaci s člověkem) do jednoho zařízení spolu s řídicí jednotkou
  - CPU a GPU od Nvidia (mobilní)
  - OS proprietární (pravděpodobně ve stylu mobilních OS)

### Zdroje

- <https://www.automa.cz/cz/casopis-clanky/vestavne-systemy-charakteristika-vyvoj-pouziti-2007_10_34242_3985/>
- Přispěvatelé Wikipedie, *Vestavěný systém* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 10. 03. 2023, 18:19 UTC, [citováno 4. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Vestav%C4%9Bn%C3%BD_syst%C3%A9m&oldid=22532985>
- Wikipedia contributors, *Home automation* [online], Wikipedia, The Free Encyclopedia; 2023 May 4, 13:12 UTC [cited 2023 May 5]. Available from: <https://en.wikipedia.org/w/index.php?title=Home_automation&oldid=1153142338>
- Přispěvatelé Wikipedie, *Bankomat* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 6. 02. 2023, 11:29 UTC, [citováno 5. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Bankomat&oldid=22415962>
- Wikipedia contributors, *Automated teller machine* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 10, 11:31 UTC [cited 2023 May 5]. Available from: <https://en.wikipedia.org/w/index.php?title=Automated_teller_machine&oldid=1149134216>
- Wikipedia contributors, *Avionics* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 28, 22:35 UTC [cited 2023 May 5]. Available from: <https://en.wikipedia.org/w/index.php?title=Avionics&oldid=1152220489>
- Wikipedia contributors, *Traffic collision avoidance system* [online], Wikipedia, The Free Encyclopedia; 2023 May 2, 03:10 UTC [cited 2023 May 5]. Available from: <https://en.wikipedia.org/w/index.php?title=Traffic_collision_avoidance_system&oldid=1152755626>
- <https://www.playstation.com/cs-cz/ps4/tech-specs/>
- Wikipedia contributors, *PlayStation* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 29, 18:24 UTC [cited 2023 May 6]. Available from: <https://en.wikipedia.org/w/index.php?title=PlayStation&oldid=1152351658>
- Wikipedia contributors, *Xbox system software* [online], Wikipedia, The Free Encyclopedia; 2023 May 2, 21:53 UTC [cited 2023 May 6]. Available from: <https://en.wikipedia.org/w/index.php?title=Xbox_system_software&oldid=1152883026>
- <https://www.cnews.cz/microsoft-xbox-series-x-specifikace>
- Wikipedia contributors, *Nintendo Switch system software* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 22, 01:25 UTC [cited 2023 May 6]. Available from: <https://en.wikipedia.org/w/index.php?title=Nintendo_Switch_system_software&oldid=1151116659>
- <https://www.nintendo.com/switch/tech-specs/>
- <https://pixhawk.org/>
- <https://wiki.paparazziuav.org/wiki/General>
- <https://www.neousys-tech.com/en/product/product-lines/industrial-computers>
- <https://ardupilot.org/ardupilot/index.html>
- <https://www.wikiskripta.eu/w/Kardiostimul%C3%A1tor>