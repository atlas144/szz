# Pulzní modulace

## Modulace obecně

Nejprve je třeba vysvětlit, co to vlastně je *modulace*. Poměrně dobrá definice z [Wikipedie](https://cs.wikipedia.org/wiki/Modulace) zní:

> ...proces, kterým se mění charakter vhodného nosného signálu pomocí modulujícího signálu.

Trochu hlouběji: máme signál (např. reprezentující data), který chceme nějakým způsobem použít (nejčastěji přenést), ale jeho aktuální forma není pro dané použití vhodná. Potřebujeme jej tedy upravit tak, aby výsledek odpovídal požadovanému užití. Tato úprava se nazývá **modulace**. Provádí se pomocí *modulátoru*, do kterého je přiveden náš signál (tzv. *modulační signál*) a zároveň *nosný signál*. Činností modulátoru je úprava nosného signálu podle zvoleného typu modulace (viz níže) a aktuální hodnoty modulačního signálu - např. používáme-li PWM (pulzně šířkovou modulaci) a hodnota modulačního signálu se zvyšuje, dochází ke zvyšování střídy signálu nosného.

Typy modulace lze dělit různě. Za základní dělení lze považovat dělení podle typu nosného signálu:

- **spojitá modulace** – nosný signál má harmonický průběh (lze jej popsat funkcí sinus či kosinus)
  - **analogová** - modulační signál je *analogový* (má spojitý průběh); např. pro rádio *AM* (amplitudová modulace) a *FM* (frekvenční modulace)
  - **digitální** – modulačním signálem je *diskrétní* (≈digitální; jeho průběh není spojitý); např. *ASK* (Amplitude-Shift Keying),
*FSK* (Frequency-Shift Keying)
- **diskrétní (pulzní) modulace** – nosný signál má nespojitý průběh (někdy se také nazývá taktovací signál); ***této skupiny se týká otázka***
  - **nekvantovaná** - modulační signál je *analogový*; např. *PWM* (pulzně šířková modulace)
  - **kvantovaná** - modulační signál je *diskrétní*; např. *PCM* (pulzně kódová modulace)

### Důležité pojmy

- **modulační signál** – vstupní signál; modulujeme jím *nosný signál*
- **nosný signál** – signál s parametry vhodnými pro naše použití; modulujeme jej *modulačním signálem*
- **modulovaný signál** – výsledný signál = výstup procesu *modulace*
- **modulátor** - zařízení, které provádí *modulaci*
- **modulační produkty** – složky *modulovaného signálu* (tento pojem se používá hlavně při zpětném rozkladu *modulovaného signálu*, např. při frekvenční analýze)
- **jednoduchá modulace** – *modulace* zpracovávající jeden *modulační signál*
- **složená modulace** – *modulace* zpracovávající několik *modulačních signálů* najednou

## Pulzní modulace

Jak již bylo řečeno výše, u pulzních modulací je nosný signál nespojitý, přesněji řečeno, jedná se o **periodickou posloupnost obdélníkových impulzů**. Nejběžnější použití pulzní modulace je u A/D převodníků a pro zdrojové kódování.

### Nekvantované

Nekvantované modulace se dále dělí podle **měněného parametru** nosného signálu. Lze tedy říct, že se jedná o ekvivalenty analogových modulací.

Princip nekvantovaných modulací spočívá ve **vzorkování** modulačního signálu. Vzorkování je proces, při kterém dochází, vždy po uplynutí vzorkovací periody, k odebrání vzorku z analogového modulačního signálu. Tyto vzorky se poté uchovávají (tato vzorkovací metoda se nazývá *Sample and Hold*; existují i jiné metody, ale tato je nejběžnější). Zařízení, které vzorkování provádí se nazývá *vzorkovač* (= vzorkovací obvod).

Důležité je při vzorkování zvolit správnou délku vzorkovací periody (respektive frekvenci vzorkovacího signálu[^frekvence-perioda]), aby nedocházelo k příliš velkému zkreslení a případným ztrátám informace. Podle **Shannonova–Nyquistova–Kotělnikovova teorému** (často se mu říká jen *Shannonův–Kotělnikovův* teorém) musí být *vzorkovací frekvence* nejméně dvakrát vyšší než *nejvyšší frekvence obsažená ve vzorkovaném (modulačním) signálu*. Řečeno matematicky:

\\[ f_{vz} \geq 2 \cdot f_{max} \\]

kde \\( f_{vz} \\) je vzorkovací frekvence a \\( f_{max} \\) maximální frekvence obsažená ve vzorkovaném signálu.

Obecným problémem nekvantovaných pulzních modulací je **náchylnost ke zkreslení** způsobenému přítomností rušivých signálů v přenosovém kanále.

[^frekvence-perioda]: Protože jak všichni jistě víme, platí že \\( T = \frac{1}{f} \\), kde \\( T \\) je perioda a \\( f \\) frekvence, a tedy tyto dvě veličiny jsou na sobě závislé. 

#### Pulzně amplitudová modulace (PAM)

PAM je metoda, při které dochází k úpravě **amplitudy** nosného signálu. Principiálně je velmi jednoduchá (jedná se prakticky o čisté vzorkování) - z *modulačního* signálu jsou v *době vzorkovacích impulzů* (vzorkovací signál zde funguje jako nosný) odebírány jeho aktuální hodnoty a ty tvoří výsledný *modulovaný* signál.

| Vlastnost nosného signálu | Mění se |
| --- | --- |
| amplituda | ANO |
| pozice pulzu v čase | NE |
| střída | NE |

PAM má dva typy:

- **PAM I *(1. druhu)*** - používá **přirozené vzorkování**, což znamená, že po dobu vzorkovacího pulzu je na výstup přiveden přímo modulační signál (není zde použit *Sample and Hold* obvod) → modulovaný signál **není obdélníkový**
- **PAM II *(2. druhu)*** - používá **uniformní vzorkování** = je použit *Sample and Hold* obvod; na výstup je po celou dobu trvání vzorkovacího pulzu přiváděna hodnota, která byla změřena při jeho začátku → modulovaný signál **je** (za ideálních podmínek) **obdélníkový**

|![PAM (I vlevo, II vpravo)](pulzni-modulace/pam.png "PAM (I vlevo, II vpravo)")|
|:--:|
|*obr. 1:* PAM (I vlevo, II vpravo)|
| *zdroj:* ŠIŠKA, M. *Impulzové modulace*. Brno: Vysoké učení technické v Brně, Fakulta elektrotechniky a komunikačních technologií, 2013. 101 s. Vedoucí diplomové práce Ing. Radim Číž, Ph.D. |

**Důležité parametry PAM:**

- Výška vzorkovacího pulzu \\( (D) \\)
- Šířka vzorkovacího pulzu \\( (\theta) \\)
- Vzorkovací perioda \\( (T_{vz}) \\)

Z pohledu použití není *PAM* příliš vhodná pro přenos informací (amplitudové modulace jsou náchylné na rušení). Používá se hlavně jako součást jiných modulačních systémů (např. [PCM](#pulzně-kódová-modulace-pcm)). Dále také v některých verzích ethernetu a nové verzi sběrnice PCI Express. Též je využívána jednou severoamerickou televizí. 

#### Pulzně šířková modulace (PWM)

Princip PWM spočívá ve změně **střídy** (= poměr mezi stavem *log. 0* a *log. 1* v rámci jedné periody (většinou se udává v procentech)) výstupního signálu na základě okamžité hodnoty modulačního signálu. Střída \\( (D) \\) je dána vztahem \\( D = \frac{\tau}{T} \\), kde \\( \tau \\) je doba, po kterou je signál v úrovni log. 1 a \\( T \\) je perioda signálu.

Činnost spočívá v komparaci modulačního signálu s referenčním **pilovým** (nebo trojúhelníkovým) signálem z generátoru. V rámci jedné periody, dokud je hodnota modulačního signálu vyšší než hodnota signálu referenčního, má výstupní signál hodnotu *log. 1*. Jakmile překročí hodnota referenčního signálu hodnotu modulačního, výstupní hodnota se mění na *log. 0*.

|![PWM (vlevo: červená - pilový referenční signál, modrá - modulační signál; vpravo: modulovaný signál)](pulzni-modulace/pwm.png "PWM (vlevo: červená - pilový referenční signál, modrá - modulační signál; vpravo: modulovaný signál)")|
|:--:|
|*obr. 1:* PWM (vlevo: červená - pilový referenční signál, modrá - modulační signál; vpravo: modulovaný signál)|
| *zdroj:* ŠIŠKA, M. *Impulzové modulace*. Brno: Vysoké učení technické v Brně, Fakulta elektrotechniky a komunikačních technologií, 2013. 101 s. Vedoucí diplomové práce Ing. Radim Číž, Ph.D. |

<br>

| Vlastnost nosného signálu | Mění se |
| --- | --- |
| amplituda | NE |
| pozice pulzu v čase | NE |
| střída | ANO |

Ve skupině spojitých modulací nemá PWM žádnou obdobu.

**TODO** přidat vysvětlení, proč PWM funguje

PWM se hojně využívá ve výkonové elektronice pro řízení DC/DC měničů nebo stejnosměrných motorů, servo motorů, ovládání jasu světelných zdrojů nebo také pro řízení otáček ventilátorů.

Velkou výhodou při použití PWM k napájení (např. právě motory) je absence ztrát, ke kterým dochází při klasickém lineárním napájení. To je díky tomu, že tranzistor (který se používá jako spínací prvek) má nejmenší ztráty ve stavu plného otevření a zavření (tedy *log. 1 a 0*).

#### Pulzně polohová modulace (PPM)

U PPM odpovídá okamžitá hodnota *modulačního* signálu poloze impulzu v odpovídající periodě *modulovaného* signálu. Princip PPM tedy spočívá v **posouvání pulzů** výstupního signálu **úměrně k aktuální hodnotě** modulačního signálu, a to buď *doprava*, pokud je aktuální hodnota vyšší než rozhodovací úroveň (což je většinou 0, tedy aktuální hodnota je kladná), anebo *doleva*, je-li aktuální hodnota nižší než rozhodovací.

PPM je obdobou *fázové spojité modulace*.

| Vlastnost nosného signálu | Mění se |
| --- | --- |
| amplituda | NE |
| pozice pulzu v čase | ANO |
| střída | NE |

<br>

|![PPM](pulzni-modulace/ppm.png "PPM")|
|:--:|
|*obr. 2:* PPM|
| *zdroj:* ŠIŠKA, M. *Impulzové modulace*. Brno: Vysoké učení technické v Brně, Fakulta elektrotechniky a komunikačních technologií, 2013. 101 s. Vedoucí diplomové práce Ing. Radim Číž, Ph.D. |

Levý obrázek znázorňuje *modulační* (modrý) a *vzorkovací* (červený) signál. Pravý *výstupní impulzy odpovídající hodnotě 0 V* (červené; slouží jako reference pro lepší představu) a *skutečné výstupní hodnoty* (modré).

PPM se používá v optických přenosových systémech (optická síť), radio-control systémech (rádiové řízení, např. dronů; pro RC byla původně vytvořena) a pro vesmírnou komunikaci.

Nevýhodou PPM je náročnost implementace, jelikož je nutná synchronizace na straně vysílače a přijímače (obvykle se řeší tak, že jednotlivé pulzy jsou posunuty relativně k sobě navzájem - *rozdílová pulzně polohová modulace*). Dalším problémem je výcecestné šíření signálu. Pokud k němu dochází (říká se tomu ozvěny), není téměř možné rozpoznat ozvěny od skutečných přenosů → PPM je nepoužitelné.

### Kvantované

Kvantované modulace využívají kromě procesu **vzorkování** ještě **kvantování** a **kódování**. Tyto procesy jsou obvykle použity jako postupné kroky.

Proces *kvantování* spočívá v přidělení každému vzorku (získanému vzorkováním) kvantizační hladinu. Počet hladin je určen počtem bitů, které máme k dispozici pro hodnoty výstupního signálu. *Lidsky řečeno*: spojitou škálu hodnot, kterých může vstupní signál nabývat rozdělíme na tolik dílů, kolik nám dává vztah \\( N_{kv} = 2^{n_b} \\), kde \\( N_{kv} \\) je počet dílů (hladin) a \\( n_b \\) počet bitů výstupního signálu a každému vzorku přiřadíme tu hladinu, která mu nejvíce odpovídá.

*Kódování* je proces převodu kvantovaných hladin jednotlivých vzorků na *binární číslo*, které je **finální produkt modulace**.

Běžné kvantované modulace jsou:

- Pulzně kódová modulace (PCM)
  - Diferenční pulzně kódovaná modulace (DPCM)
  - Adaptivní diferenciální pulzně kódová modulace (ADPCM)
- Delta modulace (DM)
  - Delta-sigma modulace (ΔΣ)
  - Adaptivní delta modulace (ADM)

## Zdroje

- ŠIŠKA, M. *Impulzové modulace*. Brno: Vysoké učení technické v Brně, Fakulta elektrotechniky a komunikačních technologií, 2013. 101 s. Vedoucí diplomové práce Ing. Radim Číž, Ph.D.
- Přispěvatelé Wikipedie, *Modulace* \[online\], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 3. 03. 2022, 11:20 UTC, [citováno 19. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Modulace&oldid=20994654>
- Přispěvatelé Wikipedie, *Pulzně amplitudová modulace* \[online\], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 30. 01. 2023, 07:47 UTC, [citováno 21. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Pulzn%C4%9B_amplitudov%C3%A1_modulace&oldid=22393152>
- Přispěvatelé Wikipedie, *Pulzně šířková modulace* \[online\], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 8. 08. 2021, 15:40 UTC, [citováno 21. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Pulzn%C4%9B_%C5%A1%C3%AD%C5%99kov%C3%A1_modulace&oldid=20340653>
- <https://robodoupe.cz/2016/pulzne-sirkova-modulace/>
- <http://fyzika.jreichl.com/main.article/view/1358-kodovani-signalu>