# Metody testování vestavných (embedded) systémů

> základní přístupy, vývojové modely (vodopád, spirálový, V-model, W-model, Delta-W model), modelování vestavných (embedded) systémů

> Zaměřit se hlavně na Delta-W model, kde se řeší paralelní vývoj/testování SW a HW.

## Motivace 

Vývoj embedded systému se skládá ze dvou odlišných a na sobě téměř nezávislých částí. První je navrhnutí a sestrojení **hardware**, často v podobě vývojové desky - **výrobní cyklus**. Tato deska je poté hozena **softwarovému týmu**, který tiše doufá, že je vše funkční a problémy, které se vyskytnou jsou **čistě softwarového rázu**. Problémem zde je fakt, že **při vývoji software na chybném hardware se hardwarové problémy mohou jevit jako softwarové**. Vývojový tým tedy může hledat problémy na špatných místech a neuvědomit si, že je potřeba opravit samotný **hardware**.

**Testování hardware je poslední částí vývojového cyklu HW. Stejně tak testování software je poslední částí vývojového cyklu SW.**

Testování hardware zahrnuje: 

- testování jednotlivých periferií (senzory, kamery, tlačítka...)
- Testování robustnosti (otřesy, vibrace)
- testování firmware 

> Tento okruh se bude dále zaobírat **testováním software na vytvořeném hardware** v embedded systémech. Je nutné si uvědomit, že při testování se vždy může objevit možnost toho, že se jedná o **hardwarový** problém.

Proč testujeme?

- **Nalezení bugů**:
  - snížení rizika jak pro uživatele, tak pro společnost, jež zařízení vyvíjí
  - čím dříve bug najdeme, tím levnější je jeho oprava
- **Snížení náročnosti vývoje a ceny údržby**:
  - chyby mají tendenci se hromadit
  - pokud netestujeme včas, může se stát, že nebudeme dělat nic jiného než opravovat chyby
- **Zlepšení výkonu**:
  - testování může identifikovat úseky, které zpomalují chod celého řešení
  - úpravou těchto úseků můžeme dosáhnout značného zrychlení

## Základní přístupy

Základní přístupy zahrnují dva způsoby testování:

- **Funkcionální testy (Black-box testing)**
- **Testy pokrytí (White-box testing)**

**Základní princip testů je ten, že se snažíme naše řešení rozbít, zničit. Snažíme se dokázat, že aplikace nefunguje. Děláme cokoliv (náhodné vstupy stylem mlácení hlavou do klávesnice, přetěžování vstupů...), o čem jako programátoři víme, že aplikaci uškodí.**

### Black-box testing

Jedná se o analogii k **unit testům**. Testujeme jednotlivé funkcionality, přičemž nás zajímá pouze vstupní set parametrů a odpovídající výstup, který můžeme porovnat s očekávaným výstupem. Nezajímá nás to, jak fungují algoritmy, které zajišťují jednotlivé funkcionality.

Díky tomu, že black-box testy **testují jednotlivé funkcionality na základě vstupních parametrů můžeme psát tyto druhy testů zároveň s vývojem aplikace**.

Příklady black-box testů:

- **Stress test**:
  - záměrně přetěžujeme vstupní kanály, vyrovnávací paměti, systémy pro správu paměti, apod.
- **Boundary value tests**:
  - testujeme vstupy, které mají být v určitém rozmezí
  - pokud máme vstup, který může být jen v intervalu <0,2>, tak tam například zkusíme poslat hodnotu 3
- **Exception tests**:
  - testy, které si kladou za cíl vyvolat výjimku, nebo přerušit program
- **Performance tests**
  - testy měřící rychlost řešení
- **Náhodné testy**:
  - testování založené na zkušenostech z předchozího vývoje podobných řešení

### White-box testing

Nevýhodou **black-box testů** je fakt, že často **neotestují všechen výkonný kód**. **Testy pokrytí** se proto snaží zajistit, že **každý rozhodovací bod v programu, každé tvrzení, každá výsledná větev, je otestována alespoň jednou**.

**Testy pokrytí** předpokládají, že programátor plně zná implementaci software. Má tedy možnost "podívat se dovnitř" (na rozdíl od black-box testů). Podoba těchto testů **plně závisí na řešení implementace, není je proto možné psát paralelně s vývojem.**

Z pohledu **embedded systémů** se jedná o nejdůležitější testy, protože nám umožní pokrýt velkou část aplikace a tím zamezit neočekávaných chybám jež se mohou objevit v budoucnu, popřípadě vyloučit, že se jedná o chyby softwarového rázu.

Příklady white-box testů:

- **Statement coverage**: testy pokrývající to, že se každé tvrzení v programu vykoná alespoň jednou
- **Branch coverage**: testy pokrývající to, že se každé větvení programu (if - > true/false) vykoná alespoň jednou
- **Condition coverage**: pokrytí podmínek. Testy zajišťují, že podmínka je otestována na co nejvyšší možné spektrum různých hodnot (aneb co se stane, pokud porovnávám dvě čísla a pošlu tam float)

## Vývojové modely (SDPM)

Vývojové modely lze najít pod zkratkou SDPM (Software development process models). **Jejich cílem je definovat jednotlivé fáze vývoje** počínaje studiem problému až po údržbu kompletního řešení. Celému procesu, který model popisuje se říká SDLC (System deployment life cycle).

Dále budou popsány existující SDLC modely.

### Waterfall model

Tento model byl představen v roce 1970 Winstonem Roycem. Skládá se z 5 částí a vývojář **musí splnit předchozí část, aby se mohl posunout na další**. **Model je tedy sekvenční/lineární.**

|![Waterfall model](metody-testovani-vestavnych-systemu/waterfall.PNG "Waterfall model")|
|:--:|
|*obr. 1:* Waterfall model|
| *zdroj:* <https://www.javatpoint.com/software-engineering-waterfall-model> |

Popis částí:

1. **Analýza požadavků a specifikací**: Vývojář si sedne se zákazníkem a společně projdou, co by měl systém umět, jaký by měl mít výkon a popřípadě jaká bude podoba uživatelského rozhraní. **Výsledkem je dokument Software Requirement Specification (SRS)**.
2. **Design**: Tato část se zabývá **vytvořením architektury aplikace na základě SRS dokumentu**. Tato architektura již umožní programování v určitém jazyce. **Výsledkem je opět dokument, tentokrát pod názvem Software Design Document (SDD)**.
3. **Implementace a Unit testy**: V této části je implementován **design** na základě SDD. Pokud je SDD kompletní, tak jde tato část hladce, neboť se řídí čistě tím, co je v SDD.
4. **Integrace a testování systému**: Tato část je **velmi důležitá**, neboť **kvalita výsledného produktu závisí na efektivitě testů prováděných v této části**.
5. **Údržba**: Udržování systému v chodu za pomocí spravování vzdálených úložišť. Tato část může být vykonávána i uživatelem samotným, který udržuje systém v chodu.

Výhody:

- jednoduchá implementace
- požadavky jsou pevně dané a není potřeba počítat s tím, že se budou měnit
- začátek a konec každé fáze je pevně daný

Nevýhody:

- rizikový faktor je vyšší, není tedy vhodné použít tento model pro komplexní projekty
- model se nedokáže adaptovat na změny v požadavcích
- je těžké vracet se k předchozím fázím
- Testování je děláno v pozdějších fázích -> je těžké odhadnout náročnost a rizika projektu

Kdy použít **waterfall model**?

- Požadavky jsou pevně dané a neměnné
- Projekt je malý
- Používané nástroje a technologie je neměnná
- Potřebné zdroje jsou připraveny k použití

### Spirálový model

Jedná se o model, který kombinuje předchozí **lineárně sekvenční postup** s možností opakované iterace. Software je vytvářen pomocí série několika vydání. Prvotní série mohou mít podobu náčrtu na papíře, nebo prototypu. S dalšími sériemi stoupá komplexita vytvořeného systému. Každá série je cyklus tvořený ze 4 částí.

|![Spiral model](metody-testovani-vestavnych-systemu/spiral.PNG "Spiral model")|
|:--:|
|*obr. 2:* Spiral model|
| *zdroj:* <https://www.javatpoint.com/software-engineering-spiral-model> |

Popis částí:

1. **Identifikace**: Začátkem každého cyklu je identifikace toho, co je účelem tohoto cyklu, identifikace způsobů, jak dosáhnout vytyčených cílů.
2. **Odhadnutí rizik**: Cílem je odhadnutí možných problémů na základě vnímání rizik v dané fází (cyklu).
3. **Development a validace**: Implementace strategií, jež potlačí nejasnosti a rizika. Může zahrnovat věci jako jsou benchmarking, simulace a prototypování.
4. **Plánování**: Projekt je zhodnocen a na základě tohoto hodnocení se určí, zdali cyklus zopakovat, nebo se přesunout na další fázi (sérii).

Výhody:

- Zaměření na analýzu rizik
- Snadná implementace pro velké projekty

Nevýhody:

- Drahý model na použití
- Analýza rizik vyžaduje expertízu
- Není vhodný pro malé projekty

Kdy použít **Spirálový model**?

- Požadavky jsou nejasné a komplexní
- Projekt je velký
- Změny mohou nastat kdykoliv 
- Projekt má vyhrazené velké finanční zdroje

## V-model

Tento model je také označovaný jako **verifikační a validační model**. Používá stejnou sekvenční logiku pro jednotlivé fáze jako [Waterfall model](#waterfall-model). Rozdíl je v tom, že **testy jsou plánované paralelně vůči fázi, ve které se projekt nachází**.

|![V-model](metody-testovani-vestavnych-systemu/V-model.PNG "V-model")|
|:--:|
|*obr. 3:* V-model|
| *zdroj:* <https://www.javatpoint.com/software-engineering-v-model> |

Model se skládá ze dvou cyklů:

- **verifikace**: statická analýza bez spouštění kódu.
- **validace**: dynamická analýza, testování je prováděno spouštěním kódu. Kontroluje, zdali jsou splnění očekávání a požadavky klienta.

**Verifikace**:

1. **Analýza business požadavků**: Zjištění požadavků klienta 
2. **Design systému**: Zhodnocení a vytvoření hrubého nástinu na základě požadavků klienta.
3. **Design architektury(High-level-design)**: Návrh jednotlivých modulů systému, vztahů mezi moduly, závislostí, struktury tabulek, letmý popis funkcionality modulů
4. **Design modulů (Low-level-design)**: Rozvržení jednotlivých modulů a jejich zjednodušení na základní moduly.
5. **Programování**: Na základě potřeby a designu projektu je zvolen vhodný programovací jazyk, v této fázi prochází řešení řadou *code-review* pro zajištění maximálního výkonu.

**Validace**: 

1. **Unit testy**: Jejich návrh je vytvořen už ve fázi **designu modulů**, ale implementace probíhá až po **programování**. Účelem je zjistit bezchybnost jednotlivých nezávislých modulů.
2. **Integrační testy**: Jejich návrh je vytvořen už ve fázi **designu architektury**. Testy ověřují, zdali jednotlivé skupiny modulů dokáží správně komunikovat mezi s sebou.
3. **Systémové testy**: Jejich návrh je vytvořen už ve fázi **designu systému**.  Jsou **vytvořeny business týmem klienta** a zaručují, že jsou naplněna očekávání klienta.
4. **Acceptance testing**: Jejich návrh je vytvořen už ve fázi **analýzy business požadavků**. Zahrnují testování software v prostředí klienta. Mohou odhalit problémy, jež souvisí s jiným software, který je využívaný klientem, nebo problémy s výkonem v uživatelském prostředí.

Výhody: 

- Jednoduchý na pochopení
- Design testů se provádí před samotným programováním
- Šíření chyb mezi jednotlivými fázemi je zabráněno 

Nevýhody:

- Není flexibilní
- Nehodí se na komplexní projekty
- Software je vytvářen společně s implementací, není tedy možnost prototypu
- Pokud se objeví nějaké změny, je potřeba upravit již vytvořené testovací dokumenty

Kdy použít **V-model**?

- požadavky jsou srozumitelné a dobře definované
- projekt je malý až střední
- počáteční zdroje jsou připravené s možností získat další zdroje v budoucnu

### W-model

Jedná se o model vycházející z **V-modelu**. **Účelem je možnost testování již od prvního dne započetí vývoje**.

|![W-model](metody-testovani-vestavnych-systemu/W-model.PNG "W-model")|
|:--:|
|*obr. 4:* W-model. Každá část verifikace (oranžová), je testována validací (černá)|
| *zdroj:* <https://www.javatpoint.com/software-engineering-v-model> |

Z obrázku je patrné, že **W-model řeší nedostatek V-modelu**. v podobě **možnosti testovat systém a kód při jeho vývoji** (nikoliv až po něm). Princip tohoto modelu zůstává jinak shodný s V-modelem.

Výhody:

- možnost testovat a vyvíjet zároveň

Nevýhody:

- komplexní a tím pádem složitý na implementaci
- špatně se odhaduje, kolik zdrojů alokovat do vývoje systému, povětšinou je prvotní odhad příliš malý
- Testování má stejnou váhu jako jiné aktivity

Kdy použít **W-model?**

- Máme mnoho různých aktivit, jež je třeba implementovat
- Pokud nestačí V-model

### Delta-W-model

Jedná se o úpravu W-modelu tak, aby vyhovoval požadavkům na vývoj embedded systémů, tedy tomu, že je **potřeba vyvinout nejen software, ale i zařídit vhodný hardware**.

Skládá se ze dvou částí, **požadavků** a **vytváření**. Testování obou částí je zajištěno. V případě **požadavků** jde o **kontroly (review)**, v případě vytváření jde o **testy**.

**Konceptuálně se jedná o W-model s přidaným paralelismem (vyvíjíme jak HW tak SW)**

|![Delta-W model](metody-testovani-vestavnych-systemu/delta-w_base.PNG "Delta-W model")|
|:--:|
|*obr. 5:* Delta-W model|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

Stejně jako v případě **W-modelu** jde testování ruku v ruce s vývojem. Premisa zůstává stále stejná **(vyvíjíme a testujeme zároveň, nejdříve navrhujeme, poté implementujeme)**

První části ze dvou je tedy zjištění požadavků. Tyto požadavky zároveň hodnotíme ve formě review testů.

|![Návrhová část Delta-W modelu](metody-testovani-vestavnych-systemu/delta-W_requirments.PNG "Návrhová část Delta-W modelu")|
|:--:|
|*obr. 6:* Návrhová část Delta-W modelu|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

|![Hodnocení požadavků (review) část Delta-W modelu](metody-testovani-vestavnych-systemu/delta-w_review.PNG "Hodnocení požadavků (review) část Delta-W modelu")|
|:--:|
|*obr. 7:* Hodnocení požadavků (review) část Delta-W modelu|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

**Poté, co máme zrevidované detailní návrhy jak softwarové, tak hardwarové části, je možné započít implementaci**

|![Implementační část Delta-W modelu](metody-testovani-vestavnych-systemu/delta-w_manufacturing.PNG "Implementační část Delta-W modelu")|
|:--:|
|*obr. 8:* Implementační část Delta-W modelu|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

**S tím, jak implementujeme jednotlivé části, je zároveň i testujeme.**

|![Testování implementace podle Delta-W modelu](metody-testovani-vestavnych-systemu/delta-w_testing.PNG "Testování implementace podle Delta-W modelu")|
|:--:|
|*obr. 9:* Testování implementace podle Delta-W modelu|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

Díky testování je poskytována **zpětná vazba**, která nám umožní vyladit nedostatky.

|![Příklad výroby hardware při použití Delta-W modelu](metody-testovani-vestavnych-systemu/delta-w_hardware_creation.PNG "Příklad výroby hardware při použití Delta-W modelu")|
|:--:|
|*obr. 10:* Příklad výroby hardware při použití Delta-W modelu|
| *zdroj:* <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf> |

## Modelování vestavných (embedded) systémů

K modelování **embedded systémů** se často využívají **modelovací jazyky**. Jedná se vizuální (grafické) jazyky, které nám umožňují lépe přiblížit danou skutečnost. Těchto jazyků je velké množství, uvedeme pár příkladů.

- UML - standart pro modelování software
- EXPRESS - pro modelování dat
- Behavioral Trees - používané pro modelování konkurence, nedeterminismu, synchronizace a komunikace
- Vývojový diagram - pro schématické znázornění určitého algoritmu

Embedded systémy sestávají ze dvou složek

- **hardware**
- **software**

Z definice embedded systému plyne, že takový systém je dělaný za konkrétním účelem. (pokud nebereme v potaz sofistikované vývojové desky typu Arduino či Raspberry). Návrh systému by měl tedy sestávat z:

1. **Vytvoření modelu hardwarového řešení**
2. **Vytvoření modelu softwarového řešení**

Vytvoření návrhů nám umožní znovupoužitelnost a hlavě přehlednost ohledně toho, z jakých komponent se daný embedded systém skládá. K tomuto účelu se nejlépe hodí vývojový model [Delta-W](#delta-w-model).

Více o modelování embedded systémů lze najít v otázce [Modelování a simulace vestavných systémů](https://atlas144.codeberg.page/szz/vs/modelovani-a-simulace-vestavnych-(embedded)-systemu.html).

## Zdroje

- Embedded Software Testing Basics. *Embedded Staff*. [online] @2011 [citováno: 12.05.2023] <br> Dostupné z: <https://www.embedded.com/the-basics-of-embedded-software-testing-part-1/>
- V-Model. *Javapoint*. [online] @2021 [citováno: 12.05.2023] <br> Dostupné z: <https://www.javatpoint.com/software-engineering-v-model>
- Waterfall model. *Javapoint*. [online] @2021 [citováno: 12.05.2023] <br> Dostupné z: <https://www.javatpoint.com/software-engineering-waterfall-model>
- V-Model. *Javapoint*. [online] @2021 [citováno: 12.05.2023] <br> Dostupné z: <https://www.javatpoint.com/software-engineering-v-model>
- What is V-model and W-model in Software Testing *Testbytes*. [online] @2017 [citováno: 12.05.2023] <br> Dostupné z: <https://www.testbytes.net/blog/v-model-and-w-model-software-testing/>
- W-development model extension for embedded system design. *Department of Microelectronics, Faculty of Electrical Engineering and Communication, BUT, Brno,
Technická 10, Brno. D. Jaroš, M. Pavlík.*[online] @2011 [citováno: 12.05.2023]  <br> Dostupné z: <https://dspace5.zcu.cz/bitstream/11025/623/1/r5c4c2.pdf>
- Modelling Language *Wikipedia: The Free Encyclopedia*. [online] @2023 [citováno: 12.05.2023] <br> Dostupné z: <https://en.wikipedia.org/wiki/Modeling_language>
