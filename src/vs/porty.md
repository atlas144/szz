# Porty

> Paralelní porty, sériové porty (asynchronní, synchronní), porty komunikačních sběrnic (I2C, RS232, CAN-BUS, Ethernet apod.). Přímý přístup do paměti DMA. Použití HW přerušení komunikace.

## Co je to port?

**Port** poskytuje rozhraní pro připojení **[periferního](https://atlas144.codeberg.page/szz/vs/periferie.html)** zařízení k počítačovému systému.  **Porty** jsou standardizované např. tvarem či počtem vodičů podle toho, jaké zařízení umožňuje daný **port připojit**. **Port je rozhraní mezi základovou deskou a periferním zařízením**. Každý **port** je připojený na **sběrnici**, která tuto **komunikaci zajišťuje**.  Struktura **portu** je dána jeho **standardem**.

## Druhy portů

V počítačích se nejvíce používají dva druhy portů, které **rozlišujeme podle toho, jaký typ sběrnice využívají**:

- **sériové** (tiskárny, modemy)
- **paralelní**

Dříve se skrze tyto porty připojovala většina **vnějších I/O periferií**. Dnes se pro připojení většiny takových **periferií** používá **USB port**.

## Způsoby komunikace

Podle toho, jestli mají jednotlivá zařízení **clock rate** dělíme komunikaci na dva způsoby

- **synchronní**
- **asynchronní**

Hlouběji je to popsáno v otázce [3.6](https://atlas144.codeberg.page/szz/vs/komunikacni-rozhrani.html).

### Synchronní komunikace

Při **synchronní** komunikaci, běží obě dvě spojené strany na tzv. **stejných hodinách**. Vyžaduje jeden drát navíc, pomocí kterého jsou obě strany schopné se **synchronizovat**, takže jsou schopné zasílat / přijímat data stejnou rychlostí. Pokud by tomu tak nebylo, tak by jedna ze stran, s pomalejšími hodinami, například poslala řetězec *100100*, ale ta druhá, s rychlejšími hodinami, řekněme 4x rychlejšími, by tento řetězec přečetla jako *11111111*, protože poté co by dorazila první jednička v podobě vysokého napětí, tak než by se toto napětí stalo opět nízkým, rychlá strana už za tu dobu přečetla 4x vysoké.

### Asynchronní komunikace

U **asynchronního** způsobu se nepoužívají **společné hodiny**. Namísto toho mají přednastavenou rychlost přenosu, tzv. **baud rate**. Je tedy zajištěné, že rychlost přenosu jedné strany, odpovídá rychlosti čtení druhé strany.

## Sériové porty 

**Sériový port**, také nazývaný **COM port** (označení z dob DOSu)  je nejstarším druhem **portu**. To, jaké má piny, jejich rozložení a napěťové hodnoty (Jak velké napětí na pinu musí být, aby bylo interpretováno jako logická 1), je dáno **[standardem RS-232](#rs-232-port)**. Skládá ze z konektoru ve tvaru písmene D (D-sub) a obsahuje 9 pinů.  Data se tímto portem přenášejí jako ucelený řetězec 1 a 0, reprezentovaný ve formě elektrických signálů. **Sériový port** poskytuje pouze jednu linku, po které se data přenášejí.

**Sériové porty** přenášejí jednotlivé byty za sebou. Při začátku komunikace zašle **sériový port** tzv. **start bit**, který má hodnotu 0. Po **každém bytu**, který odešle, zasílá **stop bit**, aby dal najevo, že byte je kompletní.

Jedná se o **bi-direktionální komunikaci**, tedy obousměrnou. Port dokáže data přijímat i odesílat. Díky tomu, že má rozdílné piny na přijímání (Rx) a odesílání (Tx), může port přijímat a odesílat data zároveň. Jedná se o **full-duplex** port.

|![Schéma zapojení dvou sériových portů, zde konkrétně základní sériový port D-SUB)](porty/serial_scheme.PNG "Schéma zapojení dvou sériových portů,zde konkrétně základní sériový port D-SUB")|
|:--:|
|*obr. 1:* Schéma zapojení dvou sériových portů,zde konkrétně základní sériový port D-SUB|
| Dostupné z:  <https://www.virtual-serial-port.org/article/what-is-serial-port/rs232-pinout/>|

## Paralelní porty

**Paralelní porty**, na rozdíl od **sériových** můžou přenášet více bitů zároveň. Přenosová rychlost je tedy výrazně vyšší, nežli je tomu u **sériové komunikace**. Vyžadují tedy více datových linek po kterých se tyto data přenášejí. Konkrétně pro každý bit mají jednu linku, jsou tedy schopné přenést celý byte najednou. 

Jsou schopné data jak přijímat, tak odesílat, jedná se tedy o **bi-direktionální port** ale pouze v jednom směru zároveň. Nemohou najednou data přijímat a odesílat. Jde tedy o **half-duplex rozhraní**.

Původní účel **paralelního portu** bylo k připojení tiskáren, ale časem se jeho použití rozšířilo na monitory a externí HDD disky.

|![Paralelní port)](porty/parallel_scheme.png "Paralelní port")|
|:--:|
|*obr. 2:* Paralelní port (Každý datový kabel D0 - D7 má své uzemnění)|
| Dostupné z:  <https://www.bristolwatch.com/pport/pportbits.htm>|


Dnes se již **paralelní porty nevyužívají**. Důvodem je nutnost synchronizace všech datových linek. Toto byl velký problém například při ohnutí kabelu (U kabelů na "vnější" straně zatáčky trvá přenos o malý čas déle, což se ve výsledku nasčítá a bylo tedy nutné i řešit problémy fyzické vrstvy). Navíc s vývojem procesorů již bylo možné získat se **sériovými porty** dostatečně velkou přenosovou rychlost.

## USB port

V dnešní době jsou oba zmíněné porty nahrazeny **USB portem**. Většina výrobců notebooků již sériové a paralelní porty (neplést si se sběrnicemi) na svá zařízení ani nepřidává.

K jedné USB **sběrnici** lze najednou mít připojeno až 127 zařízení. Každému připojenému zařízení je při připojení přiřazena adresa. Tomuto procesu se říká **enumerace**. Pokud připojíme 128 zařízení, nestane se nic, doslova. Tomuto zařízení nebude již adresa přiřazena a musíme nějaké zařízení odpojit, aby naše 128 zařízení fungovalo. Jeho struktura je dána **standartem USB**.

Na základě **enumerace** je poté každé adrese, tedy **každému zařízení připojenému na USB sběrnici** přiřazen mód, ve kterém bude fungovat, neboli, jakým způsobem bude odesílat data. Tyto módy jsou celkem 3:

- **interrupt**
  - používá se pro zařízení, které přenáší malé množství dat
  - například myši, klávesnice
- **bulk**
  - používá se pro zařízení, které potřebují najednou velké množství dat, například tiskárny
  - data se posílají např. v 64 bytových balíčcích 
- **isochronous**
  - používá se pro streamovací zařízení, jako jsou například sluchátka
  - data se zde posílají v reálném čase tak, jak zrovna zařízení potřebuje

|![USB port)](porty/USB_scheme.PNG "USB port")|
|:--:|
|*obr. 3:* USB port (USB A nahoře a USB B dole)|
| Dostupné z:  <https://www.electroschematics.com/usb-how-things-work/>|

USB využívá **synchronní sériovou komunikaci**.

Původní rychlost USB byla 12 Mb/s, ale s novými verzemi USB 2.0 a USB 3.0 tato rychlost vzrostla až na 5 Gb/s.

## Porty komunikačních sběrnic

Na **port** můžeme pohlížet jako na vyvedení **komunikační sběrnice**. Ač se (původní) sériové a paralelní **porty** již téměř nepoužívají, tak **sériové** a **paralelní sběrnice** se využívají hojně. Příkladem může být **USB port**, který využívá **USB sběrnici**. USB sběrnice je druhem **sériové sběrnice**. **USB port lze tedy označit za sérový port**. 

**Struktura každého portu je dána protokolem pro přenos dat, který využívá**.

### **I2C port**

Jedná se o **port**, který je připojený k **synchronní, multi-master/multi-slave sériovou komunikační sběrnici** I2C . Termín **slave** označuje koncové zařízení, které je připojeno do portu, **master** je zařízení, ke kterému je koncové zařízení připojeno. U I2C platí, že komunikaci vždy zahajuje **master**, který mívá podobu mikrokontroléru. 

Každý **slave** má svoji adresu, na základě které **master** určuje, s kterým zařízením si přeje komunikovat. Port obsahuje dva dráty, **SDA**, který slouží pro **přenos dat** a SCL, který slouží pro přenos hodinového signálu. **Master** generuje hodinový signál a **slave** používá tento vygenerovaný signál pro přijmutí dat, nebo pro odpověď **masterovi**.

|![PI2C port s modulem pro sériovou komunikaci](porty/I2C.jpg "I2C port s modulem pro sériovou komunikaci")|
|:--:|
|*obr. 4:* I2C port s modulem pro sériovou komunikaci)|
| Dostupné z:  <https://www.ebay.co.uk/itm/203995295578>|

Pokud žádný **slave** nemá adresu, jež je **masterem** vystavena na sběrnici, tak žádný **slave** neodpoví.

**Využití:**

Základní specifikace definuje **maximální hodinovou frekvenci na 100 kHz**. Časem toto bylo navýšeno na 400 kHz. Sběrnice dokáže operovat v tzv. **High speed** módu, která umožňuje **přenos rychlostí až 5 MHz**.

**I2C sběrnice** se používá pro připojení **I2C zařízení**. Jmenovitě se jedná například o I2C sensory pomocí **I2C portů**.

Velké využití je v **embedded systémech** k připojení EEPROM paměti, A/D and D/A převodníků, I/O rozhraních a dalších. **Důvodem je nutnost pouze 2 drátů, flexibilní rychlost a možnost adresovat jednotlivá zařízení připojená skrze I2C porty na I2C sběrnici**.

### **RS-232**

Jedná se o standart používaný pro [sériovou komunikaci](#sériové-porty). V průmyslu se **jedná o standart**, především jeho modifikace:
- RS-422
- RS-485

Standard definuje **asynchronní sériovou komunikaci pro přenos dat**. Pořadí přenosu datových bitů je **od nejméně významného bitu** (LSb, často nesprávně LSB) **po bit nejvýznamnější** (MSb, často nesprávně MSB). Počet datových bitů je volitelný, obvykle se používá 8 bitů, lze se také setkat se 7 nebo 9 bity.

**Využití:**

Dnes se RS-232 standart využívá hlavně u CNC strojů nebo kontrolérů servo-motorů. Oproti USB je totiž jeho výroba mnohem levnější. Programování portů používajících standart RS-232 je také mnohem jednodušší.

### **Ethernet port**

Jedná se o  **8-pinový port** který je na počítači připojený do **řadiče internetového rozhraní**. Port si nese **označení RJ-45**, nebo také **LAN port**.  Struktura portu je daná stejnojmenným standardem **Ethernet**.

Jde o **sériový port**, který byl vyrobený čistě za účelem **posílání paketů dat** mezi jednotlivými zařízeními.

Kabel, který spojuje **ethernetové porty** dvou různých zařízení má v sobě celkem 8 drátů, jež tvoří 4 **kroucené dvojlinky** 

Pokud propojujeme dvě stejná zařízení (není mezi nimi **modem**), tak je připojení drátů do portu na každém zařízení jiné, tedy Rx a Tx dráty jsou prohozené. Takovému zapojení se říká **crossover**. Nicméně **Dnes již není nutné řešit, jaký druh zapojení používáme, zařízení si sama dokáží určit význam vodičů (= přehodit si je)**. 

|![Propojení dvou zařízení A a B skrze Ethernet Port](porty/LAN_Cable.png "Propojení dvou zařízení A a B skrze Ethernet Port")|
|:--:|
|*obr. 5:* Propojení dvou zařízení A a B skrze Ethernet Port|
| Dostupné z:  <https://napovedy.cz/post/268>|

**Ethernetový port**, v závislosti na jeho typu a použitém kabelu podporuje rychlosti: 

- 10 Mb/s
- 100 Mb/s
- 1 Gb/s
- 10 Gb/s

**Využití:**

Využití **Ethernet portu** je hojné v **oblasti sítí využívajících Ethernet protokol**, konkrétně pro připojení zařízení k **routeru**, nebo propojení jednotlivých zařízení mezi s sebou.

## CAN-Bus port

**CAN** (Controlled Area Network) je sama o sobě **sběrnice**.  Na tuto sběrnici lze připojit port standartu RS-232 a používat ho jako CAN port, přičemž budeme využívat jen některé piny. **CAN port** je **synchronní a sériový** port.

O tom, jak funguje **CAN sběrnice** se lze dočíst v okruhu [Komunikační rozhraní](https://atlas144.codeberg.page/szz/vs/komunikacni-rozhrani.html)

|![RS-232 připojený na CAN sběrnici](porty/RS-232_CAN.PNG "RS-232 připojený na CAN sběrnici")|
|:--:|
|*obr. 6:* RS-232 připojený na CAN sběrnici|
| Dostupné z:  <https://product-help.schneider-electric.com/Machine%20Expert/V1.2/en/tm5hwlmc/tm5hwlmc/LMC258-HW-Communication_Services/LMC258-HW-Communication_Services-3.htm>|

**Využití:**

Účelem bylo vytvořit **sběrnici** která umožní propojit různé zařízení, která mají **různé porty**. CAN port může tedy být jakýkoliv port, který umožní připojení ukázané na obrázku výše.

Hlavní použití je **v automobilech pro propojení různých zařízení s centrální řídící jednotkou**. 

## DMA (Přímý přístup do paměti)

Jedná se o způsob toho, jakým si zařízení připojené k portu žádá o přístup ke čtení/zápisu.

**DMA** je požádání o přímou práci s pamětí. Zařízení si poté do paměti přistupuje samo bez přičinění procesoru.

Jedná se o jeden z **principů řízení periferií**, o kterých si lze více přečíst v okruhu [Periferie](https://atlas144.codeberg.page/szz/vs/periferie.html).

## Hardwarové přerušení

Jedná se o další způsob, jakým si zařízení připojené do portu žádá a čtení, či zápis.

**hardwarové přerušení** spočívá v **odeslání signálu procesoru pokaždé, když chce periferie číst či zapisovat data**. **Procesor přeruší to, co právě dělá, obslouží periferii** a poté se opět vrátí ke své původní práci. 

Jedná se o jeden z **principů řízení periferií**, o kterých si lze více přečíst v okruhu [Periferie](https://atlas144.codeberg.page/szz/vs/periferie.html).

## Zdroje

- Javapoint. *What is a Serial Port?* \[online\], @2021. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.javatpoint.com/what-is-a-serial-port>
- Tutorialspoint. Kiran Kumar Panigrahi. *Difference between Serial Ports and Parallel Ports* \[online\], @2022. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.tutorialspoint.com/difference-between-serial-ports-and-parallel-ports>
- Wikipedia: Free Encyclopedia. *Synchronous serial communication \[online\], @2022. [citováno 28. 04. 2023]<br>Dostupné z: <https://en.wikipedia.org/wiki/Synchronous_serial_communication>
- GeeksForGeeks. *Asynchronous serial data transfer \[online\], @2022. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.geeksforgeeks.org/asynchronous-serial-data-transfer/>
- ComputerHope. *Serial Communication Methods – Synchronous & Asynchronous \[online\], @2022. [citováno 28. 04. 2023]<br>Dostupné z: <https://pijaeducation.com/communication/serial-communication-methods-synchronous-asynchronous/>
- Pija Education. *Parallel port \[online\], @2020. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.computerhope.com/jargon/p/paraport.htm>
-  P. Marian. *USB Pinout, Wiring and How It Works \[online\], @2014. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.electroschematics.com/usb-how-things-work/>
- Bhimappa M. *I2C Protocol and Multi-master concept in I2C\[online\], @2020. [citováno 28. 04. 2023]<br>Dostupné z: <https://www.linkedin.com/pulse/i2c-protocol-multi-master-concept-bhimappa-mali>
- Wikipedia: Free Encyclopedia. *RS-232 \[online\], @2022. [citováno 28. 04. 2023]<br>Dostupné z: <https://en.wikipedia.org/wiki/RS-232>
- Sandeep Bhandari . *Difference between RJ45 and RS232 \[online\], @2023. [citováno 28. 04. 2023]<br>Dostupné z: <https://askanydifference.com/cs/difference-between-rj45-and-rs232/>
- Grant Maloy Smith. *What Is CAN Bus (Controller Area Network) and How It Compares to Other Vehicle Bus Networks \[online\], @2021. [citováno 28. 04. 2023]<br>Dostupné z: <https://dewesoft.com/blog/what-is-can-bus>
