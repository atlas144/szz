# Převodníky

> Převodníky pro elektrické a neelektrické veličiny A/D, D/A . Architektura převodníků. Pojem přesnosti a kompenzace chyb. Definice kvantování a kvantovací šum. Vzorkování a vzorkovací frekvence - její dopad do přenášeného spektra (Aliasing).

Převodník je zařízení, jehož účelem je transformace mezi různými veličinami, případně mezi různými typy jedné veličiny.

Převodníky lze z pohledu veličin dělit na:

- **elektrické → elektrické** - sem patří níže popsané A/D a D/A převodníky a pravděpodobně i modulátory (převádějí napětí na frekvenci, fázi, atd.)
- **neelektrické → elektrické** - senzory (např. digitální teploměr, vlhkoměr)
- **elektrické → neelektrické** - aktuátory (např. motory, LED)
- **neelektrické → neelektrické** - např. kovářská výheň s měchem (převádí pohybovou sílu na teplo) 

## Vzorkování

Vzorkování je proces, při kterém dochází, vždy po uplynutí vzorkovací periody, k odebrání vzorku z analogového vstupního signálu. Odebraná hodnota (v tomto případě elektrické napětí) se poté obvykle dále zpracovává. Lze říci, že se jedná o proces **diskretizace signálu v časové oblasti (definičním oboru)**.

Zařízení, které vzorkování provádí se nazývá vzorkovač (= vzorkovací obvod).

Důležité je při vzorkování zvolit správnou délku vzorkovací periody (respektive frekvenci vzorkovacího signálu1), aby nedocházelo k příliš velkému zkreslení a případným ztrátám informace. Podle **Shannonova–Nyquistova–Kotělnikovova teorému** (často se mu říká jen Shannonův–Kotělnikovův teorém) musí být vzorkovací frekvence nejméně dvakrát vyšší než nejvyšší frekvence obsažená ve vzorkovaném signálu. Řečeno matematicky:

\\[ f_{vz} \geq 2 \cdot f_{max} \\]

kde \\( f_{vz} \\) je vzorkovací frekvence a \\( f_{max} \\) maximální frekvence obsažená ve vzorkovaném signálu.

### Aliasing
 
Je jev, při kterém dochází ke zkreslení výstupného signálu, a to pokud není splněna podmínka *SNK teorému*. Jako ochrana se používá *antialiasing filtr* (*dolní propust* zařazená před převodníkem). Ten nedovolí frekvencím vyšším, než je *Nyquistova frekvence* (minimální vzorkovací frekvence podle SNK teorému), vstoupit do převodníku.

## Kvantování

Proces *kvantování* spočívá v přidělení každému vzorku (získanému vzorkováním) kvantizační hladinu. Počet hladin je určen počtem bitů, které máme k dispozici pro hodnoty výstupního signálu. *Lidsky řečeno*: spojitou škálu hodnot, kterých může vstupní signál nabývat rozdělíme na tolik dílů, kolik nám dává vztah \\( N_{kv} = 2^{n_b} \\), kde \\( N_{kv} \\) je počet dílů (hladin) a \\( n_b \\) počet bitů výstupního signálu a každému vzorku přiřadíme tu hladinu, která mu nejvíce odpovídá. Lze tedy říci, že se jedná o proces **diskretizace signálu v oboru hodnot**.

Nastavení hladin může být dvojího druhu:

- **lineární** - každá hladina zabírá stejný interval (nejběžnější)
- **nelineární** - intervaly jednotlivých hladin se liší (pro zvláštní účely; např. exponenciální, logaritmické)

### Kvantizační šum

Je jev, který nastává, pokud je *kvantování příliš hrubé* (máme málo výstupních bitů). Jedná se o nepřesnost způsobenou tím, že se skutečná hodnota vzorku musí transformovat na nejbližší hladinu, která je ale tak vzdálená, že je tento rozdíl v následujícím použití digitalizovaného signálu patrný. Je to dobře vidět např. u fotografií, kde vznikají místo plynulých přechodů tzv. *mapy* (třeba u fotografií oblohy).

Lze ho řešit zvýšením počtu kvantizačních hladin (většinou příliš náročné) nebo přidáním dodatečného šumu (dither), který rozbije jednolitost oněch ostrých přechodů a výsledek poté působí plynule.

## A/D převodník

A/D převodník slouží k převodu z **analogového na digitální** signál.

<div class="note">

### *Poznámka:* Druhy signálů

- **Analogový** - spojitý v čase i hodnotě (= je definován funkcí se spojitým nebo částečně spojitým *definičním oborem* i *oborem hodnot*)
- **Diskrétní**
  - **Vzorkovaný** - není spojitý v čase, ale v hodnotě ano (= jedná se o posloupnost vzorků)
  - **Kvantovaný** - je spojitý v čase, ale ne v hodnotě (= může nabývat pouze definovaných, které mají mezi sebou skokové rozdíly)
  - **Digitální** - není spojitý ani v čase, ani v hodnotě (= kombinace *vzorkovaného* a *kvantovaného*)

</div>

Důvodem pro převod z analogového signálu na digitální je především možnost **zpracování počítači** - počítače *umějí pracovat jen s diskrétními hodnotami* a *nemají nekonečnou paměť*, proto potřebují konečný počet záznamů = vzorků (např. výstupy senzorů často bývají analogové, proto se ke kontroléru připojují přes A/D převodník; DSP (digitální signálové procesory) též zpracovávají digitalizovaný signál). Krom toho má digitální signál výhody při přenosu signálu - lze kontrolovat správnost signálu (např. kontrolní součty) a také nedochází ke ztrátám informací při kopírování signálu.

Princip převodu spočívá ve [vzorkování](#vzorkování) signálu a následném [kvantování](#kvantování) těchto vzorků. Oba tyto procesy jsou popsány výše.

### Druhy A/D převodníků

- **Přímé** - výstupem je přímo počet kvant (tedy konkrétní hladina)
  - ***Komparační*** - využívají odporové děliče spolu s komparátory
    - *paralelní* - nejrychlejší typ (celý převod probíhá v jednom okamžiku); princip popsán v sekci [architektura A/D převodníků](#architektura-ad-převodníků); vhodný max do 8 výstupních bitů (poté už příliš mnoho součástek)
    - *s postupnou komparací* - cílem je zjednodušení (= snížení počtu potřebných komponent) paralelních převodníků při zachování podobně krátké doby zpracování
  - ***Kompenzační*** - využívají zpětnou vazbu; porovnávají vstupní napětí s výstupním a upravují ho, dokud nejsou rozdíly mezi nimi minimální; jsou konstrukčně jednoduché, relativně rychlé, středně drahé; používají se u PLC a mikrokontrolérů
    - *čítací* - hodnota čítače se zvyšuje, dokud nepřesáhne hodnotu vstupního napětí (zjišťujeme komparací přes zpětnou vazbu), poté je měření vzorku u konce
    - *sledovací* - stejný jako čítací, ale používá obousměrný čítač → může lépe reagovat na změny výstupního napětí
    - *s postupnou aproximací* - dochází k postupné úpravě jednotlivých bitů výstupu - vždy se vezme jeden bit (od MSB k LSB) a nastaví se na `1`, pokud je výsledné napětí vyšší než vstupní, vrátí se na `0`, jinak zůstává `1`; když se dojde k LSB, vzorek je hotov
- **Nepřímé** - dochází zde k mezi převodu na čas nebo frekvenci
  - ***Integrační*** - využívá integrátor a tedy výstup je průměrná hodnota za určitou dobu
  - ***S dvojitou integrací*** - často se používá v *multimetrech*; poměrně levný a obvodově jednoduchý
  - ***Sigma-delta*** - umožňují dosáhnout velmi vysoké linearity převodu při vysokém rozlišení; jsou pomalé

### Architektura A/D převodníků

Uvedeme zde pouze **paralelní komparační převodník**, jelikož je na pochopení asi nejjednodušší. Poté se jeho architekturu pokusíme zobecnit.

|![Schéma paralelního komparačního převodníku](prevodniky/ad.png "Schéma paralelního komparačního převodníku")|
|:--:|
|*obr. 1:* Schéma paralelního komparačního převodníku|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

Základními částmi tohoto typu převodníku jsou:

- **odporový dělič**
- **soustava komparátorů**
- **soustava paměťových klopných obvodů** a 
- **dekodér**

Jejich činnost a účel jsou popsány dále.

#### Odporový dělič

Je obecně soustava sériově zapojených rezistorů, kde za každým z nich následuje vývod, který při spojení se zemí vytvoří paralelní větev s napětím odpovídajícím úbytku na všech rezistorech před daným vývodem. V tomto případě mají všechny rezistory shodnou hodnotu \\( R \\) (až na první a poslední, ty mají \\( \frac{R}{2} \\)), což znamená, že přírůstek napětí na vývodech je *lineární*.

Tento dělič je napájen **referenčním napětím**, které odpovídá maximálnímu vstupnímu napětí převodníku. Na výstupech děliče tedy bude rovnoměrně rozloženo toto napětí. Počet rezistorů děliče je roven počtu kvantizačních hladin převodníku.

|![Schéma paralelního komparačního převodníku (vyznačen odporový dělič)](prevodniky/ad-delic.png "Schéma paralelního komparačního převodníku (vyznačen odporový dělič)")|
|:--:|
|*obr. 2:* Schéma paralelního komparačního převodníku (vyznačen odporový dělič)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

#### Soustava komparátorů

Komparátor je obvod, který porovnává dva vstupní signály na základě velikosti jejich napětí a na výstup dává log. `1` či `0`.

Zde máme pro každý vývod odporového děliče jeden komparátor (kromě vývodu za posledním rezistorem), který porovnává hodnotu napětí z vývodu děliče s aktuální hodnotou vstupního signálu.

|![Schéma paralelního komparačního převodníku (vyznačena soustava komparátorů)](prevodniky/ad-komp.png "Schéma paralelního komparačního převodníku (vyznačena soustava komparátorů)")|
|:--:|
|*obr. 3:* Schéma paralelního komparačního převodníku (vyznačena soustava komparátorů)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

#### Soustava paměťových klopných obvodů

Paměťový klopný obvod slouží k **dočasnému uložení hodnoty napětí**. Na jeden jeho vstup je přiveden vstupní signál, na druhý signál hodinový. Když dojde k hodinovému impulzu, do obvodu je uložena aktuální hodnota vstupního signálu a ta v něm setrvává do dalšího hodinového impulzu (poté je přepsána novou aktuální hodnotou). Uloženou hodnotu lze získat z výstupu obvodu.

Zde je za každým komparátorem jeden paměťový klopný obvod, který jako vstup přijímá hodnotu z daného komparátoru. Hodinový signál je pro celou soustavu společný.

|![Schéma paralelního komparačního převodníku (vyznačena soustava paměťových klopných obvodů)](prevodniky/ad-ko.png "Schéma paralelního komparačního převodníku (vyznačena soustava paměťových klopných obvodů)")|
|:--:|
|*obr. 4:* Schéma paralelního komparačního převodníku (vyznačena soustava paměťových klopných obvodů)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

#### Dekodér

Dekodér slouží k **převodu z výčtu hodnot na odpovídající binární číslo**, které je výstupem převodníku. Jako jeho vstupy jsou zde připojeny *paměťové klopné obvody*.

|![Schéma paralelního komparačního převodníku (vyznačen dekodér)](prevodniky/ad-dekoder.png "Schéma paralelního komparačního převodníku (vyznačen dekodér)")|
|:--:|
|*obr. 5:* Schéma paralelního komparačního převodníku (vyznačen dekodér)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

#### Popis činnosti

Neustále dochází k dělení napětí na děliči a jeho komparaci se vstupním napětím. Výsledek komparace je však ignorován, dokud zdroj hodinového signálu nevyšle impulz. V reakci na něj dojde k uložení aktuálních výsledků komparace do paměťových klopných obvodů a jejich dekódování. Dekódovaná binární hodnota je na výstupu převodníku dostupná až do vyslání dalšího hodinového impulzu.

#### Vysvětlení kontextu

Jak bylo řečeno výše, proces A/D převodu spočívá ve *vzorkování* a *kvantování* vstupního signálu. Výše popsaná činnost tomu poměrně pěkně odpovídá - reakce na hodinový (**vzorkovací**) signál a následné uložení hodnoty (**vzorku**) do paměťových KO je proces **vzorkování**, kdežto komparace vstupního napětí s referenčními hodnotami (**hladinami**) je proces **kvantování**. Krom vzorkování a kvantování zde dochází ještě ke **kódování** (v dekodéru), to se však obvykle v popisu opomíjí.

## D/A převodník

Jak název napovídá, D/A převodníky slouží k transformaci digitálního signálu na signál analogový. To je užitečné např. pro audiotechniku (ze zvukové karty do sluchátek), zobrazení signálu případně pro některé typy aktuátorů ovládaných mikrokontrolérem.

### Druhy D/A převodníků

Nejběžnější jsou:

- **s váhovými odpory** - vyžaduje velké množství přesných odporů s různými hodnotami
- **s odporovou sítí R-2R** - poměrně jednoduché zapojení

### Architektura D/A převodníků

#### S váhovými odpory

|![Schéma převodníku s váhovými odpory (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)](prevodniky/da-odpory.png "Schéma převodníku s váhovými odpory (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)")|
|:--:|
|*obr. 6:* Schéma převodníku s váhovými odpory (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

A/D převodník s váhovými odpory se skládá ze dvou hlavních částí:

- **soustava odporů**
- **soustava spínačů**

Jejich činnost a účel jsou popsány dále.

##### Soustava odporů

Skládá se z \\( n \\) rezistorů (kde \\( n \\) je počet vstupních bitů převodníku), které tvoří paralelní soustavu. Ta je připojena k referenčnímu napětí. Poslední rezistor soustavy má hodnotu odporu \\( R \\) a každý předešlý poté polovinu toho následujícího (tedy \\( \frac{R}{2} \\), \\( \frac{R}{4} \\), atd.).

|![Schéma převodníku s váhovými odpory (vyznačena soustava odporů)](prevodniky/da-odpory-odp.png "Schéma převodníku s váhovými odpory (vyznačena soustava odporů)")|
|:--:|
|*obr. 7:* Schéma převodníku s váhovými odpory (vyznačena soustava odporů)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

##### Soustava spínačů

Ke každému odporu ze soustavy odporů je připojen jeden spínací prvek (nejčastěji MOS tranzistory), který odpovídá jednomu vstupnímu bitu (odporu s hodnotou \\( R \\) odpovídá *LSB*; odporu s nejnižší hodnotou *MSB*). Tedy je-li hodnota odpovídajícího bitu log. `1`, tranzistor je otevřen, je-li `0`, je uzavřen.

|![Schéma převodníku s váhovými odpory (vyznačena soustava spínačů)](prevodniky/da-odpory-sp.png "Schéma převodníku s váhovými odpory (vyznačena soustava spínačů)")|
|:--:|
|*obr. 8:* Schéma převodníku s váhovými odpory (vyznačena soustava spínačů)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

##### Princip činnosti

Přivedením vstupní binární hodnoty dojde k otevření příslušných tranzistorů a ty začnou propouštět proud z referenčního zdroje skrze příslušné rezistory do společného uzlu. V tomto uzlu dochází ke **sloučení těchto proudů** a na výstupu se objevuje *napětí, které odpovídá výslednému proudu*. Je nutno dodat, že koeficienty jednotlivých proudů odpovídají převráceným hodnotám koeficientů odporů, tedy \\( I \\) pro \\( R \\), \\( 2I \\) pro \\( \frac{R}{2} \\), atd.

#### S odporovou sítí R-2R

Architektura těchto převodníků je velmi podobná převodníkům s váhovými odpory. Hlavním rozdílem je, že **vyžaduje pouze dvě hodnoty odporů** (\\( R \\) a \\( 2R \\)). Ty jsou zapojeny jako série děličů (s poměrem 2:1), vždy jeden dělič místo jednoho odporu u převodníků s váhovými odpory. Hodnoty protékajících proudů odpovídají proudům u převodníků s váhovými odpory.

|![Schéma převodníku s odporovou sítí R-2R (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)](prevodniky/da-mat.png "Schéma převodníku s odporovou sítí R-2R (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)")|
|:--:|
|*obr. 9:* Schéma převodníku s odporovou sítí R-2R (tranzistorové spínače jsou zde pro jednoduchost nahrazeny mechanickými)|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

Druhým rozdílem je, že spínací prvek je zde dvojice MOS tranzistorů buzených v protifázi (*to bych však možná raději neuváděl - není jisté, že to tak je vždy*).

|![Schéma jednoho bitu převodníku se skutečným spínačem](prevodniky/da-mat-sp-real.png "Schéma jednoho bitu převodníku se skutečným spínačem")|
|:--:|
|*obr. 10:* Schéma jednoho bitu převodníku se skutečným spínačem|
| *zdroj:* HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín* |

## Přesnost převodníků

Přesnost je jedním ze základních parametrů převodníků. Můžeme na ni nahlížet ze dvou úhlů:

- **rozlišení** - důležitým vlivem na přesnost převodníku je **počet bitů**; čím více bitů převodník má, tím více máme k dispozici kvantovacích hladin a tím detailnější výstup z převodníku získáme
- **kvalita součástek** - dalším vlivem na přesnost je kvalita použitých součástek, protože i když budeme mít velmi vysoké rozlišení, potřebujeme zároveň aby nedocházelo k výběru špatných hladin (u A/D) a špatné interpretaci hladin (u D/A)

## Kompenzace chyb

Jak již bylo uvedeno [dříve](#aliasing), aliasing lze kompenzovat:

- **aliasing filtrem (dolní propust)**

Kvantizační šum lze kompenzovat (rozvedeno [zde](#kvantizační-šum)):

- **zvýšením počtu kvantizačních hladin**
- **přidání dodatečného šumu (dither)**

## Zdroje

- Wikipedia contributors. *Analog-to-digital converter* [online], Wikipedia, The Free Encyclopedia, c2023, Datum poslední revize 28. 4. 2023, 14:11 UTC, [citováno 29. 04. 2023] <https://en.wikipedia.org/w/index.php?title=Analog-to-digital_converter&oldid=1152147100>
- Přispěvatelé Wikipedie, *A/D převodník* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 23. 01. 2023, 18:54 UTC, [citováno 29. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=A/D_p%C5%99evodn%C3%ADk&oldid=22375060>
- Přispěvatelé Wikipedie, *Analogový signál* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 30. 01. 2023, 20:57 UTC, [citováno 29. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Analogov%C3%BD_sign%C3%A1l&oldid=22395542>
- Přispěvatelé Wikipedie, *Diskrétní signál* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 30. 01. 2023, 20:58 UTC, [citováno 29. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Diskr%C3%A9tn%C3%AD_sign%C3%A1l&oldid=22395547>
- Přispěvatelé Wikipedie, *Vzorkování* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 31. 01. 2023, 20:18 UTC, [citováno 29. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Vzorkov%C3%A1n%C3%AD&oldid=22398751>
- Přispěvatelé Wikipedie, *Kvantování (signál)* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 21. 05. 2022, 11:34 UTC, [citováno 29. 04. 2023] <https://cs.wikipedia.org/w/index.php?title=Kvantov%C3%A1n%C3%AD_(sign%C3%A1l)&oldid=21301238>
- *Kvantování signálu* [online]. 2023-04-29 [cit. 2023-04-29]. Dostupné z: <http://fyzika.jreichl.com/main.article/view/1357-kvantovani-signalu>.
- SCHWARZ, Daniel. *Analýza a modelování dynamických biologických dat - Úvod do problematiky* [online]. 2013-10-17 [cit. 2023-04-29]. Dostupné z: <https://is.muni.cz/www/98951/41610771/43823411/43823458/Analyza_a_modelo/44257487/Bi0440-Schwarz-VJ01.pdf>.
- HORÁČEK, Jaroslav. *Prezentace pro předmět ELT na SOŠ SE Velešín*. [cit. 2023-04-29].
