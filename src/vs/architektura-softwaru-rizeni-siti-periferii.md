# Architektura softwaru řízení sítí periferii

> Robotický operační systém (ROS). Řízení
periferii v reálném čase. Sledování extrémně rychlých a pomalých veličin (v čase).

Robotický operační systém je vysvětlen v otázce č. [3.3 Architektura operačních systémů užívaných ve vestavných systémech](https://atlas144.codeberg.page/szz/vs/architektura-operacnich-systemu-uzivanych-ve-vestavnych-systemech.html).

## Co to je řízení v reálném čase a proč jej potřebujeme?

De facto jde o adaptaci systému na reálnou situaci bez odezvy, nebo s velmi, velmi, velmi, velmi atd. malou odezvou. 

Příkladem budiž sešlápnutí plynového nebo brzdového pedálu v autě. Po sešlápnutí pedálu se auto okamžitě rozjede nebo naopak brzdí. Mezi stiskem pedálu a akcelerací/decelerací není téměř žádná odezva. Postup je takový:

1. Shromáždíme data (senzorikou)
2. Data zpracujeme
3. Aktualizujeme výstup v definovaném časovém rámci (nesmí být překročen)

Pokud bychom nedodrželi časový rámec, auto by nezrychlilo, nebo naopak nebrzdilo.

###  Princip řídícího systému

1. senzorika (shromažďujeme data)
2. řídící jednotka (MCU) - vyhodnotí a zpracuje data
3. adaptace systému (chod motoru, otevření/zavření ventilu apod.)
4. komunikace mezi jednotlivými prvky systému

Komunikace by měla být nedílnou součástí celého procesu.

|![Funkční prvky řízení real-time](architektura-softwaru-rizeni-siti-periferii/scheme.PNG "Funkční prvky řízení real-time")|
|:--:|
|obr. 1: Funkční prvky řízení real-time|
| zdroj: Texas Instruments: What is “real-time control” and why do you need it? [online]. 2022 [cit. 2023-05-12]. Dostupné z: <https://e2e.ti.com/blogs_/b/analogwire/posts/what-is-real-time-control-and-why-do-you-need-it>|

#### Senzorika

Senzory zajišťují sběr (měření) dat (napětí, proud, atmosférická vlhkost či teplota, rychlost motoru...). V ideálním případě je potřeba je naměřit s nejvyšší precizností, tak aby potenciální výstup odpovídal aktuální situaci.

#### CPU

Vstupní data je třeba zpracovat v co nejmenším časovém intervalu, tak, aby mohli následně na základě příkazů, reagovat další periferie (motor, píst, ventil...), optimálně v řádu jednotek mikrosekund.

#### Adaptace

MCU zpracuje získaná data a na základě jejich vyhodnocení pošle souvztažný příkaz do periférií. Např. změna pracovního cyklu (duty-cycle) PID regulátoru, je adaptací systému.

#### Komunikace

Přesná a rychlá komunikace mezi komponenty prostřednictvím Fast Serial Interface nebo Ethernet.

### Real-time řízení v robotice

- potřebujeme neustále měnit pozici motoru (třeba serva) robotické ruky a to s přesností na desítky mikrometru
- docílíme toho tím, že budeme neustále sledovat pozici motoru a příkon, resp. aktuální proud a napětí
- MCU následně vyhodnotí aktuální data s požadovanými a dle toho upraví střídu PWM signálu (motor se otočí o požadovaný úhel)
- toto se celé musí odehrát v řádu mikrosekund

|![Schéma řízení motoru real-time](architektura-softwaru-rizeni-siti-periferii/control_scheme.PNG "Schéma řízení motoru real-time")|
|:--:|
|obr. 2: Schéma řízení motoru real-time|
| zdroj: Texas Instruments: What is “real-time control” and why do you need it? [online]. 2022 [cit. 2023-05-12]. Dostupné z: <https://e2e.ti.com/blogs_/b/analogwire/posts/what-is-real-time-control-and-why-do-you-need-it>|

Níže je znázorněno schéma řízení DC motoru v reálném čase. **Zpětnou vazbu** zde poskytují rezistory, které vrací údaje o těchto dvou veličinách do MCU. MCU následně upraví PWM signál a otočí motorem.

|![Schéma řízení DC motoru real-time a zpětné vazby](architektura-softwaru-rizeni-siti-periferii/motor_scheme.png "Schéma řízení DC motoru real-time a zpětné vazby")|
|:--:|
|obr. 3: Schéma řízení DC motoru real-time a zpětné vazby|
| zdroj: How to achieve efficient, reliable and accurate actuation in real-time motor control systems [online]. 2022 [cit. 2023-05-12]. Dostupné z: <https://e2e.ti.com/blogs_/b/analogwire/posts/how-to-achieve-efficient-reliable-and-accurate-actuation-in-real-time-motor-control-systems>|

### PID regulátor

#### Co to je?

Představte si, že jste regulátor a potřebujete korigovat množství vody ve vaně. která je aktuálně prázdná. Pustíte vodu, naplno. Vana se plní. Kohoutek trochu přivřete, protože se bojíte, že voda přeteče. Nicméně, nedali jste do vody špunt, tedy voda začne klesat. Vy na to zareagujete opětovným otevřením kohoutu (špunt tam dát neumíte, protože jste regulátor :-D). Tak voda začne opět stoupat, jenže když je voda až na okraji vany, tak kohoutek téměř zavřeme, ale voda stále teče.

Odtokovým kanálem voda teče pryč a kohout dodává menší množství vody, hladina klesá. Otočíme kohoutem více a dostáváme se do stavu, kdy objem odtékající vody je roven vodě přitékající, hladina se nemění a špunt nepotřebujeme.

JSME DOBRÝ REGULÁTOR!

**Reálný příklad** PID regulátoru je u mikropáječky a regulace teploty. Regulátor bude měřit teplotu a na základě ní ovlivňovat tok proudu, aby páječku nepřepálil. PID regulátor musí vědět, jaká bude požadovaná teplota, na kterou má páječku roztopit.

### Složky PID regulátoru

1. **Složka P** - proporcionální, jedná se o zesilovač. Regulátor odečte změřenou teplotu od požadované a rozdíl (odchylku) vynásobí konstantou. Výsledkem je výkon, kterým bude páječka topit. Bude-li teplota o mnoho nižší, tak se výkon o mnoho zvýší. Čím více se bude změřená teplota blížit teplotě požadované, tím se výkon bude zvyšovat o menší hodnotu. Nastavíme-li konstantu na hodnotu jedna a bude-li teplota o 10 stupňů nižší, tak se výkon zvýší o 10 procent. Nastavíme-li konstantu na 100 a teplota bude nižší jen o stupeň, tak se výkon o 100 % zvýší. Problémem samotného P regulátoru je, že odchylka může poměrně hodně kmitat (moc výkonu / málo výkonu) a neustálý se na nule (dosažení výkonu dle požadované teploty).

\\[u(t) = r_0e(t)\\]

- \\(u(t)\\) - akční zásah
- \\(r_0\\) - konstanta (činitel zesílení)
- \\(e(t)\\) - regulační odchylka (rozdíl teplot)

Pásmo proporcionality udává, v jakém rozsahu je regulátor schopen regulovat, aniž by dosáhl svého limitu.

\\[pp = \frac {1}{r_0} * 100\\]

|![Regulace P složky](architektura-softwaru-rizeni-siti-periferii/P.png "Regulace P složky")|
|:--:|
|obr. 4: Chování P složky PID regulátoru|
| zdroj: Regulace od Jardy: Vše co potřebujete vědět k zaregulování PID [online]. 2006 [cit. 2023-05-12]. Dostupné z: <https://ucebnaaut.wz.cz/wp-content/uploads/2017/11/Plynul%C3%A1-regulace-PID.pdf>|

2. **Složka I** - Integrační složka, integrační regulátor vezme regulační odchylku, vynásobí ji konstantou a přičte ke své složce. Bude-li změřená teplota vyšší, než požadovaná, bude se integrační složka snižovat, pokud bude teplota nižší než požadovaná, bude se integrační složka zvyšovat. Pokud bychom měli samotný integrační regulátor, bude topit nejdříve málo, výkon se zvýší a po dosažení požadované teploty se výkon bude opět snižovat. Bude-li konstanta příliš velká, zvýší se neoptimálně výkon a teplota příliš překročí teplotu požadovanou. Přibližování ideální teplotě je zde plynulejší a odchylka tolik nekmitá kolem 0 (požadované teploty), jako u P regulátoru.

\\[u(t) = r_i \int{e(\tau)d(\tau)}\\]

- \\(r_i\\) - zesílení integračního regulátoru

**PI regulátor - vzorec**

\\[u(t) = r_0e(t) + r_i\int{e(\tau)d(\tau)}\\]

3. **Složka D** - vezme rychlost změny odchylky a vynásobí ji konstantou. Opět, klesá-li teplota, derivační složka zvyšuje výkon. Čím rychleji klesá teplota, tím větším výkonem se ji regulátor snaží navýšit. Obráceně, pokud teplota klesne, derivační složka okamžitě zareaguje. Tedy začneme-li pájet, tak derivační odchylka okamžitě zvýší výkon páječky. Velikost konstanty se projevuje prudce na výkonu. Velká konstanta způsobí, že se na požadovanou teplotu bude páječka dostávat pomalu, ale bude zato prudce reagovat na změnu. Nízká konstanta naopak působí tak, že regulátor bude na změny reagovat mnohem pomaleji. Je to podobné jako brzdění v autě. Pokud před námi auto brzdí, tak také brzdíme, současně vyhodnocujeme rychlost s jakou se automobily k sobě přibližují (derivace vzdálenosti v čase). Pokud je rychlost velká, tak víte, že dotyčný před vámi musel sešlápnout brzdu pořádně. Tedy uděláte totéž, ačkoli jste ještě dost daleko. Derivační složka tedy v předstihu de facto zesiluje proporcionální složku. Potlačíme kmitání kolem 0.

\\[u(t) = r_d \frac{de(t)}{dt}\\]

**PID regulátor - vzorec**

\\[u(t) = r_0e(t) + r_i\int{e(\tau)d(\tau)} + r_d \frac{de(t)}{dt}\\]

|![Schéma PID regulátoru](architektura-softwaru-rizeni-siti-periferii/scheme_PID.png "Schéma PID regulátoru")|
|:--:|
|obr. 5: Schéma PID regulátoru|
|zdroj: PID Scheme. In: Wikipedie: Otevřená Encyklopedie [online]. 2007 [cit. 2023-05-13]. Dostupné z: <https://commons.wikimedia.org/wiki/File:Pid-feedback-nct-int-correct.png>|

## ZDROJE

- Texas Instruments: What is “real-time control” and why do you need it? [online]. 2022 [cit. 2023-05-12]. Dostupné z: <https://e2e.ti.com/blogs_/b/analogwire/posts/what-is-real-time-control-and-why-do-you-need-it>
- How to achieve efficient, reliable and accurate actuation in real-time motor control systems [online]. 2022 [cit. 2023-05-12]. Dostupné z: <https://e2e.ti.com/blogs_/b/analogwire/posts/how-to-achieve-efficient-reliable-and-accurate-actuation-in-real-time-motor-control-systems>
- Regulace od Jardy: Vše co potřebujete vědět k zaregulování PID [online]. 2006 [cit. 2023-05-12]. Dostupné z: <https://ucebnaaut.wz.cz/wp-content/uploads/2017/11/Plynul%C3%A1-regulace-PID.pdf>
- Bastlírna HWKitchen: Rychlé seznámení s PID regulátorem [online]. 2019 [cit. 2023-05-13]. Dostupné z: https://bastlirna.hwkitchen.cz/rychle-seznameni-s-pid-regulatorem/
