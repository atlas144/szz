# Architektura operačních systémů užívaných ve vestavných systémech

> Linux, Android, iOS, AppleTV, apod. Operační systémy pracujícím v reálném čase (RTOS), ROS (Robotický operační systém).

## Linux

Linux je svobodný (pod licencí *GNU GPLv2*) operační systém původně vytvořený pro architekturu IA-32 (známá též jako i386 (podle prvního procesoru této architektury - **I**ntel 80**386**)). Časem byla přidána podpora dalších architektur (*x86-64* - současné PC; *ARM*, *RISC-V* - **embedded zařízení**; atd.). Návrh systému vychází z UNIXu a dodržuje příslušné standardy POSIX a SUS (Single UNIX Specification). Je naprogramován (myslíme zde jádro) v jazyce *C* a od verze 6.1 i *Rust* (zatím jen pro ovladače).

<div class="note">

### Názvosloví

Označení *Linux* původně znamenalo pouze jádro (kernel), poměrně rychle se však vžilo i pro systém jako celek (včetně aplikací atd.) a dnes se tak používá primárně. Z toho později vznikla [kontroverze *Linux* vs *GNU/Linux*](https://cs.wikipedia.org/wiki/GNU/Linux_kontroverze). Zde uvedený popis se vztahuje primárně k jádru, ale jze jej přeneseně použít pro celý systém.

Kromě označení Linux se často používá i *linuxová distribuce* - to znamená linuxové jádro spolu s aplikacemi, které dohromady tvoří funkční systém, připravený k použití rovnou po instalaci (zde tedy záleží na parametrech konkrétní distribuce, některé je třeba před použitím ještě doupravit, ale základní systém obsahují stejně). 

</div>

### Architektura

Na architekturu vestavěného Linuxu lze hledět v různých úrovních abstrakce. Zde si uvedeme ty nejvyšší z nich. Nutno dodat, že v těchto úrovních se architektura vestavěného Linuxu prakticky neliší od architektury standardního linuxu. Rovněž je třeba zmínit, že zde **myslíme Linux jako celý systém**, nikoliv pouze jádro.

Základní vrstvy architektury jsou tyto:

- zavaděč (není přímo vrstvou, viz [níže](#zavaděč))
- kernel
- knihovny
- služby
- uživatelské aplikace

#### Zavaděč

Zavaděč (anglicky *bootloader*) je program, jehož úkolem je po startu zařízení zavést operační systém. Na osobních počítačích se nejčastěji setkáme se zavaděči *GRUB* (Linux) a *WBM* (Windows). Ty nabízejí celou řadu funkcí, které však v embedded světě nevyužijeme (např. grafické rozhraní, automatický výběr paměti pro zavedení v základním nastavení, atd.), proto se zde využívají zavaděče s minimálními nároky, uzpůsobené pro potřeby vestavěných systémů, jako např. [Das U-Boot](https://u-boot.readthedocs.io/en/latest/). Tyto zavaděče obecně poskytují v základním nastavení pokročilou funkcionalitu, kterou naopak nevyužije běžný uživatel na osobním systému (např. možnost zadávat přímo parametry kernelu). Krom toho mají podporu pro větší množství ve vestavěných systémech běžného HW.

#### Kernel

Kernel, neboli jádro je hlavní část systému. Jeho úkolem je řízení přidělování prostředků (paměťový prostor, procesorový čas) a obsluha I/O zařízení. Linuxový kernel je co se týče návrhu **modulární a monolitický**, to znamená, že umožňuje načítání externích modulů (modulární) a že zde není přístupová ochrana mezi jeho jednotlivými subsystémy, které mohou volat vzájemně své funkce (monolitický). Lze jej dělit na další součásti, viz [obr. 1](#obr1).

|<a name="obr1"></a>![Struktura linuxového kernelu](architektura-operacnich-systemu-uzivanych-ve-vestavnych-systemech/kernel.png "Struktura linuxového kernelu")|
|:--:|
|*obr. 1:* Struktura linuxového kernelu|
| *zdroj:* [ScotXW](https://commons.wikimedia.org/wiki/File:Simplified_Structure_of_the_Linux_Kernel.svg), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via Wikimedia Commons |

##### IRQ

IRQ (Interrupt ReQuest) je systém přerušení, která mohou po jádru požadovat externí zařízení. Každé přerušení má své číslo, podle kterého jádro určuje zdroj přerušení. Při detekci přerušení zavolá jádro obslužnou rutinu (funkci).

##### Dispatcher

Určuje, který proces se má stát aktivním (může vykonávat svou činnost, přistupovat k prostředkům, atd.).

##### I/O subsystem

I/O subsystem obsluhuje vstupně výstupní zařízení. Obsahuje *ovladače*, *implementace protokolů* a další komponenty, které jsou pro tuto obsluhu nutné. Má tři hlavní součásti:

- **terminál** - rozhraní pro použití terminálových emulátorů
- **síť** - ovladače, protokoly, sockety a celkově infrastruktura pro práci se sítí 
- **úložiště** - ovladače, souborové systémy a další prvky pro abstrakci práce s fyzickým úložištěm

Rovněž sem patří ovladače pro všechna periferní zařízení (možná spadají pod kategorii úložiště, jelikož v Linuxu platí že **všechno je soubor**, a tedy přístup k těmto zařízením funguje stejně jako k úložišti)

##### Memory management subsystem

Stará se o přidělování a obecnou správu paměti. Detailně popsáno v kapitolách [2.3.1](http://localhost:3000/ikt/os/jake-jsou-zakladni-funkce-operacniho-systemu.html), [2.3.2](http://localhost:3000/ikt/os/metody-spravy-pameti-pomoci-jedne-souvisle-oblasti-a-pomoci-sekci.html) a [2.3.3](http://localhost:3000/ikt/os/metoda-spravy-pameti-pomoci-strankovani-a-strankovani-na-zadost.html).

##### Process management subsystem

Řídí životní cyklu procesů, jejich komunikaci a řazení pro přístup k prostředkům. Detailně popsáno v kapitolách [2.3.1](http://localhost:3000/ikt/os/jake-jsou-zakladni-funkce-operacniho-systemu.html) a [2.3.4](http://localhost:3000/ikt/os/stavovy-diagram-procesu.html).

##### SCI

SCI (System Call Interface) je API jádra, skrze které mohou programy běžící v *user space* přistupovat k prostředkům jádra (tedy ke *kernel space*), či mu zasílat požadavky. Zajišťuje bezpečnost (aby programy nemohly přistupovat tam, kam nemají = přímo do *kernel space*).

<div class="note">

### Kernel & user space

Jedná se o rozdělení prostoru virtuální paměti na dvě části, za účelem zvýšení bezpečnosti.

- **kernel space** - prostor, kam můžou přistupovat pouze služby jádra (plánovač, správce paměti, atd.)
- **user space** - sem mohou přistupovat uživatelské aplikace a další přídavné moduly, které nejsou přímo součástí jádra (detailně popsáno v sekci [RTOS - mikrokernel](#rtos))

</div>

#### Knihovny

Jedná se o knihovny, které lze využít při tvorbě uživatelských aplikací, pro přístup k prostředkům jádra a komunikaci s ním. Nejdůležitější z nich je standardní knihovna jazyka C (na klasických PC je to obvykle *glib*, na vestavěných např. *uClibc*).

#### Služby

Službami se myslí programy, které běží na pozadí a starají se o důležité části systému, které už však leží v *user space* (nejsou součástí jádra). Nejdůležitější službou je *init system*, což je první proces, který jádro spustí hned po svém zavedení. Jeho úkolem je spouštění dalších procesů. Na běžných PC se téměř výhradně užívá *systemd*, na vestavěných např. *OpenRC*, *sninit*.

#### Uživatelské aplikace

Sem spadá zbytek aplikací z *user space*. Jedná se o aplikace, které uživatel spouští cíleně, aby s nimi pracoval. U vestavěných systémů to obvykle bývá konkrétní řídicí program (nebo soustava spolupracujících programů) pro ovládání systému.

### Obecné rozdíly mezi klasickým a vestavěným Linuxem

Vzhledem k tomu, že vestavěné systémy mají obecně méně zdrojů (RAM, výkon procesoru, atd.), je snahou co nejvíce omezit nároky systému. Toho se dociluje **odstraněním nepotřebných aplikací** (např. grafické prostředí a X (grafický) server, aplikace přímo pro uživatele, atd.), použitím **menší sady základních utilit** (základní utility jsou např. `ls`, `cp`, `rm`, atd.) či **kompaktnější implementace standardní C knihovny** (místo *glibc* např. *uClibc*) nebo úpravou jádra pro **běh v reálném čase (RTOS)**. Vzhledem k tomu, že vestavěné systémy mají obvykle jedno specifické užití a předem známý HW, **není** u nich **nutná variabilita ovladačů**, a proto je možné nepotřebné odebrat.

### Výhody a nevýhody použití vestavěného Linuxu (oproti jiným vestavěným OS)

#### Výhody

- podpora široké škály HW
- takřka neomezená možnost úpravy - přizpůsobení pro konkrétní řešení (v rámci podmínek *GNU GPLv2*)
- nulové licenční poplatky
- podpora není omezena na konkrétního dodavatele (možnost výběru)
- vysoká stabilita

#### Nevýhody

- vyšší náročnost na zdroje oproti klasickým RTOSům a podobným jednoduchým systémům

### Vestavěné distribuce

- [Mobian](https://mobian-project.org/) - pro mobilní telefony
- [Ubuntu Touch](https://ubuntu-touch.io/) - pro mobilní telefony
- [Real-Time Linux](https://www.linuxfoundation.org/blog/blog/intro-to-real-time-linux-for-embedded-developers) - linuxový RTOS
- [OpenWrt](https://openwrt.org/) - pro síťové prvky
- E2 Linux - pro set-top boxy a IPTV přijímače
- [webOS](https://webostv.developer.lge.com/) - pro mobilní zařízení (původně mobilní telefony, dnes hlavně chytré televize LG)
- [Raspberry Pi OS](https://www.raspberrypi.com/software/operating-systems/) - prakticky plnohodnotný OS (s grafickým rozhraním a programy pro běžné uživatele), který běží na jednodeskových počítačích (které lze použít ve vestavěných zařízeních)

#### Platformy

- [OpenEmbedded](https://www.openembedded.org/wiki/Main_Page) - platforma (framework) pro automatické sestavování a překlad distribucí pro vestavěná zařízení
- [Yocto Project](https://www.yoctoproject.org/) - tvorba procesů a nástrojů pro stavbu vestavěných distribucí, nezávislých na konkrétní HW platformě
- [ELKS (Embeddable Linux Kernel Subset)](https://github.com/jbruchon/elks) - zmenšené jádro pro použití ve VS

## Android

Android je mobilní operační systém vyvíjený společností Google (skrze konsorcium Open Handset Alliance). Využívá upravené Linuxové jádro. Je vyvíjen jako *Android Open Source Project (AOSP)*, který si výrobci upravují pro svá zařízení.

Z pohledu vestavěných zařízení jsou primárním cílem mobilní telefony a tablety (což nejsou vestavěná zařízení v pravém slova smyslu), **nejedná se tedy o úplně klasický vestavěný OS**. Nicméně má své místo u vestavěných systémů, které vyžadují grafický výstup, jako jsou např. reklamní kiosky a informační zařízení v automobilech (existuje upravená verze pro televize - *Android TV*). Krom toho existují upravené verze androidu, které na vestavěné systémy cílí přímo a umožňují relativně snadnou úpravu pro požadované řešení, jako např. [emteria.OS](https://emteria.com/emteria-os). Existovala i jedna přímo od Googlu, ta však byla [zrušena](https://android-developers.googleblog.com/2019/02/an-update-on-android-things.html) (jak je u Googlu [dobrým zvykem](https://killedbygoogle.com/)). Tvorba vlastní vestavěné distribuce Androidu (přímo z AOSP) je podle všeho velmi náročná.

### Architektura Androidu obecně

Vzhledem k tomu, že distribucí Androidu přímo určených pro vestavěná zařízení není mnoho a jsou obvykle proprietární, není jejich architektura veřejně dostupná. Dá se však předpokládat, že bude podobná té obecné a tak si zde popíšeme tuto.

Struktura architektury je následující (jdeme od HW směrem k uživatelským aplikacím):

- Linux kernel
- HAL
- Nativní C/C++ knihovny & Android runtime
- Java API framework
- Systémové a uživatelské aplikace


#### Linux kernel

Je detailně popsán v části [Linux](#linux).

#### HAL

HAL (Hardware Abstraction Layer) je zde API, skrze které může *Java API framework* přistupovat k HW prostředkům systému. Nejedená se o HAL v klasickém slova smyslu, protože "leží" nad linuxovým jádrem a tedy provádí abstrakci nad ním, nikoliv přímo nad HW.

#### Nativní C/C++ knihovny & Android runtime

Tyto dvě vrstvy leží na stejné úrovni proto jsou uvedeny dohromady.

##### Nativní C/C++ knihovny

Části systému, jako HAL a ART jsou napsány v C/C++ a tedy využívají nativní knihovny. Některé z jejich funkcionalit lze využít přímo při vývoji vlastních aplikací, skrze Java API framework. Rovněž je zde *Android NDK*, který umožňuje práci s těmito knihovnami přímo v C/C++ kódu.

##### Android runtime

Je to alternativa JRE (Java Runtime Environment), která funguje od Android 5. Každá aplikace na Androidu běží ve vlastním procesu a má vlastní instanci Android runtimu (ART). Aplikace jsou pro běh překládány do bytekódu DEX (ten je optimalizovaný pro běh na zařízeních s malou pamětí), který ART spouští.

*ART umí:*

- *ahead-of-time* (AOT) a *just-in-time* (JIT) kompilaci
- optimalizovaný *garbage collection* (GC)
- překlad DEX kódu do menšího *machine* kódu (od Android 9)
- podporu pro různé debug nástroje
- obsahuje důležité Java knihovny, které obsahují většinu funkcionality Java 8

#### Java API framework

Je hlavní rozhraní, které slouží k vývoji aplikací. Funguje jako kompletní ekosystém. Nabízí např. následující funkcionalitu:

- **systém tzv. "Views"** - tedy komponent, ze kterých lze skládat UI aplikace a které lze upravovat a vytvářet vlastní
- **správce notifikací** - umožňuje zobrazovat notifikace uživateli
- **správce aktivit** - řeší navigaci v aplikaci (přepínání mezi obrazovkami)
- **poskytovatel obsahu** - umožňuje přístup k datům jiných aplikací
- **správce zdrojů (resources)** - umožňuje pracovat se soubory, které nejsou zdrojový kód

#### Systémové a uživatelské aplikace

Všechny aplikace využívají Java API framework. Systémové aplikace jsou ty, které jsou předinstalovány v systému (*Kontakty*, *Kalkulačka*, atd.). Většinu z nich lze nahradit uživatelskými (s pár výjimkami, např. *Nastavení*).

### Výhody a nevýhody použití Androidu (oproti jiným vestavěným OS)

#### Výhody

- známý a poměrně odladěný ekosystém pro vývoj aplikací
- připravené grafické prostředí

#### Nevýhody

- velmi náročná (nebo drahá) úprava pro vlastní řešení
- ekosystém není plně otevřený
- vysoké nároky na prostředky

## iOS

Jedná se o proprietární operační systém od firmy Apple. Z pohledu vestavěných systémů je použitelný pouze jako mobilní OS. Nelze jej totiž použít jinde než na mobilních telefonech a tabletech od firmy Apple (tedy *ARMv7* a *ARMv8*). Je to odlehčená verze desktopového *macOS* (jedná se tedy o OS UNIXového typu).

### Architektura iOS

Architektura systému iOS je vrstvená. Obsahuje čtyři vrstvy, viz obr. 2.

|<a name="obr2"></a>![Architektura iOS](architektura-operacnich-systemu-uzivanych-ve-vestavnych-systemech/ios.png "Architektura iOS")|
|:--:|
|*obr. 2:* Architektura iOS|
| *zdroj:* <https://redfoxsec.com/blog/ios-architecture/> |

#### Core OS

Jedná se o jádro systému. Jeho funkce přibližně odpovídají [jádru Linuxu](#kernel).

#### Core Services

Vrstva abstrakce nad jádrem. Obsahuje služby, které jsou využívány vyššími vrstvami, jako např. správy účtů, volání, identifikátorů pro reklamu, nákupů v AppStore, atd.

#### Media Layer

Slouží k práci s audiem, videem a grafikou, a to jak v rámci systému samotného, tak pro vývojáře k užití při vývoji aplikací.

#### Cocoa Touch

Obsahuje obecné nástroje pro vývoj aplikací (UI prvky, zpracování událostí, notifikace, atd.). Odpovídá vrstvě [Java API framework](#java-api-framework) u Androidu.

### Výhody a nevýhody použití iOS (oproti jiným vestavěným OS)

#### Výhody

- poměrně jednoduchý vývoj, díky vysokoúrovňovým nástrojům (Swift, Xcode)

#### Nevýhody

- prakticky se nejedná o vestavěný systém, nelze ho tedy téměř porovnat
- pouze jedna platforma
- pro vývoj je třeba konkrétní SW (Xcode), který běží jen na OS od firmy Apple, které běží zase jen na extrémně předraženém HW rovněž od firmy Apple
- kompletně uzavřený ekosystém

## AppleTV

V zařízeních Apple TV se užívá operační systém **tvOS**. Ten byl původně založen na *macOS*, ale nyní na iOS, ze kterého přebírá velkou část funkcionality a dalších vlastností (je to uzavřený systém, pro jeden typ zařízení, atd.).

O architektuře *tvOS* se bohužel nikde nic nepíše, ale lze odhadovat, že je podobná té z *iOS*.

## RTOS

Definice z [Wikipedie](https://cs.wikipedia.org/w/index.php?title=Opera%C4%8Dn%C3%AD_syst%C3%A9m_re%C3%A1ln%C3%A9ho_%C4%8Dasu&oldid=21906385):

> Pokud lze dokázat, že realtime systém splní svá ultimáta (deadlines) (a to za použití chování systému v nejhorším možném případě, nikoliv analýzou průměrného chování systému), potom můžeme říci, že chování systému je předvídatelné.

RTOS (Real Time Operating System) se od klasických operačních systémů liší principem činnosti **plánovače** (*scheduleru*). V běžném OS (jako např. *Linux*) je jeho cílem přepínat procesy tak, aby všichni uživatelé systému dostávali podobný čas a aby se jim běh systému zdál plynulý. Ve vestavěných systémech obvykle nemáme více uživatelů (často vůbec nemáme koncept *uživatele*) a mnohem více nás zajímá **předvídatelnost systému**.

Často jsou vyžadovány požadavky reálného času (real time requirements), což znamená, že systém musí vykonat nějakou činnost v definovaném čase (*deadline*). Aby bylo možno tyto požadavky naplnit, systém musí být **deterministický**, tedy musí mít předvídatelné plánování (*scheduling*).

Determinismu při multitaskingu se u RTOSů obvykle dosahuje pomocí priorit, které se přidělují jednotlivým vláknům (nazývaným *task*). Na základě těchto priorit určuje plánovač, které vlákno bude v činnosti jako následující.

Základní požadavky na RTOS jsou:

- **preemptivní plánovač** - právě vykonávanou úlohu může přerušit bez její asistence
- **přesné hodiny reálného času**
- **velký počet priorit, které lze nastavit vláknům**

Základní požadavky na plánovač RTOSu jsou:

- **minimální latence při reakci na událost**
- **minimální latence při přepínání vláken**
- **minimalizace časových okamžiků, kdy je zakázáno přerušení**

RTOSy lze delit na dva druhy:

- **hard RTOS** - vyžaduje striktní determinismus, tedy vykonání požadavku v konkrétním čase je naprosto nutné; používá se v kritických systémech
- **soft RTOS** - povoluje drobné odchylky

Příklady používaných algoritmů pro plánování:

- **Rate monotonic scheduling (RMS)** - priority se přidělují staticky; vyžaduje periodické úlohy
- **Earliest deadline first (EDF)** - zpracování úloh probíhá na základě mezní doby platnosti procesu; nevyžaduje periodické úlohy

### Architektura RTOS

Hlavní částí architektury RTOSu je **kernel**. Ten obsahuje podobné části jako např. linuxový kernel, tedy *správu paměti*, *přidělování procesorového času*, *komunikaci a synchronizaci mezi procesy*, *obsluhu přerušení* a *obsluhu I/O zařízení*. Rozdílem je, že neobsahuje *souborový systém*, *síťové protokoly* a *ovladače zařízení*. Tyto a další komponenty (např. terminál a nástroje pro debug) jsou vytvořeny jako samostatné moduly, které lze přidávat a odebírat podle potřeby konkrétního řešení (často se používá kernel samotný).

RTOSové kernely jsou ve většině případů tvz. **mikrokernely**. To znamená, že v *kernel space* běží jen minimum kódu (komunikace mezi procesy, správa paměti, přidělování prostředků, atd.), a zbylé služby (souborový systém, obsluha sítě, atd.) běží v *user space* (to odpovídá popisu výše). Kromě toho jsou také user space moduly od sebe odděleny a komunikují spolu skrze kernel space část. To zvyšuje bezpečnost a odolnost systému (když padne jeden modul, neshodí celé jádro), ale za cenu snížení výkonu.

Uživatelské aplikace mohou běžet přímo nad kernelem (u jednodušších aplikací je kernel to jediné, co aplikace potřebuje). Pokud aplikace pro svou činnost vyžaduje využití funkcionality některého z dalších modulů, lze jej jednoduše přidat.

### Výhody a nevýhody použití RTOS (oproti jiným vestavěným OS)

Porovnání je opět problematické, jelikož se nejedná o jeden konkrétní OS, ale o typ. K porovnání tedy zvolíme *FreeRTOS*.

#### Výhody

- mnohem menší nároky na prostředky
- vhodný pro kritické aplikace
- otevřený (neplatí pro RTOSy obecně)
- mnoho podporovaných platforem
- nevyžaduje přítomnost MMU (jednotka správy paměti - součást procesorů; řeší např. stránkování)

#### Nevýhody

- obecně se nehodí pro aplikace, které přímo interagují s člověkem (např. informační kiosky)

## ROS

ROS (Robot Operating System) je otevřený (licencovaný pod BSD licencí) soubor frameworků pro vývoj SW k řízení robotických systémů. Nejedná se tedy přímo o OS, ale o nástavbu nad ním. Lze jej použít nad Linuxem, Windows a macOS.

### Architektura ROS

ROS je navržen jako middleware, který propojuje jednotlivé "mini aplikace" (nazývané *procesy*) a umožňuje jim vzájemně komunikovat. Procesy fungují jako uzly grafu, kde hrany jsou reprezentovány tématy (*topic*), skrz které si mohou procesy posílat zprávy, nabízet a konzumovat služby, či přistupovat do společné databáze.

Před zahájením komunikace se musejí procesy zaregistrovat u centrálního procesu, nazývaného **ROS Master**. Ten mezi nimi vytvoří *peer-to-peer* spojení, skrze které následně komunikují (jedná se tedy o distribuovaný systém).

Komunikace skrze témata funguje na principu **publish-subscribe**. Tedy jeden proces provede *subscribe* (odběr) na nějaké téma a čeká na zprávu, kterou na dané téma publikuje (*publish*) jiný proces. Formát takto posílaných zpráv je čistě v režii uživatele.

Výše zmíněné služby fungují jako jednorázové akce, které přímo nabízí proces ostatním (např. získání snímku z kamery).

Společná databáze (*parameter server*) slouží k ukládání málo se měnících dat, která jsou potřebná pro větší množství procesů (např. hmotnost robotu).

Kromě této základní struktury ROS obsahuje velkou sadu nástrojů, které lze využít při tvorbě procesů. Jedná se např. o nástroje na *vizualizaci* (rviz), *vylepšené logování* (rosbag), *build systém* (catkin), *konfigurace a spouštění procesů* (roslaunch). Kromě toho také obsahu správce balíčku, který obsahuje nepřeberné množství knihoven pro řešení problémů nejen z oblastni robotiky.

### Výhody a nevýhody použití ROS (oproti jiným vestavěným OS)

#### Výhody

- mnoho nástrojů
- poměrně snadná tvorba vlastních procesů
- otevřený ekosystém
- odladěný pro oblast robotiky

#### Nevýhody

- běží jako nadstavba standardních OS, a tedy pouze tam, kde běží ony (verze 2 rozšiřuje podporu na běžnější vestavěná zařízení)

## Zdroje

### Linux

- Přispěvatelé Wikipedie, *Linux (jádro)* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 25. 04. 2023, 16:43 UTC, [citováno 1. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Linux_(j%C3%A1dro)&oldid=22737469>
- Přispěvatelé Wikipedie, *Linux* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 9. 04. 2023, 15:34 UTC, [citováno 1. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Linux&oldid=22646583>
- Přispěvatelé Wikipedie, *Vestavěný Linux* [online], Wikipedie: Otevřená encyklopedie, c2020, Datum poslední revize 24. 10. 2020, 13:40 UTC, [citováno 1. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Vestav%C4%9Bn%C3%BD_Linux&oldid=19101906>
- <http://etutorials.org/Linux+systems/embedded+linux+systems/Chapter+2.+Basic+Concepts/2.4+Generic+Architecture+of+an+Embedded+Linux+System/>
- <https://www.eeweb.com/embedded-linux-design-kernel-analysis/>
- <https://thenewstack.io/the-major-components-of-an-embedded-linux-system/>
- Wikipedia contributors, *Bootloader* [online], Wikipedia, The Free Encyclopedia; 2023 Jan 1, 12:02 UTC [cited 2023 May 2]. Available from: <https://en.wikipedia.org/w/index.php?title=Bootloader&oldid=1130875419>.
- Přispěvatelé Wikipedie, *Jádro operačního systému* [online], Wikipedie: Otevřená encyklopedie, c2021, Datum poslední revize 23. 07. 2021, 22:12 UTC, [citováno 2. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=J%C3%A1dro_opera%C4%8Dn%C3%ADho_syst%C3%A9mu&oldid=20236887>
- <https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html>
- <https://tldp.org/LDP/sag/html/init-process.html>

### Android

- <https://emteria.com/blog/embedded-android>
- <https://developer.android.com/guide/platform>
- Přispěvatelé Wikipedie, *Android (operační systém)* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 9. 04. 2023, 16:35 UTC, [citováno 2. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Android_(opera%C4%8Dn%C3%AD_syst%C3%A9m)&oldid=22646852>

### iOS

- Wikipedia contributors, *IOS* [online], Wikipedia, The Free Encyclopedia; 2023 Apr 25, 11:49 UTC [cited 2023 May 2]. Available from: <https://en.wikipedia.org/w/index.php?title=IOS&oldid=1151657740>
- <https://redfoxsec.com/blog/ios-architecture/>
- Přispěvatelé Wikipedie, *IOS* [online], Wikipedie: Otevřená encyklopedie, c2023, Datum poslední revize 9. 04. 2023, 15:37 UTC, [citováno 3. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=IOS&oldid=22646601>

### AppleTV

- Wikipedia contributors, *TvOS* [online], Wikipedia, The Free Encyclopedia; 2023 May 2, 21:15 UTC [cited 2023 May 3]. Available from: <https://en.wikipedia.org/w/index.php?title=TvOS&oldid=1152877912>

### RTOS

- <https://www.freertos.org/about-RTOS.html>
- Přispěvatelé Wikipedie, *Operační systém reálného času* [online], Wikipedie: Otevřená encyklopedie, c2022, Datum poslední revize 18. 11. 2022, 16:57 UTC, [citováno 3. 05. 2023] <https://cs.wikipedia.org/w/index.php?title=Opera%C4%8Dn%C3%AD_syst%C3%A9m_re%C3%A1ln%C3%A9ho_%C4%8Dasu&oldid=21906385>
- <https://robocraze.com/blogs/post/architecture-of-rtos-part-1>
- <https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html>

### ROS

- <https://www.ros.org/blog/why-ros/>
- Wikipedia contributors, *Robot Operating System* [online], Wikipedia, The Free Encyclopedia; 2023 May 2, 10:25 UTC [cited 2023 May 3]. Available from: <https://en.wikipedia.org/w/index.php?title=Robot_Operating_System&oldid=1152793296>